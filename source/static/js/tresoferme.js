var Agricogest = {
  onReady : function () {    

    $('.goBack').on('click', function() {
        window.history.back();
    });

    $('#menuCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');      
    });

    var url = window.location.pathname;
    var currentpath = url.split("tresoferme").pop();
    
    // $("#sidebar ul.components > a menu-link").each(function () {
    $(".menu-link").each(function () {
        menupath = this.href.split("tresoferme").pop();
        menupathok = menupath.split("?")[0];
        urlRegExp = new RegExp(menupathok);
        // menupath = RegExp, if = currentpath => active
        if (urlRegExp.test(currentpath) & (url != '/tresoferme/')) {
            $(this).addClass('active');
            $(this).parent().closest('ul').addClass('show');
            // var classes = $(this).parent().closest('ul').attr('class').split(' ');
            // console.log(classes);
        }
    });

  },
}

$(document).ready(function () {
	Agricogest.onReady();
	
	//ajout
	var sourceSwap = function () {
        var $this = $(this);
        var newSource = $this.data('alt-src');
        $this.data('alt-src', $this.attr('src'));
        $this.attr('src', newSource);
    }
	
    $('img[data-alt-src]').each(function() { 
        new Image().src = $(this).data('alt-src'); 
    }).hover(sourceSwap, sourceSwap); 
	//ajout
});

//ajout for sidebar
! function(t) {
    "use strict";
    t("#sidebarToggle, #sidebarToggleTop").on("click", function(o) {
        t("body").toggleClass("sidebar-toggled"), t(".sidebar").toggleClass("toggled"), t(".sidebar").hasClass("toggled") && t(".sidebar .collapse").collapse("hide")
    }), t(window).resize(function() {
        t(window).width() < 768 && t(".sidebar .collapse").collapse("hide")
    }), t("body.fixed-nav .sidebar").on("mousewheel DOMMouseScroll wheel", function(o) {
        if (768 < t(window).width()) {
            var e = o.originalEvent,
                l = e.wheelDelta || -e.detail;
            this.scrollTop += 30 * (l < 0 ? 1 : -1), o.preventDefault()
        }
    }), t(document).on("scroll", function() {
        100 < t(this).scrollTop() ? t(".scroll-to-top").fadeIn() : t(".scroll-to-top").fadeOut()
    // Code below cause a infinite loop with jQuery error: w.easing[this.easing] is not a function
    // }), t(document).on("click", "a.scroll-to-top", function(o) {
    //     var e = t(this);
    //     t("html, body").stop().animate({
    //         scrollTop: t(e.attr("href")).offset().top
    //     }, 1e3, "easeInOutExpo"), o.preventDefault()
    })
}(jQuery);