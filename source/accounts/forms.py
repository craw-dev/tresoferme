# encoding:utf-8
from __future__ import unicode_literals

import re
from collections import OrderedDict
from django.contrib.auth import get_user_model
from django.forms import (Form, ModelForm, ValidationError,
                          FileField, ModelChoiceField, TextInput, Textarea, CharField, BooleanField,
                          Select, ChoiceField, HiddenInput)
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UsernameField
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, ButtonHolder, Submit, Button, HTML
from crispy_forms.bootstrap import TabHolder, Tab
from .models import SupportStructure, GeoZip


class BaseForm(Form):
    """
    Overwrites BaseForm class which strips leading and trailing spaces
    source: https://chriskief.com/2012/12/21/trim-spaces-in-django-forms/
    """
    # strip leading or trailing whitespace

    def _clean_fields(self):
        for name, field in self.fields.items():
            # value_from_datadict() gets the data from the data dictionaries.
            # Each widget type knows how to retrieve its own data, because some
            # widgets split data over several HTML fields.
            value = field.widget.value_from_datadict(self.data, self.files, self.add_prefix(name))
            try:
                if isinstance(field, FileField):
                    initial = self.initial.get(name, field.initial)
                    value = field.clean(value, initial)
                else:
                    if isinstance(value, str):
                        value = field.clean(value.strip())
                    else:
                        value = field.clean(value)
                self.cleaned_data[name] = value
                if hasattr(self, 'clean_%s' % name):
                    value = getattr(self, 'clean_%s' % name)()
                    self.cleaned_data[name] = value
            except ValidationError as e:
                self.add_error(name, e)


class Form(BaseForm, Form):
    pass


class ModelForm(BaseForm, ModelForm):
    pass


class PhoneCheck(object):

    def _test_phone(self, nr):
        re_pattern = re.compile(r'^(|(?:\+32|0|)[0-9]{8-9})$')
        if re_pattern.match(nr) and len(nr) > 0:
            return True
        else:
            return False

    def set_32(self, nr):
        if nr[0] == '0':
            nr = '+32' + nr[1:]
        return nr


class ProfileUpdateForm(PhoneCheck, ModelForm):
    """
    Base form used for fields that are always required
    Customised form in order to remove privacy, mugshot.
    """
    zipsearch = CharField(label=_("Localité recherche"), max_length=10,
                        required=False,
                        help_text=_("Encodez les 3 premiers chiffres/caractères du code postal ou de la localité"),
                        widget=TextInput(attrs={
                            "class": "form-control",
                            "placeholder": _("Recherche code postal ou localité"),
                            "onkeyup": 'fillin_ddl_zip()'
                            })
                        )    

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("connected_user", None)
        self.clicked = kwargs.pop("clicked_user", None)
        super(ProfileUpdateForm, self).__init__(*args, **kwargs)
        # Set zip qs
        self.fields['zip'].queryset = GeoZip.objects.none()
        if 'zipsearch' in self.data:
            try:
                zipsearch = self.data.get('zipsearch')
                if zipsearch.isnumeric():
                    qs = GeoZip.objects.filter(postal_code__istartswith=zipsearch)
                else:
                    qs = GeoZip.objects.filter(place_name__istartswith=zipsearch)
                self.fields['zip'].queryset = qs
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            if self.instance.zip:
                qs = GeoZip.objects.filter(pk=self.instance.zip.id)
                self.fields['zip'].queryset = qs

        # Set required fields @signup
        self.fields['first_name'] = CharField(required=True, label="Prénom")
        self.fields['last_name'] = CharField(required=True, label="Nom")
        self.fields['is_active'] = BooleanField(
            label="Utilisateur validé?",
            required=False,
            help_text="Cocher/décocher cette case pour activer/désactiver l'accès à TresoFerme pour cet utilisateur!")

        # Remove 'is_active', 'groups' if not member of SupportStructure
        if not self.user.is_manager:
            del self.fields['is_active']
            # del self.fields['groups']
        # remove Admin from list if not member of staff (access to django admin)
        if not self.user.is_staff:
            self.fields['support_structure'] = ModelChoiceField(queryset=SupportStructure.objects.filter(is_active=True))
        # begin cripy-forms elements
        self.helper = FormHelper()
        self.helper.form_id = 'accounts_id'
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.field_class = 'col-md-9'
        self.helper.form_method = 'post'
        # self.helper.form_action = '.' # A ne pas faire sinon ajout / en fin d'url

        tab1 = Tab('Identification', 'username', 'first_name', 'last_name',
                   'email', 'support_structure')
        tab2 = Tab('Contact', 'mobile', 'phone', 'street', 'streetnr', 'zipsearch', 'zip')
        tab3 = Tab('Ferme', 'company', 'company_legalform',
                   'vatnr', 'website', 'invoice_comm')
        tab3user = Tab('Ferme', 'company', 'company_legalform',
                   'company_logo', 'vatnr', 'iban', 'bic', 'website', 'invoice_comm')
        # tab4 = Tab('Gestion', 'is_active')
        data_access_title = HTML("<h5>Accès aux données</h5>")
        data_access_txt = HTML("{% include '_data_access_info.html' %}")
        tab4user = Tab('Gestion des données', 'support_structure_access', 'data_access', data_access_title, data_access_txt)

        if self.user.id == self.clicked.id:
            self.helper.layout = Layout(TabHolder(tab1, tab2, tab3user, tab4user))
        else:
            self.helper.layout = Layout(TabHolder(tab1, tab2, tab3))
        self.helper.layout.append(
            ButtonHolder(
                Submit('submit', 'Enregistrer', css_class='btn btn-danger2'),
                Button('name', 'Retour', css_class='btn btn-main2 goBack')
            )
        )

    class Meta:
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name',
                  'email', 'support_structure',
                  'support_structure_access', 'data_access',
                  'mobile', 'phone', 'street', 'streetnr', 'zip',
                  'company', 'company_legalform', 'company_logo', 'invoice_comm',
                  'vatnr', 'iban', 'bic', 'website')
        widgets = {
            # "invoice_comm": Textarea(attrs={'rows':5, 'cols':70}),
            "vatnr": TextInput(attrs={"placeholder": "ex: BE01234567890"}),
            "iban": TextInput(attrs={"placeholder": "ex: BE00112233445566"}),
            "bic": TextInput(attrs={"placeholder": "ex: NICABEBB"}),
        }

    def clean_company(self):
        if (self.cleaned_data['company'] is None or self.cleaned_data['company'] == ''):
            raise ValidationError(_('Compléter le nom de la ferme svp.'))
        else:
            return(self.cleaned_data['company'])

    def clean_first_name(self):
        if (self.cleaned_data['first_name'] is None or self.cleaned_data['first_name'] == ''):
            raise ValidationError(_('Compléter votre prénom svp.'))
        else:
            return(self.cleaned_data['first_name'])

    def clean_last_name(self):
        if self.cleaned_data['last_name'] is None or self.cleaned_data['last_name'] == '':
            raise ValidationError(_('Compléter votre nom svp.'))
        else:
            return(self.cleaned_data['last_name'].upper())


    def clean_vatnr(self):
        val = self.cleaned_data['vatnr']
        re_pattern = re.compile(r'^BE0[0-9]{9,10}$')
        if val is None:
            return None
        if re_pattern.match(val) and len(val) > 0:
            return val
        else:
            raise ValidationError(_('Respecter le format: \
                                    BE0 suivi de 9 ou 10 chiffres.'))

    def clean_iban(self):
        val = self.cleaned_data['iban']
        re_pattern = re.compile(r'^BE[0-9]{14}$')
        if val is None:
            return None
        if re_pattern.match(val) and len(val) > 0:
            return val
        else:
            raise ValidationError(_('Respecter le format: \
                                    BE suivi de 14 chiffres.'))


class ProfileSignUpForm(PhoneCheck, UserCreationForm):
    """
    based on django UserCreationForm
    """
    zipsearch = CharField(label=_("Localité recherche"), max_length=10,
                        required=True,
                        help_text=_("Encodez les 3 premiers chiffres/caractères du code postal ou de la localité"),
                        widget=TextInput(attrs={
                            "class": "form-control",
                            "placeholder": _("Recherche code postal ou localité"),
                            "onkeyup": 'fillin_ddl_zip()'
                            })
                        )

    class Meta:
        model = get_user_model()
        fields = ("username", "first_name", "last_name", "mobile", "company", "email", "data_access", "cgu", "zip")
        field_classes = {'username': UsernameField}

    def __init__(self, *args, **kw):
        super(ProfileSignUpForm, self).__init__(*args, **kw)
        # Set required fields
        self.fields['first_name'].widget.attrs['required'] = 'required'
        self.fields['last_name'].widget.attrs['required'] = 'required'
        self.fields['mobile'].widget.attrs['required'] = 'required'
        self.fields['zip'].widget.attrs['required'] = 'required'

        # Set zip qs
        self.fields['zip'].queryset = GeoZip.objects.none()
        if 'zipsearch' in self.data:
            try:
                zipsearch = self.data.get('zipsearch')
                if zipsearch.isnumeric():
                    qs = GeoZip.objects.filter(postal_code__istartswith=zipsearch)
                else:
                    qs = GeoZip.objects.filter(place_name__istartswith=zipsearch)
                self.fields['zip'].queryset = qs
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            if self.instance.zip:
                qs = GeoZip.objects.filter(pk=self.instance.zip.id)
                self.fields['zip'].queryset = qs

        # reorder the fields
        first_fields = list(self.fields.items())[:6]
        last_fields = [
            ('zipsearch', self.fields['zipsearch']),
            ('zip', self.fields['zip']),
            ('password1', self.fields['password1']),
            ('password2', self.fields['password2']),
            ('data_access', self.fields['data_access']),
            ('cgu', self.fields['cgu'])            
            ]
        new_order = first_fields + last_fields
        self.fields = OrderedDict(new_order)  

    # def clean_mobile(self):
    #     if self.cleaned_data['mobile'] is None:
    #         return None
    #     elif self._test_phone(self.cleaned_data['mobile']):
    #         return self.set_32(self.cleaned_data['mobile'])
    #     else:
    #         raise ValidationError(_('Respecter le format: (+32 ou 0) \
    #                                 suivi de 8 ou 9 chiffres.'))

    def clean_cgu(self):
        if self.cleaned_data['cgu']:
            return(self.cleaned_data['cgu'])
        else:
            raise ValidationError(_("Vous devez accepter les conditions d'utilisation."))


class ProfileSearchForm(Form):
    ACTIV_LIST = [("NADA", "Compte Activé?"),
                ("YES", "Activé"),
                ("NO", "Désactivé")]
    txtsearch = CharField(max_length=30,
                          required=False,
                          widget=TextInput(attrs={
                              "class": "form-control",
                              "placeholder": _("Rechercher")
                              })
                          )
    is_active = ChoiceField(required=False,
                    choices=ACTIV_LIST,
                    widget=Select(attrs={'class': "form-control"}))
    exportxls = CharField(required=True, widget=HiddenInput)

    def __init__(self, request=None):
        super().__init__()
        if request:
            self.fields["txtsearch"].initial = request.GET.get('txtsearch', None)
            self.fields["is_active"].initial = request.GET.get('is_active', None)


class MyAuthenticationForm(AuthenticationForm):
    """
    Overriding Authentication Form in order to adapt Inactive User message
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    error_messages = {
        'invalid_login': _(
            "Please enter a correct %(username)s and password. Note that both "
            "fields may be case-sensitive."
        ),
        'inactive': _("Compte en attente de validation (ou désactivé) par le responsable de l'application."),
    }
