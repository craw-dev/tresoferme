function fillin_ddl_zip() {
    var input = document.getElementById('id_zipsearch');
    var keyx = input.value.toUpperCase();
    if (keyx.length > 2) {
        var zipsearch = keyx
        // $("#id_zip").show();
        // $("label[for=id_zip]").show();
        // Ajax call
        $.ajax({
            url: '/ajax/ddl_zip/',
            data: {
                'zipsearch': zipsearch
            },
            success: function (data) {
                $("#id_zip").html(data);
                var firstVal = $('#id_zip option:first').val();
                $('#id_zip').val(firstVal);
            }
        });
    }
}
