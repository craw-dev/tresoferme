# users/urls.py
"""
accounts.urls description
"""
from django.urls import path, include
from django.contrib.auth import views as auth_views
from .views import (
    ProfileDetailView, ProfileUpdateView, set_structuser, ProfileList, ProfileActivateView,
    MyLoginView, SignUpView, SignUpDoneView, signup_confirm, SignUpFailedView, ProfileDeleteView)

urlpatterns = [
    path('signup', SignUpView.as_view(), name='signup'),
    path('signup_done', SignUpDoneView.as_view(), name='signup_done'),
    path('signup_confirm/<uidb64>/<token>/', signup_confirm, name='signup_confirm'),
    path('signup_failed', SignUpFailedView.as_view(), name='signup_failed'),

    path('login', MyLoginView.as_view(), name='mylogin'),

    path('profile_list', ProfileList.as_view(), name='profile_list'),
    path('<pk>', ProfileDetailView.as_view(), name='profile_detail'),
    path('<pk>/upd', ProfileUpdateView.as_view(), name='profile_update'),
    path('<pk>/del', ProfileDeleteView.as_view(), name='profile_delete'),
    path('<pk>/setuser', set_structuser, name='set_structuser'),
    path('<pk>/activateuser', ProfileActivateView.as_view(), name='profile_activate'),
    path('', include('django.contrib.auth.urls')),
]
