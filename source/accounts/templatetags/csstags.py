# -*- coding: utf-8 -*-
"""
Template tags to modify classes of a field directly into the templates.

Inspired from http://vanderwijk.info/blog/adding-css-classes-formfields-in-django-templates/

"""
from datetime import date
from urllib.parse import urlencode
from django import template
from django.db.models.query import QuerySet
from django.db.models import Sum, Min
from money.models import Client

register = template.Library()

__author__ = 'hbp'
__email__ = 'p.houben@cra.wallonie.be'
__copyright__ = 'Copyright 2018, Patrick Houben'
__license__ = 'GPLv3'
__date__ = '2018-08-06'
__version__ = '1.0'
__status__ = 'Development'


@register.simple_tag
def get_verbose_field_name(objname, field_name):
    """
    Returns verbose_name for a field.
    """
    # Check wether objname is a QuerySet of directly the object instance!
    # QuerySet option should not be used since we do first() for each call in header
    if isinstance(objname, QuerySet):
        objcopy = objname
        instance = objcopy.first()
    else:
        instance = objname
    fname = instance._meta.get_field(field_name).verbose_name
    return fname


@register.simple_tag
def get_verbose_model_class_name(objname):
    """
    Returns verbose_name for a field.
    """
    # Check wether objname is a QuerySet of directly the object instance!
    if isinstance(objname, QuerySet):
        instance = objname.first()
    else:
        instance = objname
    return instance._meta.verbose_name


@register.simple_tag
def define(val=None):
    return val

# @register.simple_tag
# def ctxidx(val, nbr):
#     ctx = "{}{}".format(val, nbr)
#     return ctx

@register.simple_tag
def frompageline(pagenr, recnr, totrec):
    """From page"""
    if totrec == 0:
        return 0
    if pagenr == 1:
        return 1
    else:
        return ((pagenr-1)*recnr)+1


@register.simple_tag
def topageline(pagenr, recnr, totrec):
    """To page"""
    if pagenr == 1:
        result = recnr
    else:
        result = pagenr * recnr

    if result > totrec:
        return totrec
    else:
        return result


# https://stackoverflow.com/questions/2047622/how-to-paginate-django-with-other-get-variables
@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    query = context['request'].GET.dict()
    query.update(kwargs)
    return urlencode(query)


@register.inclusion_tag('money/_client_supplier_href_inclusion.html', takes_context=True)
def show_client_supplier_href(context, inst):
    # Init vars
    solde = 0
    if inst._meta.object_name == "Client":
        qs = inst.revenue_set.filter(payflag=False, revcat__id='FRM_FAC')
        url_list = "revenue_list"
        display_txt = "Aucune vente"
    if inst._meta.object_name == "Supplier":
        qs = inst.expense_set.filter(payflag=False)
        url_list = "expense_list"
        display_txt = "Aucun achat"

    # Recover ajd
    ajd = context["ajd"]

    if qs:
        amt = qs.filter(payduedate__lt=date.today()).aggregate(amountvat=Sum("amountvat"), payamount=Sum("payamount"))
        amountvat = amt["amountvat"]
        payamount = amt["payamount"]

        # solde
        if amountvat is None:
            amountvat = 0
        if payamount is None:
            payamount = 0
        solde = amountvat - payamount

        # payduedate
        if solde == 0:
            payduedate = qs.filter(payflag=False).aggregate(payduedate=Min("payduedate"))["payduedate"]
            if payduedate is None:
                display_txt = "Payé"
            else:
                display_txt = "pour le {0}".format(payduedate.strftime('%d/%m/%Y'))
        else:
            display_txt = round(solde, 0)

    return {'solde': solde, 'display_txt': display_txt, 'ajd': ajd, 'url_list': url_list, 'pk': int(inst.id)}


@register.filter(name='lookup')
def lookup(d, key):
    return d[key]


@register.simple_tag
def concat(ctx, idx):
    return ctx+idx


@register.filter(name='lookup_element')
def lookup_element(d, args):
    key, element = args.split(',')
    element = int(element)
    return d[key][element]


@register.simple_tag(name='substract')
def substract(value, arg):
    res = value - arg
    res1 = round(res, 2)
    return res1