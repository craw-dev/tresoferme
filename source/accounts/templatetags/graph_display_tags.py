from django import template

register = template.Library()

__author__ = 'hbp'
__email__ = 'p.houben@cra.wallonie.be'
__copyright__ = 'Copyright 2018, Patrick Houben'
__license__ = 'GPLv3'
__date__ = '2018-08-06'
__version__ = '1.0'
__status__ = 'Development'

@register.inclusion_tag('treso/_treso_inclusion.html')
def display_treso_graph(graph_id, class_bg, graph):
    """
    used in treso/treso_graph.html
    graph_id: id of the graphic
    class_bg: class style
    graph: plotly object html div
    """
    my_dict = {'rev': 'Entrée', 'exp': 'Sortie', 'solde': 'Solde'}
    my_dict_class = {'rev': 'graph-form-R', 'exp': 'graph-form-D', 'solde': 'graph-form-RD'}
    # my_dict.pop(graph_id, None)
    # my_dict_class.pop(graph_id, None)
    ctx = {
        'graph_id': graph_id,
        'class_bg': class_bg,
        'graph': graph,
        'my_dict': my_dict,
        'my_dict_class': my_dict_class
    }
    return ctx


@register.inclusion_tag('renta/_renta_group_inclusion.html')
def display_2graph(graph_id, graph1, graph2):
    return {'graph_id': graph_id, 'graph1': graph1, 'graph2': graph2}


@register.inclusion_tag('_month_table.html', takes_context=True)
def display_table_global(context, nbr, what):
    ctx = {}
    ctx['MONTH'] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
    if 'EMP'+nbr in context:
        ctx['EMP'] = context['EMP' + nbr]
    if 'REVEMP'+nbr in context:
        ctx['REVEMP'] = context['REVEMP' + nbr]
    if 'REVCRE'+nbr in context:
        ctx['REVCRE'] = context['REVCRE' + nbr]        
    if 'INV'+nbr in context:
        ctx['INV'] = context['INV' + nbr]
    if 'REV'+nbr in context:
        ctx['REV'] = context['REV' + nbr]
    if 'EXP'+nbr in context:
        ctx['EXP'] = context['EXP' + nbr]

    if what == "RENTA":
        ctx['EMP_title'] = "Intérêt"
        ctx['INV_title'] = "Amort."
        ctx['REV_title'] = "Produit"
        ctx['EXP_title'] = "Charge"

    if what == "TRESO":
        ctx['REVEMP_title'] = "Entrée Emprunt"
        ctx['REVCRE_title'] = "Entrée Crédit"
        ctx['EMP_title'] = "Annuité"
        ctx['INV_title'] = "Invest."
        ctx['REV_title'] = "Entrée"
        ctx['EXP_title'] = "Sortie"
    return ctx
