# -*- coding: utf-8 -*-
"""
source.cms_pages.templatetags.settings_value description
"""
from django import template
from config import settings

__author__ = 'jph'
__email__ = 'j.huart@cra.wallonie.be'
__copyright__ = 'Copyright 2018, Jean Pierre Huart'
__license__ = 'GPLv3'
__date__ = '2018-05-18'
__version__ = '1.0'
__status__ = 'Development'

register = template.Library()

# settings value
@register.simple_tag
def settings_value(name):
    allowed_keys = ('APP_VERSION',
                    'LANGUAGES',
                    'TIME_ZONE',
                    'LOCAL_TIMEZONE',
                    'TERMS_OF_SERVICE',
                    )
    if name in allowed_keys:
        return getattr(settings, name, "")
    
    return ''
