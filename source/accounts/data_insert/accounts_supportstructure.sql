--
-- Data for Name: accounts_supportstructure; Type: TABLE DATA; Schema:  Owner: tresoadmin_demo
--

INSERT INTO accounts_supportstructure (id, name, is_active, website) VALUES (0, 'Admin', 0, 'NoAccess');
INSERT INTO accounts_supportstructure (id, name, is_active, website) VALUES (5, 'GASAP', 1, 'https://gasap.be/');
INSERT INTO accounts_supportstructure (id, name, is_active, website) VALUES (2, 'Diversiferm', 0, 'http://diversiferm.be');
INSERT INTO accounts_supportstructure (id, name, is_active, website) VALUES (3, 'Agricall-Finagri', 0, 'http://www.agricall.be/finagri');
INSERT INTO accounts_supportstructure (id, name, is_active, website) VALUES (1, 'CRA-W', 0, 'http://www.cra.wallonie.be/fr');
INSERT INTO accounts_supportstructure (id, name, is_active, website) VALUES (6, 'Demo', 1, 'https://demo.tresoferme.be');


--
-- Name: accounts_supportstructure_id_seq; Type: SEQUENCE SET; Schema:  Owner: tresoadmin_demo
--

-- SELECT pg_catalog.setval('accounts_supportstructure_id_seq', 6, true);

