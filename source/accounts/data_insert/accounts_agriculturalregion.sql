--
-- Data for Name: accounts_agriculturalregion; Type: TABLE DATA; Schema:  Owner: tresoadmin_demo
--

INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (15, '14', 'Haute Ardenne');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (1, '1', 'Dunes');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (2, '1', 'Polders');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (3, '2', 'Sablonneuse');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (4, '3', 'Campine');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (5, '4', 'Sablo-limoneuse');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (6, '5', 'Limoneuse');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (7, '6', 'Campine hennuyère');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (8, '7', 'Condroz');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (9, '8', 'Herbagere');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (10, '9', 'Fagne');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (11, '10', 'Famenne');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (12, '11', 'Ardenne');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (13, '12', 'Jurassique');
INSERT INTO accounts_agriculturalregion (id, code, name) VALUES (14, '13', 'Haute Ardenne');


--
-- Name: accounts_agriculturalregion_id_seq; Type: SEQUENCE SET; Schema:  Owner: tresoadmin_demo
--

-- SELECT pg_catalog.setval('accounts_agriculturalregion_id_seq', 16, true);



