--
-- Data for Name: accounts_companylegalform; Type: TABLE DATA; Schema:  Owner: tresoadmin_demo
--

INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (1, '006', 'S.C.I.S.', 'Société coopérative à responsabilité illimitée et solidaire');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (2, '007', 'S.C.I.S.R.', 'Société coopérative à responsabilité illimitée et solidaire de participation');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (3, '8', 'S.C.R.L.', 'Société coopérative à responsabilité limitée');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (4, '9', 'S.C.R.L.P.', 'Société coopérative à responsabilité limitée de participation');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (5, '16', 'S.C.', 'Société coopérative');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (6, '11', 'S.N.C.', 'Société en nom collectif');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (7, '12', 'S. COM.', 'Société en commandite simple');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (8, '13', 'S.C.A.', 'Société en commandite par actions');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (9, '14', 'S.A.', 'Société anonyme');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (10, '15', 'S.P.R.L.', 'Société privée à responsabilité limitée');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (11, '25', 'S. AGR.', 'Société agricole');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (12, '30', 'Etablissement stable', 'Société étrangère ayant un établissement stable en Belgique');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (13, '230', 'Bien immobilier', 'Société étrangère ayant un bien immobilier en Belgique');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (14, '235', 'Responsable T.V.A.', 'Société étrangère ayant un représentant responsable pour la T.V.A. en Belgique');
INSERT INTO accounts_companylegalform (id, code, accronym, name) VALUES (15, '0', 'Non connus', ' ');


--
-- Name: accounts_companylegalform_id_seq; Type: SEQUENCE SET; Schema:  Owner: tresoadmin_demo
--

-- SELECT pg_catalog.setval('accounts_companylegalform_id_seq', 15, true);


--
-- PostgreSQL database dump complete
--

