--
-- Data for Name: accounts_supportstructureaccess; Type: TABLE DATA; Schema:  Owner: tresoadmin_demo
--

INSERT INTO accounts_supportstructureaccess (id, name) VALUES ('RO', 'Lecture uniquement');
INSERT INTO accounts_supportstructureaccess (id, name) VALUES ('FULL', 'Accès global');
INSERT INTO accounts_supportstructureaccess (id, name) VALUES ('NADA', 'Aucun accès');


--
-- PostgreSQL database dump complete
--

