# -*- coding: utf-8 -*-
"""
accounts.views
"""
import logging
from datetime import date

# External source
from braces.views import LoginRequiredMixin, UserPassesTestMixin, AnonymousRequiredMixin
# Divers
from django.views.generic import DetailView, UpdateView, CreateView, TemplateView, ListView, DeleteView
from django.urls import reverse_lazy
from django.shortcuts import redirect, render, resolve_url
from django.template.loader import render_to_string
from django.db.models import Q, F
from django.db.models.functions import TruncDate, Coalesce

from django.http import HttpResponse
# Contrib
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView
from django.contrib.auth import get_user_model
# Utils
from django.utils import timezone
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.translation import ugettext as _
# Accounts App
from config.settings import PASSWORD_RESET_TIMEOUT_DAYS, LOGIN_REDIRECT_URL, DATABASES
from .tokens import account_activation_token
from .forms import ProfileUpdateForm, ProfileSignUpForm, ProfileSearchForm, MyAuthenticationForm

from money.utils_tools import SaveAsXLS, set_prevlast_year, ExcelResponse

LOG = logging.getLogger(__name__)

class MessageMixin():
    """
    Message management
    !!!url MUST be defined with add, upd and del keywords!!!
    """
    @property
    def build_message(self):
        action = ""
        gender = ""
        if "pk" in self.kwargs:
            self.pk = self.kwargs.get("pk")
        else:
            self.pk = ""
        #         self.display_name = self.pk
        """Build message"""
        if "upd" in self.request.path:
            action = "modifié"
        if "add" in self.request.path:
            action = "ajouté"
        if "del" in self.request.path:
            action = "supprimé"

        if self.pk == "":
            who = self.model._meta.verbose_name
        else:
            inst = self.model.objects.get(pk=self.pk)
            who = inst.get_title()
        msg = "{0} {1}{2} !".format(who, action, gender)

        return msg

    def form_valid(self, form):
        """display message when form is valid"""
        messages.info(self.request, self.build_message)
        return super(MessageMixin, self).form_valid(form)

    def delete(self, request, *args, **kwargs):
        """MessageMixin hooks to form_valid which is not present on DeleteView
        => Use of delete method to display success msg"""
        messages.info(self.request, self.build_message)
        return super(MessageMixin, self).delete(request, *args, **kwargs)


class SignUpView(AnonymousRequiredMixin, CreateView):
    """
    SignUpView with email activation
    https://simpleisbetterthancomplex.com/tutorial/2017/02/18/how-to-create-user-sign-up-view.html#sign-up-with-confirmation-mail
    """
    template_name = 'signup_form.html'
    form_class = ProfileSignUpForm
    success_url = 'signup_done'
#     extra_context={'PASSWORD_RESET_TIMEOUT_DAYS': PASSWORD_RESET_TIMEOUT_DAYS}

    def form_valid(self, form):
        new_user = form.save()
        new_user.is_active = False
        new_user.save()

        current_site = get_current_site(self.request)
        email_subject = _("Inscription sur le site {0}".format(current_site.name))

        email_body = render_to_string('signup_email.html', {
            'user': new_user,
            'domain': current_site.domain,
            'site_name': current_site.name,
            'uid': urlsafe_base64_encode(force_bytes(new_user.pk)),
            'token': account_activation_token.make_token(new_user),
            'protocol': 'https' if self.request.is_secure() else 'http',
        })
        new_user.email_user(email_subject, email_body)

        return redirect(self.success_url)


class SignUpDoneView(AnonymousRequiredMixin, TemplateView):
    template_name = 'signup_done.html'
    title = _("Merci pour votre inscription.")
    extra_context = {'PASSWORD_RESET_TIMEOUT_DAYS': PASSWORD_RESET_TIMEOUT_DAYS,
                     'title': title}


def signup_confirm(request, uidb64, token):
    usl_email = ""
    usermodel = get_user_model()
    try:
        # logger.debug('uidb64:{0}'.format(uidb64))
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = usermodel.objects.get(pk=uid)
        # except Exception as e:
        # logger.debug('userid:{0}, error:{1}'.format(uid, e))
    except (TypeError, ValueError, OverflowError, usermodel.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = False
        user.email_confirmed = True
        user.save()
        struct = user.support_structure

        user_struct_list = usermodel.objects.filter(support_structure=struct, is_manager=True)
        if not user_struct_list:
            err = _("Pas de gestionnaire défini {0}").format(struct)
            return render(request, 'signstruct_activate_fail.html', {'error': err})

        try:
            # Envoi d'un mail à chaque gestionnaire de structure d'encadrement
            for usl in user_struct_list:
                # Mail must be send to support_struct to advice user has clicked on activation link
                usl_email = usl.email
                current_site = get_current_site(request)
                email_subject = _("{0}: demande validation inscription {1}".format(current_site.name.upper(), user.username))
                email_body = render_to_string('signstruct_activate_email.html', {
                    'user': user,
                    'userid': user.id,
                    'domain': current_site.domain,
                    'site_name': current_site.name,
                    'protocol': 'https' if request.is_secure() else 'http',
                })
                usl.email_user(email_subject, email_body)
        except Exception as err:
            LOG.error(err, extra={'user': user})
            return render(request, 'signstruct_activate_fail.html', {'error': err})

        return render(request, 'signstruct_activate_done.html', {'struct': struct, 'user_struct_email': usl_email})
    else:
        return redirect('signup_failed')


class SignUpFailedView(TemplateView):
    template_name = 'signup_failed.html'
    title = _("Votre inscription a échoué.")
    extra_context = {'PASSWORD_RESET_TIMEOUT_DAYS': PASSWORD_RESET_TIMEOUT_DAYS,
                     'title': title}


class MyLoginRequiredMixin(LoginRequiredMixin):
    """
    set myuser as instance! & check accounts fields have been filled-in
    """
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission(request)

        if "myuserpk" in request.session:
            usermodel = get_user_model()
            userpk = request.session["myuserpk"]
            self.myuser = usermodel.objects.get(id=userpk)
        else:
            self.myuser = request.user
            request.session["myuserpk"] = request.user.id

        if "year1" not in request.session:
            set_prevlast_year(request, self.myuser)
        return super(MyLoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class ProfileList(MyLoginRequiredMixin, ListView):
    model = get_user_model()
    title = "Mes utilisateurs"
    template_name = "accounts/profile_list.html"
    form = ProfileSearchForm(None)
    modeltxt = "User"
    jours = ""
    # paginate_by = 100

    def get(self, request, *args, **kwargs):
        # https://stackoverflow.com/questions/18930234/django-modifying-the-request-object
        # self.param = request.GET.copy()
        self.param = request.GET
        xlsparam = self.param.get("exportxls")
        if xlsparam == 'XLSGO':
            xls = SaveAsXLS(self.get_queryset())
            msg_back = xls.get_model_and_columns()
            if msg_back != "OK":
                return render(request, "500.html", {"error_msg": msg_back})
            else:
                workbook = xls.create_workbook()
                sheet_added, msg_back = xls.add_sheet(self.modeltxt)
                if sheet_added:
                    filename = "{}.xls".format(self.modeltxt)
                    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                    response['Content-Disposition'] = 'attachment; filename={0}'.format(filename)
                    workbook.save(response)
                    return response
                else:
                    return render(request, "500.html", {"error_msg": msg_back})
        return super().get(request, *args, **kwargs)

    def build_where(self, param):
        where = Q(pk__gt=0)
        if param.get("txtsearch"):
            search_list = param.get("txtsearch", "").split()
            for search_item in search_list:
                where &= (
                    Q(username__icontains=search_item)
                    |Q(company__icontains=search_item)
                    | Q(email__icontains=search_item)
                    | Q(first_name__icontains=search_item)
                    | Q(last_name__icontains=search_item)
                    | Q(zip__place_name__icontains=search_item)
                    | Q(zip__postal_code__icontains=search_item)
                )
        if param.get("is_active"):
            flag = param.get("is_active", "NADA")
            if flag != "NADA":
                if flag == "YES":
                    where &= Q(is_active=True)
                if flag == "NO":
                    where &= Q(is_active=False)

        return where

    def get_queryset(self):
        self.form = ProfileSearchForm(self.request)
        struct_id = self.request.user.support_structure.id
        qs = self.model.objects.filter(support_structure__exact=struct_id)
        qs = qs.exclude(id=self.request.user.id)
        qsf = qs.filter(self.build_where(param=self.request.GET))
        if 'postgresql' in DATABASES['default']['ENGINE']:
            qsf = qsf.annotate(lastlog=TruncDate(Coalesce('last_login', timezone.now())))
            qsf = qsf.annotate(jlastlog=date.today() - F('lastlog'))
        else:
            qsf = qsf.annotate(jlastlog=F('last_login'))
        return qsf

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'sqlite3' in DATABASES['default']['ENGINE']:
            context['jours'] = ''
        else:
            context['jours'] = 'jours'
        return context


class ProfileDetailView(MyLoginRequiredMixin, DetailView):
    model = get_user_model()
    template_name = "accounts/profile_detail.html"
    manager_mobile = []
    manager_phone = []
    context = {}

    def get(self, request, *args, **kwargs):
        usermodel = get_user_model()
        user = self.get_object()
        # get suport struct of the user
        struct = user.support_structure
        user_struct_list = usermodel.objects.filter(support_structure=struct, is_manager=True)
        self.manager_mobile = []
        self.manager_phone = []
        for usl in user_struct_list:
            if usl.mobile is not None:
                self.manager_mobile.append(usl.mobile)
            if usl.phone is not None:
                self.manager_phone.append(usl.phone)

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["manager_mobile"] = ", ".join([str(a) for a in self.manager_mobile])
        context["manager_phone"] = ", ".join([str(a) for a in self.manager_phone])

        context["is_manager"] = self.request.user.is_manager
        context["upd_allowed"] = False
        user = self.get_object()
        if self.request.user.is_manager and user.support_structure_access.id == "FULL":
            context["upd_allowed"] = True
        if self.request.user == user:
            context["upd_allowed"] = True
        return context


class ProfileUpdateView(MyLoginRequiredMixin, MessageMixin, UpdateView):
    model = get_user_model()
    template_name = "accounts/profile_form.html"
    form_class = ProfileUpdateForm

    def get_form_kwargs(self):
        """used to pass user in form and display tab4 accordingly"""
        kwargs = super(ProfileUpdateView, self).get_form_kwargs()
        kwargs["connected_user"] = self.request.user
        kwargs["clicked_user"] = self.get_object()
        return kwargs


class ProfileDeleteView(MyLoginRequiredMixin, MessageMixin, DeleteView):
    """User Delete"""
    model = get_user_model()
    # obligatoire sinon django pointe sur modelname_confirm_delete.html
    template_name = "accounts/profile_detail.html"
    success_url = reverse_lazy("profile_list")
    confirm_message = _("Supprimer cet utilisateur?")


@login_required
def set_structuser(request, *agrs, **kwargs):
    """
    Function triggered from profile_list.html which affect session myuserpk
    This is the same as accounts.set_myuserpk but must be duplicated to avoid
    django.core.exceptions.ImproperlyConfigured: AUTH_USER_MODEL refers to model 'accounts.User' that has not been installed
    """
    pk = kwargs["pk"]
    request.session["myuserpk"] = pk
    usermodel = get_user_model()
    try:
        user = usermodel.objects.get(pk=pk)
    except usermodel.DoesNotExist:
        user = request.user
        msg = "User.DoesNotExist for pk={}".format(pk)
        return render(request, "500.html", {"error_msg": msg})
    return redirect('money_index')


class MyLoginView(LoginView):
    form_class = MyAuthenticationForm
    """
    Overwrite django.LoginView in order to redirect to admin page when staff member
    """
    def get_success_url(self):
        url = self.get_redirect_url()
        redirect_to = LOGIN_REDIRECT_URL
        if self.request.user.is_staff:
            redirect_to = 'admin:index'
        if self.request.user.username == "Mary":
            redirect_to = 'profile_list'
        return url or resolve_url(redirect_to)


class ProfileActivateView(MyLoginRequiredMixin, UpdateView):
    model = get_user_model()
    fields = ["is_active"]
    template_name = "accounts/profile_activate.html"
    success_url = "profile_list"
    action = _("Activer")

    def get(self, request, *args, **kwargs):
        super().get(request, *args, **kwargs)
        if self.object.is_active:
            self.action = _("Désactiver")
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.is_active = not form.instance.is_active
        # Check if is_active has changed and send email to user
        # if 'is_active' in form.changed_data:
        user_active = form.instance
        if user_active.is_active:
            try:
                current_site = get_current_site(self.request)
                email_subject = _("{0}: inscription validée".format(current_site.name.upper()))
                email_body = render_to_string('signstruct_inform_email.html', {
                    'user_active': user_active,
                    'domain': current_site.domain,
                    'site_name': current_site.name,
                    'protocol': 'https' if self.request.is_secure() else 'http',
                })
                user_active.email_user(email_subject, email_body)
                messages.success(self.request, _("Utilisateur activé. Email envoyé à l'utilisateur concerné."))
            except Exception as xcpt:
                LOG.error(xcpt, extra={'user': self.request.user})
        else:
            messages.success(self.request, _("Utilisateur désactivé."))

        return super().form_valid(form)


    def get_success_url(self):
        return resolve_url('profile_list')
