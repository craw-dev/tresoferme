# encoding:utf-8
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from django.utils.translation import ugettext as _
from django.contrib.auth import get_user_model

from .models import SupportStructure, CompanyLegalForm, SupportStructureAccess, GeoZip

# change title in admin
USERMODEL = get_user_model()
admin.site.site_header = _('TresoFerme: Administration')
admin.site.site_title = _('TresoFerme: Administration')


class ProfileAdmin(UserAdmin):
    list_display = ('username', 'full_name_display', 'company',
                    'support_structure', 'email', 'last_login')
    ordering = ('last_login',)
    search_fields = ('first_name', 'last_name', 'email', 'company')
    list_filter = ('support_structure', 'is_manager')
    fieldsets = [
        (_('Utilisateur'), {'fields': ['username', 'email', 'first_name',
                                       'last_name', 'password']}),
        (_('Accès'), {'fields': ['email_confirmed', 'is_active',
                                'support_structure', 'support_structure_access',
                                'is_manager',
                                'is_staff', 'groups']}),
        (_('Date'), {'fields': ['date_joined', 'last_login']}),
        (_('Contact'), {'fields': ['mobile', 'phone', 'street', 'zip']}),
        (_('Entreprise'), {'fields': ['company', 'company_legalform',
                                      'company_logo', 'vatnr',
                                      'iban', 'bic', 'website']})
    ]

    def full_name_display(self, profile):
        return '{0} {1}'.format(profile.first_name, profile.last_name)
    full_name_display.short_description = _('Nom')


class GeoZipAdmin(admin.ModelAdmin):
    list_display = ('country_code', 'postal_code', 'place_name', 'admin_name2', 'admin_name3')
    # filter_horizontal = ('culture',)
    list_filter = ('admin_name2', 'admin_name3')
    search_fields = ('postal_code', 'place_name', 'admin_name2',)

# admin.site.unregister(User)
# admin.site.unregister(Group)
admin.site.register(USERMODEL, ProfileAdmin)
admin.site.register(SupportStructureAccess)
admin.site.register(SupportStructure)
admin.site.register(CompanyLegalForm)
admin.site.register(GeoZip, GeoZipAdmin)
