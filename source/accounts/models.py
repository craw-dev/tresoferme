# -*- coding: utf-8 -*-
"""
accounts.models description
"""
import logging
from pgcrypto import fields

# from sendgrid import SendGridAPIClient
# from sendgrid.helpers.mail import Mail
from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import AbstractUser, Group, Permission
from django.urls import reverse_lazy
from django.utils.translation import ugettext as _
from django.utils import timezone
from django.contrib.auth.signals import user_logged_in
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.auth.base_user import AbstractBaseUser
# For ExportXLS
from django.db.models.constants import LOOKUP_SEP
from django.core.exceptions import FieldDoesNotExist
from django.utils.encoding import force_text

__author__ = 'hbp'
__email__ = 'p.houben@cra.wallonie.be'
__copyright__ = 'Copyright 2018 Patrick Houben'
__license__ = 'GPLv3'
__date__ = '2018-07-22'
__version__ = '1.0'
__status__ = 'Development'

LOG = logging.getLogger(__name__)


class AgriculturalRegion(models.Model):
    """
    Belgian farming area
    """
    code = models.CharField(max_length=4)
    name = models.CharField(_('Région agricole'), max_length=100)

    class Meta:
        verbose_name = _("Région agricole")
        ordering = ['name']

    def __str__(self):
        return self.name


class GeoZip(models.Model):
    """
    Source: GeoNames.org
    Zip from https://download.geonames.org/export/zip/
    To be deleted
    """
    country_code = models.CharField(_('Pays'), max_length=2)
    postal_code = models.CharField(_('Code postal'), max_length=20)
    place_name = models.CharField(_('Localité'), max_length=180)
    admin_name1 = models.CharField(_('Etat'), max_length=100, blank=True, null=True)
    admin_code1 = models.CharField(_('Code Etat'), max_length=20, blank=True, null=True)
    admin_name2 = models.CharField(_('Province/Département'), max_length=100, blank=True, null=True)
    admin_code2 = models.CharField(_('Code Province/Département'), max_length=20, blank=True, null=True)
    admin_name3 = models.CharField(_('Commune'), max_length=100, blank=True, null=True)
    admin_code3 = models.CharField(_('Code commune'), max_length=20, blank=True, null=True)
    latitude = models.DecimalField(_('Latitude wgs84'), max_digits=9, decimal_places=6)
    longitude = models.DecimalField(_('Longitude wgs84'), max_digits=9, decimal_places=6)
    accuracy = models.PositiveSmallIntegerField(_('Précision'), blank=True, null=True)
    agricultural_region = models.ForeignKey(AgriculturalRegion,
                                            on_delete=models.CASCADE,
                                            null=True, blank=True)

    class Meta:
        verbose_name = _("Localité")
        ordering = ['country_code', 'postal_code', 'place_name']
        unique_together = ('country_code', 'postal_code', 'place_name')
        indexes = [
            models.Index(fields=['country_code'], name='country_code_idx'),
            models.Index(fields=['postal_code'], name='postal_code_idx'),
        ]

    def get_zip_display(self):
        return '{0} {1}'.format(self.postal_code, self.place_name)

    def __str__(self):
        return '{0} {1} {2}'.format(self.country_code, self.postal_code, self.place_name)


class SupportStructure(models.Model):
    """
    Support Stucture
    """
    name = models.CharField(max_length=100, verbose_name=_("Nom"))
    is_active = models.BooleanField(default=False, verbose_name=_("Actif"))
    website = models.URLField(_('Site web'))

    class Meta:
        verbose_name = _("Structure d'encadrement")
        ordering = ["name"]

    def __str__(self):
        return self.name


class SupportStructureAccess(models.Model):
    """
    Define access rights per user for support structure
    """
    id = models.CharField(max_length=4, primary_key=True, verbose_name=_("Code"))
    name = models.CharField(max_length=50, verbose_name=_("Description"))

    class Meta:
        verbose_name = _("Types accès structure encadrement")

    def __str__(self):
        return self.name


class CompanyLegalForm(models.Model):
    """
    Statut juridique
    https://www.inasti.be/fr/liste-des-formes-juridiques
    """
    code = models.CharField(_('Code'), max_length=3)
    accronym = models.CharField(_('Accronyme'), max_length=20)
    name = models.CharField(_('Nom'), max_length=255)

    class Meta:
        verbose_name = _("Statut juridique")
        ordering = ['code']

    def __str__(self):
        return '{0} - {1}'.format(self.accronym, self.name)


class ContactGeneric(models.Model):
    """
    Abstract class for contact info
    """
    mobile = models.CharField(_('Gsm'), max_length=15, blank=True, null=True)
    phone = models.CharField(_('Téléphone'), max_length=15, blank=True, null=True)
    company = models.CharField(_('Ferme'), max_length=100, null=True)
    street = models.CharField(_('Rue'), max_length=255, blank=True, null=True, help_text=_('Nom de rue.'))
    streetnr = models.CharField(_('Nr de rue'), max_length=20,
                                blank=True, null=True,
                                help_text=_('Numéro de rue.'))
    mailboxnr = models.CharField(_('Nr de boîte'), max_length=20,
                                 blank=True, null=True,
                                 help_text=_('Numéro de boite, si nécessaire.'))
    zip = models.ForeignKey(
        GeoZip, on_delete=models.CASCADE, blank=True, null=True,
        verbose_name=_('Localité'))
    
    def get_phone_display(self):
        if self.phone is None:
            tel = ""
        else:
            tel = "Tel: {}".format(self.phone)
        if self.mobile is None:
            mobile = ""
        else:
            mobile = "Gsm: {}".format(self.mobile)

        if tel != "" and mobile != "":
            phone = tel + ' | ' + mobile
        else:
            phone = tel + mobile
        return phone

    def get_street_display(self):
        adr = ""
        if self.street is not None:
            if self.streetnr is not None and self.streetnr != "":
                adr = "{0}, {1}".format(self.street, self.streetnr)
            else:
                adr = self.street
        return adr

    class Meta:
        abstract = True


class ContactGeneric2(models.Model):
    """
    Abstract class for contact info
    """
    mobile = fields.CharPGPPublicKeyField(_('Gsm'), max_length=15, blank=True, null=True)
    phone = fields.CharPGPPublicKeyField(_('Téléphone'), max_length=15, blank=True, null=True)
    company = fields.CharPGPPublicKeyField(_('Ferme'), max_length=100, null=True)
    street = fields.CharPGPPublicKeyField(_('Rue'), max_length=255, blank=True, null=True, help_text=_('Nom de rue.'))
    streetnr = models.CharField(_('Nr de rue'), max_length=20,
                                blank=True, null=True,
                                help_text=_('Numéro de rue.'))
    mailboxnr = models.CharField(_('Nr de boîte'), max_length=20,
                                 blank=True, null=True,
                                 help_text=_('Numéro de boite, si nécessaire.'))
    zip = models.ForeignKey(
        GeoZip, on_delete=models.CASCADE, blank=True, null=True,
        verbose_name=_('Localité'), help_text=_("Encodez rapidement les premières lettres pour réduire la liste"))


    def get_phone_display(self):
        if self.phone is None:
            tel = ""
        else:
            tel = "Tel: {}".format(self.phone)
        if self.mobile is None:
            mobile = ""
        else:
            mobile = "Gsm: {}".format(self.mobile)

        if tel != "" and mobile != "":
            phone = tel + ' | ' + mobile
        else:
            phone = tel + mobile
        return phone

    def get_street_display(self):
        adr = ""
        if self.street is not None:
            if self.streetnr is not None and self.streetnr != "":
                adr = "{0}, {1}".format(self.street, self.streetnr)
            else:
                adr = self.street
        return adr

    class Meta:
        abstract = True


# https://stackoverflow.com/questions/34239877/django-save-user-uploads-in-seperate-folders
def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.id, filename)


class User(AbstractBaseUser, PermissionsMixin, ContactGeneric2):
    """
    User class base on AbstractBaseUser
    https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#abstractbaseuser
    """
    username_validator = UnicodeUsernameValidator()

    username = fields.CharPGPPublicKeyField(
        _('username'),
        max_length=30,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    email = fields.EmailPGPPublicKeyField(_('email address'), unique=True)
    first_name = fields.CharPGPPublicKeyField(_('first name'), max_length=30, blank=True)
    last_name = fields.CharPGPPublicKeyField(_('last name'), max_length=30, blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    support_structure = models.ForeignKey(
        SupportStructure, on_delete=models.CASCADE, default=1,
        verbose_name=_("Gestionnaire"),
        help_text=_("Défini le gestionnaire d'un utilisateur (gestionnaire ou non).")
    )
    support_structure_access = models.ForeignKey(
        SupportStructureAccess, on_delete=models.CASCADE, default="FULL",
        verbose_name=_("Accès au gestionnaire"),
        help_text=_("Précise les droits d'accès du gestionnaire pour un utilisateur de base.")
    )
    is_manager = models.BooleanField(
        _("Gestionnaire?"),
        default=False,
        help_text=_("Précise si l'utilisateur est gestionnaire (point de contact).")
    )
    email_confirmed = models.BooleanField(
        default=False,
        verbose_name=_("Email confirmé"),
        help_text=_("Demande d'inscription confirmée par email?"))
    vatnr = fields.CharPGPPublicKeyField(_("nr TVA ou d'entreprise"), max_length=13,
                             blank=True, null=True)
    iban = fields.CharPGPPublicKeyField(_("Nr de compte (IBAN)"), max_length=16,
                            blank=True, null=True)
    bic = fields.CharPGPPublicKeyField(_("BIC"), max_length=10,
                           blank=True, null=True)
    website = fields.CharPGPPublicKeyField(verbose_name="Site web",
                              blank=True, null=True, max_length=250)
    company_legalform = models.ForeignKey(CompanyLegalForm,
                                          on_delete=models.CASCADE,
                                          blank=True, null=True,
                                          verbose_name=_('Statut juridique'))
    company_logo = models.FileField(upload_to=user_directory_path,
                                    verbose_name=_('Logo'),
                                    blank=True, null=True)
    invoice_comm = models.TextField(_("Communication facture"), blank=True, null=True)
    data_access = models.BooleanField(
        default=True,
        verbose_name=_("Je consens à l'accès à mes données anonymisées à des fins statistiques?"))
    cgu = models.BooleanField(
        default=False,
        verbose_name=_("J’accepte les conditions d’utilisations"))
    # Overwrite groups and permissions to avoid clash with auth_group and auth_group_permissions
    groups = models.ManyToManyField(
        Group,
        verbose_name=_('groups'),
        blank=True,
        help_text=_(
            'The groups this user belongs to. A user will get all permissions '
            'granted to each of their groups.'
        ),
        related_name="user_set",
        related_query_name="user",
    )
    user_permissions = models.ManyToManyField(
        Permission,
        verbose_name=_('user permissions'),
        blank=True,
        help_text=_('Specific permissions for this user.'),
        related_name="user_set",
        related_query_name="user",
    )
    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'first_name', 'last_name', 'support_structure']

    class Meta:
        verbose_name = _("Utilisateur")
        verbose_name_plural = _('Utilisateurs')
        ordering = ("username",)

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def get_email_display(self):
        if self.email is not None:
            return "Email: {}".format(self.email)
        else:
            return ""

    def get_vat_display(self):
        if self.vatnr is not None:
            return "Nr TVA: {}".format(self.vatnr)
        else:
            return ""

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_absolute_url(self):
        return reverse_lazy('profile_detail', args=[str(self.id)])

    def get_update_url(self):
        return reverse_lazy('profile_update', args=[str(self.id)])
#         return reverse('profile_detail', kwargs = {'pk': str(self.id)}) # ALSO OK

    def get_title(self):
        return "{0} {1}".format(self._meta.verbose_name, self.username)

    def get_verbose_name(self, lookup):
        # will return first non relational field's verbose_name in lookup
        # https://stackoverflow.com/questions/40176689/django-orm-get-verbose-name-of-field-via-filter-lookup
        model = self
        if LOOKUP_SEP in lookup:
            for part in lookup.split(LOOKUP_SEP):
                try:
                    f = model._meta.get_field(part)
                except FieldDoesNotExist:
                    # check if field is related
                    for f in model._meta.related_objects:
                        if f.get_accessor_name() == part:
                            break
                    else:
                        raise ValueError("Invalid lookup string")
                if f.is_relation:
                    model = f.related_model
                    continue
                return force_text(f.verbose_name)
        else:
            return model._meta.get_field(lookup).verbose_name.title()

    def __str__(self):
        return "{}".format(self.username)


class Profile(AbstractBaseUser, PermissionsMixin, ContactGeneric):
    """
    Used for local install without encryption
    User class base on AbstractBaseUser
    https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#abstractbaseuser
    """
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=30,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    support_structure = models.ForeignKey(
        SupportStructure, on_delete=models.CASCADE, default=1,
        verbose_name=_("Gestionnaire"),
        help_text=_("Défini le gestionnaire d'un utilisateur (gestionnaire ou non).")
    )
    support_structure_access = models.ForeignKey(
        SupportStructureAccess, on_delete=models.CASCADE, default="FULL",
        verbose_name=_("Accès au gestionnaire"),
        help_text=_("Précise les droits d'accès du gestionnaire pour un utilisateur de base.")
    )
    is_manager = models.BooleanField(
        _("Gestionnaire?"),
        default=False,
        help_text=_("Précise si l'utilisateur est gestionnaire (point de contact).")
    )
    email_confirmed = models.BooleanField(
        default=False,
        verbose_name=_("Email confirmé"),
        help_text=_("Demande d'inscription confirmée par email?"))
    vatnr = models.CharField(_("nr TVA ou d'entreprise"), max_length=13,
                             blank=True, null=True)
    iban = models.CharField(_("Nr de compte (IBAN)"), max_length=16,
                            blank=True, null=True)
    bic = models.CharField(_("BIC"), max_length=10,
                           blank=True, null=True)
    website = models.CharField(verbose_name="Site web",
                              blank=True, null=True, max_length=250)
    company_legalform = models.ForeignKey(CompanyLegalForm,
                                          on_delete=models.CASCADE,
                                          blank=True, null=True,
                                          verbose_name=_('Statut juridique'))
    company_logo = models.FileField(upload_to=user_directory_path,
                                    verbose_name=_('Logo'),
                                    blank=True, null=True)
    invoice_comm = models.TextField(_("Communication facture"), blank=True, null=True)
    data_access = models.BooleanField(
        default=True,
        verbose_name=_("Je consens à l'accès à mes données anonymisées à des fins statistiques?"))
    cgu = models.BooleanField(
        default=False,
        verbose_name=_("J’accepte les conditions d’utilisations"))
    # Overwrite groups and permissions to avoid clash with auth_group and auth_group_permissions
    groups = models.ManyToManyField(
        Group,
        verbose_name=_('groups'),
        blank=True,
        help_text=_(
            'The groups this user belongs to. A user will get all permissions '
            'granted to each of their groups.'
        ),
        related_name="profile_set",
        related_query_name="profile",
    )
    user_permissions = models.ManyToManyField(
        Permission,
        verbose_name=_('user permissions'),
        blank=True,
        help_text=_('Specific permissions for this user.'),
        related_name="profile_set",
        related_query_name="profile",
    )
    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'first_name', 'last_name', 'support_structure']

    class Meta:
        verbose_name = _("Utilisateur")
        verbose_name_plural = _('Utilisateurs')
        ordering = ("username",)

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def get_email_display(self):
        if self.email is not None:
            return "Email: {}".format(self.email)
        else:
            return ""

    def get_vat_display(self):
        if self.vatnr is not None:
            return "Nr TVA: {}".format(self.vatnr)
        else:
            return ""

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_absolute_url(self):
        return reverse_lazy('profile_detail', args=[str(self.id)])

    def get_update_url(self):
        return reverse_lazy('profile_update', args=[str(self.id)])
#         return reverse('profile_detail', kwargs = {'pk': str(self.id)}) # ALSO OK

    def get_title(self):
        return "{0} {1}".format(self._meta.verbose_name, self.username)

    def get_verbose_name(self, lookup):
        # will return first non relational field's verbose_name in lookup
        # https://stackoverflow.com/questions/40176689/django-orm-get-verbose-name-of-field-via-filter-lookup
        model = self
        if LOOKUP_SEP in lookup:
            for part in lookup.split(LOOKUP_SEP):
                try:
                    f = model._meta.get_field(part)
                except FieldDoesNotExist:
                    # check if field is related
                    for f in model._meta.related_objects:
                        if f.get_accessor_name() == part:
                            break
                    else:
                        raise ValueError("Invalid lookup string")
                if f.is_relation:
                    model = f.related_model
                    continue
                return force_text(f.verbose_name)
        else:
            return model._meta.get_field(lookup).verbose_name.title()

    def __str__(self):
        return self.username


# from config.settings import SERVER_NAME

# def get_myuser_model():
#     if SERVER_NAME == "pythonprod":
#         return User
#     else:
#         return Profile

class UserGeneric(models.Model):
    """
    User always in working tables tables (IN & OUT)
    """
    profile = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        abstract = True


def set_myuserpk(user, request, **kwargs):
    """
    Set myuserpk session variable used by default for user management
    """
    request.session["myuserpk"] = user.id
    # if user.is_staff:
    #     LOGIN_REDIRECT_URL = 'admin_index'


user_logged_in.connect(set_myuserpk)
