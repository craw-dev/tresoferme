# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations


def load_from_sql():
    import os
    from config.settings import BASE_DIR
    DATA_DIR = 'accounts/data_insert/'
    sql_statements = ""
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'auth_group.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'accounts_agriculturalregion.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'accounts_companylegalform.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'accounts_geozip.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'accounts_supportstructure.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'accounts_supportstructureaccess.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'accounts_profile.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'accounts_profile_groups.sql'), 'r').read()
    return sql_statements


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(load_from_sql()),
    ]
