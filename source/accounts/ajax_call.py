# -*- coding: utf-8 -*-
"""
accounts.ajax_call
Specific module designed for ajax call
Usually used for Drop Down List (ddl) view refresh
"""
# import logging

from django.shortcuts import render
from .models import GeoZip

# LOG = logging.getLogger(__name__)


def ddl_zip(request):
    """
    used in signup_form.html
    """
    zipsearch = request.GET.get("zipsearch")
    if zipsearch.isnumeric():
        qs = GeoZip.objects.filter(postal_code__istartswith=zipsearch)
    else:
        qs = GeoZip.objects.filter(place_name__istartswith=zipsearch)

    return render(
        request, "dropdown/dropdown_zip_list.html", {"ddl_list": qs}
    )
