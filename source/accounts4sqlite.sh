#!/bin/bash
<<COMMENT1
----------------------------------------------------------------
Shell script to be stored in source django folder
This script adapt accounts.models.py file from User to Profile
----------------------------------------------------------------
Author: Patrick Houben
Date: 24/06/2020
----------------------------------------------------------------
COMMENT1

# adapt accounts/models.py file with appropriate user_model
echo "adapt accounts/models.py file with appropriate user_model"
sed -i 's/ForeignKey(User/ForeignKey(Profile/g' accounts/models.py
echo "OK"
