"""TresoFerme URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# djauth/urls.py
from . import settings
from accounts import ajax_call as accounts_ajax_call
from money import ajax_call
from money.views import my_home, revenueproduct_delete, revenuepayment_delete, expensepayment_delete
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView


urlpatterns = [
    path('', my_home, name='home'),
    path('chapeau/', admin.site.urls),
    path('accounts/', include('accounts.urls'), name='accounts'),
    path('tresoferme/', include('farm.urls')),
    path('tresoferme/', include('money.urls')),
    path('info/', include('info.urls')),
]

urlpatterns = urlpatterns + [
    path('ajax/ddl_zip/', accounts_ajax_call.ddl_zip, name='ajax_ddl_zip'),
    path('ajax/ddl_year/', ajax_call.ddl_year, name='ajax_ddl_year'),
    path('ajax/ddl_product/', ajax_call.ddl_product, name='ajax_ddl_product'),
    path('ajax/ddl_supplier/', ajax_call.ddl_supplier, name='ajax_ddl_supplier'),
    path('ajax/ddl_revcat/', ajax_call.ddl_revcat, name='ajax_ddl_revcat'),
    path('ajax/ddl_expcat/', ajax_call.ddl_expcat, name='ajax_ddl_expcat'),
    # Below return json of expcat based on supplier
    path('ajax/ddl_get_expcat/', ajax_call.ddl_get_expcat, name='ajax_ddl_get_expcat'),
    path('ajax/get_item2hide/', ajax_call.get_item2hide, name='ajax_get_item2hide'),
    ]

urlpatterns = urlpatterns + [
    path('ajax/revenueproduct/del', revenueproduct_delete, name='revenueproduct_delete'),
    path('ajax/revenuepayment/del', revenuepayment_delete, name='revenuepayment_delete'),
    path('ajax/expensepayment/del', expensepayment_delete, name='expensepayment_delete')
]


# if settings.DEBUG:
#     import debug_toolbar
#     urlpatterns = [
#         path('__debug__/', include(debug_toolbar.urls)),
#     ] + urlpatterns
