from django.apps import AppConfig


class FarmConfig(AppConfig):
    name = 'farm'
    verbose_name = 'Ferme'
