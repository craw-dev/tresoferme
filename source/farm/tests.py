# https://stackoverflow.com/questions/38060232/django-left-outer-join
from django.test import TestCase

from farm.models import Atelier, AtelierList
from django.contrib.auth.models import User

from django.db.models import Q


class TestSuite(TestCase):

    def setUp(self):
        a1 = AtelierList.objects.create(id="BovViande", name="Bovin Viande")
        a2 = AtelierList.objects.create(id="BovLait", name="Lait")
        a3 = AtelierList.objects.create(id="Pord", name="Porc 1")
        # 2 for Johnny
        emilie = User.objects.create(username="Emilie")

        aa1 = Atelier.objects.create(user=emilie, atelier=a1)
#         aa2 = Atelier.objects.create(emilie, a3)

    def test_raw(self):
        print('\nraw\n---')
#         print(emilie.id)
        with self.assertNumQueries(2):
            ee = User.objects.get(username="Emilie")
            print(ee.id)
            ateliers = Atelier.objects.raw('''
                select * from farm_atelierlist
                    left join (select atelier_id as tid from farm_atelier where user_id = 2) aa
                    on aa.tid = farm_atelierlist.id
                ''')
            for atelier in ateliers:
                print(atelier.name, atelier.tid)

#     def test_orm(self):
#         print('\norm\n---')
#         with self.assertNumQueries(1):
#             topics = Atelier.objects.filter(Q(atelier__user_id=1)).values_list('name', 'atelier')
#             for topic in topics:
#                 print(*topic)
