
--
-- Data for Name: farm_atelier; Type: TABLE DATA; Schema: public; Owner: tresoadmin_demo
--

INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (7, 'GEN', 3);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (11, 'GEN', 10);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (12, 'GEN', 8);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (13, 'MAR', 8);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (14, 'MAR', 10);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (15, 'GEN', 12);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (16, 'MAR', 12);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (18, 'GEN', 17);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (19, 'GEN', 21);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (20, 'MAR', 21);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (21, 'BOVLAI', 3);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (22, 'GEN', 24);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (23, 'BOVLAI', 24);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (24, 'BOVELE', 24);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (25, 'CAP', 24);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (26, 'FOU', 24);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (27, 'OVILAI', 24);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (28, 'OVIELE', 24);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (29, 'PORENG', 24);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (30, 'PPOND', 24);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (31, 'PCHAIR', 24);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (32, 'BOVENG', 24);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (33, 'PORELE', 24);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (35, 'ARB', 17);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (36, 'CER', 8);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (37, 'GEN', 33);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (38, 'BOVLAI', 33);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (41, 'PORENG', 33);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (42, 'PPOND', 33);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (43, 'GEN', 34);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (44, 'BOVLAI', 8);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (45, 'BOVLAI', 34);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (46, 'BOVELE', 34);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (47, 'PORENG', 34);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (48, 'PPOND', 34);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (49, 'PCHAIR', 34);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (50, 'ARB', 34);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (51, 'CER', 34);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (52, 'OVIELE', 34);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (53, 'GCA', 3);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (54, 'EPI', 8);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (55, 'API', 8);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (56, 'ARB', 8);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (57, 'GCA', 8);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (58, 'API', 3);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (59, 'ARB', 3);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (60, 'BET', 8);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (61, 'BOVELE', 8);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (62, 'BOVENG', 8);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (63, 'GCA', 34);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (64, 'API', 17);
INSERT INTO farm_atelier (id, atelier_id, profile_id) VALUES (65, 'OVIELE', 17);


--
-- Name: farm_atelier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tresoadmin_demo
--

-- SELECT pg_catalog.setval('farm_atelier_id_seq', 67, true);


--
-- PostgreSQL database dump complete
--

