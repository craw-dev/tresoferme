--
-- Data for Name: farm_atelierlist; Type: TABLE DATA; Schema: public; Owner: tresoadmin_demo
--

INSERT INTO farm_atelierlist (id, name, family) VALUES ('GEN', 'Général', 'ALL');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('BOVLAI', 'Bovin laitier', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('BOVELE', 'Bovin viandeux d''élevage', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('BOVENG', 'Bovin viandeux enrgaissement', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('PORELE', 'Porc élevage', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('PORENG', 'Porc engraissement', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('OVIELE', 'Ovin viandeux', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('OVILAI', 'Ovin laitier', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('CAP', 'Caprin', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('EQU', 'Equin', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('PPOND', 'Poule pondeuse', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('PCHAIR', 'Poulet de chair', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('PDT', 'Pomme de terre', 'GC');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('BET', 'Betterave', 'GC');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('CER', 'Céréales', 'GC');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('GCA', 'Autre grande culture', 'GC');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('FOU', 'Fourrage', 'GC');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('LPC', 'Légume plein champ', 'GC');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('MAR', 'Maraîchage', 'CUL');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('ARB', 'Arboriculture', 'CUL');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('EPI', 'Epicerie', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('API', 'Apiculture', 'ELE');
INSERT INTO farm_atelierlist (id, name, family) VALUES ('ACC', 'Accueil à la ferme', 'ELE');


--
-- PostgreSQL database dump complete
--

