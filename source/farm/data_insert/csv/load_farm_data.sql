﻿-- farm_atelierlist
delete from public.farm_atelierlist;
COPY farm_atelierlist(id, name, sector) 
FROM '/home/pat/dev/pipenv/agnew/source/farm/data_insert/atelier_list.csv' DELIMITER ',' CSV HEADER;

-- money_expenselist
/*
delete from farm_ateliersublist;
COPY farm_animallist(atelier_id, name) 
FROM '/home/pat/dev/pipenv/agnew/source/farm/data_insert/ateliersub_list.csv' DELIMITER ',' CSV HEADER;
*/

pg_dump -h localhost -p 5432 -U "postgres" -C -data-only --table=farm_atelierlist --column-inserts --format plain --verbose --file "/home/pat/dumps/agricogest/agnew.sql" --dbname=agricogest_00
