# -*- coding: utf-8 -*-
"""
farm.models description
"""
from django.db import models
from django.utils.translation import ugettext as _
from accounts.models import UserGeneric


class UserManager(models.Manager):
    def get_queryset(self):
        return super(UserManager, self).get_queryset()

    def for_user(self, user):
        return super(UserManager, self).get_queryset().filter(profile=user)


class AtelierList(models.Model):
    FAMILYLIST = (
        ("ALL", _("Élevage & Culture")),
        ("ELE", _("Élevage")),
        ("GC", _("Grande culture")),
        ("CUL", _("Culture"))
    )
    id = models.CharField(max_length=8, primary_key=True)
    name = models.CharField(max_length=40, verbose_name=_("Atelier"))
    family = models.CharField(max_length=3, choices=FAMILYLIST, default="ELE",
                              verbose_name=_("Famille"))

    class Meta:
        verbose_name = _("Liste atelier")
        ordering = ['name']

    def __str__(self):
        return self.name


class Atelier(UserGeneric):
    """
    is_deletable comes from https://gist.github.com/freewayz/69d1b8bcb3c225bea57bd70ee1e765f8
    """
    atelier = models.ForeignKey(AtelierList, on_delete=models.CASCADE,
                                verbose_name="Atelier")
    objects = UserManager()

    def is_deletable(self):
        related_list = []
        # get all the related object
        for rel in self._meta.get_fields():
            try:
                # check if there is a relationship with at least one related object
                related = rel.related_model.objects.filter(**{rel.field.name: self})
                if related.exists():
                    related_list.append(related)
                    # if there is return a Tuple of flag = False the related_model object
            except AttributeError:  # an attribute error for field occurs when checking for AutoField
                pass  # just pass as we dont need to check for AutoField
        return related_list

    class Meta:
        unique_together = ('profile', 'atelier')
        verbose_name = _("Atelier")

    def get_title(self):
        return "{0} {1}".format(self._meta.verbose_name, self.atelier)

    def __str__(self):
        return "{0}".format(self.atelier)
