'''
Created on 21 aug 2018
@author: p.houben
'''
from django.urls import path
from . import views

urlpatterns = [
    path('ateliers', views.my_atelier, name='my_atelier')
]
