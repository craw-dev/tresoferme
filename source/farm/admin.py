# encoding:utf-8
from django.contrib import admin

from .models import AtelierList, Atelier

admin.site.register(AtelierList)
admin.site.register(Atelier)

