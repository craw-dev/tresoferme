'''
Created on 21 aug 2018
@author: p.houben
'''
import logging

from django.shortcuts import render, redirect, get_object_or_404
from django.db import IntegrityError
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .models import Atelier, AtelierList

LOG = logging.getLogger(__name__)

@login_required
def my_atelier(request):
    """
    Function view based on raw select in order to manage outer join
    and display
    """
    upd_allowed = False
    if "myuserpk" in request.session:
        userpk = request.session["myuserpk"]
        usermodel = get_user_model()
        myuser = get_object_or_404(usermodel, pk=userpk)
        if request.user.is_manager and myuser.support_structure_access.id == "FULL":
            upd_allowed = True
        if myuser == request.user:
            upd_allowed = True
    else:
        myuser = request.user
        upd_allowed = True

    ateliers = Atelier.objects.raw('''
    select al.id, al.name, aa.aid from farm_atelierlist as al
    left join (select atelier_id as aid
                 from farm_atelier
                where profile_id = {0}) aa
    on aa.aid = al.id
    where al.id != 'GEN'
    order by al.name ASC
    '''.format(myuser.id))

    if request.method == 'POST' and upd_allowed:
        # Add GENERAL first
        Atelier.objects.get_or_create(atelier=AtelierList.objects.get(id='GEN'),
                                      profile=myuser)

        # selected elements provided by the form
        selected = request.POST.getlist("atelier")
        for ticked in selected:
            ticked_inst = AtelierList.objects.get(id=ticked)
            Atelier.objects.for_user(myuser).get_or_create(profile=myuser,
                                                           atelier=ticked_inst)

        # Get unselected elements and remove them
        unselected_inst = AtelierList.objects.exclude(id__in=selected).exclude(id='GEN')
        for unticked in unselected_inst:
            try:
                obj = Atelier.objects.for_user(myuser).get(atelier=unticked)
                # check related before delete
                related_list = Atelier.is_deletable(obj)
                if related_list:
                    messages.error(request,
                        _("Pour supprimer l'atelier '{0}', " +\
                        "supprimez d'abord les enregistrements qui y font référence.").format(obj)
                    )
                    for related in related_list:
                        related_model = related.model._meta.verbose_name
                        related_values = [i.__str__() for i in related.all()]
                        msg = "{0}: {1}".format(related_model, related_values)
                        messages.warning(request, msg)
                    return redirect('my_atelier')
                else:
                    obj.delete()
            except Atelier.DoesNotExist:
                pass

            # try:
            #     remove_atelier = Atelier.objects.for_user(myuser).get(atelier=unticked)
            #     remove_atelier.delete()
            # except IntegrityError:
            #     messages.error(request, _("Impossible de supprimer l'atelier {0} car "\
            #         "il existe encore ailleurs (Entrée/Sortie Récurrente ou non - Référence/Produit).").format(remove_atelier)
            #     )
            #     return redirect('my_atelier')
            # except Atelier.DoesNotExist:
            #     pass
        messages.success(request, _('Modifications sauvegardées'),
                                fail_silently=True)
        
        return redirect('my_atelier')

    # ne pas changer user: request.user!
    context = {'user': request.user,
               'ateliers': ateliers,
               'myuser': myuser,
               'upd_allowed': upd_allowed}

    return render(request, 'select_atelier_form.html', context)
