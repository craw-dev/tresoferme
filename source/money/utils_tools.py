# https://djangosnippets.org/snippets/10332/
import xlwt, json, datetime, logging, pytz
# import StringIO

from django.core.exceptions import ObjectDoesNotExist
from django.forms.models import model_to_dict

from django.utils import timezone
from django.db.models import Max
from django.db.models.functions import Extract
from money.models import Expense, Revenue, Client, Supplier, Product, ExpCat, RevCat
from accounts.models import User
from config.settings import TIME_ZONE

LOG = logging.getLogger(__name__)

def set_prevlast_year(request, myuser):
    """
    List all years from Expense & Revenue + set max_year Revenue and max_year Expense
    """
    exp_list = list()
    rev_list = list()
    qs_exp = Expense.objects.for_user(myuser).exclude(expcat__group__in=('FIX', 'EMP')).values_list('inputdate__year', flat=True).distinct()
    qs_rev = Revenue.objects.for_user(myuser).exclude(revcatgrp__in=('FIX')).exclude(revcat__id="FRM_BDC").values_list('inputdate__year', flat=True).distinct()

    exp_list = list(qs_exp)
    rev_list = list(qs_rev)

    if len(exp_list) == 0 and len(rev_list) == 0:
        maxyear = timezone.now().year
    else:
        maxyear = max(exp_list + rev_list)

    # convert to str because THOUSAND SEPARATOR add blank space for integer like 2 019
    request.session["year1"] = str(maxyear - 1)
    request.session["year2"] = str(maxyear)

    if exp_list:
        request.session["exp_year"] = str(max(exp_list))
        request.session["exp_year_list"] = json.dumps(exp_list)
    else:
        request.session["exp_year"] = str(maxyear)
        request.session["exp_year_list"] = json.dumps([maxyear])

    if rev_list:
        request.session["rev_year"] = str(max(rev_list))
        request.session["rev_year_list"] = json.dumps(rev_list)
    else:
        request.session["rev_year"] = str(maxyear)
        request.session["rev_year_list"] = json.dumps([maxyear])
    
    

def get_year_list(request, which_list='BOTH'):
    """
    in param is session list like [2018, 2019]
    out param is a list of tuple like [('2018', '2018'), ('2019', '2019')]
    """
    year_list_out = []
    exp_year_list = json.loads(request.session["exp_year_list"])
    rev_year_list = json.loads(request.session["rev_year_list"])

    if which_list == 'BOTH':
        result = set(exp_year_list + rev_year_list)
    if which_list == 'EXP':
        result = set(exp_year_list)
    if which_list == 'REV':
        result = set(rev_year_list)
    # Obligé d'ajouter artificiellement une année pour que le solde du graphique annuel s'affiche
    if len(result) == 1:
        year_value = list(result)[0]
        # year_value = next(iter(result)) fonctionne aussi
        year_value2 = year_value - 1
        # ajout valeur à un set
        result.add(year_value2)
    for i in result:
        # line = ("{}".format(i), "{}".format(i))
        line = (str(i), str(i))
        year_list_out.append(line)
    return year_list_out


class SaveAsXLS(object):
    """
    Save as XLS snippet based on https://djangosnippets.org/snippets/10332/
    Do not forget:
    1. Add def get_verbose_name to the model
    2. Add Columns definition for COLUMNS dict below
    3. Add 'exportxls' HiddenField on the form
    4. Add Export XLS btn with id = id_exportbtn
    5. Add js which submit the form whith XLSGO as param for 'exportxls' HiddenField
    Due to issue "can't subtract offset-naive and offset-aware datetimes"
    rm CELL_STYLE_MAP and add styles dict
    https://github.com/django-xxx/django-excel-response2/blob/3b76041a1f5430af557bc0d6cf8a1af35cf67966/django-excel-response2.py
    CELL_STYLE_MAP loop over the keys instead of checking value directly
    """
    COLUMNS = {
        "User": ("username", "date_joined", "last_login", "jlastlog", "is_active", "email_confirmed",
                 "email", "first_name", "last_name", "email", "mobile", "phone", "company",
                 "zip__country_code", "zip__postal_code", "zip__place_name", "zip__admin_name2", "zip__agricultural_region__name"),
        "Expense": ("expnr", "inputdate",
                    "amountvat", "vat__name", "vatvalue", "amount",
                    "supplier__company", "supplier__zip__country_code",
                    "expcat__group__name", "expcat__name", "atelier__atelier__name",
                    "expcat__comptaplan__name", "expcat__categclass__name"),
        "Revenue": ("invnr", "inputdate",
                    "amountvat", "vat__name", "vatvalue", "amount",
                    "client__company", "client__zip__country_code",
                    "revcat__group__name", "revcat__name", "atelier__atelier__name", "aide_minimi"),
        "Product": ("name", "atelier__atelier__name", "processed", "buyprice", "unit", "sellpricevat", "vat", "sellprice"),
        "Client": ("company", "contact_name", "clienttype", "remarks", "vatnr", "email",
                    "mobile", "phone", "street", "streetnr", "mailboxnr",
                    "zip__country_code", "zip__postal_code", "zip__place_name", "zip__admin_name2", "zip__agricultural_region__name"),
        "Supplier": ("company", "contact_name", "remarks", "vatnr", "email",
                    "mobile", "phone", "street", "streetnr", "mailboxnr",
                    "zip__country_code", "zip__postal_code", "zip__place_name", "zip__admin_name2", "zip__agricultural_region__name",
                    "expcat__group__name", "expcat__name",
                    "expcat__comptaplan__name", "expcat__categclass__name"),
        "ExpCat": ("group__name", "name", "categclass__name", "comptaplan__name", "comptaagri__name", "rentatxt", "tresotxt"),
        "RevCat": ("group__name", "name", "categclass__name", "comptaplan__name", "comptaagri__name", "rentatxt", "tresotxt"),
    }
    model = None
    columns = "NADA"
    workbook = None
    header_style = xlwt.easyxf('font: bold on')
    styles = {'datetime': xlwt.easyxf(num_format_str='yyyy-mm-dd hh:mm:ss'),
            'date': xlwt.easyxf(num_format_str='yyyy-mm-dd'),
            'time': xlwt.easyxf(num_format_str='hh:mm:ss'),
            'default': xlwt.Style.default_style}

    def __init__(self, qs):
        self.qs = qs
        self.modeltxt = qs.model.__name__

    def get_model(self):
        model = "NADA"
        msg = "OK"
        try:
            model = eval(self.modeltxt)
        except NameError:
            msg = "Le modèle {} n'a pas été importé au préalable.".format(self.modeltxt)
        return model, msg

    def get_columns(self):
        model = "NADA"
        msg = "OK"
        columns = "NADA"
        if self.modeltxt in self.COLUMNS:
            columns = self.COLUMNS[self.modeltxt]
        else:
            msg = "Aucune colonne n'a  été définie pour {} dans money.utils_tools.SaveAsXLS".format(self.modeltxt)
            LOG.error(msg)
        return columns, msg

    def get_model_and_columns(self):
        """
        1. get model
        2. get columns
        """
        msg = "OK"
        self.model, msg = self.get_model()
        if msg == "OK":
            self.columns, msg = self.get_columns()
        return msg

    def create_workbook(self):
        self.workbook = xlwt.Workbook()
        return self.workbook

    def add_sheet(self, sheet_name):
        """
        Add a sheet in a workbook
        get_verbose_name must be defined in each model we want to export
        https://stackoverflow.com/questions/40176689/django-orm-get-verbose-name-of-field-via-filter-lookup
        return true, ok
        """
        if self.workbook is None:
            msg = "Workbook does not exist! create_workbook() must be called from the View. Model is {0}".format(self.modeltxt)
            LOG.error(msg)
            return False, msg
        else:
            sheet = self.workbook.add_sheet(sheet_name)

        try:
            inst = self.model.objects.first()
            for y, column in enumerate(self.columns):
                if column == "solde":
                    verbose_name = "Solde"
                elif column == "jlastlog":
                    verbose_name = "Innactif depuis (jours)"
                else:
                    verbose_name = inst.get_verbose_name(column)
                sheet.write(0, y, verbose_name, self.header_style)
        except AttributeError as error:
            LOG.error(error)
            return False, error

        try:
            qs = self.qs.values(*self.columns)
            for x, dico in enumerate(qs, start=1):
                for y, column in enumerate(self.columns):
                    value = dico[column]
                    if isinstance(value, datetime.datetime):
                        if timezone.is_aware(value):
                            value = timezone.make_naive(value, pytz.timezone(TIME_ZONE))
                        cell_style = self.styles['datetime']
                    elif isinstance(value, datetime.date):
                        cell_style = self.styles['date']
                    elif isinstance(value, datetime.time):
                        cell_style = self.styles['time']
                    # elif font:
                    #     cell_style = styles['font']
                    else:
                        cell_style = self.styles['default']
                    sheet.write(x, y, value, style=cell_style)
        except Exception as xcpt:
            LOG.error(xcpt)
            error_msg = "money.utils_tools.add_sheet: {}".format(xcpt)
            return False, error_msg

        return True, "OK"

"""
A function extends of Tarken's django-excel-response
django-excel-response
1、djangosnippets - http://djangosnippets.org/snippets/1151/
2、pypi - https://pypi.python.org/pypi/django-excel-response/1.0
When using Tarken's django-excel-response. We find that
Chinese is messed code when we open .xls in Mac OS.
As discussed in http://segmentfault.com/q/1010000000095546.
We realize django-excel-response2
Based on Tarken's django-excel-response to solve this problem
By adding a Param named font to set font.
"""

# from django.conf import settings
from django.db.models.query import QuerySet
from django.http import HttpResponse
# from django.utils import timezone

# import datetime
# import pytz


class ExcelResponse(HttpResponse):

    content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    # mimetype = 'application/vnd.ms-excel'

    def __init__(self, data, filename='excel_data', headers=None,
                 force_csv=False, encoding='utf8', font=''):

        # Make sure we've got the right type of data to work with
        valid_data = False
        # if isinstance(data, ValuesQuerySet):
        #     data = list(data)
        if isinstance(data, QuerySet):
            data = list(data.values())
        if hasattr(data, '__getitem__'):
            if isinstance(data[0], dict):
                if headers is None:
                    headers = data[0].keys()

                data = [[row[col] for col in headers] for row in data]
                data.insert(0, headers)
            if hasattr(data[0], '__getitem__'):
                valid_data = True
        assert valid_data is True, "ExcelResponse requires a sequence of sequences"

        from io import BytesIO, StringIO
        output = BytesIO()
        # Excel has a limit on number of rows; if we have more than that, make a csv
        use_xls = False
        if len(data) <= 65536 and force_csv is not True:
            try:
                import xlwt
            except ImportError:
                # xlwt doesn't exist; fall back to csv
                pass
            else:
                use_xls = True
        if use_xls:
            book = xlwt.Workbook(encoding=encoding)
            sheet = book.add_sheet('Sheet 1')
            styles = {'datetime': xlwt.easyxf(num_format_str='yyyy-mm-dd hh:mm:ss'),
                      'date': xlwt.easyxf(num_format_str='yyyy-mm-dd'),
                      'time': xlwt.easyxf(num_format_str='hh:mm:ss'),
                      'font': xlwt.easyxf('%s %s' % (u'font:', font)),
                      'default': xlwt.Style.default_style}

            for rowx, row in enumerate(data):
                for colx, value in enumerate(row):
                    if isinstance(value, datetime.datetime):
                        if timezone.is_aware(value):
                            value = timezone.make_naive(value, pytz.timezone(TIME_ZONE))
                        cell_style = styles['datetime']
                    elif isinstance(value, datetime.date):
                        cell_style = styles['date']
                    elif isinstance(value, datetime.time):
                        cell_style = styles['time']
                    elif font:
                        cell_style = styles['font']
                    else:
                        cell_style = styles['default']
                    sheet.write(rowx, colx, value, style=cell_style)
            book.save(output)
            
            file_ext = 'xls'
        else:
            for row in data:
                out_row = []
                for value in row:
                    if not isinstance(value, basestring):
                        value = unicode(value)
                    value = value.encode(encoding)
                    out_row.append(value.replace('"', '""'))
                output.write('"%s"\n' %
                             '","'.join(out_row))

            file_ext = 'csv'
        output.seek(0)

        super(ExcelResponse, self).__init__(content=output.getvalue())
        self['Content-Disposition'] = 'attachment; filename={0}'.format(filename)

