# -*- coding: utf-8 -*-
"""
money.ajax_call
Specific module designed for ajax call
Usually used for Drop Down List (ddl) view refresh
"""
import logging

from datetime import date
from django.shortcuts import render, get_object_or_404
from django.db.models.query import QuerySet
from django.http import JsonResponse
from django.contrib.auth import get_user_model
from farm.models import Atelier
from .utils_tools import get_year_list, set_prevlast_year
from .formsearch import ddl_expcat_inst, ddl_revcat_inst
from .models import Loan, Product, Supplier, ExpCat, Credit, Client, RevCat, RevCatGrp

LOG = logging.getLogger(__name__)


def ddl_year(request):
    """
    Used in expense form
    remove Facture to avoid direct facturation
    """
    usermodel = get_user_model()
    userpk = request.session["myuserpk"]
    myuser = usermodel.objects.get(id=userpk)

    set_prevlast_year(request, myuser)
    ddlb_list = get_year_list(request, "BOTH")
    return render(
        request,
        "dropdown/dropdown_year_list.html",
        {"ddl_list": ddlb_list},
    )


def ddl_expcat(request):
    """used in expense_form"""
    return render(
        request,
        "dropdown/dropdown_field2show_list.html",
        {"ddl_list": ddl_expcat_inst(request)},
    )


def ddl_revcat(request):
    """
    Used in expense form
    remove Facture to avoid direct facturation
    """
    inst = ddl_revcat_inst(request)
    return render(
        request,
        "dropdown/dropdown_field2show_list.html",
        {"ddl_list": inst},
    )


def ddl_get_expcat(request):
    """
    used in expense_form (adapt cat when supplier selected)
    different from ddl_expcat user in SupplierForm when Group is selected!
    """
    mydict = {"categ": "", "group": ""}
    if "supplier" in request.GET:
        val = request.GET.get("supplier")
        if val:
            supplier = Supplier.objects.get(pk=val)
            if supplier:
                categ = [i.id for i in supplier.expcat.all()]
                group = [i.group.id for i in supplier.expcat.all()]
            mydict = {"categ": categ, "group": group}

    return JsonResponse(mydict, safe=False)


def ddl_product(request):
    """used in revenue_form.js"""
    my_list = QuerySet
    if "atelier" in request.GET:
        if "myuserpk" in request.session:
            userpk = request.session["myuserpk"]
            usermodel = get_user_model()
            myuser = get_object_or_404(usermodel, pk=userpk)
        else:
            myuser = request.user

        val = request.GET.get("atelier")
        if val:
            atelier_id = Atelier.objects.values_list("atelier_id", flat=True).get(pk=val)
            if atelier_id == "GEN":
                my_list = Product.objects.for_user(myuser).all().order_by("name")
            else:
                my_list = (
                    Product.objects.for_user(myuser)
                    .filter(atelier=val)
                    .order_by("name")
                )
    return render(request, "dropdown/dropdown_product_list.html", {"ddl_list": my_list})


def ddl_supplier(request):
    """used in expense_form.js"""
    userpk = request.session["myuserpk"]
    usermodel = get_user_model()
    myuser = get_object_or_404(usermodel, pk=userpk)
    supplier_list = Supplier.objects.for_user(myuser).all()
    
    categ = request.GET.get("categ")
    group = request.GET.get("group")
    # print('categ:{}'.format(categ))
    # print('group:{}'.format(group))
    if group == 'INV':
        supplier_list = supplier_list.filter(expcat__group__id='INV').distinct()
    if categ is not None:
        supplier_list = supplier_list.filter(expcat=categ)
    return render(
        request, "dropdown/dropdown_company_list.html", {"ddl_list": supplier_list}
    )


def get_item2hide(request):
    """
    Used in expense_form.js
    objective: provide distinct values from column field2show
    field2show is a comma separated field names which must be hidden when
    related field is selected
    """
    if "whichmodel" in request.GET:
        tbl = request.GET.get("whichmodel")
        tblinst = eval(tbl)
        f2s = tblinst.objects.exclude(field2show=None).order_by("field2show").distinct("field2show")
        f2s = list(f2s.values_list("field2show", flat=True))
        data = {"myList": ",".join(f2s)}
    return JsonResponse(data)
