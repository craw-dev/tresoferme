# encoding:utf-8
from django.contrib import admin
from django.utils.translation import ugettext as _

from .models import CategClass, VatList, UnitList, ExpCatGrp, \
                    ExpCat, RevCatGrp, RevCat, \
                    Product, Client, Supplier, Expense, Revenue


class SupplierAdmin(admin.ModelAdmin):
    list_display = ('company', 'get_expcatgrp', 'get_expcat')
    list_filter = ('profile', 'expcat__group__name', 'expcat')
    search_fields = ('company',)
    fieldsets = [
        (_(''), {'fields': ['profile']}),
        (_('Société'), {'fields': ['company',
                                   'expcat', 'remarks']}),
        (_('Contact'), {'fields': ['contact_name', 'email', 'mobile', 'phone',
                                   'street', 'zip', 'zipoutbe']}),
    ]


# class ExpenseAdmin(admin.ModelAdmin):
#     list_display = ('expnr', 'inputdate',
#                     'supplier', 'atelier', 'amountvat')
#     list_filter = ('profile', 'expcat__group__name', 'expcat', 'atelier', 'supplier')
#     search_fields = ('expnr', 'inputdate')
#     fieldsets = [
#         (_('Identification'), {'fields': ['expnr', 'refdesc']}),
#         (_('Groupe'), {'fields': ['expcat',
#                                   'atelier', 'supplier']}),
#         (_('Quantité'), {'fields': ['quantity', 'unit']}),
#         (_('Montant'), {'fields': ['amountvat', 'vat', 'amount']}),
#         (_('Date & Payement'), {'fields': ['inputdate', 'payduedate',
#                                            'duedatelist',
#                                            'payamount', 'payflag']}),
#     ]


# class RevenueAdmin(admin.ModelAdmin):
#     list_display = ('invnr', 'inputdate', 'revcatgrp', 'revcat',
#                     'client', 'atelier', 'amountvat')
#     list_filter = ('profile', 'revcatgrp', 'revcat', 'client', 'atelier')
#     search_fields = ('invnr', 'inputdate')
#     fieldsets = [
#         (_('Identification'), {'fields': ['invnr', 'refdesc']}),
#         (_('Groupe'), {'fields': ['revcatgrp', 'revcat',
#                                   'client', 'atelier']}),
#         (_('Montant'), {'fields': ['loan', 'amountvat', 'vat', 'amount']}),
#         (_('Date & Payement'), {'fields': ['inputdate', 'payduedate',
#                                            'duedatelist',
#                                            'payamount', 'payflag']}),
#     ]


class ExpCatAdmin(admin.ModelAdmin):
    list_display = ('group', 'name', 'categclass')
    list_filter = ('categclass', 'group')
    search_fields = ('name',)


class RevCatAdmin(admin.ModelAdmin):
    list_display = ('group', 'name', 'categclass')
    list_filter = ('categclass', 'group',)
    list_order = ('group',)
    search_fields = ('name',)


admin.site.register(CategClass)
admin.site.register(VatList)
admin.site.register(UnitList)
admin.site.register(ExpCatGrp)
admin.site.register(ExpCat, ExpCatAdmin)
admin.site.register(RevCatGrp)
admin.site.register(RevCat, RevCatAdmin)
# admin.site.register(Product)
# admin.site.register(Client)
# admin.site.register(Supplier, SupplierAdmin)
# admin.site.register(Expense, ExpenseAdmin)
# admin.site.register(Revenue, RevenueAdmin)
