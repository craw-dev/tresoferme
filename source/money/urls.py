'''
Created on 15 jan. 2017
@author: p.houben
'''
from django.urls import path
from django.utils.translation import ugettext as _
from . import views

urlpatterns = [
    path('', views.MoneyIndex.as_view(), name='money_index'),
    path('treso_month', views.GraphIndexTreso.as_view(
            what="MONTH",
            template_name="treso/treso_month.html"), name='treso_month'),
    path('renta_month', views.GraphIndexRenta.as_view(
            what="MONTH",
            template_name="renta/renta_month.html"), name='renta_month'),
    path('recette_atelier', views.GraphIndexRenta.as_view(
            what="REV_ATELIER",
            template_name="renta/renta_group.html"), name='renta_rev_atelier'),
    path('depense_atelier', views.GraphIndexRenta.as_view(
            what="EXP_ATELIER",
            template_name="renta/renta_group.html"), name='renta_exp_atelier'),
    path('depense_fixvariable', views.GraphIndexRenta.as_view(
            what="EXP_FIX_VARIABLE",
            template_name="renta/renta_group.html"), name='renta_exp_fixvariable'),
    path('recette_categ', views.GraphIndexRenta.as_view(
            what="REV_GROUP_CATEG",
            template_name="renta/renta_group.html"), name='renta_rev_groupjs'),
    path('depense_categ', views.GraphIndexRenta.as_view(
            what="EXP_GROUP_CATEG",
            template_name="renta/renta_group.html"), name='renta_exp_groupjs'),
    path('recette_client_produit', views.GraphIndexRenta.as_view(
            what="CLIENT_PRODUIT",
            template_name="renta/renta_group.html"), name='renta_clientproduct'),
    path('depense_fournisseur', views.GraphIndexRenta.as_view(
            what="SUPPLIER",
            template_name="renta/renta_group.html"), name='renta_supplier'),
    # url below is not used
    # path('tresorerie_index', views.GraphIndexRenta.as_view(quid="tresorerie"), name='graph_treso_index'),
    path('treso_prev_graph', views.treso_prev_graph, name='treso_prev_graph'),
    path('treso_prev_index', views.treso_prev_index, name='treso_prev_index'),
    path('treso_prev_select', views.treso_prev_select, name='treso_prev_select'),
    path('treso_prev_list', views.treso_prev_list, name='treso_prev_list'),
    path('treso_prev_update', views.treso_prev_update, name='treso_prev_update'),
    path('treso_prev_delete', views.treso_prev_delete, name='treso_prev_delete'),
    ]

# Dynamic url
# MYMODELS = ['Client', 'Supplier', 'Product', 'Expense', 'FixExpense', 'FixRevenue', 'Loan', 'Credit']
MYMODELS = ['Client', 'Supplier', 'Product', 'Expense', 'Loan', 'Credit']

for modelX in MYMODELS:
    urlpatterns = urlpatterns + [
        path(modelX.lower(), views.MyListView.as_view(model=modelX), name='{0}_list'.format(modelX.lower())),
        path('{0}/add'.format(modelX.lower()), views.MyCreateView.as_view(model=modelX), name='{0}_create'.format(modelX.lower())),
        path('{0}/<pk>'.format(modelX.lower()), views.MyDetailView.as_view(model=modelX), name='{0}_detail'.format(modelX.lower())),
        path('{0}/<pk>/upd'.format(modelX.lower()), views.MyUpdateView.as_view(model=modelX), name='{0}_update'.format(modelX.lower())),
        # path('{0}/<pk>/del'.format(modelX.lower()), views.MyDeleteView.as_view(model=modelX), name='{0}_delete'.format(modelX.lower())),
    ]

# Specific url for non js colors and BDC
urlpatterns = urlpatterns + [
    path('bdc', views.MyListView.as_view(model='Revenue'), name='bdc_list'),
    path('bdc/add', views.RevenueCreateView.as_view(), name='bdc_create'),
    path('bdc/<pk>/upd', views.RevenueUpdateView.as_view(), name='bdc_update'),
    path('bdc/<pk>', views.RevenueDetailView.as_view(), name='bdc_detail'),
    path('invest/', views.MyListView.as_view(model='Expense'), name='invest_list'),
    path('invest/add', views.MyCreateView.as_view(model='Expense'), name='invest_create'),
    path('credit/<pk>/del', views.MyDeleteView.as_view(model='Credit'), name='credit_delete'),
]

# Delete
MYMODELS = ['Client', 'Supplier', 'Product', 'Expense']
for modelX in MYMODELS:
    urlpatterns = urlpatterns + [
        path('{0}/<pk>/del'.format(modelX.lower()), views.MyDeleteView.as_view(model=modelX), name='{0}_delete'.format(modelX.lower())),
    ]

# Fix & Loan => Delete (real Delete View) & Validate
# FIXMODELS = ['FixExpense', 'FixRevenue']
# for modelX in FIXMODELS:
#     urlpatterns = urlpatterns + [
#         path('{0}/<pk>/del'.format(modelX.lower()), views.FixDeleteView.as_view(model=modelX), name='{0}_delete'.format(modelX.lower())),
#         path('{0}/<pk>/valid'.format(modelX.lower()), views.FixValidateView.as_view(model=modelX), name='{0}_valid'.format(modelX.lower())),
#     ]

# Loan real delete and Validate => tablea amortissement
urlpatterns = urlpatterns + [
    path('loan/<pk>/amortization', views.loan_refund, name='loan_refund'),
    path('loan/<pk>/del', views.FixDeleteView.as_view(model='Loan'), name='loan_delete'),
    path('loan/<pk>/valid', views.LoanValidateView.as_view(), name='loan_valid'),
    path('loan/<pk>/close', views.LoanCloseView.as_view(), name='loan_close'),
]

urlpatterns = urlpatterns + [
    path('revenue', views.MyListView.as_view(model='Revenue'), name='revenue_list'),
    path('revenue/add', views.RevenueCreateView.as_view(), name='revenue_create'),
    path('revenue/<pk>', views.RevenueDetailView.as_view(), name='revenue_detail'),
    path('revenue/<pk>/upd', views.RevenueUpdateView.as_view(), name='revenue_update'),
    path('revenue/<pk>/del', views.RevenueDeleteView.as_view(), name='revenue_delete'),
    path('revenue/<pk>/lock', views.RevenueLockView.as_view(), name='revenue_lock'),
    path('revenue/<pk>/pdf', views.RevenueDetailView.as_view(), name='invoice_pdf'),
    path('revenue/<pk>/copy', views.revenue_copy, name='revenue_copy'),

    path('revenueproduct/<pk>/upd', views.revenueproduct_update, name='revenueproduct_update'),
    path('revenuepayment/<pk>/upd', views.payment_update, name='revenuepayment_update'),
    path('expensepayment/<pk>/upd', views.payment_update, name='expensepayment_update'),
    path('expense/<pk>/copy', views.expense_copy, name='expense_copy'),
    ]

urlpatterns = urlpatterns + [
    path('credit/<pk>/creditrefund/add', views.creditrefund_create, name='creditrefund_create'),
    # path('credit/<pk_master>/creditrefund/<pk_detail>/upd', views.creditrefund_update, name='creditrefund_update'),
    # path('credit/<pk>/creditrefund/add', views.CreditRefundCreate.as_view(), name='creditrefund_create'),
    path('credit/creditrefund/<pk>/upd', views.CreditRefundUpdate.as_view(), name='creditrefund_update'),
    path('credit/creditrefund/<pk>/del', views.CreditRefundDelete.as_view(), name='creditrefund_delete'),
]
