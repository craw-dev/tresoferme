# encoding:utf-8
# https://www.abidibo.net/blog/2017/10/16/add-data-attributes-option-tags-django-admin-select-field/

from django.forms.widgets import Select
from .models import Product
from bootstrap_datepicker_plus import DatePickerInput
# https://django-bootstrap-datepicker-plus.readthedocs.io/en/latest/Usage.html?highlight=language


class MyDatePickerInput(DatePickerInput):
    """
    Add format and options to DatePickerInput
    """
    format = '%d/%m/%Y'
    options = {'locale': 'fr'}


class DataAttributesSelect(Select):
    """
    Add extra attributes to widget.
    ie: not only id and label but type for example
    newattr is a tuple list of newatrributes to be added to the options
    data2 if the is a second element in newattr
    """

    def __init__(self, attrs=None, choices=(), data={}, newattr=""):
        super(DataAttributesSelect, self).__init__(attrs, choices)
        self.data = data
        self.newattr = newattr
        self.attrs['class'] = 'form-control'

    def create_option(self, name, value, label, selected, index, subindex=None, attrs=None): # noqa
        option = super(DataAttributesSelect, self).create_option(name, value, label, selected, index, subindex=None, attrs=None) # noqa
        # création de l'attribut newattr pour chaque option value (id): 1, 2, 3, 4, etc.
        # data[1] récupère newattr à l'indice 1, data[2] récupère newattr à l'indice 2, etc.
        if option['value'] != '':
            option['attrs'][self.newattr] = self.data[option['value'].value]
        return option


class ProductAttributesSelect(Select):
    """
    Add extra attributes to widget.
    ie: not only id and label but type for example
    newattr is a tuple list of newatrributes to be added to the options
    """

    def __init__(self, attrs=None, choices=(), user=None):
        super(ProductAttributesSelect, self).__init__(attrs, choices)
        self.user = user
        self.data1 = dict(Product.objects.for_user(self.user).values_list("id", "sellprice"))
        self.data1[""] = "------------"  # empty option

        self.data2 = dict(Product.objects.for_user(self.user).values_list("id", "vat"))
        self.data2[""] = "------------"  # empty option

        self.data3 = dict(Product.objects.for_user(self.user).values_list("id", "sellpricevat"))
        self.data3[""] = "------------"  # empty option

        self.data4 = dict(Product.objects.for_user(self.user).values_list("id", "atelier_id"))
        self.data4[""] = "------------"  # empty option

        self.data5 = dict(Product.objects.for_user(self.user).values_list("id", "unit"))
        self.data5[""] = "------------"  # empty option

        self.attrs['class'] = 'form-control product-change'

    def create_option(self, name, value, label, selected, index, subindex=None, attrs=None): # noqa
        option = super(ProductAttributesSelect, self).create_option(name, value, label, selected, index, subindex=None, attrs=None) # noqa
        if option['value'] != '':
            option['attrs']['sellprice'] = self.data1[option['value'].value]
            option['attrs']['vat'] = self.data2[option['value'].value]
            option['attrs']['sellpricevat'] = self.data3[option['value'].value]
            option['attrs']['atelier_id'] = self.data4[option['value'].value]
            option['attrs']['unit'] = self.data5[option['value'].value]
        return option
