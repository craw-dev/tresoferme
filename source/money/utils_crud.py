# -*- coding: utf-8 -*-
"""
Set of functions used for database data insert
"""
import logging
import numpy as np

from datetime import date
from dateutil.relativedelta import relativedelta

from django.db.models import Sum, Max
from django.db import IntegrityError
from django.contrib import messages
from django.utils.translation import ugettext as _

from .models import (Expense, Revenue, RevenueProduct, ExpensePayment, RevenuePayment, LoanRefund,
                    ExpCatGrp, ExpCat, Atelier, DueDateList, VatList, RevCatGrp, RevCat,
                    Client, Supplier, Loan, Credit)

LOG = logging.getLogger(__name__)
LOG_DEBUG = logging.getLogger("mydebug")


def insert_loanamount_into_revenue(loan, user, request):
    """
    Insert loan amount into Revenue
    """
    try:
        atelier = Atelier.objects.for_user(user).get(atelier="GEN")
        retval, client = get_or_create_client(loan, user, request)
        novat = VatList.objects.get(id=0)
        now_duedate = DueDateList.objects.get(id="NOW")
        loantype = RevCat.objects.get(id=loan.loantype.id)
    except Exception as xcpt:
        LOG.error(xcpt, extra={'user': user})
        msg = _("Erreur lors de la sélection des éléments à insérer dans les recettes.")
        messages.error(request, msg)
        return False

    try:
        Revenue.objects.get(loan=loan)
    except Revenue.DoesNotExist:
        try:
            rev = Revenue.objects.create(
                profile=user,
                loan=loan,
                invnr='E_{0}'.format(loan.refshort),
                inputdate=loan.startdate,
                refdesc=loan.bankrefdesc,
                revcatgrp=loantype.group,
                revcat=loantype,
                client=client,
                atelier=atelier,
                amountvat=loan.loanamount,
                vat=novat,
                amount=loan.loanamount,
                payflag=True,
                payamount=loan.loanamount,
                payduedate=loan.startdate,
                duedatelist=now_duedate
                )

        except Exception as xcpt:
            LOG.error(xcpt, extra={'user': user})
            msg = _("Erreur lors de l'insertion du montant emprunté dans les recettes.")
            messages.error(request, msg)
            return False
    return True


def insert_closeindemnity_into_expense(loan, user, tot_indemnity):
    """
    Insert close indemnity into Expense
    """
    gen_atel = Atelier.objects.for_user(user).get(atelier="GEN")
    novat = VatList.objects.get(id=0)
    now_duedate = DueDateList.objects.get(id="NOW")
    try:
        Expense.objects.for_user(user).get(expnr__exact='{0}_CLOSE'.format(loan.refshort))
    except Expense.DoesNotExist:
        try:
            exp = Expense.objects.create(
                profile=user,
                loan=loan,
                # loanrfd=loan NA,
                expnr='E_{0}_CLOSE'.format(loan.refshort),
                inputdate=loan.closedate,
                refdesc=loan.bankrefdesc,
                expcat=loan.loantype,
                atelier=gen_atel,
                supplier=None,
                amountvat=tot_indemnity,
                vat=novat,
                amount=tot_indemnity,
                payflag=True,
                duedatelist=now_duedate,
                payduedate=loan.closedate
                )

            # Le payement doit être encodé par l'utilisateur
            # ExpensePayment.objects.create(
            #     expense=exp,
            #     paydate=loan.closedate,
            #     amount=tot_indemnity,
            #     profile=user
            # )

        except Exception as xcpt:
            LOG.error(xcpt, extra={'user': user})
            return False, None
    return True, exp.id


def amortize(loan, user):
    """
    Construit la tableau d'amortissement pour les payements ou capital fixe
    loan: instance of Loan
    user: instance of User
    return: Boolean
    Source: http://pbpython.com/amortization-model.html + http://pbpython.com/amortization-model-revised.html
    https://github.com/chris1610/pbpython/blob/master/notebooks/Amortization-Model-Article.ipynb
    https://github.com/MartinPyka/financial_life (à analyser!)
    """
    # First delete the data
    try:
        loan_refund = LoanRefund.objects.filter(loan=loan)
        loan_refund.delete()
    except Exception as xcpt:
        LOG.error(xcpt, extra={'user': user})
        return False
    # Init vars
    first_except = False
    # No sense to have additional payments per month
    cap_add = 0
    capcum = 0
    period = 1
    irate = float(loan.interestrate/100)
    prate = (1+irate)**(1/loan.yearfreq.id)-1
    period_tot = loan.loanyears*loan.yearfreq.id
    pay = -np.pmt(prate, period_tot, loan.loanamount)
    # cap = -np.ppmt(prate, period, period_tot, loanamount) slt pour pay fix
    cap = loan.loanamount/period_tot
    beg_balance = loan.loanamount
    end_balance = loan.loanamount
    rundate = loan.startdate

    while end_balance > 0.1:
        if rundate > date.today():
            debitflag = False
        else:
            debitflag = True

        # Recalculate the interest based on the current solde
        interest = prate * beg_balance
        # Determine payment based on whether or not this period will pay off the loan
        if loan.loantype.id in ("EMP_PAY", "EMP_STL"):
            pay = min(pay, beg_balance + interest)
            cap = pay - interest
        if loan.loantype.id == "EMP_CAP":
            cap = min(cap, beg_balance + interest)
            pay = cap + interest
        capcum = capcum + cap

        # Ensure additional payment gets adjusted if the loan is being paid off
        cap_add = min(cap_add, beg_balance - cap)
        end_balance = beg_balance - (cap + cap_add)
        try:
            LoanRefund.objects.create(
                loan=loan,
                period=period,
                payment=pay,
                capital=cap,
                capitalcum=capcum,
                interest=interest,
                balance=end_balance,
                debitflag=debitflag,
                debitdate=rundate,
                profile=user
                )
        except Exception as xcpt:
            if not first_except:
                first_except = True
                # cause = "money.utils_crud.amort_pay_fix for loan_id {0}".format(loan.id)
                # msg = build_error_message(cause, e, user)
                LOG.error(xcpt, extra={'user': user})
                return False
        # Build list to insert in Expense after
        # if debitflag:
        #     myid_list.append(rfd.id)

        # Increment the counter, solde and date
        period += 1
        rundate += relativedelta(months=loan.yearfreq.nbrmonth)
        beg_balance = end_balance
    return True


def delete_loanrfd_into_expensepay(loanrfd):
    """
    Delete data into Expense based on unticked elements of LoanRefund table
    """
    try:
        rfdids = loanrfd.values_list("id", flat=True)
        exp = Expense.objects.filter(loanrfd__id__in=rfdids)
        for e in exp:
            pay = ExpensePayment.objects.filter(expense=e)
            pay.delete()
            # Mise à jour dans Expense
            e.payamount = 0
            e.payflag = False
            e.save()

    except Exception as xcpt:
        LOG.error(xcpt, extra={'user': loanrfd.profile})
        return False

    return True


def get_or_create_supplier(obj, user, request=None):
    """
    obj can be Credit or Loan Instance
    Always before insert_loanrfd_into_expense
    loantype = expcat
    """
    supplier = None
    retval = True
    loantype = obj.loantype.group.name
    try:
        company = "{} - {}".format(loantype, obj.refshort)
        expcat = obj.loantype
        # m2m supplier-expcat creation
        if isinstance(obj, Loan):
            supplier, created = expcat.supplier_set.get_or_create(
                company=company,
                profile=user,
                loan=obj,
                classification='BQE'
            )
        if isinstance(obj, Credit):
            supplier, created = expcat.supplier_set.get_or_create(
                company=company,
                profile=user,
                credit=obj,
                classification='BQE'
            )
    except Exception as xcpt:
        retval = False
        LOG.error(xcpt, extra={'user': user})
        if request is not None:
            messages.warning(request,
                            _("Erreur lors de la création du fournisseur {}. Message envoyé à l'administrateur du site.".format(loantype)))

    return retval, supplier


def get_or_create_client(obj, user, request):
    """
    obj can be Credit or Loan Instance
    loantype = revcat
    """
    client = None
    retval = True
    loantype = obj.loantype.group.name
    try:
        company = "{} - {}".format(loantype, obj.refshort)
        if isinstance(obj, Loan):
            client, created = Client.objects.get_or_create(
                company=company,
                profile=user,
                loan=obj,
                clienttype='B2B',
                classification='BQE'
            )
        if isinstance(obj, Credit):
            client, created = Client.objects.get_or_create(
                company=company,
                profile=user,
                credit=obj,
                clienttype='B2B',
                classification='BQE'
            )
    except Exception as xcpt:
        retval = False
        LOG.error(xcpt, extra={'user': user})
        messages.warning(request,
                        _("Erreur lors de la création du client {}. Message envoyé à l'administrateur du site.".format(loantype)))

    return retval, client


def insert_rfd_into_expense(rfd, user, supplier, request=None):
    """
    Insert expense per loan refund
    """
    try:
        gen_atel = Atelier.objects.for_user(user).get(atelier="GEN")
        now_duedate = DueDateList.objects.get(id="NOW")
        novat = VatList.objects.get(id=0)
        if rfd.debitdate > date.today():
            payflag = False
            payamount = 0
            lastpaydate = None
        else:
            payflag = True
            payamount = rfd.payment
            rfd.debitflag = True
            lastpaydate = rfd.debitdate
            rfd.save()

        Expense.objects.create(
            profile=user,
            loan=rfd.loan,
            loanrfd=rfd,
            expnr='E_{0}_{1}'.format(rfd.loan.refshort, rfd.period),
            inputdate=rfd.debitdate,
            refdesc=rfd.loan.bankrefdesc,
            expcat=rfd.loan.loantype,
            atelier=gen_atel,
            supplier=supplier,
            amountvat=rfd.payment,
            vat=novat,
            amount=rfd.payment,
            payflag=payflag,
            duedatelist=now_duedate,
            payduedate=rfd.debitdate,
            payamount=payamount,
            lastpaydate=lastpaydate
            )
    except Exception as xcpt:
        LOG.error(xcpt, extra={'user': user})
        msg = _("Erreur lors de l'insertion des remboursements d'emprunt dans les dépenses.")
        if request is not None:
            messages.error(request, msg)
        return False
    return True


def insert_loanrfd_into_expense(loan, user, request=None):
    """
    Insert LoanRefund data into Expense
    Payment before today are considered as paid
    Payment after today are considered as unpaid
    """
    retval, supplier = get_or_create_supplier(loan, user, request)
    if not retval:
        return False
    else:
        loanrfd = LoanRefund.objects.filter(loan=loan).order_by('period')

        for rfd in loanrfd:
            try:
                Expense.objects.get(loanrfd=rfd)
            except Expense.DoesNotExist:
                insert_ok = insert_rfd_into_expense(rfd, user, supplier)
                if not insert_ok:
                #     continue
                # else:
                    return False
    return True


class ManageLoanRfdPayment(object):
    """
    Insert ExpensePayment for ticked LoanRefund data
    loan: Loan instance
    loanrfd: instance of LoanRefund to be inserted
    user: User instance
    payids: ticked debitflag in loanrfd
    """

    def __init__(self, user, loan, payids, request):
        self.user = user
        self.loan = loan
        self.request = request
        loanrfd = loan.loanrefund_set.all().order_by('period')
        self.loan_insert = loanrfd.filter(id__in=payids)
        self.loan_delete = loanrfd.exclude(id__in=payids)

    def process_payment(self):
        if self.delete_payment():
            if self.insert_payment():
                return self.validate_form_change()
            else:
                return False
        else:
            return False

    def delete_payment(self):
        """
        Récupération des Exp dont paiement doit être supprimé
        Boucle à travers Expense sur base des unticked loanrfd
        Exp => ExpPay
        """
        try:
            rfdids = self.loan_delete.values_list("id", flat=True)
            exp = Expense.objects.filter(loanrfd__id__in=rfdids)
            for e in exp:
                pay = ExpensePayment.objects.filter(expense=e)
                pay.delete()
                # Mise à jour dans Expense
                e.payamount = 0
                e.payflag = False
                e.save()

        except Exception as xcpt:
            LOG.error(xcpt, extra={'user': self.user})
            return False
        return True

    def insert_payment(self):
        """
        Boucle à travers loanrfd sur base des ticked loanrfd
        loanrfd => Exp => ExpPay
        """
        exp = None
        retval, supplier = get_or_create_supplier(self.loan, self.user, request=self.request)
        if not retval:
            return False

        self.rfdignore_id = list()
        for rfd in self.loan_insert:
            # 1. Check Expense is present
            try:
                exp = Expense.objects.get(loanrfd=rfd)
            except Expense.DoesNotExist:
                # msg = "Expense set associated to Loan {0} Does Not Exist for user id {1}. Run ./manage.py ins_exp {1} {0}".format(rfd.loan_id, self.user.id)
                # LOG.error(msg)
                insert_ok = insert_rfd_into_expense(rfd, self.user, supplier, request=self.request)
                if not insert_ok:
                    return False

            # 2. Insert payment only if not exist
            # if pay exist check if over or under paid
            try:
                ExpensePayment.objects.get(expense=exp)
                tot = (ExpensePayment.objects.filter(expense=exp)
                       .aggregate(tot=Sum("amount"))["tot"])
                # print(tot)
                if tot > exp.amountvat:
                    self.rfdignore_id.append(rfd.id)
                if tot < exp.amountvat:
                    ExpensePayment.objects.create(
                        expense=exp,
                        paydate=date.today(),
                        amount=rfd.payment,
                        profile=self.user)
                    # Mise à jour dans Expense
                    exp.payamount = rfd.payment + tot
                    solde = exp.amountvat - exp.payamount
                    if solde <= 0:
                        exp.payflag = True
                    else:
                        exp.payflag = False
                    exp.save()

            except ExpensePayment.DoesNotExist:
                ExpensePayment.objects.create(
                    expense=exp,
                    paydate=date.today(),
                    amount=rfd.payment,
                    profile=self.user)
                # Mise à jour dans Expense
                exp.payamount = rfd.payment
                solde = exp.amountvat - exp.payamount
                if solde <= 0:
                    exp.payflag = True
                else:
                    exp.payflag = False
                exp.save()

            except Exception as xcpt:
                LOG.error(xcpt, extra={'user': self.user})
                return False
        return True

    def validate_form_change(self):
        """
        Si tout est OK => sauver ce qui a été coché
        Sauf ceux dont le payement est partiel
        """
        try:
            for rfd in self.loan_insert:
                if rfd.id not in self.rfdignore_id:
                    rfd.debitflag = True
                    rfd.save()
            for rfd in self.loan_delete:
                rfd.debitflag = False
                rfd.save()
        except Exception as xcpt:
            LOG.error(xcpt, extra={'user': self.user})
            return False
        return True


def insert_fix_into_exprev(src, user, modeltxt="FixExpense", modif=False):
    """
    Inserts data into Expense or Revenue according to FixExpense or FixRevenue definition
    src is the Source Object from which to take the data: FixExpense or FixRevenue
    """
    try:
        first_except = False
        ctn = 0
        if modeltxt == "FixExpense" and modif:
            ctn = Expense.objects.for_user(user).filter(fixexpense=src).count()
        if modeltxt == "FixRevenue" and modif:
            ctn = Revenue.objects.for_user(user).filter(fixrevenue=src).count()

        rundate = src.begindate
        # duedate = rundate + relativedelta(months=1)
        while rundate < src.enddate:
            ctn += 1
            if rundate > date.today():
                payflag = False
                payamount = 0
                lastpaydate = None
            else:
                payflag = True
                payamount = src.amountvat
                lastpaydate = rundate

            try:
                if modeltxt == "FixExpense":
                    # print('{0}_{1}'.format(src.ref, ctn))
                    exp, created = Expense.objects.get_or_create(
                        profile=user,
                        fixexpense=src,
                        expnr='D_{0}_{1}'.format(src.refshort, ctn),
                        inputdate=rundate,
                        refdesc=src.refdesc,
                        expcat=src.expcat,
                        atelier=src.atelier,
                        supplier=src.supplier,
                        amountvat=src.amountvat,
                        vat=src.vat,
                        amount=src.amount,
                        payflag=payflag,
                        payduedate=rundate,
                        lastpaydate=lastpaydate,
                        payamount=payamount
                        )
                    # if created and payflag:
                    #     ExpensePayment.objects.create(
                    #         expense=exp,
                    #         paydate=rundate,
                    #         amount=src.amountvat,
                    #         profile=user
                    #     )
                if modeltxt == "FixRevenue":
                    inv, created = Revenue.objects.get_or_create(
                        profile=user,
                        fixrevenue=src,
                        invnr='R_{0}_{1}'.format(src.refshort, ctn),
                        inputdate=rundate,
                        refdesc=src.refdesc,
                        revcatgrp=src.revcatgrp,
                        revcat=src.revcat,
                        atelier=src.atelier,
                        client=src.client,
                        amountvat=src.amountvat,
                        vat=src.vat,
                        amount=src.amount,
                        payflag=payflag,
                        payduedate=rundate,
                        lastpaydate=lastpaydate,
                        payamount=payamount
                        )
                    # if created and payflag:
                    #     RevenuePayment.objects.create(
                    #         revenue=inv,
                    #         paydate=rundate,
                    #         amount=src.amountvat,
                    #         profile=user
                    #     )

                rundate += relativedelta(months=src.yearfreq.nbrmonth)
                # duedate = rundate + relativedelta(months=1)

            except Exception as xcpt:
                if not first_except:
                    first_except = True
                    LOG.error(xcpt, extra={'user': user})
                    return False

    except Exception as xcpt:
        LOG.error(xcpt, extra={'user': user})
        return False

    return True


def update_fix_into_exprev(src, user, modeltxt="FixExpense"):
    """
    Inserts data into Expense or Revenue according to FixExpense or FixRevenue definition
    src is the Source Object from which to take the data: FixExpense or FixRevenue
    """
    try:
        ret = False
        # check enddate
        if src.enddate > src.enddate_old:
            src.begindate = src.enddate_old
            ret = insert_fix_into_exprev(src, user, modeltxt, modif=True)
        elif src.enddate < src.enddate_old:
            if modeltxt == "FixExpense":
                del_rec = Expense.objects.filter(fixexpense=src, inputdate__gt=src.enddate)
            if modeltxt == "FixRevenue":
                del_rec = Revenue.objects.filter(fixrevenue=src, inputdate__gt=src.enddate)
            del_rec.delete()
            ret = True
    except Exception as xcpt:
        ret = False
        LOG.error("Error 1 {0}".format(xcpt))

    return ret


def compute_paytot_amount(parent_inst, child_inst, payfield="amount", datefield="paydate"):
    """
    parent_inst: instance of Expense or Revenue
    child_inst: instance of ExpensePayment or RevenuePayment related to parent
    Set payamount, lastpaydate and payflag in Expense table
    Payfield="amount" and Datefield="paydate" for ExpensePayments
    Payfield="amountvat" and Datefield="inputdate" for Expense comming from Invest
    """
    try:
        payed = child_inst.aggregate(total=Sum(payfield))["total"]
        if payed is None:
            payed = 0
        parent_inst.payamount = payed
        lastpaydate = child_inst.aggregate(maxdate=Max(datefield))["maxdate"]
        parent_inst.lastpaydate = lastpaydate
        # Get Solde
        solde = round(parent_inst.amountvat, 2) - round(parent_inst.payamount, 2)
        print("solde={}".format(solde))
        if solde <= 0.09:
            parent_inst.payflag = True
        else:
            parent_inst.payflag = False
        parent_inst.save()
        # LOG_DEBUG.debug("save_ok")
    except Exception as xcpt:
        LOG.error(xcpt, extra={'user': parent_inst.profile})
        return False, xcpt
    return True, parent_inst.payflag


def insert_producttot_into_revenue(inst):
    try:
        revprod = RevenueProduct.objects.filter(revenue=inst)
        prodvat = revprod.values('vat').annotate(
            amounttot=Sum('amounttot'),
            amounttotvat=Sum('amounttotvat'),
        )
        totHTVA = 0
        totTVAC = 0
        # Sum up per VAT group (6, 12, 21%)
        for pv in prodvat:
            totHTVA = totHTVA + pv['amounttot']
            totTVAC = totTVAC + pv['amounttotvat']          
        inst.amountvat = round(totTVAC, 2)
        inst.amount = round(totHTVA, 2)
        valTVA = totTVAC - totHTVA
        inst.vatvalue = round(valTVA, 2)
        inst.save()
    except Exception as xcpt:
        LOG.error(xcpt, extra={'user': inst.profile})
        return False
    return True


def insert_credit_into_revenue(creditrfd, user):
    """
    Insert credit amount into Revenue where creditrfdCreate
    """
    credit = creditrfd.credit
    gen_atel = Atelier.objects.for_user(user).get(atelier="GEN")
    bqe_cli = creditrfd.credit.client
    novat = VatList.objects.get(id=0)
    now_duedate = DueDateList.objects.get(id="NOW")
    # get same instance into revenue!
    try:
        credittype = RevCat.objects.get(id=credit.loantype.id)
    except RevCat.DoesNotExist:
        LOG.error("Vérifier présence id = '{}' dans RevCat.".format(credit.loantype.id))
        return False

    amount = creditrfd.withdrawamount
    credit_date = creditrfd.withdrawdate
    if credit_date > date.today():
        payflag1 = False
    else:
        payflag1 = True

    revnr = 'C_{0}_{1}'.format(credit.refshort, creditrfd.id)

    try:
        Revenue.objects.create(
            profile=user,
            creditrfd=creditrfd,
            invnr=revnr,
            inputdate=credit_date,
            refdesc=credit.bankrefdesc,
            revcatgrp=credittype.group,
            revcat=credittype,
            client=bqe_cli,
            atelier=gen_atel,
            amountvat=amount,
            vat=novat,
            amount=amount,
            payflag=payflag1,
            payamount=amount,
            payduedate=credit_date,
            duedatelist=now_duedate
            )
    except Exception as xcpt:
        LOG.error(xcpt, extra={'user': user})
        return False
    return True


def insert_crecaisse_into_expense(creditrfd, user):
    """
    Insert credit amount into Expense, when creditrfdUpdate
    """
    credit = creditrfd.credit
    gen_atel = Atelier.objects.for_user(user).get(atelier="GEN")
    bqe_sup = creditrfd.credit.supplier
    novat = VatList.objects.get(id=0)
    now_duedate = DueDateList.objects.get(id="NOW")
    if credit.loantype.id == "CRE_CAI":
        amount = creditrfd.withdrawamount + creditrfd.compute_interest()
        credit_date = creditrfd.refunddate
        if credit_date > date.today():
            payflag = False
        else:
            payflag = True
    # if credit.loantype.id == "CRE_STR":
    #     amount = credit.loanrefund
    #     credit_date = credit.closedate
    expnr = 'C_{0}_{1}'.format(credit.refshort, creditrfd.id)

    try:
        # This case should not happen!
        exp = Expense.objects.get(profile=user, creditrfd=creditrfd, expnr=expnr)
        exp.inputdate = credit_date
        exp.payduedate = credit_date
        exp.save()
        exppay = exp.expensepayment_set.all()
        for i in exppay:
            i.paydate = credit_date
            i.save()
    except Expense.DoesNotExist:
        try:
            Expense.objects.create(
                profile=user,
                creditrfd=creditrfd,
                expnr=expnr,
                inputdate=credit_date,
                refdesc=credit.bankrefdesc,
                expcat=credit.loantype,
                atelier=gen_atel,
                supplier=bqe_sup,
                amountvat=amount,
                vat=novat,
                amount=amount,
                payflag=payflag,
                duedatelist=now_duedate,
                payduedate=credit_date
                )
        except Exception as xcpt:
            LOG.error(xcpt, extra={'user': user})
            return False
    return True


def insert_strloan_into_expense(credit, supplier, user):
    """
    Insert credit amount into Expense, when creditrfdUpdate
    """
    gen_atel = Atelier.objects.for_user(user).get(atelier="GEN")
    novat = VatList.objects.get(id=0)
    now_duedate = DueDateList.objects.get(id="NOW")
    expnr = 'C_{0}_{1}'.format(credit.refshort, credit.id)

    try:
        # in case of update in Credit (Straight Loan)
        exp = Expense.objects.get(profile=user, expnr=expnr)
        exp.inputdate = credit.closedate
        # exp.payduedate = credit.closedate
        exp.amountvat = credit.loanrefund
        exp.amount = credit.loanrefund
        exp.save()
        # exppay = exp.expensepayment_set.all()
        # for i in exppay:
        #     i.paydate = credit.closedate
        #     i.amount = credit.loanrefund
        #     i.save()
    except Expense.DoesNotExist:
        try:
            Expense.objects.create(
                profile=user,
                expnr=expnr,
                inputdate=credit.closedate,
                refdesc=credit.bankrefdesc,
                expcat=credit.loantype,
                atelier=gen_atel,
                supplier=supplier,
                amountvat=credit.loanrefund,
                vat=novat,
                amount=credit.loanrefund,
                payflag=False,
                duedatelist=now_duedate,
                payduedate=credit.closedate
                )
        except Exception as xcpt:
            LOG.error(xcpt, extra={'user': user})
            return False
    return True


def upd_expcat_when_supplier_categ_change(supplier):
    """
    Specific funtion to update expcat in Expense when supplier categ has changed
    BUT this must be done by the user if:
    1. Supplier is associated to more than one categ (new values)
    2. Expense is associated to more than one categ (old values)
    return charok and msg
    char=Success, Warning, Error
    msg
    list=categ associated to Supplier or Expense
    """
    categ = supplier.expcat.all()
    if categ.count() > 1:
        expcat = categ.values_list("name", flat=True)
        expcat_list = list(expcat)
        msg = "Plus de 2 catégories {} associées au fournisseur {}".format(expcat_list, supplier.company)
        LOG.warning(msg, extra={'user': supplier.profile})
        return "WARNING", msg
    else:
        categ_inst = categ.first()
        # check if more than 1 categ for 1 supplier
        exp = Expense.objects.filter(supplier=supplier)
        expcat = exp.values_list("expcat__name", flat=True)
        if expcat.count() > 1:
            expcat_list = list(expcat)
            expnr_list = list(exp.values_list("expnr", flat=True))
            msg = "Plus de 2 catégories {} associées pour les dépenses {} du fournisseur {}".format(expcat_list, expnr_list, supplier.company)
            LOG.warning(msg, extra={'user': supplier.profile})
            return "WARNING", msg
        # check if categ is different in Expense
        else:
            try:
                for e in exp:
                    if e.expcat.id != categ_inst.id:
                        LOG.warning('Update {} to {} for user {}'.format(e.expcat, categ_inst, supplier.profile),
                            extra={'user': supplier.profile})
                        e.expcat = categ_inst
                        e.save()
                    else:
                        print('keep')
            except Exception as xcpt:
                LOG.error(xcpt, extra={'user': supplier.profile})
                return "ERROR", xcpt
    return "SUCCESS", "OK"
