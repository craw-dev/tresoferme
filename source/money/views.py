# Import http://django-braces.readthedocs.io/en/latest/
"""
Comme Revenue possède aussi bien prod et pay en détail + lock des bon de commandes,
on est passé par des vues spécifiques: RevenueCreateView, Detail, Update, Delete, Lock
Par contre pour Expense on est resté avec les vues génériques My... CRUD
MAIS avec conditions sur modeltxt pour MyDetailView et MyDeleteView
"""
import logging, math, json, re
import pandas as pd

from datetime import date
from urllib.parse import urlencode

from braces.views import LoginRequiredMixin

from django.views import generic
from django.db.models import Case, When, Q, F, Max, Sum, DateField
# from django.db.models import Case, CharField, Value, When
from django.db.models.functions import Coalesce, ExtractDay
from django.http.response import HttpResponseForbidden
from django.db.models.functions import Extract
from django.db import IntegrityError
from django.http import HttpResponse, JsonResponse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse, reverse_lazy, resolve
from django.utils.translation import ugettext as _

from farm.models import Atelier
from .models import (
    Client, Supplier, Product,
    Expense, Revenue, RevenueProduct, RevenuePayment, RevCat,
    Expense, ExpensePayment, ExpCat,
    FixExpense, FixRevenue,
    Loan, LoanRefund, MonthlyPrevision, Credit, CreditRefund
)
from .forms import (
    ClientForm, SupplierForm, ProductForm,
    ExpenseForm, RevenueForm, PaymentForm,
    ProductFormSet, PaymentRevenueFormSet, PaymentExpenseFormSet,
    FixRevenueForm, FixExpenseForm,
    LoanForm, LoanRefundForm, MonthlyPrevisionFormSet,
    CreditForm, CreditRefundForm
)

from .formsearch import (
    SupplierSearch, ClientSearch, ProductSearch,
    ExpenseSearch, RevenueSearch, GraphParamForm, GraphExpCatGrp
)

from .utils_crud import (insert_fix_into_exprev,
                         update_fix_into_exprev,
                         insert_producttot_into_revenue,
                         compute_paytot_amount, amortize,
                         insert_loanrfd_into_expense,
                         ManageLoanRfdPayment,
                         insert_loanamount_into_revenue,
                         insert_closeindemnity_into_expense,
                         get_or_create_supplier,
                         get_or_create_client,
                         insert_credit_into_revenue,
                         insert_crecaisse_into_expense,
                         insert_strloan_into_expense,
                         upd_expcat_when_supplier_categ_change
                        )

from . import graph_collect, graph_display, utils_display
from .widgets import ProductAttributesSelect
from .utils_tools import SaveAsXLS, set_prevlast_year
from easy_pdf.rendering import render_to_pdf_response
from config.settings import MEDIA_ROOT, PROJECT_NAME

LOG = logging.getLogger(__name__)


class MessageMixin():
    """
    Message management
    !!!url MUST be defined with add, upd and del keywords!!!
    """
    @property
    def build_message(self):
        """Build message"""
        action = ""
        gender = ""
        if "pk" in self.kwargs:
            self.pk = self.kwargs.get("pk")
        else:
            self.pk = ""
        if "upd" in self.request.path:
            action = "modifié"
        if "add" in self.request.path:
            action = "ajouté"
        if "del" in self.request.path:
            action = "supprimé"
        if "lock" in self.request.path:
            action = "facturé"
        if self.model.__name__ in ("Expense", "Revenue", "FixExpense", "FixRevenue"):
            gender = "e"
            if action == "facturé":
                gender = ""
        else:
            gender = ""

        if self.pk == "":
            if hasattr(self, 'obj'):
                who = self.obj.get_title()
                if hasattr(self.obj, 'revcat'):
                    who = self.obj.revcat.name
                    categ = self.obj.revcat.id
                    if categ in ("FRM_BDC", "FRM_DEV"):
                        gender = ""
            else:
                who = self.model._meta.verbose_name
        else:
            # inst = self.model.objects.get(pk=self.pk)
            who = self.get_object().get_title()
        msg = "{0} {1}{2} !".format(who, action, gender)
        # msg = "{0} {1} {2}{3} !".format(self.model._meta.verbose_name,
        #                                 self.pk, action, gender)
        return msg

    def form_valid(self, form):
        """display message when form is valid"""
        messages.info(self.request, self.build_message)
        return super(MessageMixin, self).form_valid(form)

    def delete(self, request, *args, **kwargs):
        """MessageMixin hooks to form_valid which is not present on DeleteView
        => Use of delete method to display success msg"""
        messages.info(self.request, self.build_message)
        return super(MessageMixin, self).delete(request, *args, **kwargs)


class MyLoginRequiredMixin(LoginRequiredMixin):
    """
    set myuser as instance! & check accounts fields have been filled-in
    myuserpk MUST BE in dispatch => so that self.myuser is present on GET & POST
    """
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission(request)

        if "myuserpk" in request.session:
            usermodel = get_user_model()
            userpk = request.session["myuserpk"]
            self.myuser = usermodel.objects.get(id=userpk)
        else:
            self.myuser = request.user
            request.session["myuserpk"] = request.user.id

        if "year1" not in request.session:
            set_prevlast_year(request, self.myuser)

        return super(MyLoginRequiredMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["upd_allowed"] = False
        context["myuser"] = self.myuser
        if self.request.user.is_manager and self.myuser.support_structure_access.id == "FULL":
            context["upd_allowed"] = True
        if self.request.user == self.myuser:
            context["upd_allowed"] = True
        if "ajd" not in context:
            context["ajd"] = date.today().strftime('%Y-%m-%d')
        return context


def my_home(request):
    if request.user.is_authenticated:
        return redirect('money_index')
    else:
        return render(request, 'index.html')


class MoneyIndex(MyLoginRequiredMixin, generic.TemplateView):
    template_name = "money_index.html"
    title = _("Bienvenue sur TresoFerme.")
    success_url = "/"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        unpaid_invest = utils_display.get_upaid_invest(self.myuser)
        soldepay_tot = utils_display.get_soldepay_tot(self.myuser)
        unpaid_loan = utils_display.get_unpaid_loan(self.myuser)
        unpaid_loancaisse = utils_display.get_unpaid_creditcaisse(self.myuser)
        unpaid_supplier = utils_display.get_unpaid_supplier(self.myuser)
        unpaid_client = utils_display.get_unpaid_client(self.myuser)
        solde_final = soldepay_tot + unpaid_loan + unpaid_loancaisse + unpaid_supplier + unpaid_client
        ctx = {
            "title": self.title,
            "soldepay_tot": soldepay_tot,
            "unpaid_invest": unpaid_invest,
            "unpaid_loan": unpaid_loan,
            "unpaid_loancaisse": unpaid_loancaisse,
            "unpaid_supplier": unpaid_supplier,
            "unpaid_client": unpaid_client,
            "solde_final": solde_final
        }
        inst = utils_display.LoanDisplay(self.myuser)
        context.update(inst.get_context())
        context.update(ctx)
        return context


@login_required
def treso_prev_index(request):
    userpk = request.session["myuserpk"]
    usermodel = get_user_model()
    myuser = get_object_or_404(usermodel, pk=userpk)

    qs = MonthlyPrevision.objects.filter(profile=myuser)
    if qs:
        return redirect("treso_prev_list")
    else:
        return redirect("treso_prev_select")


@login_required
def treso_prev_delete(request):
    userpk = request.session["myuserpk"]
    usermodel = get_user_model()
    myuser = get_object_or_404(usermodel, pk=userpk)
    try:
        qs = MonthlyPrevision.objects.filter(profile=myuser)
        qs.delete()
    except Exception as xcpt:
        LOG.error(xcpt, extra={'user': myuser})
        return render(request, "500.html", {"error_msg": xcpt})

    messages.success(request, "Année prévisionnelle supprimée.")
    return redirect("treso_prev_select")


@login_required
def treso_prev_select(request):
    userpk = request.session["myuserpk"]
    usermodel = get_user_model()
    myuser = get_object_or_404(usermodel, pk=userpk)
    year1 = date.today().year

    if request.method == "GET":
        form = GraphParamForm(request=request)
        if request.GET.get("year1") is not None:
            year1 = int(request.GET.get("year1"))
        gt = graph_collect.CollectTreso(myuser, year1)
        df = gt.collect_data()
        if df.empty:
            df_rec_str = ""
            dfhtml = "<p>Aucune dépense ou recette encodé.</p>"
            graph1 = None
        else:
            df_records = df.to_dict('records')
            df_rec_str = json.dumps(df_records)
            # df formating
            df = df[["month", "rev", "exp", "solde"]]
            graph1 = graph_display.build_treso_select(df, year1)

            df.columns = ("Mois", "Entrée(€)", "Sortie(€)", "Solde(€)")
            dfhtml = graph_display.format2html(df)

        context = {
            'df': dfhtml,
            'form': form,
            'graph1': graph1,
            'df_records': df_rec_str,
            'myuser': myuser
                    }

    if request.method == "POST":
        try:
            df_rec_str = request.POST.get("df_records")
            df_records = json.loads(df_rec_str)
            # rm existing lines
            mp = MonthlyPrevision.objects.filter(profile=myuser)
            mp.delete()
            model_instances = [MonthlyPrevision(
                    profile=myuser,
                    exp=record['exp'],
                    rev=record['rev'],
                    solde=record['solde'],
                    month=record['month']
                ) for record in df_records]
            MonthlyPrevision.objects.bulk_create(model_instances)
        except Exception as xcpt:
            # cause = "delete or bulk_create in view.treso_prev_select"
            # msg = build_error_message(cause, xcpt, myuser)
            LOG.error(xcpt, extra={'user': myuser})
            return render(request, "500.html", {"error_msg": xcpt})

        return redirect("treso_prev_list")

    return render(request, 'treso/treso_prev_select.html', context)


@login_required
def treso_prev_list(request):
    userpk = request.session["myuserpk"]
    usermodel = get_user_model()
    myuser = get_object_or_404(usermodel, pk=userpk)
    qs = MonthlyPrevision.objects.filter(profile=myuser).order_by("month")
    qs_select = qs.values("month", "exp", "rev", "solde")
    df = pd.DataFrame(list(qs_select))
    graph1 = graph_display.build_treso_select(df, 0)

    context = {
        'mp_list': qs,
        'graph1': graph1
                }

    return render(request, 'treso/treso_prev_list.html', context)


@login_required
def treso_prev_update(request):
    userpk = request.session["myuserpk"]
    usermodel = get_user_model()
    myuser = get_object_or_404(usermodel, pk=userpk)
    data = request.POST or None
    formset = MonthlyPrevisionFormSet(
        data=data, queryset=MonthlyPrevision.objects.filter(profile=myuser)
    )

    if request.method == "POST":
        if formset.is_valid():
            for form in formset:
                form.save()
            messages.success(request, _("Année prévisionnelle modifiée."), fail_silently=True)
            return redirect("treso_prev_list")

    return render(request, "treso/treso_prev_update.html", {'formset': formset})


@login_required
def treso_prev_graph(request):
    userpk = request.session["myuserpk"]
    usermodel = get_user_model()
    myuser = get_object_or_404(usermodel, pk=userpk)
    year1 = date.today().year

    qs = MonthlyPrevision.objects.filter(profile=myuser).order_by("month")
    if qs:
        qs_select = qs.values("month", "exp", "rev", "solde")
        df_prev = pd.DataFrame(list(qs_select))
        df_prev["soldecum"] = df_prev["solde"].cumsum(axis=0)
        df_prev["revcum"] = df_prev["rev"].cumsum(axis=0)
        df_prev["expcum"] = df_prev["exp"].cumsum(axis=0)
        # df_prev = graph.compute_cumsum(df_prev)
        prev_missing = False
    else:
        df_prev = pd.DataFrame({'month': range(1, 13), 'exp': 0, 'rev': 0, 'solde': 0,
                                                       'expcum': 0, 'revcum': 0, 'soldecum': 0})
        prev_missing = True
        # messages.info(request, _("Année prévisionnelle manquante. Rendez-vous dans le menu 'Trésorerie/prévision'\
        #     pour créer/modifier une année prévisionnelle."))

    if request.method == "GET":
        form = GraphParamForm(request=request)
        if request.GET.get("year1") is not None:
            year1 = int(request.GET.get("year1"))
        gt = graph_collect.CollectTreso(myuser, year1)
        df_obs = gt.collect_data()

        graph_solde = graph_display.build_treso(df_obs, df_prev, year1, "solde")
        graph_rev = graph_display.build_treso(df_obs, df_prev, year1, "rev")
        graph_exp = graph_display.build_treso(df_obs, df_prev, year1, "exp")

    context = {
        'year': str(year1),
        'form': form,
        'graph_solde': graph_solde,
        'graph_rev': graph_rev,
        'graph_exp': graph_exp,
        'myuser': myuser,
        'prev_missing': prev_missing
        }

    return render(request, 'treso/treso_graph.html', context)


class GraphIndexTreso(MyLoginRequiredMixin, generic.TemplateView):
    """
    Affichage graphique trésorerie
    """
    what = None
    template_name = "treso/treso_month.html"
    year1 = None
    year2 = None
    graph_month_context = {}
    form = None

    def __init__(self, what, template_name):
        super().__init__()
        self.template_name = template_name
        self.what = what

    def get(self, request, *args, **kwargs):
        self.form = GraphParamForm(request=request)

        year1 = request.GET.get("year1")
        if year1 == '' or year1 is None:
            set_prevlast_year(request, self.myuser)
            self.year1 = int(request.session["year1"])
        else:
            self.year1 = request.GET.get("year1")
            request.session["year1"] = str(self.year1)

        year2 = request.GET.get("year2")
        if year2 == '' or year2 is None:
            set_prevlast_year(request, self.myuser)
            self.year2 = int(request.session["year2"])
        else:
            self.year2 = request.GET.get("year2")
            request.session["year2"] = str(self.year2)

        self.year1 = int(self.year1)
        self.year2 = int(self.year2)

        gt1 = graph_collect.CollectTreso(self.myuser, self.year1)
        dfe1 = gt1.get_data(Expense)
        dfr1 = gt1.get_data(Revenue)

        gt2 = graph_collect.CollectTreso(self.myuser, self.year2)
        dfe2 = gt2.get_data(Expense)
        dfr2 = gt2.get_data(Revenue)

        self.graph_month_context = graph_collect.create_month_context(
            dfe1, dfr1, self.year1,
            dfe2, dfr2, self.year2,
            "yvat_e", "yvat_r", "Trésorerie")

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = self.form
        context.update(self.graph_month_context)
        return context


class GraphIndexRenta(MyLoginRequiredMixin, generic.TemplateView):
    """
    Affichage graphique rentabilité
    """
    # init vars
    what = "NADA"
    template_name = "renta/renta_group.html"
    # working vars
    title = _("Rentabilité")
    subtitle = _("(montant FACTURÉ HTVA)")
    # context vars
    year1 = None
    year2 = None
    graph_group_context = {}
    graph_month_context = {}
    form = None

    def __init__(self, what, template_name):
        super().__init__()
        self.template_name = template_name
        self.what = what

    def get(self, request, *args, **kwargs):
        self.form = GraphParamForm(request=request)
        dfe1 = pd.DataFrame
        dfe2 = pd.DataFrame
        dfr1 = pd.DataFrame
        dfr2 = pd.DataFrame

        self.year1 = request.GET.get("year1", None)
        if self.year1 is None:
            set_prevlast_year(request, self.myuser)
            self.year1 = int(request.session["year1"])
        else:
            request.session["year1"] = str(self.year1)
            self.year1 = int(self.year1)

        self.year2 = request.GET.get("year2", None)
        if self.year2 is None:
            set_prevlast_year(request, self.myuser)
            self.year2 = int(request.session["year2"])
        else:
            request.session["year2"] = str(self.year2)
            self.year2 = int(self.year2)

        ggen1 = graph_collect.CollectRenta(self.myuser, self.year1)
        dfe1 = ggen1.get_data("Expense")
        dfr1 = ggen1.get_data("Revenue")

        ggen2 = graph_collect.CollectRenta(self.myuser, self.year2)
        dfe2 = ggen2.get_data("Expense")
        dfr2 = ggen2.get_data("Revenue")

        if self.what == "MONTH":
            self.graph_month_context = graph_collect.create_month_context(
                dfe1, dfr1, self.year1,
                dfe2, dfr2, self.year2,
                "y_e", "y_r", "Rentabilité")

        if self.what == "REV_ATELIER":
            df1 = graph_collect.get_product(self.myuser, self.year1)
            df2 = graph_collect.get_product(self.myuser, self.year2)
            self.graph_group_context = graph_collect.create_group_context(
                df1, df2, self.year1, self.year2,
                "atelier_id", "atelier",
                "#21bf6b", "atelier", "Produit", "graph-form-RD",
                None, None)

        if self.what == "EXP_ATELIER":
            self.graph_group_context = graph_collect.create_group_context(
                dfe1, dfe2, self.year1, self.year2,
                "atelier__atelier__id", "atelier__atelier__name",
                "#f46036", "atelier", "Charge", "graph-form-RD",
                None, None)

        if self.what == "EXP_FIX_VARIABLE":
            self.graph_group_context = graph_collect.create_group_context(
                dfe1, dfe2, self.year1, self.year2,
                "expcat__categclass__id", "expcat__categclass__name",
                "#f46036", "frais fixe/variable", "Charge", "graph-form-RD",
                "expcat__name", "categorie")

        if self.what == "CLIENT_PRODUIT":
            df1 = graph_collect.get_product(self.myuser, self.year1)
            df2 = graph_collect.get_product(self.myuser, self.year2)
            self.graph_group_context = graph_collect.create_group_context(
                df1, df2, self.year1, self.year2,
                "client_id", "client",
                "#21bf6b", "client", "Produit", "graph-form-RD",
                "product", "produit")

        if self.what == "SUPPLIER":
            self.graph_group_context = graph_collect.create_group_context(
                dfe1, dfe2, self.year1, self.year2,
                "supplier__id", "supplier__company",
                "#f46036", "fournisseur", "Charge", "graph-form-RD",
                None, None)

        if self.what == "REV_GROUP_CATEG":
            self.graph_group_context = graph_collect.create_group_context(
                dfr1, dfr2, self.year1, self.year2,
                "revcat__group__id", "revcatgrp__name",
                "#21bf6b", "groupe", "Produit", "graph-form-RD",
                "revcat__name", "categorie")

        if self.what == "EXP_GROUP_CATEG":
            self.graph_group_context = graph_collect.create_group_context(
                dfe1, dfe2, self.year1, self.year2,
                "expcat__group__id", "expcat__group__name",
                "#f46036", "groupe", "Charge", "graph-form-RD",
                "expcat__name", "categorie")

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = self.form
        context.update(self.graph_month_context)
        context.update(self.graph_group_context)
        return context


class MyDetailView(MyLoginRequiredMixin, generic.DetailView):
    def __init__(self, *args, **kwargs):
        super(MyDetailView, self).__init__(*args, **kwargs)
        self.modeltxt = self.model
        self.model = eval(self.modeltxt)

    def get_context_data(self, **kwargs):
        context = super(MyDetailView, self).get_context_data(**kwargs)
        if self.modeltxt == "Expense":
            context["pay"] = ExpensePayment.objects.filter(expense=self.object)
        if self.modeltxt == "Credit":
            context["withdraw"] = CreditRefund.objects.filter(credit=self.object).order_by("-withdrawdate")
        return context


class MyCreateView(MyLoginRequiredMixin, MessageMixin, generic.CreateView):
    """
    Generic Create View
    obj is used for generic message display (MessageMixin) when create
    """
    obj = None

    def __init__(self, *args, **kwargs):
        super(MyCreateView, self).__init__(*args, **kwargs)
        self.modeltxt = self.model
        self.model = eval(self.modeltxt)
        self.model_verbosename = self.model._meta.verbose_name
        self.template_name = "base_money_form.html"
        self.form_class = eval("{0}Form".format(self.modeltxt))

    def get_form_kwargs(self):
        """used to change queryset in expense form"""
        kwargs = super(MyCreateView, self).get_form_kwargs()
        kwargs["connected_user"] = self.myuser
        kwargs["operation"] = "CREATE"
        if self.modeltxt in ("FixRevenue", "FixExpense"):
            kwargs["modeltxt_kwarg"] = self.modeltxt
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["success_url"] = reverse("{0}_list".format(self.modeltxt.lower()))
        context["list_path"] = "{0}_list".format(self.modeltxt.lower())
        context["title"] = "{0} > {1}".format(self.model._meta.verbose_name, _("Nouveau"))
        context["form_html_id"] = "{0}_form".format(self.modeltxt.lower())
        if self.modeltxt in ("Expense", "Revenue", "Product",
                             "FixExpense", "FixRevenue", "Loan",
                             "Supplier", "Credit"):
            context["form_js"] = "{0}_form.js".format(self.modeltxt.lower())
        # check for invest_list
        current_url = resolve(self.request.path_info).url_name
        if "Expense" in self.modeltxt and current_url == "invest_create":
            context["success_url"] = reverse("invest_list")
            context["list_path"] = "invest_list"
            context["title"] = "Investissement > Nouveau"
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.profile_id = self.request.session["myuserpk"]
        obj.save()
        self.obj = obj # for msg display when create
        if self.modeltxt in ("FixRevenue", "FixExpense"):
            messages.success(
                self.request,
                _("Cliquer sur 'Valider' pour insérer les données modifiées dans 'Entrée/Sortie'."),
                fail_silently=True,
            )
        if self.modeltxt == "Expense" and not obj.payflag:
            messages.success(
                self.request,
                _("Cliquer sur 'Gérer les payements' pour ajouter des payements."),
                fail_silently=True,
            )
        if self.modeltxt == "Loan":
            messages.info(
                self.request,
                _("N’oubliez pas de générer le tableau d’amortissement (voir ci-dessous)."),
                fail_silently=True,
            )
        if self.modeltxt == "Credit":
            # When Credit is created Insert Supplier & Client
            retval, supplier = get_or_create_supplier(obj, self.myuser, self.request)
            retval, client = get_or_create_client(obj, self.myuser, self.request)
            # Straight Loan => insert Expense full amount when finished
            if obj.loanrefund is not None and obj.closedate is not None:
                retval2 = insert_strloan_into_expense(obj, supplier, self.myuser)
                if not retval2:
                    msg = _("Erreur lors de la création de la dépense associée au crédit.")
                    return render(self.request, "500.html", {"error_msg": msg})

        return super(MyCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        try:
            return(super().post(request, *args, **kwargs))
        except IntegrityError as e:
            messages.error(request, _('{0}: {1}').format(self.model_verbosename, e))
            return render(request, template_name=self.template_name, context=self.get_context_data())


class MyUpdateView(MyLoginRequiredMixin, MessageMixin, generic.UpdateView):
    def __init__(self, *args, **kwargs):
        super(MyUpdateView, self).__init__(*args, **kwargs)
        self.modeltxt = self.model
        self.model = eval(self.modeltxt)
        self.template_name = "base_money_form.html"
        self.form_class = eval("{0}Form".format(self.modeltxt))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = None
        context["success_url"] = self.get_object().get_absolute_url()
        title = self.get_object().get_title()
        context["title"] = "{0} > {1}".format(title, _("Modifier"))
        context["list_path"] = "{0}_list".format(self.modeltxt.lower())
        context["form_html_id"] = "{0}_form".format(self.modeltxt.lower())
        if self.modeltxt in ("Expense", "Revenue", "Product", "FixExpense", "FixRevenue", "Loan", "Supplier", "Credit"):
            context["form_js"] = "{0}_form.js".format(self.modeltxt.lower())
        return context

    def get_form_kwargs(self):
        """used to change queryset in expense form"""
        kwargs = super(MyUpdateView, self).get_form_kwargs()
        kwargs["connected_user"] = self.myuser
        kwargs["operation"] = "UPDATE"
        return kwargs

    def form_valid(self, form):
        if self.modeltxt in ("FixRevenue", "FixExpense"):
            messages.success(
                self.request,
                _(
                    "Cliquer sur 'Valider' pour insérer/supprimer les données modifiées dans 'Entrée/Sortie'."
                ),
                fail_silently=True,
            )
        if self.modeltxt == "Credit":
            # obj = form.instance ceci garde les anciennes valeurs!
            obj = form.save(commit=True)
            # When Credit is created get Supplier
            retval, supplier = get_or_create_supplier(obj, self.myuser, self.request)
            if not retval:
                msg = _("Erreur lors de la création du fournisseur associée au crédit.")
                return render(self.request, "500.html", {"msg": msg})
            # Straight Loan => insert Expense full amount when finished
            if obj.loanrefund is not None and obj.closedate is not None:
                retval2 = insert_strloan_into_expense(obj, supplier, self.myuser)
                if not retval2:
                    msg = _("Erreur lors de la mise à jour de la dépense associée au crédit.")
                    return render(self.request, "500.html", {"msg": msg})
        if self.modeltxt == "Supplier":
            obj = form.save(commit=True)
            retstr, msg = upd_expcat_when_supplier_categ_change(obj)
            if retstr == "WARNING":
                messages.warning(self.request, msg)
                return render(self.request, "money/supplier_detail.html", {"object": obj, "warn_msg": True})
            if retstr == "ERROR":
                msg = _("Erreur lors de la mise à jour de la catégorie dans les dépenses.")
                user_txt = "Utilisateur: {}".format(self.myuser)
                supplier_txt = "Fournisseur: {}".format(obj)
                return render(self.request, "500.html", {"error_msg": msg,
                                                         "user_txt": user_txt,
                                                         "extra_txt": supplier_txt})
        return super(MyUpdateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        # When all saved and OK check upd Expense for Categ change in Supplier
        # if self.modeltxt == "Supplier":
        #     obj = self.form.instance
        #     LOG.debug(obj)
        try:
            return(super().post(request, *args, **kwargs))
        except IntegrityError as e:
            messages.error(request, _('{0}: {1}').format(self.model._meta.verbose_name, e))
            return render(request, template_name=self.template_name, context=self.get_context_data())


class MyDeleteView(MyLoginRequiredMixin, MessageMixin, generic.DeleteView):
    """Generic delete View"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.modeltxt = self.model
        self.model = eval(self.modeltxt)
        self.model_verbosename = self.model._meta.verbose_name
        # obligatoire sinon django pointe sur modelname_confirm_delete.html
        self.template_name = "money/{0}_detail.html".format(self.modeltxt.lower())
        self.success_url = reverse("{0}_list".format(self.modeltxt.lower()))
        self.confirm_message = _("Supprimer cet enregistrement?")


    def get_context_data(self, **kwargs):
        # Uniquement pour Expense
        context = super().get_context_data(**kwargs)
        if self.modeltxt == "Expense":
            context["pay"] = ExpensePayment.objects.filter(expense=self.object.id)
        return context

    def is_deletable(self, obj):
        """
        return a list of related objects if not deletable
        Inspired from https://gist.github.com/freewayz/69d1b8bcb3c225bea57bd70ee1e765f8
        """
        related_list = []
        # get all the related object
        for rel in obj._meta.get_fields():
            try:
                # check if there is a relationship with at least one related object
                related = rel.related_model.objects.filter(**{rel.field.name: obj})
                if related.exists():
                    related_list.append(related)
            except AttributeError:  # an attribute error for field occurs when checking for AutoField
                pass  # just pass as we dont need to check for AutoField
        return related_list

    def delete(self, request, *args, **kwargs):
        """
        Delete supplier associated to Loan or Credit
        Check related before delete and display warning message
        If DB Integrity Error, display error
        """
        obj = self.model.objects.get(pk=kwargs["pk"])
        # do not check related for Credit or Loan
        if self.modeltxt in ("Credit", "Loan"):
            # obj = self.model.objects.get(pk=kwargs["pk"])
            company = "{} {}".format(obj.loantype.group.name, obj.refshort)
            try:
                supplier = Supplier.objects.get(company=company, profile=obj.profile)
                supplier.delete()
            except Supplier.DoesNotExist:
                pass

        # check related before delete
        related_list = self.is_deletable(obj)
        if related_list:
            messages.error(request,
                _("Pour supprimer l'enregistrement '{0}', " +\
                "supprimez d'abord les enregistrements qui y font référence.").format(obj)
            )
            for related in related_list:
                related_model = related.model._meta.verbose_name
                related_values = [i.__str__() for i in related.all()]
                msg = "{0}: {1}".format(related_model, related_values)
                messages.warning(request, msg)
            return render(request, template_name=self.template_name, context={'object': obj})

        # delete() but display nice error if integrity error
        try:
            return(super().delete(request, *args, **kwargs))
        except IntegrityError:
            messages.error(request, _('Impossible de supprimer un enregistrement {0} ' +\
                                  'qui existe dans Entrée/Sortie.').format(self.model_verbosename)
            )
            return render(request, template_name=self.template_name, context=self.get_context_data())


@login_required
def expense_copy(request, pk):
    """
    Copy existing expense
    ExpensePayment is not copied => Absurde!
    """
    exp = Expense.objects.get(id=pk)
    expnr = "{0}-copie".format(exp.expnr)
    try:
        expcopy1 = Expense.objects.get(profile=exp.profile, expnr=expnr)
        expcopy1.delete()
    except Expense.DoesNotExist:
        pass

    expcopy = Expense.objects.create(
        profile=exp.profile,
        expnr=expnr,
        inputdate=date.today(),
        refdesc=exp.refdesc,
        expcat=exp.expcat,
        supplier=exp.supplier,
        atelier=exp.atelier,
        amountvat=exp.amountvat,
        vat=exp.vat,
        amount=exp.amount,
        payflag=exp.payflag,
        payduedate=exp.payduedate,
        duedatelist=exp.duedatelist
        )

    messages.success(request, _("Sortie dupliquée. Veillez à adapter l'identifiant unique et les champs voulu."), fail_silently=True)
    return redirect("expense_detail", expcopy.id)


class RevenueCreateView(MyLoginRequiredMixin, MessageMixin, generic.CreateView):
    model = Revenue
    form_class = RevenueForm
    template_name = "base_money_form.html"
    modeltxt = "Revenue"
    success_url = reverse_lazy("revenue_list")
    # objrev defined for spec msg in MessageMixin
    obj = None

    def __init__(self):
        self.model_verbosename = self.model._meta.verbose_name

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "{0} > {1}".format(self.model._meta.verbose_name, _("Nouvelle"))
        context["list_path"] = "revenue_list"
        context["success_url"] = reverse("revenue_list")
        context["form_js"] = "revenue_form.js"
        context["form_html_id"] = "revenue_form"
        return context

    def get_form_kwargs(self):
        """used to change queryset in expense form"""
        kwargs = super(RevenueCreateView, self).get_form_kwargs()
        kwargs["connected_user"] = self.myuser
        kwargs["operation"] = "CREATE"
        if "bdc" in self.request.path_info:
            kwargs["origin"] = "BDCDEVIS"
        return kwargs

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.profile_id = self.myuser.id
        obj.save()
        self.obj = obj
        self.success_url = self.model.get_absolute_url(obj)

        # !!! Message display for Product & Payment !!!
        if obj.revcat.id in ("FRM_BDC", "FRM_FAC", "FRM_DEV"):
            messages.success(self.request,
                _("Cliquer sur 'Gérer les produits' pour ajouter des produits à votre bon de commande."),
                fail_silently=True)
        else:
            if not obj.payflag:
                messages.success(self.request,
                    _("Cliquer sur 'Gérer les payements' pour ajouter des payements."),
                    fail_silently=True)
        return super(RevenueCreateView, self).form_valid(form)

    def post(self, request, *args, **kwargs):
        try:
            return(super().post(request, *args, **kwargs))
        except IntegrityError as e:
            # messages.error(request,
            #     _('Vous avez déjà enregistré un(e) {0} avec ce Nr/Nom. ' + \
            #     'Tous vos Nr/Nom de {0}s doivent être uniques.').format(self.model_verbosename)
            # )
            messages.error(request, _('{0}: {1}').format(self.model_verbosename, e))
            return render(request, template_name=self.template_name, context=self.get_context_data())


class RevenueAll(object):
    """Revenue specific def"""
    def dispatch(self, request, **kwargs):
        # Used in context
        self.revpay = RevenuePayment.objects.filter(revenue=kwargs["pk"])
        self.revprod = RevenueProduct.objects.filter(revenue=kwargs["pk"])
        self.revprodvat = self.revprod.values('vat__name').annotate(
            amounttot=Sum('amounttot'),
            amounttotvat=Sum('amounttotvat'),
            )
        return super().dispatch(request, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["pay"] = self.revpay
        context["prod"] = self.revprod
        context["prodvat"] = self.revprodvat
        ProductFormSet
        # check presence lotnr
        lotmax = self.revprod.aggregate(lot=Max("lot"))["lot"]
        if lotmax is None:
            context["lotpresent"] = False
        else:
            context["lotpresent"] = True
        return context


class RevenueDetailView(MyLoginRequiredMixin, RevenueAll, generic.DetailView):
    model = Revenue

    def get_context_data(self, **kwargs):
        self.context = super(RevenueDetailView, self).get_context_data(**kwargs)
        return self.context

    def get(self, request, *args, **kwargs):
        response = super(RevenueDetailView, self).get(request, *args, **kwargs)
        obj = self.get_object()
        if obj.revcat.id == "FRM_FAC":
            if self.myuser.company_logo is None or self.myuser.company_logo == '':
                self.missing_logo = True

        if 'pdf' in request.path:
            context = self.context
            company_logo = self.myuser.company_logo
            user_iban = self.myuser.iban
            if company_logo == '':
                context['path_supplier'] = None
            else:
                context['path_supplier'] = MEDIA_ROOT + '/' + str(context['myuser'].company_logo)
            context['path_client'] = MEDIA_ROOT + '/' + str(obj.client.profile.company_logo)
            context['vat_amount'] = obj.amountvat - obj.amount
            if self.myuser.invoice_comm is not None:
                context['invoice_comm'] = self.myuser.invoice_comm
            else:
                context['invoice_comm'] = "Toutes nos factures sont payables au comptant. \
                    Leur non-paiement total ou partiel entraîne de plein droit un intérêt de 5% par an. \
                    Toute réclamation doit être adressée par mail ou par courrier dans les trois jours ouvrables \
                    qui suivent la réception de la marchandise. \
                    En cas de litige, le tribunal de commerce de Nivelles sera seul compétent."
            context['revenue'].inputdate = obj.inputdate.strftime("%d/%m/%Y")
            context['revenue'].payduedate = obj.payduedate.strftime("%d/%m/%Y")
            return render_to_pdf_response(request, 'easy_pdf/invoice4print.html', self.context)
        else:
            return response


class RevenueUpdateView(MyLoginRequiredMixin, MessageMixin, RevenueAll, generic.UpdateView):
    model = Revenue
    form_class = RevenueForm
    template_name = "base_money_form.html"

    def get_form_kwargs(self):
        """used to change queryset or dropdown in form"""
        kwargs = super(RevenueUpdateView, self).get_form_kwargs()
        kwargs["connected_user"] = self.myuser
        kwargs["operation"] = "UPDATE"
        title = kwargs["instance"].get_title()
        self.title = "{0} > {1}".format(title, _("Modifier"))
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["success_url"] = self.get_object().get_absolute_url()
        context["list_path"] = "revenue_list"
        context["form_html_id"] = "revenue_form"
        context["form_js"] = "revenue_form.js"
        context["title"] = "Recette"
        return context


@login_required
def revenue_copy(request, pk):
    """
    Dupliquer un revenu existant
    """
    rev = Revenue.objects.get(id=pk)
    invnr = "{0}-copie".format(rev.invnr)
    try:
        revcopy1 = Revenue.objects.get(profile=rev.profile, invnr=invnr)
        revcopy1.delete()
    except Revenue.DoesNotExist:
        pass

    revcopy = Revenue.objects.create(
        profile=rev.profile,
        # loan=rev,
        invnr=invnr,
        inputdate=date.today(),
        refdesc=rev.refdesc,
        revcatgrp=rev.revcatgrp,
        revcat=rev.revcat,
        client=rev.client,
        atelier=rev.atelier,
        amountvat=rev.amountvat,
        vat=rev.vat,
        amount=rev.amount,
        payflag=rev.payflag,
        payamount=rev.payamount,
        payduedate=rev.payduedate,
        duedatelist=rev.duedatelist
        )

    revprod=rev.revenueproduct_set.all()
    for prod in revprod:
        RevenueProduct.objects.create(
            profile=rev.profile,
            revenue=revcopy,
            atelier=prod.atelier,
            product=prod.product,
            lot=prod.lot,
            dlc=prod.dlc,
            amountvat=prod.amountvat,
            vat=prod.vat,
            amount=prod.amount,
            quantity=prod.quantity,
            unit=prod.unit,
            amounttot=prod.amounttot,
            amounttotvat=prod.amounttotvat
        )

    # DO NOT COPY the Payment!!! ABSURDE
    # revpay=rev.revenuepayment_set.all()
    # for pay in revpay:
    #     RevenuePayment.objects.create(
    #         profile=rev.profile,
    #         revenue=revcopy,
    #         paydate=pay.paydate,
    #         amount=pay.amount
    #     )

    messages.success(request, _("Entrée dupliquée. Veillez à adapter l'identifiant unique et les champs voulu."), fail_silently=True)
    if rev.revcat.id in ("FRM_BDC", "FRM_DEV"):
        success_url = redirect("bdc_detail", revcopy.id)
    else:
        success_url = redirect("revenue_detail", revcopy.id)

    return success_url


class RevenueDeleteView(MyLoginRequiredMixin, MessageMixin, RevenueAll, generic.DeleteView):
    """Revenue Delete"""
    model = Revenue
    # obligatoire sinon django pointe sur modelname_confirm_delete.html
    template_name = "money/revenue_detail.html"
    confirm_message = _("Supprimer cet enregistrement?")

    def get_success_url(self):
        rev = self.get_object()
        maxyear = date.today().year
        if rev.revcat.id in ("FRM_BDC", "FRM_DEV"):
            base_url = reverse('bdc_list')
        else:
            base_url = reverse('revenue_list')
        url = '{0}?year={1}'.format(base_url, maxyear)
        return url


class RevenueLockView(
    MyLoginRequiredMixin, MessageMixin, RevenueAll, generic.UpdateView):
    # You don’t even need to provide a success_url for CreateView or UpdateView
    # they will use get_absolute_url() on the model object if available.
    model = Revenue
    fields = ["invnr", "locked"]
    template_name = "money/revenue_detail.html"
    confirm_message = _("Facturer ce bon de commande?")

    def form_valid(self, form):
        facture_categ = RevCat.objects.get(id="FRM_FAC")
        form.instance.locked = True
        form.instance.revcat = facture_categ
        # form.instance.inputdate = date.today()
        return super(RevenueLockView, self).form_valid(form)


class MoneySearchMixin(object):
    param = object

    def build_where(self):

        where = Q(pk__gt=0)
        # Redirect Loan link from Index page
        if self.modeltxt in ("Expense", "Revenue"):
            if self.param.get("invoice_send"):
                invoice_send = self.param.get("invoice_send", "NADA")
                if invoice_send == "OUI":
                    where &= (Q(invoice_send=True))
                if invoice_send == "NON":
                    where &= (Q(invoice_send=False))
            if self.param.get("minimi"):
                minimi = self.param.get("minimi", "NADA")
                if minimi == "OUI":
                    where &= (Q(aide_minimi=True))
                if minimi == "NON":
                    where &= (Q(aide_minimi=False))
            if self.param.get("payflag"):
                payflag = self.param.get("payflag", "NADA")
                if payflag == "Paid":
                    where &= (Q(payflag=True))
                if payflag == "ToPay":
                    where &= (Q(payflag=False))
            if self.param.get("payduedate"):
                where &= (Q(payduedate__lt=self.param.get("payduedate", "")))

        if self.param.get("expnr"):
            where &= (Q(expnr__icontains=self.param.get("expnr", "")))

        if self.param.get("invnr"):
            where &= (Q(invnr__icontains=self.param.get("invnr", "")))

        if self.param.get("txtsearch"):
            search_list = self.param.get("txtsearch", "").split()

            for search_item in search_list:
                if self.modeltxt in ("Product"):
                    where &= (
                        Q(name__icontains=search_item)
                        )                
                if self.modeltxt in ("ExpCat", "RevCat"):
                    where &= (
                        Q(group__name__icontains=search_item) |
                        Q(name__icontains=search_item)
                        )
                    if self.modeltxt == "ExpCat":
                        where |= (
                            Q(comptaplan__name__icontains=search_item) |
                            Q(comptaagri__name__icontains=search_item)
                        )
                if self.modeltxt in ("Supplier", "Client"):
                    where &= (
                        Q(company__icontains=search_item)
                        | Q(email__icontains=search_item)
                        | Q(zip__postal_code__icontains=search_item)
                        | Q(zip__place_name__icontains=search_item)
                        | Q(street__icontains=search_item)
                        | Q(remarks__icontains=search_item)
                        )
                    if self.modeltxt == "Client":
                        where |= Q(vatnr__icontains=search_item)
                    if self.modeltxt == "Supplier":
                        where |= (
                            Q(expcat__group__name__icontains=search_item) |
                            Q(expcat__name__icontains=search_item)
                        )

        if self.param.get("zip"):
            where &= Q(zip=self.param.get("zip", ""))

        if self.param.get("product"):
            # product_id = int(self.param.get("product"))
            where &= Q(id=self.param.get("product"))

        if self.param.get("atelier"):
            where &= Q(atelier=self.param.get("atelier", ""))

        if self.param.get("supplier"):
            where &= Q(supplier=self.param.get("supplier", ""))

        if self.param.get("client"):
            where &= Q(client=self.param.get("client", ""))

        if self.param.get("expcatgrp"):
            where &= Q(expcat__group=self.param.get("expcatgrp", ""))
            # where &= Q(expcatgrp__in=self.param.getlist("expcatgrp"))

        if self.param.get("expcat"):
            where &= Q(expcat=self.param.get("expcat", ""))

        if self.param.get("revcatgrp"):
            where &= Q(revcatgrp=self.param.get("revcatgrp", ""))

        if self.param.get("revcat"):
            where &= Q(revcat=self.param.get("revcat", ""))

        if self.param.get("year") and self.modeltxt in ("Expense", "Revenue"):
            where &= Q(inputdate__year=self.param.get("year", ""))

        if self.param.get("loan"):
            where &= Q(loan=self.param.get("loan", ""))

        if self.param.get("fixrevenue"):
            where &= Q(fixrevenue=self.param.get("fixrevenue", ""))

        if self.param.get("fixexpense"):
            where &= Q(fixexpense=self.param.get("fixexpense", ""))
        return where

    def sort_default(self):
        sort = ["-id"] # used only for CreditRefund
        if self.modeltxt == "Expense":
            sort = ["-inputdate", "-id"]
        if self.modeltxt == "Revenue":
            sort = ["-inputdate", "-id"]
        if self.modeltxt == "FixRevenue":
            sort = ["-begindate", "-id"]
        if self.modeltxt == "FixExpense":
            sort = ["-begindate", "-id"]
        if self.modeltxt == "Supplier":
            sort = ["company",]
        if self.modeltxt == "Client":
            sort = ["company"]
        if self.modeltxt == "Product":
            sort = ["atelier", "name"]
        if self.modeltxt == "Loan":
            sort = ["-startdate"]
        if self.modeltxt == "ExpensePayment":
            sort = ["expense", "-paydate"]
        if self.modeltxt == "Credit":
            sort = ["-startdate"]
        if self.modeltxt == "Invest":
            sort = ["-inputdate"]
        return sort

    def sort_user(self):
        """
        if sortBy presence is pair=>desc, unpair=>asc
        """
        if self.param.get("sortBy"):
            sorting = []
            myurl = self.request.get_full_path()
            urlcut = myurl.split('&')
            mysort = [x for x in urlcut if 'sortBy' in x]
            for i in mysort:
                ordering = ""
                n = urlcut.count(i)
                # si modulo=0 => nbr pair => desc
                if (n % 2) == 0:
                    ordering = "-"
                sorting.append("{0}{1}".format(ordering, i[7:]))
        else:
            sorting = self.sort_default()
        return sorting


class MyListView(MyLoginRequiredMixin, MoneySearchMixin, generic.ListView):

    def __init__(self, *args, **kwargs):
        super(MyListView, self).__init__(*args, **kwargs)
        self.modeltxt = self.model
        self.model = eval(self.modeltxt)
        self.create_path = "{0}_create".format(self.modeltxt.lower())
        if self.modeltxt in ('Client', 'Supplier', 'Product', 'Expense', 'Revenue'):
            self.formName = eval("{0}Search".format(self.modeltxt))
        self.paginate_by = 100

    def get_queryset(self):
        qs = self.model.objects.for_user(self.myuser).all()
        #qs = self.model.objects.filter(profile__id=4).select_related().all()
        if self.modeltxt in ("Expense", "Revenue"):
            qs = qs.annotate(solde=F('amountvat') - F('payamount'),
                             lastdate=Case(
                                    When(payflag=True, then='lastpaydate'),
                                    default=date.today(),
                                    output_field=DateField(),
                                ),
                            # lastdate=Coalesce('lastpaydate', date.today())
                            )
            qs = qs.annotate(jretard=ExtractDay(F('lastdate') - F('payduedate')))
            if self.modeltxt == "Revenue" and "bdc" in self.request.path_info:
                self.create_path = "bdc_create"
                qs = qs.filter(revcat__id__in=("FRM_BDC", "FRM_DEV"))
            if self.modeltxt == "Revenue" and "revenue" in self.request.path_info:
                qs = qs.exclude(revcat__id="FRM_BDC")
            if self.modeltxt == "Expense" and "invest" in self.request.path_info:
                self.create_path = "invest_create"
                qs = qs.filter(expcat__group__id="INV")
        qsf = qs.filter(self.build_where())
        qsf = qsf.order_by(*self.sort_user())

        # Spécifique pour appel de l'index
        if self.modeltxt in ('Expense') and self.param.get("supplieronly"):
            qsf = qsf.exclude(expcat__group="EMP")

        # Spécifique pour Client et Fournisseur
        if self.modeltxt in ('Client', 'Supplier'):

            if self.modeltxt in ('Client'):
                payflag = self.param.get("payflag", "NADA")
                if payflag != "NADA":
                    if payflag == "Paid":
                        qsf = qsf.exclude(revenue__payflag=False)
                    if payflag == "ToPay":
                        qsf = qsf.filter(revenue__payflag=False)
            if self.modeltxt == 'Supplier':
                # distinct obligatoire pour fournisseur avec plus d'une categ
                qsf = qsf.distinct()
                #qsf = qsf.exclude(expcat__group="EMP")
                payflag = self.param.get("payflag", "NADA")
                if payflag != "NADA":
                    if payflag == "Paid":
                        qsf = qsf.exclude(expense__payflag=False)
                    if payflag == "ToPay":
                        qsf = qsf.filter(expense__payflag=False)
        # Pour les ddlb dynamiques
        id_list = qsf.values_list('id', flat=True)
        if self.modeltxt in ('Client', 'Supplier', 'Product', 'Expense', 'Revenue'):
            self.form = self.formName(request=self.request, id_list=id_list)
        if self.modeltxt in ('Expense', 'Revenue'):
            self.amountvattot = (qsf.aggregate(tot=Sum("amountvat"))["tot"])
            self.payamounttot = (qsf.aggregate(tot=Sum("payamount"))["tot"])
            self.solde = (qsf.aggregate(solde=Sum(F("amountvat") - F("payamount")))["solde"])

        # self.qsf = qsf
        return qsf


    def get(self, request, *args, **kwargs):
        # https://stackoverflow.com/questions/18930234/django-modifying-the-request-object
        # self.param = request.GET.copy()
        self.param = request.GET
        xlsparam = self.param.get("exportxls")
        if xlsparam == 'XLSGO':
            qsf = self.get_queryset()
            xls = SaveAsXLS(qsf)
            msg_back = xls.get_model_and_columns()
            if msg_back != "OK":
                return render(request, "500.html", {"error_msg": msg_back})
            else:
                workbook = xls.create_workbook()
                sheet_added, msg_back = xls.add_sheet(self.modeltxt)
                if sheet_added:
                    filename = "{}.xls".format(self.modeltxt)
                    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                    response['Content-Disposition'] = 'attachment; filename={0}'.format(filename)
                    workbook.save(response)
                    return response
                else:
                    return render(request, "500.html", {"error_msg": msg_back})
        return super(MyListView, self).get(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["create_path"] = self.create_path
        context["list_path"] = "{0}_list".format(self.modeltxt.lower())
        context["title"] = self.model._meta.verbose_name

        if self.modeltxt == "Loan":
            inst = utils_display.LoanDisplay(self.myuser)
            context.update(inst.get_context())
        if self.modeltxt == "Invest":
            unpaid_invest = utils_display.get_upaid_invest(self.myuser)
            context["unpaid_invest"] = unpaid_invest

        return context


# https://taranjeet.cc/adding-forms-dynamically-to-a-formset/
# https://simpleisbetterthancomplex.com/questions/2017/03/22/how-to-dynamically-filter-modelchoices-queryset-in-a-modelform.html
@login_required
def revenueproduct_update(request, pk):
    usermodel = get_user_model()
    userpk = request.session["myuserpk"]
    myuser = get_object_or_404(usermodel, pk=userpk)
    revenue = get_object_or_404(Revenue, pk=pk)
    template_name = "money/revenueproduct.html"
    data = request.POST or None
    formset = ProductFormSet(
        data=data, queryset=RevenueProduct.objects.filter(revenue=revenue)
    )

    for form in formset:
        form.fields["atelier"].queryset = (
            Atelier.objects.for_user(myuser)
            .exclude(atelier_id="GEN")
            .order_by("atelier")
        )
        # form.fields["atelier"].empty_label = None
        # Pas utilisable dans Formset sinon check required for all
        form.fields["product"].queryset = Product.objects.for_user(
            myuser
        ).order_by("name")
        # Add 3 attributes to product ddl
        form.fields["product"].widget = ProductAttributesSelect(
            choices=form.fields["product"].choices, user=myuser
        )

    if request.method == "POST":
        if formset.is_valid():
            for form in formset:
                # Save only for line where product has been filled in
                if form.cleaned_data.get("product"):
                    user_inst = usermodel.objects.get(username=myuser)
                    product = form.save(commit=False)
                    product.profile = user_inst
                    product.revenue = revenue
                    product.save()
            messages.success(request, _("Produit(s) ajouté(s)."), fail_silently=True)
            retbool = insert_producttot_into_revenue(revenue)
            if not retbool:
                messages.warning(request, _("Erreur lors du calcul du montant total. Message envoyé à l'administrateur du site."), fail_silently=True)
            if revenue.revcat.id in ("FRM_BDC", "FRM_DEV"):
                redirect_to = redirect("bdc_detail", pk)
            else:
                redirect_to = redirect("revenue_detail", pk)
            return redirect_to
        # else: ceci n'affiche pas les erreurs sur la page du formulaire
        #     msg = "form {0} invalid".format(formset.__name__)
        #     LOG.error(msg)
        #     return render(request, "500.html", {"error_msg": msg})

    return render(
        request,
        template_name,
        {
            "object": revenue,
            "formset": formset,
            "pk": pk
        },
    )


@login_required
def revenueproduct_delete(request):
    if request.method == "POST":
        ip = 0
        # if "pk" in request.GET:
        ip_id = request.POST.get("ip_id")
        print(ip_id)
        try:
            ip = get_object_or_404(RevenueProduct, pk=ip_id)
            ip.delete()
            insert_producttot_into_revenue(ip.revenue)
        except Exception as e:
            data = {
                "msg": "Erreur {0} lors de la suppression revenue_product_id: {1}.".format(
                    e, ip_id
                )
            }
        data = {"msg": "{0} supprimé.".format(ip)}

    return JsonResponse(data)


@login_required
def payment_update(request, pk):
    userpk = request.session["myuserpk"]
    usermodel = get_user_model()
    myuser = get_object_or_404(usermodel, pk=userpk)
    usermodel = get_user_model()
    if "revenue" in request.path:
        inst = get_object_or_404(Revenue, pk=pk)
        template_name = "money/revenuepayment.html"
        success_url = redirect("revenue_detail", pk)
        instdate = inst.inputdate
        pay = RevenuePayment.objects.filter(revenue=inst)
        formset = PaymentRevenueFormSet(queryset=pay)
        solde = inst.get_solde()
    elif "expense" in request.path:
        inst = get_object_or_404(Expense, pk=pk)
        template_name = "money/expensepayment.html"
        success_url = redirect("expense_detail", pk)
        instdate = inst.inputdate
        pay = ExpensePayment.objects.filter(expense=inst)
        formset = PaymentExpenseFormSet(queryset=pay)
        solde = inst.get_solde()

    solde = round(solde, 2)
    form = PaymentForm(solde=solde)

    if request.method == "POST":
        form = PaymentForm(request.POST, solde=solde, instdate=instdate)
        if form.is_valid():
            if form.cleaned_data.get("amount") and form.cleaned_data.get("paydate"):
                try:
                    if "revenue" in request.path:
                        cause = "RevenuePayment.objects.create"
                        user_inst = usermodel.objects.get(username=myuser)
                        RevenuePayment.objects.create(
                            profile=user_inst,
                            revenue=inst,
                            amount=form.cleaned_data.get("amount"),
                            paydate=form.cleaned_data.get("paydate"),
                        )
                    if "expense" in request.path:
                        cause = "ExpensePayment.objects.create"
                        user_inst = usermodel.objects.get(username=myuser)
                        ExpensePayment.objects.create(
                            profile=user_inst,
                            expense=inst,
                            amount=form.cleaned_data.get("amount"),
                            paydate=form.cleaned_data.get("paydate"),
                        )
                    retbool, payflag = compute_paytot_amount(inst, pay)
                    if not retbool:
                        messages.warning(request,
                        _("Erreur lors du calcul du montant total payé. Message envoyé à l'administrateur du site."))
                    # si totalement payé et q'il s'agit d'un emprunt autre que crédit de caisse
                    # cocher payflag dans tableau amortissement
                    # HBP to check!!!
                    try:
                        if "expense" in request.path and payflag and hasattr(inst, "loanrfd"):
                            if inst.loanrfd is not None:
                                inst.loanrfd.debitflag = True
                                inst.loanrfd.save()
                    except AttributeError as error:
                        LOG.error(error)

                except Exception as xcpt:
                    LOG.error(xcpt, extra={'user': myuser})
                    return render(request, "500.html", {"error_msg": xcpt})

                return success_url

    return render(
        request,
        template_name,
        {
            "object": inst,
            "pay": pay,
            "formset": formset,
            "form": form,
            "pk": pk
        },
    )


@login_required
def revenuepayment_delete(request):
    if request.method == "POST":
        msg = ""
        try:
            ip_id = request.POST.get("ip_id")
            ip_id = int(ip_id)
            ip = RevenuePayment.objects.get(id=ip_id)
            revenue = ip.revenue
            ip.delete()
            pay = RevenuePayment.objects.filter(revenue=revenue)
            retbool, payflag = compute_paytot_amount(revenue, pay)
            if not retbool:
                messages.warning(request,
                _("Erreur lors du calcul du montant total payé. Message envoyé à l'administrateur du site."))
        except Exception as e:
            data = {
                "msg": "Erreur {0} lors de la suppression revenue_payment_id: {1}.".format(
                    e, ip_id
                )
            }
        data = {"pk": revenue.id, "root_url": PROJECT_NAME}

    return JsonResponse(data)


@login_required
def expensepayment_delete(request):
    if request.method == "POST":
        try:
            ip_id = request.POST.get("ip_id")
            ip_id = int(ip_id)
            ip = ExpensePayment.objects.get(id=ip_id)
            expense = ip.expense
            ip.delete()
            pay = ExpensePayment.objects.filter(expense=expense)
            compute_paytot_amount(expense, pay)
        except Exception as e:
            data = {
                "msg": "Erreur {0} lors de la suppression expense_payment_id: {1}.".format(
                    e, ip_id
                )
            }
        # data = {"msg": "{0} supprimé.".format(ip_id)}
        data = {"pk": expense.id, "root_url": PROJECT_NAME}

    return JsonResponse(data)


class FixDeleteView(MyLoginRequiredMixin, MessageMixin, generic.DeleteView):
    def __init__(self, *args, **kwargs):
        super(FixDeleteView, self).__init__(*args, **kwargs)
        self.modeltxt = self.model
        self.model = eval(self.modeltxt)
        self.template_name = "money/{0}_detail.html".format(self.modeltxt.lower())
        self.success_url = reverse("{0}_list".format(self.modeltxt.lower()))
        if self.modeltxt == "FixRevenue":
            targetObj = _("les recettes")
        if self.modeltxt == "FixExpense":
            targetObj = _("dépenses")
        if self.modeltxt == "Loan":
            targetObj = _("le tableau d'amortissement")
        self.confirm_message = _(
            "Supprimer cet enregistrement \
                                 ainsi que tout ce qui a été généré \
                                 dans {0}?"
        ).format(targetObj)


class FixValidateView(MyLoginRequiredMixin, generic.UpdateView):
    # You don’t even need to provide a success_url for CreateView or UpdateView
    # they will use get_absolute_url() on the model object if available.
    def __init__(self, *args, **kwargs):
        super(FixValidateView, self).__init__(*args, **kwargs)
        self.modeltxt = self.model
        self.model = eval(self.modeltxt)
        self.fields = ["validated"]
        self.template_name = "money/{0}_detail.html".format(self.modeltxt.lower())
        if self.modeltxt == "FixRevenue":
            self.targetObj = _("recettes")
        if self.modeltxt == "FixExpense":
            self.targetObj = _("dépenses")
        self.confirm_message = _(
            "Valider cette {0} et insérer toutes les occurences \
                dans les {1}?"
        ).format(self.model._meta.verbose_name.lower(), self.targetObj)

    def form_valid(self, form):
        fix_inst = self.model.objects.get(pk=self.kwargs["pk"])

        frm = form.save(commit=False)
        if fix_inst.validated:
            retval = update_fix_into_exprev(
                src=fix_inst, user=self.myuser, modeltxt=self.modeltxt
            )
        else:
            retval = insert_fix_into_exprev(
                src=fix_inst, user=self.myuser, modeltxt=self.modeltxt
            )
        # Check return Value of update or insert
        if retval:
            form.instance.validated = True
            frm.save()
            msg = _("{0} récurrentes insérées avec succès dans les {1}").format(
                self.targetObj.capitalize(), self.targetObj
            )
            messages.success(self.request, msg)
        else:
            # If already True, keep it to True, even when Error
            if fix_inst.validated:
                form.instance.validated = True
                frm.save()
            messages.error(self.request, _("Erreur lors de l'insertion/suppression des données récurrentes. Message envoyé à l'administrateur du site."))

        return super(FixValidateView, self).form_valid(form)

    def get_success_url(self):
        # https://realpython.com/django-redirects/
        if self.modeltxt == "FixRevenue":
            base_url = reverse('revenue_list')
            query_string = urlencode({'fixrevenue': self.kwargs["pk"]})
            url = '{}?{}'.format(base_url, query_string)
        if self.modeltxt == "FixExpense":
            base_url = reverse('expense_list')
            query_string = urlencode({'fixexpense': self.kwargs["pk"]})
            url = '{}?{}'.format(base_url, query_string)
        return url


class LoanValidateView(MyLoginRequiredMixin, generic.UpdateView):
    """
    Creation tableau amortissement
    => insert Amount into Revenue
    => insert LoanRefund into Expense (mensualités)
    """
    model = Loan
    fields = ["validated"]
    template_name = "money/loan_detail.html"
    success_url = "amortization" # ajoute amortization après url de base
    confirm_message = _("Générer le tableau d'amortissement de cet emprunt?")

    def form_valid(self, form):
        retval = False
        loan = self.model.objects.get(pk=self.kwargs["pk"])

        frm = form.save(commit=False)
        if loan.validated:
            msg = _("Emprunt déjà validé mais pas de tableau d'amortissement.")
            messages.warning(self.request, msg)
            return super().form_valid(form) # on quite
        else:
            retval = amortize(loan, self.myuser)
        # Check return Value for Insert into Expense based on ticked Refund
        if retval:
            form.instance.validated = True
            frm.save()
            msg = _("Tableau d'amortissement généré.")
            messages.success(self.request, msg)

            # Insert rows into Expense
            if insert_loanrfd_into_expense(loan, self.myuser, self.request):
                msg = _("Remboursement d'emprunt avant aujourd'hui (cases cochée déjà débité) inséré dans les paiements dépenses.")
                messages.success(self.request, msg)

            # Insert 1 row into Revenue
            if insert_loanamount_into_revenue(loan, self.myuser, self.request):
                msg = _("Montant emprunté ajouté aux recettes à la date du 1er débit: {0}".format(loan.startdate.strftime("%d/%m/%Y")))
                messages.success(self.request, msg)

        else:
            # If already True, keep it to True, even when Error
            # if loan.validated:
            #     form.instance.validated = True
            # else:
            #     form.instance.validated = False
            #     frm.save()
            messages.error(
                self.request,
                _(
                    "Erreur lors de la création du tableau d'amortissement. \
                    Message envoyé à l'administrateur du site."
                ),
            )

        return super().form_valid(form)


@login_required
def loan_refund(request, pk):
    userpk = request.session["myuserpk"]
    usermodel = get_user_model()
    user = get_object_or_404(usermodel, pk=userpk)
    loan = get_object_or_404(Loan, pk=pk)
    # loanrfd = LoanRefund.objects.filter(loan=loan).order_by('period')
    title = _('Tableau amortissement > {0} {1} ({2})'.format(loan.bankrefid, loan.bankrefdesc, loan.loantype))

    if request.method == 'POST':
        mystrids = request.POST.getlist('debitflag')
        myids = [int(i) for i in mystrids]
        manage_refund = ManageLoanRfdPayment(user, loan, myids, request)
        retval = manage_refund.process_payment()
        if retval:
            messages.success(request, _('Paiements emprunt {0} mis à jour dans les dépenses').format(loan.bankrefdesc))
        else:
            messages.error(request, _("Erreur gestion remboursements d'emprunt. Message envoyé à l'administrateur du site."))
        response = redirect('expense_list')
        response['Location'] += '?loan={0}'.format(pk)
        return response

    context = {'user': user,
               'title': title,
               'loan': loan,
               'loanrefund': loan.loanrefund_set.all().order_by('period')
               }

    return render(request, 'money/loanrefund_form.html', context)


class LoanCloseView(MyLoginRequiredMixin, generic.UpdateView):
    """
    A la clôture, il faut rembourser le SRD (balance)
    plus une éventuelle indemnité
    """
    fields = ["closedate", "closeindemnity", "closeflag"]
    template_name = "money/loan_detail.html"
    confirm_message = _("Clôturer cet emprunt?")
    model = Loan

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     pk = self.object.pk
    #     loan = self.model.objects.get(pk=pk)
    #     sum_interest = loan.loanrefund_set.filter(debitflag=False).aggregate(interest=Sum("interest"))["interest"]
    #     # http://www.cpe-credit.com/blog/indemnite-de-remploi/
    #     indemnity_calc = sum_interest + srd * 0.04
    #     context["indemnity_calc"] = indemnity_calc
    #     return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.closedate = date.today()
        obj.closeflag = True
        obj.save()
        closeindemnity = obj.closeindemnity
        # Get SRD
        loan = self.model.objects.get(pk=self.kwargs["pk"])
        messages.info(self.request, _('{0} clôturé.'.format(loan)))
        srd = loan.loanrefund_set.filter(debitflag=False).aggregate(SRD=Max("balance"))["SRD"]
        # SumUp the 2 values
        indemnitytot = srd + closeindemnity
        retval, expense_id = insert_closeindemnity_into_expense(loan, self.myuser, indemnitytot)

        if retval:
            messages.success(
                self.request,
                _("Confirmer le paiement de l'indemnité ci-dessous. \
                  Sinon, adapter la date d'échéance en conséquence.")
                )
            # Doit être défini ici sinon plante en cas d'erreur car expense_id n'existe pas
            self.success_url = reverse('expense_detail', args=[expense_id])
        else:
            messages.error(
                self.request,
                _("Erreur lors de l'insertion de l'indemnité de clôture dans \
                  les dépenses'. Message envoyé à l'administrateur du site.")
                )
        return super().form_valid(form)


@login_required
def creditrefund_create(request, pk):
    title = "{0} > {1}".format(CreditRefund._meta.verbose_name, _("Nouveau"))
    credit = Credit.objects.get(pk=pk)
    form = CreditRefundForm(credit=credit)
    usermodel = get_user_model()

    if request.method == "POST":
        form = CreditRefundForm(request.POST, credit=credit)
        if form.is_valid():
            userpk = request.session["myuserpk"]
            user = get_object_or_404(usermodel, pk=userpk)
            obj = form.save(commit=False)
            obj.profile_id = credit.profile.id
            obj.credit_id = credit.id
            obj.save()
            retval1 = insert_credit_into_revenue(obj, user)
            if not retval1:
                msg1 = _("Erreur lors de la création de la recette associée au crédit.")
                return render(request, "500.html", {"error_msg": msg1})

            if obj.refunddate is not None:
                retval2 = insert_crecaisse_into_expense(obj, user)
                if not retval2:
                    msg2 = _("Erreur lors de la création de la dépense associée au crédit.")
                    return render(self.request, "500.html", {"error_msg": msg2})
            return redirect(credit.get_absolute_url())

    return render(
        request,
        "base_money_form.html",
        {
            "form": form,
            "success_url": credit.get_absolute_url(),
            "pk": pk,
            "title": title,
            "form_html_id": "creditrefund_form"
        },
    )


class CreditRefundUpdate(MyLoginRequiredMixin, MessageMixin, generic.UpdateView):
    model = CreditRefund
    form_class = CreditRefundForm
    template_name = "base_money_form.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["operation"] = "UPDATE"
        kwargs["credit"] = self.get_object().credit
        return kwargs

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.save()
        retval = insert_crecaisse_into_expense(obj, self.myuser)
        if not retval:
            msg = _("Erreur lors de la création de la dépense associée au crédit.")
            return render(self.request, "500.html", {"error_msg": msg})
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["success_url"] = self.get_object().credit.get_absolute_url()
        context["form_html_id"] = "creditrefund_form"
        return context


class CreditRefundDelete(MyLoginRequiredMixin, MessageMixin, generic.DeleteView):
    model = CreditRefund
    confirm_message = _("Supprimer cet enregistrement?")

    def get_success_url(self):
        return self.get_object().credit.get_absolute_url()
