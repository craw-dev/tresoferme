# -*- coding: utf-8 -*-
"""
Insert loanrefund record into Expense
input param: user_id & loan_id
"""
from django.core.management.base import BaseCommand, CommandError
from money.utils_crud import insert_loanrfd_into_expense
from money.models import Loan
from django.contrib.auth import get_user_model


class Command(BaseCommand):
    help = 'Insert loanrfd into expense'

    def add_arguments(self, parser):
        parser.add_argument('user_id', type=int)
        parser.add_argument('loan_id', type=int)

    def handle(self, *args, **options):
        try:
            user_id = options['user_id']
            loan_id = options['loan_id']
            usermodel = get_user_model()
            user = usermodel.objects.get(id=user_id)
            loan = Loan.objects.get(id=loan_id)
            retbool, msg = insert_loanrfd_into_expense(loan, user)
            if retbool:
                self.stdout.write(self.style.SUCCESS("LoanRefund of Loan '{0}' for User {1} inserted into Expense!".format(loan, user)))
            else:
                self.stdout.write(self.style.ERROR(msg))
        except Exception as xcpt:
            raise CommandError('Failed "{0}"'.format(xcpt))
