# -*- coding: utf-8 -*-
"""
Mise à jour payamount dans Expense et Revenue
sur base de ExpensePayment et RevenuePayment
https://django.readthedocs.io/en/2.1.x/howto/custom-management-commands.html
"""
import sys
from django.db.models import Sum, Max
from money.models import Expense, Revenue, ExpensePayment, RevenuePayment
from django.core.management.base import BaseCommand


def upd_fact():
    try:
        exp = Expense.objects.all()
        for e in exp:
            eptot = ExpensePayment.objects.filter(expense=e).aggregate(tot=Sum("amount"))["tot"]
            epmaxdate = ExpensePayment.objects.filter(expense=e).aggregate(max=Max("paydate"))["max"]
            if eptot is None:
                eptot = 0
            if round(eptot, 2) >= round(e.amountvat, 2):
                e.payflag = True
            else:
                e.payflag = False
            e.payamount = eptot
            e.lastpaydate = epmaxdate
            e.save()
    except Exception as x:
        msg = "Expense update: %s\n" % x
        return False, msg

    try:
        rev = Revenue.objects.all()
        for r in rev:
            rptot = RevenuePayment.objects.filter(revenue=r).aggregate(tot=Sum("amount"))["tot"]
            rpmaxdate = RevenuePayment.objects.filter(revenue=r).aggregate(max=Max("paydate"))["max"]
            if rptot is None:
                rptot = 0
            if round(rptot, 2) >= round(r.amountvat, 2):
                r.payflag = True
            else:
                r.payflag = False
            r.payamount = rptot
            r.lastpaydate = rpmaxdate
            r.save()
    except Exception as x:
        msg = "Revenue update: %s\n" % x
        return False, msg

    return True, "OK"


class Command(BaseCommand):
    help = 'Update amount paid in Expense & Revenue'

    def handle(self, *args, **options):
        try:
            retbool, msg = upd_fact()
            if retbool:
                self.stdout.write(self.style.SUCCESS('Successfully update payamount'))
            else:
                self.stdout.write(self.style.ERROR(msg))
        except Exception:
            self.stdout.write(
                self.style.ERROR(
                    'Failed "{0}"'.format(sys.exc_info())
                    )
                )
            # self.stdout.write('Failed "{0}"'.format(sys.exc_info()))
