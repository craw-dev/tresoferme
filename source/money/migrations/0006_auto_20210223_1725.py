# Generated by Django 3.1.5 on 2021-02-23 16:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('money', '0005_auto_20210107_1512'),
    ]

    operations = [
        migrations.AddField(
            model_name='revenue',
            name='invoice_send',
            field=models.BooleanField(default=False, verbose_name='Facture envoyée?'),
        ),
        migrations.AlterField(
            model_name='client',
            name='vatnr',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Nr entreprise'),
        ),
        migrations.AlterField(
            model_name='product',
            name='name',
            field=models.CharField(max_length=255, verbose_name='Produit'),
        ),
        migrations.AlterField(
            model_name='revenue',
            name='refdesc',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Descriptif'),
        ),
        migrations.AlterField(
            model_name='supplier',
            name='vatnr',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Nr entreprise'),
        ),
    ]
