# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def load_from_sql():
    import os
    from config.settings import BASE_DIR
    DATA_DIR = 'money/data_insert/'
    sql_statements = ""
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_categclass.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_comptaagri.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_comptaplan.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_duedatelist.sql'), 'r').read()

    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_yearfreqlist.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_vatlist.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_unitlist.sql'), 'r').read()
    
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_revcatgrp.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_revcat.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_expcatgrp.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_expcat.sql'), 'r').read()
    
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_supplier.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_supplier_expcat.sql'), 'r').read()

    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_client.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_product.sql'), 'r').read()
    
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_expense.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_expensepayment.sql'), 'r').read()

    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_revenue.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_revenuepayment.sql'), 'r').read()
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'money_revenueproduct.sql'), 'r').read()    
    

    return sql_statements


class Migration(migrations.Migration):

    dependencies = [
        ('money', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(load_from_sql()),
    ]
