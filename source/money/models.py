# -*- coding: utf-8 -*-
"""
money.models description
----------------------------
deleted field added in RevenuePayment, ExpensePayment, RevenueProduct,
FixExpense and FixRevenue
just to be able to work with for_user object but it is not used!
"""
import logging, decimal
from datetime import date
from pgcrypto import fields
# from dateutil.relativedelta import relativedelta
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.db.models.constants import LOOKUP_SEP
from django.core.exceptions import FieldDoesNotExist
from django.utils.encoding import force_text

from accounts.models import UserGeneric, ContactGeneric
from farm.models import Atelier

__author__ = 'hbp'
__email__ = 'p.houben@cra.wallonie.be'
__copyright__ = 'Copyright 2018, Patrick Houben'
__license__ = 'GPLv3'
__date__ = '2018-07-19'
__version__ = '1.3'
__status__ = 'Development'

LOG = logging.getLogger(__name__)
LOG_DEBUG = logging.getLogger("mydebug")


class UserManager(models.Manager):
    def get_queryset(self):
        return super(UserManager, self).get_queryset()

    def for_user(self, user):
        return super(UserManager, self).get_queryset().filter(profile=user)


class DueDateList(models.Model):
    """
    List of deadlines: immédiat, 15 jours, 30 jours, 30 jours fin de mois.
    """
    id = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=50, verbose_name=_("Nom"))
    daysadd = models.PositiveSmallIntegerField(verbose_name=_('Jours à ajouter'),
                                               default=0)

    class Meta:
        verbose_name = _("Liste échéance")

    def __str__(self):
        return '{0}'.format(self.name)


class UnitList(models.Model):
    """
    List of units: kg, l, nbr, etc.
    """
    id = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=50, verbose_name=_("Unités"))

    class Meta:
        verbose_name = _("Liste unité")

    def __str__(self):
        return '{0}'.format(self.name)


class YearFreqList(models.Model):
    """
    Year frequency list
    """
    id = models.PositiveSmallIntegerField(primary_key=True)
    name = models.CharField(max_length=50, verbose_name=_("Fréquence"))
    nbrmonth = models.PositiveSmallIntegerField(default=12)

    class Meta:
        verbose_name = _("Liste unité")

    def __str__(self):
        return '{0}'.format(self.name)


class VatList(models.Model):
    """
    VAT list with percentage
    """
    id = models.PositiveSmallIntegerField(primary_key=True,
                                          verbose_name=_('code TVA'))
    percent = models.PositiveSmallIntegerField(
                            verbose_name=_('Pourcentage nbr'))
    name = models.CharField(max_length=50,
                            verbose_name=_("Pourcentage"))

    class Meta:
        verbose_name = _("Liste TVA")
        ordering = ['id']

    def __str__(self):
        return '{0} %'.format(self.name)


class CategClass(models.Model):
    id = models.CharField(max_length=3, primary_key=True)
    name = models.CharField(max_length=50, verbose_name=_("Fixe ou Variable"))

    class Meta:
        verbose_name = _("Frais fixes-variables")

    def __str__(self):
        return '{0}'.format(self.name)


class ComptaPlan(models.Model):
    name = models.CharField(max_length=255, verbose_name=_("Plan comptable"))

    class Meta:
        verbose_name = _("Plan comptable")
        ordering = ['id']

    def __str__(self):
        return '{0}'.format(self.name)


class ComptaAgri(models.Model):
    name = models.CharField(max_length=255, verbose_name=_("Compta agricole"))

    class Meta:
        verbose_name = _("Compta agricole")
        ordering = ['name']

    def __str__(self):
        return '{0}'.format(self.name)


class ExpCatGrp(models.Model):
    id = models.CharField(max_length=3, primary_key=True)
    name = models.CharField(max_length=50, verbose_name=_("Groupe"))

    class Meta:
        verbose_name = _("Sortie Groupe")
        ordering = ['name']

    def __str__(self):
        return '{0}'.format(self.name)


class ExpCat(models.Model):
    id = models.CharField(max_length=7, primary_key=True)
    group = models.ForeignKey(ExpCatGrp, on_delete=models.PROTECT,
                              verbose_name=_("Groupe"))
    name = models.CharField(max_length=50, verbose_name=_("Catégorie"))
    categclass = models.ForeignKey(CategClass, on_delete=models.PROTECT,
                            verbose_name=_("Fixe/Variable"), blank=True, null=True)
    comptaplan = models.ForeignKey(ComptaPlan, on_delete=models.PROTECT,
                              verbose_name=_("Plan comptable"),
                              blank=True, null=True)
    comptaagri = models.ForeignKey(ComptaAgri, on_delete=models.PROTECT,
                              verbose_name=_("Comptabilité agricole"),
                              blank=True, null=True)
    rentatxt = models.CharField(max_length=255,
                                verbose_name=_("Calcul rentabilité"),
                                blank=True, null=True)
    tresotxt = models.CharField(max_length=255,
                                verbose_name=_("Calcul trésorerie"),
                                blank=True, null=True)                                                                
    field2show = models.CharField(max_length=255,
                                  verbose_name=_("Pour Patrick: affichage JS"),
                                  blank=True, null=True)
    renta_exclude = models.BooleanField(default=False, verbose_name=_("Exclu du calcul rentabilité"))

    class Meta:
        verbose_name = _("Sortie Catégorie")
        ordering = ['group', 'name']

    def get_verbose_name(self, lookup):
        # will return first non relational field's verbose_name in lookup
        # https://stackoverflow.com/questions/40176689/django-orm-get-verbose-name-of-field-via-filter-lookup
        model = self
        if LOOKUP_SEP in lookup:
            for part in lookup.split(LOOKUP_SEP):
                try:
                    f = model._meta.get_field(part)
                except FieldDoesNotExist:
                    # check if field is related
                    for f in model._meta.related_objects:
                        if f.get_accessor_name() == part:
                            break
                    else:
                        raise ValueError("Invalid lookup string")
                if f.is_relation:
                    model = f.related_model
                    continue
                return force_text(f.verbose_name)
        else:
            return model._meta.get_field(lookup).verbose_name.title()

    def __str__(self):
        return '{0}'.format(self.name)


class Credit(UserGeneric):
    """payflag only for Straight Loan"""
    bankrefid = models.CharField(max_length=50,
                                 verbose_name=_("Identifiant bancaire"))
    bankrefdesc = models.CharField(max_length=50, verbose_name=_("Descriptif"))
    refshort = models.CharField(max_length=4, verbose_name=_("Étiquette (id) 4 caractères"))
    loantype = models.ForeignKey(ExpCat, on_delete=models.PROTECT,
                                 verbose_name=_("Type de credit"))
    loanamount = models.PositiveIntegerField(verbose_name=_("Montant sollicité (€)"),
                                             help_text=_("Maximum 100.000 €."))
    loanrefund = models.DecimalField(max_digits=16, decimal_places=9,
                                     verbose_name=_("Montant à rembourser à l'échéance"),
                                     blank=True, null=True)
    startdate = models.DateField(verbose_name="Date souscription")
    closedate = models.DateField(verbose_name="Date clôture", blank=True, null=True)
    payflag = models.BooleanField(verbose_name=_("Remboursé?"), default=False,
            help_text=_("Cocher cette case pour préciser le remboursement du montant total dû."))
    interestrate = models.DecimalField(max_digits=6, decimal_places=3,
                                       verbose_name=_("Taux d'intérêt %"),
                                       blank=True, null=True,
                                       help_text=_("Suppérieur à 7%"))
    objects = UserManager()

    class Meta:
        verbose_name = _("Crédit")
        unique_together = ('profile', 'refshort')

    def get_title(self):
        return "{0} - {1}".format(self.loantype, self.bankrefdesc)

    def get_absolute_url(self):
        return reverse('credit_detail', args=[str(self.id)])

    def compute_interest(self):
        tot = 0
        for i in self.creditrefund_set.all():
            if i.refunddate is None:
                val = i.compute_interest()
                tot += val
        return tot

    def save(self, *args, **kwargs):
        if self.closedate is not None:
            if date.today() > self.closedate:
                self.closeflag = True
        super().save(*args, **kwargs)

    def __str__(self):
        return self.bankrefdesc


class CreditRefund(UserGeneric):
    """
    Linked to Revenue Expense
    refund applies only for Crédit de caisse
    """
    credit = models.ForeignKey(Credit, on_delete=models.CASCADE)
    withdrawdate = models.DateField(_("Date prélèvement"))
    withdrawamount = models.DecimalField(_("Montant prélevé (€)"),
                                         max_digits=10, decimal_places=2)
    refunddate = models.DateField(_("Date remboursement"), blank=True, null=True)
    objects = UserManager()

    class Meta:
        verbose_name = _("Retrait crédit")
        ordering = ['refunddate']

    def get_absolute_url(self):
        return reverse('credit_detail', args=[str(self.credit.id)])

    def compute_interest(self):
        """get difference between refund and withdraw amount"""
        calc = 0
        interest = self.credit.interestrate/100/365
        if self.refunddate is None:
            diffdate = date.today() - self.withdrawdate
        else:
            diffdate = self.refunddate - self.withdrawdate
        calc = self.withdrawamount * interest * diffdate.days
        return round(calc, 2)

    def get_title(self):
        return "{0}".format(self._meta.verbose_name)

    def __str__(self):
        if self.refunddate is None:
            return '{0} - {1}'.format(self.credit, self.withdrawdate)
        else:
            return '{0} - {1} - {2}'.format(self.credit, self.withdrawdate, self.refunddate)


class Loan(UserGeneric):
    bankrefid = models.CharField(max_length=50,
                                 verbose_name=_("Identifiant bancaire"))
    bankrefdesc = models.CharField(max_length=50, verbose_name=_("Descriptif"))
    refshort = models.CharField(max_length=4, verbose_name=_("Étiquette (id) 4 caractères"))
    loantype = models.ForeignKey(ExpCat, on_delete=models.PROTECT,
                                 verbose_name=_("Type d'emprunt"))
    loanamount = models.PositiveIntegerField(verbose_name=_("Montant sollicité (€)"))
    startdate = models.DateField(verbose_name="Date 1er débit")
    loanyears = models.PositiveSmallIntegerField(verbose_name=_("Durée emprunt (année)"))
    yearfreq = models.ForeignKey(YearFreqList, on_delete=models.PROTECT,
                                 verbose_name=_("Nombre de paiements par an"))
    interestrate = models.DecimalField(max_digits=6, decimal_places=3,
                                       verbose_name=_("Taux d'intérêt %"))
    validated = models.BooleanField(default=False, verbose_name=_("Validé?"))
    closedate = models.DateField(verbose_name="Date clôture",
                                 blank=True, null=True)
    closeflag = models.BooleanField(verbose_name=_("Clôturé?"), default=False)
    closeindemnity = models.DecimalField(max_digits=16, decimal_places=9,
                                         verbose_name=_("Indemnité de clôture"),
                                         blank=True, null=True)
    objects = UserManager()

    class Meta:
        verbose_name = _("Emprunt")
        unique_together = ('profile', 'refshort')

    def get_title(self):
        return "{0}".format(self.bankrefdesc)

    def get_absolute_url(self):
        return reverse('loan_detail', args=[str(self.id)])

    def save(self, *args, **kwargs):
        if self.closedate is not None:
            if date.today() > self.closedate:
                self.closeflag = True
        super().save(*args, **kwargs)

    def __str__(self):
        return self.bankrefdesc


class LoanRefund(UserGeneric):
    loan = models.ForeignKey(Loan, on_delete=models.CASCADE,
                             verbose_name=_("Emprunt"))
    period = models.PositiveSmallIntegerField(verbose_name=_("Période"))
    payment = models.DecimalField(max_digits=16, decimal_places=9, blank=True,
                                  null=True, verbose_name=_("Paiement"))
    capital = models.DecimalField(max_digits=16, decimal_places=9, blank=True,
                                  null=True, verbose_name=_("Capital"))
    interest = models.DecimalField(max_digits=16, decimal_places=9, blank=True,
                                   null=True, verbose_name=_("Intérêt"))
    balance = models.DecimalField(max_digits=16, decimal_places=9, blank=True,
                                  null=True, verbose_name=_("SRD"))
    capitalcum = models.DecimalField(max_digits=16, decimal_places=9,
                                     blank=True, null=True,
                                     verbose_name=_("Capital cumulé"))
    debitflag = models.BooleanField(default=False,
                                    verbose_name=_("Déjà débité?"))
    debitdate = models.DateField(verbose_name="Date débit")
    objects = UserManager()

    class Meta:
        verbose_name = _("Tableau amortissement")

    def __str__(self):
        return '{0} - {1}'.format(self.id, self.loan)


class Supplier(ContactGeneric, UserGeneric):
    credit = models.OneToOneField(Credit, on_delete=models.CASCADE,
                                  verbose_name=_("Crédit"),
                                  blank=True, null=True)
    loan = models.OneToOneField(Loan, on_delete=models.CASCADE,
                                  verbose_name=_("Emprunt"),
                                  blank=True, null=True)
    company = models.CharField(_("Société"), max_length=100)
    expcat = models.ManyToManyField(ExpCat, verbose_name=_("Catégorie"))
    contact_name = models.CharField(max_length=50,
                                    verbose_name=_("Personne de contact"),
                                    blank=True, null=True)
    remarks = models.CharField(max_length=255,
                               verbose_name=_("Remarque"),
                               blank=True, null=True)
    email = models.EmailField(_("Email"), max_length=100,
                              blank=True, null=True)
    vatnr = models.CharField(_("Nr entreprise"), max_length=50,
                             blank=True, null=True)
    classification = models.CharField(max_length=3,
                                      verbose_name=_("Classification fournisseur"),
                                      blank=True, null=True)
    objects = UserManager()

    class Meta:
        verbose_name = _("Fournisseur")
        unique_together = ('profile', 'company')
        ordering = ('company',)

    def get_absolute_url(self):
        return reverse('supplier_detail', args=[str(self.id)])

    def get_expcat(self):
        return ", ".join([str(a.name) for a in self.expcat.all()])

    def get_expcatgrp(self):
        return ", ".join([str(a.group.name) for a in self.expcat.all()])

    def get_title(self):
        return "{0} {1}".format(self._meta.verbose_name, self.company)

    def get_email_display(self):
        if self.email is not None:
            return "Email: {}".format(self.email)
        else:
            return ""

    def get_vat_display(self):
        if self.vatnr is not None:
            return "Nr TVA: {}".format(self.vatnr)
        else:
            return ""

    def get_verbose_name(self, lookup):
        # will return first non relational field's verbose_name in lookup
        # https://stackoverflow.com/questions/40176689/django-orm-get-verbose-name-of-field-via-filter-lookup
        model = self
        if LOOKUP_SEP in lookup:
            for part in lookup.split(LOOKUP_SEP):
                try:
                    f = model._meta.get_field(part)
                except FieldDoesNotExist:
                    # check if field is related
                    for f in model._meta.related_objects:
                        if f.get_accessor_name() == part:
                            break
                    else:
                        raise ValueError("Invalid lookup string")
                if f.is_relation:
                    model = f.related_model
                    continue
                return force_text(f.verbose_name)
        else:
            return model._meta.get_field(lookup).verbose_name.title()

    def __str__(self):
        return self.company


class Client(ContactGeneric, UserGeneric):
    CLIENTTYPE_LIST = (
        ('B2B', _('Intermédiaire (B2B)')),
        ('B2C', _('Consommateur final (B2C)'))
    )
    credit = models.OneToOneField(Credit, on_delete=models.CASCADE,
                                  verbose_name=_("Crédit"),
                                  blank=True, null=True)
    loan = models.OneToOneField(Loan, on_delete=models.CASCADE,
                                  verbose_name=_("Emprunt"),
                                  blank=True, null=True)
    company = models.CharField(_("Client"), max_length=100)
    contact_name = models.CharField(max_length=50,
                                    verbose_name=_("Personne de contact"),
                                    blank=True, null=True)
    clienttype = models.CharField(max_length=3, verbose_name=_("Type"),
                                  choices=CLIENTTYPE_LIST,
                                  default="B2C")
    remarks = models.CharField(max_length=255, verbose_name=_("Remarque"),
                               blank=True, null=True)
    email = models.EmailField(_("Email"), max_length=100,
                              blank=True, null=True)
    vatnr = models.CharField(_("Nr entreprise"), max_length=50,
                             blank=True, null=True)
    classification = models.CharField(max_length=3,
                                      verbose_name=_("Classification client"),
                                      blank=True, null=True)
    objects = UserManager()

    class Meta:
        verbose_name = _("Client")
        unique_together = ('profile', 'company')
        ordering = ('company',)

    def get_absolute_url(self):
        return reverse('client_detail', args=[str(self.id)])

    def get_title(self):
        return "{0} {1}".format(self._meta.verbose_name, self.company)

    def get_email_display(self):
        if self.email is not None:
            return "Email: {}".format(self.email)
        else:
            return ""

    def get_vat_display(self):
        if self.vatnr is not None:
            return "Nr TVA: {}".format(self.vatnr)
        else:
            return ""

    def get_verbose_name(self, lookup):
        # will return first non relational field's verbose_name in lookup
        # https://stackoverflow.com/questions/40176689/django-orm-get-verbose-name-of-field-via-filter-lookup
        model = self
        if LOOKUP_SEP in lookup:
            for part in lookup.split(LOOKUP_SEP):
                try:
                    f = model._meta.get_field(part)
                except FieldDoesNotExist:
                    # check if field is related
                    for f in model._meta.related_objects:
                        if f.get_accessor_name() == part:
                            break
                    else:
                        raise ValueError("Invalid lookup string")
                if f.is_relation:
                    model = f.related_model
                    continue
                return force_text(f.verbose_name)
        else:
            return model._meta.get_field(lookup).verbose_name.title()

    def __str__(self):
        return self.company


class Product(UserGeneric):
    atelier = models.ForeignKey(Atelier, on_delete=models.CASCADE,
                                verbose_name=_("Atelier"))
    name = models.CharField(_("Produit"), max_length=255)
    processed = models.BooleanField(default=False,
                                    verbose_name=_("Produit transformé?"))
    buysell = models.BooleanField(default=False,
                                  verbose_name=_("Achat/Revente?"))
    buyprice = models.DecimalField(max_digits=16, decimal_places=9,
                                   verbose_name=_("Prix d'achat(€)"),
                                   blank=True, null=True)
    unit = models.ForeignKey(UnitList, on_delete=models.PROTECT,
                             verbose_name=_("Unité"),
                             blank=True, null=True)
    sellprice = models.DecimalField(
        max_digits=16, decimal_places=9,
        verbose_name=_("Prix de vente(€) HTVA"),
        blank=True, null=True)
    vat = models.ForeignKey(VatList, on_delete=models.PROTECT,
                            verbose_name="TVA", blank=True, null=True,
                            help_text=_("À encoder avant le montant TVAC ou HTVA"))
    sellpricevat = models.DecimalField(max_digits=16, decimal_places=9,
                                       verbose_name=_("Prix de vente(€) TVAC"),
                                       blank=True, null=True)
    objects = UserManager()

    class Meta:
        verbose_name = ("Produit")
        unique_together = ('profile', 'atelier', 'name')

    def get_absolute_url(self):
        return reverse('product_detail', args=[str(self.id)])

    # def save(self, *args, **kwargs):
    #     # Compute NO VAT if different 'Autre'
    #     if self.vat.id != 100 and self.sellpricevat is not None:
    #         if self.vat.id == 0:
    #             self.sellprice = self.sellpricevat
    #         else:
    #             self.sellprice = self.sellpricevat - (decimal.Decimal(self.vat.percent) / 100 * self.sellpricevat)
    #     super().save(*args, **kwargs)

    def get_title(self):
        return "{0} {1}".format(self._meta.verbose_name, self.name)

    def get_verbose_name(self, lookup):
        # will return first non relational field's verbose_name in lookup
        # https://stackoverflow.com/questions/40176689/django-orm-get-verbose-name-of-field-via-filter-lookup
        model = self
        if LOOKUP_SEP in lookup:
            for part in lookup.split(LOOKUP_SEP):
                try:
                    f = model._meta.get_field(part)
                except FieldDoesNotExist:
                    # check if field is related
                    for f in model._meta.related_objects:
                        if f.get_accessor_name() == part:
                            break
                    else:
                        raise ValueError("Invalid lookup string")
                if f.is_relation:
                    model = f.related_model
                    continue
                return force_text(f.verbose_name)
        else:
            return model._meta.get_field(lookup).verbose_name.title()

    def __str__(self):
        return self.name


class RevCatGrp(models.Model):
    id = models.CharField(max_length=3, primary_key=True)
    name = models.CharField(max_length=50, verbose_name=_("Revenu"))

    class Meta:
        verbose_name = _("Entrée Groupe")
        ordering = ['name']

    def __str__(self):
        return '{0}'.format(self.name)


class RevCat(models.Model):
    id = models.CharField(max_length=7, primary_key=True)
    group = models.ForeignKey(RevCatGrp, on_delete=models.PROTECT,
                            verbose_name=_("Groupe"))
    name = models.CharField(max_length=50, verbose_name=_("Catégorie"))
    categclass = models.ForeignKey(CategClass, on_delete=models.PROTECT,
                            verbose_name=_("Fixe/Variable"), blank=True, null=True)
    comptaplan = models.ForeignKey(ComptaPlan, on_delete=models.PROTECT,
                            verbose_name=_("Plan comptable"),
                            blank=True, null=True)
    comptaagri = models.ForeignKey(ComptaAgri, on_delete=models.PROTECT,
                            verbose_name=_("Comptabilité agricole"),
                            blank=True, null=True)
    rentatxt = models.CharField(max_length=255,
                                verbose_name=_("Calcul rentabilité"),
                                blank=True, null=True)
    tresotxt = models.CharField(max_length=255,
                                verbose_name=_("Calcul trésorerie"),
                                blank=True, null=True)                            
    renta_exclude = models.BooleanField(default=False,
                            verbose_name=_("Exclu du calcul rentabilité"))

    def get_verbose_name(self, lookup):
        # will return first non relational field's verbose_name in lookup
        # https://stackoverflow.com/questions/40176689/django-orm-get-verbose-name-of-field-via-filter-lookup
        model = self
        if LOOKUP_SEP in lookup:
            for part in lookup.split(LOOKUP_SEP):
                try:
                    f = model._meta.get_field(part)
                except FieldDoesNotExist:
                    # check if field is related
                    for f in model._meta.related_objects:
                        if f.get_accessor_name() == part:
                            break
                    else:
                        raise ValueError("Invalid lookup string")
                if f.is_relation:
                    model = f.related_model
                    continue
                return force_text(f.verbose_name)
        else:
            return model._meta.get_field(lookup).verbose_name.title()

    class Meta:
        verbose_name = _("Entrée Catégorie")
        ordering = ['group', 'name']

    def __str__(self):
        return '{0}'.format(self.name)


class Fix(models.Model):
    refshort = models.CharField(max_length=4, verbose_name=_("Étiquette (id) 4 caractères"))
    refdesc = models.CharField(max_length=100,
                               blank=True, null=True,
                               verbose_name=_("Descriptif"))
    atelier = models.ForeignKey(Atelier, on_delete=models.CASCADE,
                                verbose_name=_("Atelier"))
    amountvat = models.DecimalField(max_digits=16, decimal_places=9,
                                    verbose_name=_("Montant(€) périodique TVAC"))
    vat = models.ForeignKey(VatList, on_delete=models.PROTECT,
                            verbose_name="TVA",
                            help_text=_("À encoder avant le montant TVAC ou HTVA"))
    amount = models.DecimalField(max_digits=16, decimal_places=9,
                                 verbose_name=_("Montant(€) périodique HTVA"))
    begindate = models.DateField(verbose_name="Date début")
    enddate = models.DateField(verbose_name="Date fin")
    enddate_old = models.DateField(verbose_name="Date fin old", blank=True, null=True)
    yearfreq = models.ForeignKey(YearFreqList, on_delete=models.PROTECT, default=12,
                                 verbose_name=_("Nombre de paiements par an"),
                                 help_text=_("12 => Mensuel, 2 => semestriel, 3 => Quadrimestre, 4 => trimestriel"))
    validated = models.BooleanField(default=False, verbose_name=_("Validé?"))
    objects = UserManager()

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super(Fix, self).__init__(*args, **kwargs)
        self.__original_enddate = self.enddate

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        """
        If enddate has changed, keep enddate_old to be able to manage
        views.utils_crud.update_fix_into_exprev
        """
        if self.enddate != self.__original_enddate:
            self.enddate_old = self.__original_enddate
        super(Fix, self).save(force_insert, force_update, *args, **kwargs)
        self.__original_enddate = self.enddate

    def diff_date_year(self):
        """Substract begindate from endate and retun value in year with 1 decimal"""
        diff = self.enddate - self.begindate
        return round(diff.days/365, 1)

    def get_title(self):
        """Used for title display"""
        return "{0} {1}".format(self._meta.verbose_name, self.refdesc)

    def __str__(self):
        return "{0}".format(self.refshort)


class FixExpense(UserGeneric, Fix):
    expcatgrp = models.ForeignKey(ExpCatGrp, on_delete=models.PROTECT,
                                  verbose_name=_("Groupe"))
    expcat = models.ForeignKey(ExpCat, on_delete=models.PROTECT,
                               verbose_name=_("Catégorie"),
                               blank=True, null=True)
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE,
                                 verbose_name=_("Fournisseur"))

    class Meta:
        verbose_name = _("Sortie récurrente")
        unique_together = ('profile', 'refshort')

    def get_absolute_url(self):
        return reverse('fixexpense_detail', args=[str(self.id)])


class FixRevenue(UserGeneric, Fix):
    revcatgrp = models.ForeignKey(RevCatGrp, on_delete=models.PROTECT,
                                  verbose_name=_("Groupe"))
    revcat = models.ForeignKey(RevCat, on_delete=models.PROTECT,
                               verbose_name=_("Categorie"))
    client = models.ForeignKey(Client, on_delete=models.CASCADE,
                               verbose_name=_("Client"))

    class Meta:
        verbose_name = _("Entrée récurrente")
        unique_together = ('profile', 'refshort')

    def get_absolute_url(self):
        return reverse('fixrevenue_detail', args=[str(self.id)])


class Revenue(UserGeneric):
    # 18/03/2019 if delete form Loan => delete all records in Expense
    loan = models.ForeignKey(Loan,
                             on_delete=models.CASCADE,
                             verbose_name=_("Emprunt"),
                             blank=True, null=True)                            
    creditrfd = models.OneToOneField(CreditRefund, on_delete=models.CASCADE,
                                  verbose_name=_("Prélèvement crédit"),
                                  blank=True, null=True)
    fixrevenue = models.ForeignKey(FixRevenue,
                                   on_delete=models.CASCADE,
                                   verbose_name=_("Récurrent"),
                                   blank=True, null=True)
    invnr = models.CharField(_("Identifiant unique"), max_length=50)
    inputdate = models.DateField(_("Date"))
    atelier = models.ForeignKey(Atelier, on_delete=models.CASCADE,
                                verbose_name=_("Atelier"))
    revcatgrp = models.ForeignKey(RevCatGrp, on_delete=models.PROTECT,
                                  verbose_name=_("Groupe"))
    revcat = models.ForeignKey(RevCat, on_delete=models.PROTECT,
                               verbose_name=_("Categorie"))
    client = models.ForeignKey(Client, on_delete=models.CASCADE,
                               verbose_name=_("Client"),
                               help_text=_("Encodez rapidement les premières lettres pour réduire la liste"))
    refdesc = models.CharField(_("Descriptif"), max_length=255,
                               blank=True, null=True)
    amountvat = models.DecimalField(max_digits=16, decimal_places=9,
                                    verbose_name=_("Montant total (€) TVAC"))
    vat = models.ForeignKey(VatList, on_delete=models.PROTECT,
                            verbose_name="TVA %", default=0,
                            help_text=_("À encoder avant le montant TVAC ou HTVA"))
    vatvalue = models.DecimalField(max_digits=16, decimal_places=9,
                                    verbose_name=_("Valeur TVA (€)"),
                                    blank=True, null=True)
    amount = models.DecimalField(max_digits=16, decimal_places=9,
                                 verbose_name=_("Montant total (€) HTVA"))
    duedatelist = models.ForeignKey(DueDateList, on_delete=models.PROTECT,
                                    verbose_name=_("Échéance"),
                                    default="30D")
    payduedate = models.DateField(_("À recevoir au plus tard le"))
    lastpaydate = models.DateField(_("Date dernier paiement reçu"),
                                  blank=True, null=True)
    payamount = models.DecimalField(max_digits=16, decimal_places=9,
                                    verbose_name=_("Déjà reçu(€)"),
                                    default=0)
    payflag = models.BooleanField(default=False, verbose_name=_("Payé?"),
                                  help_text=_("Cocher cette case pour préciser que l'intégralité du montant a déjà été réceptionné."))
    locked = models.BooleanField(default=False, verbose_name=_("Facturé?"))
    aide_minimi = models.BooleanField(default=False, verbose_name=_("Aide Minimi?"))
    invoice_send = models.BooleanField(default=False, verbose_name=_("Facture envoyée?"))
    objects = UserManager()

    class Meta:
        verbose_name = _("Entrée")
        unique_together = ('profile', 'invnr')
        # indexes = [models.Index(fields=["year"], name="revenue_year_idx")]

    def get_absolute_url(self):
        if self.revcat.id in ("FRM_BDC", "FRM_DEV"):
            url = reverse('bdc_detail', args=[str(self.id)])
        else:
            url = reverse('revenue_detail', args=[str(self.id)])
        return url
    
    def get_days(self):
        if self.lastpaydate is not None:
            diff = self.lastpaydate - self.payduedate
        else:
            diff = date.today() - self.payduedate
        nbrdays = diff.days
        return nbrdays

    def get_solde(self):
        """Used in revenue_detail.html"""
        solde = 0
        if self.amountvat is None:
            return 0
        if self.payamount is None:
            solde = self.amountvat
        else:
            solde = self.amountvat - self.payamount
        return solde

    def get_solde_label(self):
        """Used in revenue_detail.html"""
        solde_label = _("Solde à payer")
        try:
            if self.amountvat is None:
                return _("Solde à payer")
            elif self.payamount is None:
                solde = self.amountvat
            else:
                solde = self.amountvat - self.payamount
            if solde <= 0:
                if solde == 0:
                    solde_lalel = _("Payé")
                else:
                    solde_lalel = _("À rembourser")
            else:
                solde_lalel = _("À recevoir")

        except Exception as xcpt:
            LOG_WARNING.warning(xcpt)
            return "Error"
        return solde_label

    def save(self, *args, **kwargs):
        # if self.revcatgrp.id != "FRM":
        #     self.locked = True

        if self.payflag and self.payamount == 0 and self.revcat.id not in ("FRM_BDC", "FRM_DEV", "FRM_FAC"):
            # montant payé et date dernier payement ne sont pris en compte que
            # si différend de BDC, pas encore de montant et payflag coché!
            self.payamount = self.amountvat
            self.lastpaydate = self.payduedate

        if self.payamount > 0.5:
            # On met à True que s'il y a déjà eu des paiements!
            if round(self.payamount, 2) >= round(self.amountvat, 2):
                self.payflag = True
            else:
                self.payflag = False
        super().save(*args, **kwargs)

    def get_title(self):
        if hasattr(self.revcat, 'name'):
            cat = ": {0}".format(self.revcat.name)
        else:
            cat = ""
        return "{0} {1}{2}".format(self._meta.verbose_name, self.invnr, cat)

    def get_verbose_name(self, lookup):
        # will return first non relational field's verbose_name in lookup
        # https://stackoverflow.com/questions/40176689/django-orm-get-verbose-name-of-field-via-filter-lookup
        model = self
        if LOOKUP_SEP in lookup:
            for part in lookup.split(LOOKUP_SEP):
                try:
                    f = model._meta.get_field(part)
                except FieldDoesNotExist:
                    # check if field is related
                    for f in model._meta.related_objects:
                        if f.get_accessor_name() == part:
                            break
                    else:
                        raise ValueError("Invalid lookup string")
                if f.is_relation:
                    model = f.related_model
                    continue
                return force_text(f.verbose_name)
        else:
            return model._meta.get_field(lookup).verbose_name.title()

    def __str__(self):
        return self.invnr


class RevenuePayment(UserGeneric):
    revenue = models.ForeignKey(Revenue,
                                on_delete=models.CASCADE,
                                verbose_name=_("Nr facture"))
    paydate = models.DateField(_("Date paiement"))
    amount = models.DecimalField(_("Montant reçu"),
                                 max_digits=10, decimal_places=2)
    objects = UserManager()

    class Meta:
        verbose_name = _("Paiement facture")
        ordering = ['paydate']

    def get_days(self):
        diff = self.paydate - self.revenue.payduedate
        return diff.days

    def get_days10(self):
        diff = self.paydate - self.revenue.payduedate
        return diff.days * 10

    def __str__(self):
        return '{0} - {1}'.format(self.revenue, self.paydate)


class RevenueProduct(UserGeneric):
    revenue = models.ForeignKey(Revenue,
                                on_delete=models.CASCADE,
                                verbose_name=_("Nr facture"),
                                blank=True, null=True)
    atelier = models.ForeignKey(Atelier,
                                on_delete=models.CASCADE,
                                verbose_name=_("Atelier"))
    product = models.ForeignKey(Product,
                                on_delete=models.CASCADE,
                                verbose_name=_("Produit"))
    lot = models.CharField(_("Lot"), max_length=50, blank=True, null=True)
    dlc = models.DateField(_("DLC"), blank=True, null=True)
    amountvat = models.DecimalField(max_digits=16, decimal_places=9,
                                    verbose_name=_("€ TVAC"))
    vat = models.ForeignKey(VatList, on_delete=models.PROTECT,
                            verbose_name="TVA", default=6,
                            help_text=_("À encoder avant le montant TVAC ou HTVA"))
    amount = models.DecimalField(max_digits=16, decimal_places=9,
                                 verbose_name=_("€ HTVA"))
    quantity = models.DecimalField(max_digits=10, decimal_places=2,
                                   verbose_name=_("Qt/Nbre"))
    unit = models.ForeignKey(UnitList, on_delete=models.PROTECT,
                             verbose_name=_("Unité"),
                             default='kg')
    amounttot = models.DecimalField(max_digits=16, decimal_places=9,
                                    verbose_name=_("Total(€) HTVA"),
                                    blank=True)
    amounttotvat = models.DecimalField(max_digits=16, decimal_places=9,
                                       verbose_name=_("Total(€) TVAC"),
                                       blank=True)
    objects = UserManager()

    class Meta:
        verbose_name = _("Détail bdc/devis/facture")

    def save(self, *args, **kwargs):
        # Compute NO VAT if different 'Autre'
        # if self.vat.id != 100:
        #     if self.vat.id == 0:
        #         self.amount = self.amountvat
        #     else:
        #         # self.amount = self.amountvat - (decimal.Decimal(self.vat.percent) / 100 * self.amountvat)
        #         self.amount = self.amountvat / ((100 + decimal.Decimal(self.vat.percent)) * 100)

        self.amounttot = self.amount * self.quantity
        self.amounttotvat = self.amountvat * self.quantity
        super().save(*args, **kwargs)

    def __str__(self):
        return '{0} - {1}'.format(self.revenue, self.product)


class Expense(UserGeneric):
    """
    If delete form Loan => delete all records in Expense
    Used for Crédit de caisse
    """
    NR10_LIST = (
        (1, 1), (2, 2), (3, 3), (4, 4), (5, 5),
        (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)
    )
    loan = models.ForeignKey(Loan, on_delete=models.CASCADE,
                             verbose_name=_("Emprunt"),
                             blank=True, null=True)
    creditrfd = models.OneToOneField(CreditRefund, on_delete=models.CASCADE,
                                  verbose_name=_("Remboursement crédit"),
                                  blank=True, null=True)
    # Used for insert/delete LoanRefund
    loanrfd = models.ForeignKey(LoanRefund, on_delete=models.CASCADE,
                                verbose_name=_("Tableau d'amortissement"),
                                blank=True, null=True)
    # If delete form FixExpense => delete all records in Expense
    fixexpense = models.ForeignKey(FixExpense,
                                   on_delete=models.CASCADE,
                                   verbose_name=_("Récurrent"),
                                   blank=True, null=True)
    expnr = models.CharField(max_length=50, verbose_name=_("Identifiant unique"))
    inputdate = models.DateField(verbose_name="Date")
    expcat = models.ForeignKey(ExpCat, on_delete=models.PROTECT,
                               verbose_name=_("Catégorie"))
    refdesc = models.CharField(_("Descriptif"), max_length=45,
                               blank=True, null=True)
    atelier = models.ForeignKey(Atelier, on_delete=models.CASCADE,
                                verbose_name=_("Atelier"))
    # if field2show 'quantity'
    quantity = models.DecimalField(max_digits=10, decimal_places=2,
                                   blank=True, null=True,
                                   verbose_name=_("Quantité/Nombre"))
    unit = models.ForeignKey(UnitList, on_delete=models.PROTECT,
                             blank=True, null=True,
                             verbose_name=_("Unité"))
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE,
                                 verbose_name=_("Fournisseur"),
                                 help_text=_("Encodez rapidement les premières lettres pour réduire la liste"))
    amortyear = models.PositiveSmallIntegerField(
                                choices=NR10_LIST,
                                null=True, blank=True,
                                verbose_name="Durée amortissement (année)")
    amountvat = models.DecimalField(max_digits=16, decimal_places=9,
                                    verbose_name=_("Montant(€) TVAC"))
    vat = models.ForeignKey(VatList, on_delete=models.PROTECT,
                            verbose_name="TVA (%)", default=0,
                            help_text=_("À encoder avant le montant TVAC ou HTVA"))
    vatvalue = models.DecimalField(max_digits=16, decimal_places=9,
                                   verbose_name=_("Valeur TVA (€)"),
                                   blank=True, null=True)
    amount = models.DecimalField(max_digits=16, decimal_places=9,
                                 verbose_name=_("Montant(€) HTVA"))
    duedatelist = models.ForeignKey(DueDateList, on_delete=models.PROTECT,
                                    verbose_name=_("Échéance"),
                                    default="30D")
    payduedate = models.DateField(_("À payer au plus tard le"))
    lastpaydate = models.DateField(_("Date dernier paiement reçu"),
                                   blank=True, null=True)
    payamount = models.DecimalField(max_digits=16, decimal_places=9,
                                    verbose_name=_("Déjà payé"),
                                    default=0)
    payflag = models.BooleanField(default=False, verbose_name=_("Payé?"),
                                  help_text=_("Cocher cette case pour préciser que l'intégralité du montant a déjà été payé."))
    objects = UserManager()

    class Meta:
        verbose_name = _("Sortie")
        unique_together = ('profile', 'expnr')

    def get_absolute_url(self):
        return reverse('expense_detail', args=[str(self.id)])

    def get_expcat(self):
        return ", ".join([str(a.name) for a in self.expcat.all()])

    def get_expcatgrp(self):
        return ", ".join([str(a.group.name) for a in self.expcat.all()])

    def get_days(self):
        if self.lastpaydate is not None:
            diff = self.lastpaydate - self.payduedate
        else:
            diff = date.today() - self.payduedate
        nbrdays = diff.days
        return nbrdays

    def get_solde(self):
        """Used in expense_detail.html"""
        solde = 0
        if self.amountvat is None:
            return 0
        if self.payamount is None:
            solde = self.amountvat
        else:
            solde = self.amountvat - self.payamount
        return solde

    def get_solde_label(self):
        """Used in expense_detail.html"""
        solde = 0
        try:
            if self.amountvat is None:
                solde_lalel = _("Solde à payer")
            if self.payamount is None:
                solde = self.amountvat
            else:
                solde = self.amountvat - self.payamount
            if solde <= 0:
                if solde == 0:
                    solde_lalel = _("Payé")
                else:
                    solde_lalel = _("Surpayé")
            else:
                solde_lalel = _("À payer")

        except Exception as xcpt:
            LOG_WARNING.warning(xcpt)
            solde_lalel = _("get_solde_label error")
        return solde_lalel

    def save(self, *args, **kwargs):
        # Compute NO VAT if different 'Autre'
        # if self.vat.id != 100 and self.amountvat is not None:
        #     if self.vat.id == 0:
        #         self.amount = self.amountvat
        #     else:
        #         self.amount = self.amountvat - (decimal.Decimal(self.vat.percent) / 100 * self.amountvat)
        if self.payflag and self.payamount == 0:
            self.payamount = self.amountvat
            self.lastpaydate = self.payduedate

        if self.payamount > 0.5:
            # On met à True que s'il y a déjà eu des paiements!
            if round(self.payamount, 2) >= round(self.amountvat, 2):
                self.payflag = True
            else:
                self.payflag = False
        super().save(*args, **kwargs)

    def get_title(self):
        if hasattr(self.expcat, 'name'):
            cat = ": {0}".format(self.expcat.name)
        else:
            cat = ""
        return "{0} {1}{2}".format(self._meta.verbose_name, self.expnr, cat)

    def get_verbose_name(self, lookup):
        # will return first non relational field's verbose_name in lookup
        # https://stackoverflow.com/questions/40176689/django-orm-get-verbose-name-of-field-via-filter-lookup
        model = self
        if LOOKUP_SEP in lookup:
            for part in lookup.split(LOOKUP_SEP):
                try:
                    f = model._meta.get_field(part)
                except FieldDoesNotExist:
                    # check if field is related
                    for f in model._meta.related_objects:
                        if f.get_accessor_name() == part:
                            break
                    else:
                        raise ValueError("Invalid lookup string")
                if f.is_relation:
                    model = f.related_model
                    continue
                return force_text(f.verbose_name)
        else:
            return model._meta.get_field(lookup).verbose_name.title()

    def __str__(self):
        return self.expnr


class ExpensePayment(UserGeneric):
    expense = models.ForeignKey(Expense,
                                on_delete=models.CASCADE,
                                verbose_name=_("Nr sortie"))
    paydate = models.DateField(_("Date paiement"))
    amount = models.DecimalField(_("Montant payé"),
                                 max_digits=10, decimal_places=2)
    objects = UserManager()

    class Meta:
        verbose_name = _("Paiement sortie")
        ordering = ['paydate']

    # def get_amount10(self):
    #     return int(self.amount/10)

    def get_days(self):
        diff = self.paydate - self.expense.payduedate
        return diff.days

    def get_days10(self):
        diff = self.paydate - self.expense.payduedate
        return diff.days * 10

    def __str__(self):
        return '{0} - {1}'.format(self.expense, self.paydate)


# @receiver(post_save, sender=Expense) DO NOT USE!!! Beter to register with connect
# if more than 1 signal django missmatch the instances!!!
def create_expense_payment(instance, created, **kwargs):
    """
    Direct insert paiments when payflag is True @ creation
    """
    # msg = "create_expense_payment, created: {0}, sender.payflag: {1}".format(created, sender.payflag)
    # LOG_DEBUG.debug(msg)
    if created and instance.payflag:
        try:
            ExpensePayment.objects.create(expense=instance,
                                        paydate=instance.payduedate,
                                        amount=instance.amountvat,
                                        profile=instance.profile)
        except Exception as xcpt:
            LOG_ERROR.error(xcpt)


# @receiver(post_save, sender=Revenue) #DO NOT USE!!! Beter to register with connect
def create_revenue_payment(instance, created, **kwargs):
    """
    Direct insert payments when payflag is True @ creation
    Do not insert when categ = BDC
    """
    # msg = "create_revenue_payment, created: {0}, instance.payflag: {1}".format(created, instance.payflag)
    # LOG.debug(msg)
    if hasattr(instance.revcat, 'id'):
        cat = instance.revcat.id
    else:
        cat = "NADA"
    if created and instance.payflag and cat != "FRM_BDC":
        RevenuePayment.objects.create(revenue=instance,
                                      paydate=instance.payduedate,
                                      amount=instance.amountvat,
                                      profile=instance.profile)


# https://docs.djangoproject.com/en/2.2/topics/signals/
post_save.connect(receiver=create_expense_payment, sender=Expense, weak=False)
post_save.connect(receiver=create_revenue_payment, sender=Revenue, weak=False)

# post_save.connect(receiver=update_lastpaydate, sender=ExpensePayment, weak=False)

class MonthlyPrevision(UserGeneric):
    # class Meta:
    #     db_table = 'mytable' # This tells Django where the SQL table is
    #     managed = False
    month = models.PositiveSmallIntegerField(verbose_name=_("Mois"))
    rev = models.DecimalField(max_digits=16, decimal_places=9,
                                verbose_name=_("Entrée (€)"))
    exp = models.DecimalField(max_digits=16, decimal_places=9,
                                    verbose_name=_("Sortie (€)"))
    solde = models.DecimalField(max_digits=16, decimal_places=9,
                                    verbose_name=_("Solde(€)"))
