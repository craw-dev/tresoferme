
--
-- Data for Name: money_revcatgrp; Type: TABLE DATA; Schema:  Owner: tresoadmin_demo
--

INSERT INTO money_revcatgrp (id, name) VALUES ('PRI', 'Prime');
INSERT INTO money_revcatgrp (id, name) VALUES ('COM', 'Revenu complémentaire');
INSERT INTO money_revcatgrp (id, name) VALUES ('FRM', 'Produit de la ferme');
INSERT INTO money_revcatgrp (id, name) VALUES ('EMP', 'Emprunts');
INSERT INTO money_revcatgrp (id, name) VALUES ('TAR', 'TVA à recevoir');
INSERT INTO money_revcatgrp (id, name) VALUES ('IMR', 'Remboursement impôts');
INSERT INTO money_revcatgrp (id, name) VALUES ('FIN', 'Produits financiers');
INSERT INTO money_revcatgrp (id, name) VALUES ('TRE', 'Trésorerie de départ');
INSERT INTO money_revcatgrp (id, name) VALUES ('MEN', 'Revenu privé');
INSERT INTO money_revcatgrp (id, name) VALUES ('LOI', 'Remboursement lois sociales');
INSERT INTO money_revcatgrp (id, name) VALUES ('CAP', 'Aides en capital et intérêt');
INSERT INTO money_revcatgrp (id, name) VALUES ('CRE', 'Crédits');


--
-- PostgreSQL database dump complete
--

