
INSERT INTO money_categclass (id, name) VALUES ('FIX', 'Fixe');
INSERT INTO money_categclass (id, name) VALUES ('VAR', 'Variable');
INSERT INTO money_categclass (id, name) VALUES ('AMO', 'Amortissement');
INSERT INTO money_categclass (id, name) VALUES ('TVA', 'TVA');
INSERT INTO money_categclass (id, name) VALUES ('PRI', 'Privé');
INSERT INTO money_categclass (id, name) VALUES ('REC', 'Recette');


--
-- Name: money_categclass_id_seq; Type: SEQUENCE SET; Schema:  Owner: tresoadmin_demo
--

--SELECT pg_catalog.setval('money_categclass_id_seq', 1, 0);


--
-- PostgreSQL database dump complete
--

