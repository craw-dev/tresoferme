--
-- Data for Name: money_yearfreqlist; Type: TABLE DATA; Schema:  Owner: tresoadmin_demo
--

INSERT INTO money_yearfreqlist (id, name, nbrmonth) VALUES (1, '1 => annuel', 12);
INSERT INTO money_yearfreqlist (id, name, nbrmonth) VALUES (2, '2 => semestriel', 6);
INSERT INTO money_yearfreqlist (id, name, nbrmonth) VALUES (3, '3 => quadrimestriel', 4);
INSERT INTO money_yearfreqlist (id, name, nbrmonth) VALUES (4, '4 => trimestriel', 3);
INSERT INTO money_yearfreqlist (id, name, nbrmonth) VALUES (6, '6 => bi-mensuel', 2);
INSERT INTO money_yearfreqlist (id, name, nbrmonth) VALUES (12, '12 => mensuel', 1);
INSERT INTO money_yearfreqlist (id, name, nbrmonth) VALUES (0, 'Pas d''application', 0);


--
-- PostgreSQL database dump complete
--

