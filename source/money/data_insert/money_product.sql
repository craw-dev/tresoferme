
--
-- Data for Name: money_product; Type: TABLE DATA; Schema:  Owner: tresoadmin_demo
--

INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (24, 'Paniers de légumes', 0, NULL, 1146.70, 1146.70, 13, 8, 'nr', 100, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (25, 'panier', 0, NULL, NULL, NULL, 14, 10, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (20, 'Beurre', 1, NULL, 7.99, 8.50, 21, 3, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (21, 'Fromage frais', 1, NULL, 6.86, 7.30, 21, 3, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (22, 'Fromage aux herbe', 1, NULL, 13.16, 14.00, 21, 3, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (26, 'Lait bio entier', 0, NULL, 0.42, 0.45, 21, 3, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (27, 'Lait bio écrémé', 0, NULL, 0.24, 0.25, 21, 3, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (28, 'Produit test', 1, NULL, 12.22, 13.00, 21, 3, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (29, 'Fromage XXX', 1, NULL, 14.57, 15.50, 21, 3, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (31, 'Panier de fromage de vache mixte', 1, NULL, 11.75, 12.50, 23, 24, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (32, 'panier mixte fromage', 1, NULL, 23.50, 25.00, 23, 24, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (33, 'panier mixte (pâte dure et frais)', 1, NULL, 11.75, 12.50, 23, 24, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (34, 'Maquée', 1, NULL, 18.10, 19.25, 25, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (35, 'Maquée brebis', 1, NULL, 21.31, 22.67, 27, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (36, 'Maquée brebis fines herbes', 1, NULL, 21.31, 22.67, 27, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (37, 'Maquée vache', 1, NULL, 8.65, 9.20, 23, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (38, 'Fromage rapé', 1, NULL, 19.74, 21.00, 23, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (39, 'Crème glacée', 1, NULL, 6.11, 6.50, 23, 24, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (40, 'Lait cru', 0, NULL, 1.08, 1.15, 23, 24, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (41, 'crème fraiche', 1, NULL, 14.80, 15.75, 23, 24, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (42, 'Yaourt nature', 1, NULL, 8.25, 8.78, 25, 24, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (43, 'Yaourt fruits chèvre', 1, NULL, 8.25, 8.78, 25, 24, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (44, 'Yaourt nature brebis', 1, NULL, 12.01, 12.78, 27, 24, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (45, 'Yaourt nature vache', 1, NULL, 6.99, 7.44, 23, 24, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (46, 'Yaourt fruit vache', 1, NULL, 6.99, 7.44, 23, 24, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (47, 'Lait cru brebis', 0, NULL, 2.21, 2.35, 27, 24, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (48, 'Lait cru chèvre', 0, NULL, 1.97, 2.10, 25, 24, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (49, 'yaourt à boire vache fruit', 1, NULL, 8.46, 9.00, 23, 24, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (50, 'Yaourt boire chèvre', 1, NULL, 9.21, 9.80, 25, 24, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (51, 'lard fumé', 1, NULL, 18.33, 19.50, 29, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (52, 'Noix de jambon', 1, NULL, 22.09, 23.50, 29, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (53, 'jambon cuit', 1, NULL, 25.85, 27.50, 29, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (54, 'saucisse sèche', 1, NULL, 2.35, 2.50, 29, 24, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (55, 'cervelas maison', 1, NULL, 18.80, 20.00, 29, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (56, 'lasagne', 1, NULL, 12.42, 13.21, 24, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (57, 'boulette sauce tomate', 1, NULL, 5.64, 6.00, 32, 24, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (58, 'Pain de viande', 1, NULL, 16.92, 18.00, 29, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (59, 'burger porc/boeuf', 1, NULL, 4.70, 5.00, 29, 24, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (60, 'saucisse', 1, NULL, 16.92, 18.00, 29, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (61, 'Boudin blanc', 1, NULL, 15.65, 16.65, 29, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (62, 'haché porc et boeuf', 1, NULL, 16.45, 17.50, 29, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (63, 'colis boeuf 3 kg', 1, NULL, 45.12, 48.00, 32, 24, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (64, 'Steack', 1, NULL, 26.13, 27.80, 32, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (65, 'carbonnade', 1, NULL, 16.26, 17.30, 32, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (66, 'Roti boeuf', 1, NULL, 26.60, 28.30, 32, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (67, 'Entrecote', 1, NULL, 27.73, 29.50, 32, 24, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (68, 'New produit', 1, NULL, 13.16, 14.00, 21, 3, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (69, 'Fromages', 1, NULL, NULL, NULL, 38, 33, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (70, 'viande', 1, NULL, NULL, NULL, 38, 33, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (71, 'oeufs', 0, NULL, NULL, NULL, 42, 33, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (72, 'Porc', 0, NULL, 3.29, 3.50, 47, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (74, 'boeuf', 0, NULL, NULL, NULL, 46, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (75, 'cuir de taureau', 0, NULL, 20.00, 20.00, 46, 34, 'nr', 0, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (76, 'cuir de vache', 0, NULL, 23.00, 23.00, 46, 34, 'nr', 0, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (77, 'peau de broutard', 0, NULL, 5.00, 5.00, 46, 34, 'nr', 0, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (78, 'lait', 0, NULL, 0.85, 0.90, 45, 34, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (79, 'lait battu', 0, NULL, 0.75, 0.80, 45, 34, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (80, 'fromage blanc', 1, NULL, 1.40, 1.49, 45, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (81, 'yoghourt 500g', 1, NULL, 1.40, 1.49, 45, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (82, 'Yoghourt 400 gr', 1, NULL, 1.13, 1.20, 45, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (83, 'crème', 1, NULL, 1.40, 1.49, 45, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (84, 'beurre', 1, NULL, 7.71, 8.20, 45, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (30, 'Beurre', 1, NULL, 12.03, 12.80, 23, 24, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (88, 'poulet entier', 0, NULL, 11.83, 12.59, 49, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (89, 'poulet découpé', 1, NULL, 15.79, 16.80, 49, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (90, 'canard', 0, NULL, 22.56, 24.00, 49, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (91, 'lapin', 0, NULL, 15.98, 17.00, 49, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (92, 'Pintade', 0, NULL, 12.78, 13.60, 49, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (93, 'porc au kilo', 1, NULL, 3.76, 4.00, 47, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (94, 'boeuf au kilo', 1, NULL, 4.70, 5.00, 46, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (95, 'pommes', 0, NULL, 1.60, 1.70, 50, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (96, 'jus de pomme', 1, NULL, 1.60, 1.70, 50, 34, 'l', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (97, 'farine de froment', 1, NULL, 1.19, 1.27, 51, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (73, 'oeufs', 0, NULL, 0.24, 0.25, 48, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (85, 'brie fermier', 1, NULL, 11.75, 12.50, 45, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (98, 'farine epeautre', 1, NULL, 1.99, 2.12, 51, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (100, 'mouton au kg', 0, NULL, 6.58, 7.00, 52, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (86, 'Petit fermier', 1, NULL, 13.16, 14.00, 45, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (87, 'tomme fermière', 1, NULL, 12.95, 13.78, 45, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (101, 'lin', 0, NULL, 0.28, 0.30, 53, 3, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (102, 'Maïs', 0, NULL, 0.42, 0.45, 53, 3, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (103, 'Pomme de terre', 0, NULL, 0.24, 0.25, 53, 3, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (99, 'poules de réforme', 0, NULL, 5.24, 5.57, 48, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (104, 'yoghourt 100gr', 1, NULL, 0.28, 0.30, 45, 34, 'nr', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (105, 'pdt', 0, NULL, 0.70, 0.75, 50, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (106, 'légumes divers', 0, NULL, 4.98, 5.30, 50, 34, 'kg', 6, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (107, 'Service', 0, NULL, 592.50, 750.00, 20, 21, 'nr', 21, 0);
INSERT INTO money_product (id, name, processed, buyprice, sellprice, sellpricevat, atelier_id, profile_id, unit_id, vat_id, buysell) VALUES (108, 'Houblon', 0, NULL, 47.00, 50.00, 35, 17, 'kg', 6, 0);


--
-- Name: money_product_id_seq; Type: SEQUENCE SET; Schema:  Owner: tresoadmin_demo
--

--SELECT pg_catalog.setval('money_product_id_seq', 108, 1);


--
-- PostgreSQL database dump complete
--

