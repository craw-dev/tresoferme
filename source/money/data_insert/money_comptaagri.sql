
--
-- Data for Name: money_comptaagri; Type: TABLE DATA; Schema:  Owner: tresoadmin_demo
--

INSERT INTO money_comptaagri (id, name) VALUES (1, 'Aides à la production et au revenu');
INSERT INTO money_comptaagri (id, name) VALUES (2, 'Aides en capital et en intérêts (invesitssement et installation)');
INSERT INTO money_comptaagri (id, name) VALUES (3, 'Autres produits de l’exploitation ');
INSERT INTO money_comptaagri (id, name) VALUES (4, 'Charges (de structure ) calculées  ');
INSERT INTO money_comptaagri (id, name) VALUES (5, 'Charges de structure réelles  amortissement économique');
INSERT INTO money_comptaagri (id, name) VALUES (6, 'Charges de structure réelles  sauf intérêt, armotissement et exceptionnelle');
INSERT INTO money_comptaagri (id, name) VALUES (7, 'Charges de structure réelles exceptionnelles');
INSERT INTO money_comptaagri (id, name) VALUES (8, 'Charges de structure réelles intérêt brut');
INSERT INTO money_comptaagri (id, name) VALUES (9, 'Charges opérationnelles affectées');
INSERT INTO money_comptaagri (id, name) VALUES (10, 'Charges opérationnelles non affectées');
INSERT INTO money_comptaagri (id, name) VALUES (11, 'Emprunt (apport)  (ind. Financier)');
INSERT INTO money_comptaagri (id, name) VALUES (12, 'Intérêt brut');
INSERT INTO money_comptaagri (id, name) VALUES (13, 'Investissement (ind. Financier)');
INSERT INTO money_comptaagri (id, name) VALUES (14, 'Produits des activités');
INSERT INTO money_comptaagri (id, name) VALUES (15, 'Produits exceptionnels');
INSERT INTO money_comptaagri (id, name) VALUES (16, 'Régularisation');
INSERT INTO money_comptaagri (id, name) VALUES (17, 'Remboursement du capital emprunté  (ind. Financier)');


--
-- Name: money_comptaagri_id_seq; Type: SEQUENCE SET; Schema:  Owner: tresoadmin_demo
--

-- SELECT pg_catalog.setval('money_comptaagri_id_seq', 17, 1);


--
-- PostgreSQL database dump complete
--

