
INSERT INTO money_duedatelist (id, name, daysadd) VALUES ('NOW', 'Immédiat', 0);
INSERT INTO money_duedatelist (id, name, daysadd) VALUES ('15D', '15 jours', 15);
INSERT INTO money_duedatelist (id, name, daysadd) VALUES ('30D', '30 jours', 30);
INSERT INTO money_duedatelist (id, name, daysadd) VALUES ('30DM', '30 jours fin de mois', 30);
INSERT INTO money_duedatelist (id, name, daysadd) VALUES ('USR', 'Manuel', 1);


--
-- PostgreSQL database dump complete
--

