﻿-- money_unitlist
delete from money_unitlist;
COPY money_unitlist(id, name)
FROM '/home/pat/dev/pipenv/agnew/source/money/data_insert/csv/unit_list.csv' DELIMITER ',' CSV HEADER;

-- money_vatlist
delete from money_vatlist;
COPY money_vatlist(id, percent, name)
FROM '/home/pat/dev/pipenv/agnew/source/money/data_insert/csv/vat_list.csv' DELIMITER ',' CSV HEADER;

-- money_expcatgrp
delete from money_expcatgrp;
COPY money_expcatgrp(id, name)
FROM '/home/pat/dev/pipenv/agnew/source/money/data_insert/csv/expcatgrp.csv' DELIMITER ',' CSV HEADER;

-- money_expcat
delete from money_expcat;
COPY money_expcat(group_id, block, name, field2show)
FROM '/home/pat/dev/pipenv/agnew/source/money/data_insert/csv/expcat.csv' DELIMITER ',' CSV HEADER;

-- money_revcatgrp
delete from money_revcatgrp;
COPY money_revcatgrp(id, name)
FROM '/home/pat/dev/pipenv/agnew/source/money/data_insert/csv/revcatgrp.csv' DELIMITER ',' CSV HEADER;

-- money_revcat
delete from money_revcat;
COPY money_revcat(group_id, name)
FROM '/home/pat/dev/pipenv/agnew/source/money/data_insert/csv/revcat.csv' DELIMITER ',' CSV HEADER;

-- client
delete from money_client;
COPY money_client(profile_id, company)
FROM '/home/pat/dev/pipenv/agnew/source/money/data_insert/csv/client.csv' DELIMITER ',' CSV HEADER;

select * from capob_atelier

-- supplier
delete from money_supplier;
COPY money_supplier(profile_id, category_id, company)
FROM '/home/pat/dev/pipenv/agnew/source/money/data_insert/csv/supplier.csv' DELIMITER ',' CSV HEADER;

-- product
delete from money_product;
COPY money_product(atelier_id, profile_id, name, costprice, unit_id, sellprice, vat_id, sellpricevat)
FROM '/home/pat/dev/pipenv/agnew/source/money/data_insert/csv/product.csv' DELIMITER ',' CSV HEADER;

-- loan
delete from money_loan;
COPY money_loan(profile_id, bankrefid, bankrefdesc, loanamount, signupdate, startdate, loanyears, nbrpaymentyear, interestrate, amortyears)
FROM '/home/pat/dev/pipenv/agnew/source/money/data_insert/csv/loan.csv' DELIMITER ',' CSV HEADER;
