--
-- Data for Name: money_unitlist; Type: TABLE DATA; Schema:  Owner: tresoadmin_demo
--

INSERT INTO money_unitlist (id, name) VALUES ('kg', 'kg');
INSERT INTO money_unitlist (id, name) VALUES ('l', 'litre');
INSERT INTO money_unitlist (id, name) VALUES ('nr', 'nombre');
INSERT INTO money_unitlist (id, name) VALUES ('m3', 'm3');
INSERT INTO money_unitlist (id, name) VALUES ('h', 'heure');


--
-- PostgreSQL database dump complete
--

