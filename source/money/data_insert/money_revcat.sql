--
-- Data for Name: money_revcat; Type: TABLE DATA; Schema:  Owner: tresoadmin_demo
--

INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('PRI_BIO', 'Bio', 'PRI', 1, 758, 'REC', 0, 'Montant total', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('PRI_DIV', 'Divers', 'PRI', 1, 758, 'REC', 0, 'Montant total', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('PRI_MAE', 'MAE', 'PRI', 1, 758, 'REC', 0, 'Montant total', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('PRI_RED', 'Redistributif', 'PRI', 1, 758, 'REC', 0, 'Montant total', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('PRI_COU', 'Soutien Couplé', 'PRI', 1, 758, 'REC', 0, 'Montant total', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('PRI_VER', 'Verdissement', 'PRI', 1, 376, 'REC', 0, 'Montant total', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('CAP_AUI', 'Subventions sur intérêt autre que matériel', 'CAP', 2, 39, 'REC', 0, 'Montant fractionné sur 5 ou 7 ans', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('PRI_INV', 'Subventions sur intérêt pour matériel', 'CAP', 2, 39, 'REC', 0, 'Montant fractionné sur 5 ou 7 ans', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('CAP_MAC', 'Aide en capital pour matériel', 'CAP', 2, 39, 'REC', 0, 'Montant fractionné sur 5 ou 7 ans', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('CAP_AUC', 'Aide en capital autre que matériel', 'CAP', 2, 39, 'REC', 0, 'Montant fractionné sur 5 ou 7 ans', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('MEN_IMP', 'Impôts sur le revenu (remboursement)', 'IMR', NULL, 819, 'REC', 0, 'Non pris en compte', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('MEN_SOC', 'Lois sociales (remboursement)', 'LOI', NULL, 553, 'REC', 0, 'Non pris en compte', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('MEN_SAL', 'Autres Salaires', 'MEN', NULL, 387, 'REC', 1, 'Non pris en compte', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('MEN_DIV', 'Divers - Revenus du ménage', 'MEN', NULL, 387, 'REC', 1, 'Non pris en compte', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('EMP_CAP', 'Capital fixe', 'EMP', NULL, 68, 'REC', 1, 'Non pris en compte', 'Montant total emprunté');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('EMP_PAY', 'Payement fixe', 'EMP', NULL, 68, 'REC', 1, 'Non pris en compte', 'Montant total emprunté');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('FRM_FAC', 'Facture', 'FRM', 14, 735, 'REC', 0, 'HTVA', 'TVAC');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('FRM_PAN', 'Panier/commande récurrente', 'FRM', 14, 735, 'REC', 0, 'HTVA', 'TVAC');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('FRM_VTE', 'Vente comptoir', 'FRM', 14, 735, 'REC', 0, 'HTVA', 'TVAC');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('COM_FRM', 'Accueil à la ferme', 'COM', 3, 735, 'REC', 0, 'HTVA', 'TVAC');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('COM_ACP', 'Activité complémentaire', 'COM', 3, 735, 'REC', 0, 'HTVA', 'TVAC');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('COM_ENE', 'Production énergie', 'COM', 3, 735, 'REC', 0, 'HTVA', 'TVAC');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('COM_TIR', 'Travaux pour tiers', 'COM', 3, 735, 'REC', 0, 'HTVA', 'TVAC');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('TRE_BAN', 'Solde compte bancaire', 'TRE', NULL, 420, 'REC', 0, 'Non pris en compte', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('TRE_CAI', 'Solde en caisse', 'TRE', NULL, 428, 'REC', 0, 'Non pris en compte', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('TAR_HOR', 'TVA hors investissement', 'TAR', NULL, 304, 'REC', 0, 'Non pris en compte', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('TAR_SUR', 'TVA sur investissement', 'TAR', NULL, 304, 'REC', 0, 'Non pris en compte', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('MEN_FIN', 'Revenus financiers', 'MEN', 15, 793, 'REC', 1, 'Non pris en compte', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('CRE_CAI', 'Crédit de caisse', 'CRE', NULL, 68, 'REC', 1, 'Non pris en compte', 'Montant total emprunté');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('CRE_STR', 'Straight loan', 'CRE', NULL, 68, 'REC', 1, 'Non pris en compte', 'Montant total emprunté');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('FIN_PRO', 'Produits financiers', 'FIN', 15, 793, NULL, 0, 'Montant total', 'Montant total');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('FRM_BDC', 'Bon de commande', 'FRM', 14, 735, 'REC', 1, 'Non pris en compte', 'Non pris en compte');
INSERT INTO money_revcat (id, name, group_id, comptaagri_id, comptaplan_id, categclass_id, renta_exclude, rentatxt, tresotxt) VALUES ('FRM_DEV', 'Devis', 'FRM', 14, 735, 'REC', 1, 'Non pris en compte', 'Non pris en compte');


--
-- PostgreSQL database dump complete
--

