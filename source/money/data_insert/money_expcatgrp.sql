--
-- Data for Name: money_expcatgrp; Type: TABLE DATA; Schema:  Owner: tresoadmin_demo
--

INSERT INTO money_expcatgrp (id, name) VALUES ('CUL', 'Culture');
INSERT INTO money_expcatgrp (id, name) VALUES ('ELE', 'Élevage');
INSERT INTO money_expcatgrp (id, name) VALUES ('MEN', 'Frais du ménage');
INSERT INTO money_expcatgrp (id, name) VALUES ('EMP', 'Emprunts');
INSERT INTO money_expcatgrp (id, name) VALUES ('VDI', 'Vente directe');
INSERT INTO money_expcatgrp (id, name) VALUES ('INV', 'Investissement');
INSERT INTO money_expcatgrp (id, name) VALUES ('SAL', 'Salaires et frais de personnel');
INSERT INTO money_expcatgrp (id, name) VALUES ('BAT', 'Entretien et réparation');
INSERT INTO money_expcatgrp (id, name) VALUES ('PET', 'Petit matériel et outillage');
INSERT INTO money_expcatgrp (id, name) VALUES ('MAT', 'Consommation');
INSERT INTO money_expcatgrp (id, name) VALUES ('FER', 'Fermage');
INSERT INTO money_expcatgrp (id, name) VALUES ('FIX', 'Frais généraux');
INSERT INTO money_expcatgrp (id, name) VALUES ('IMP', 'Impôt sur les revenus');
INSERT INTO money_expcatgrp (id, name) VALUES ('TVA', 'TVA à payer');
INSERT INTO money_expcatgrp (id, name) VALUES ('CRE', 'Crédits');


--
-- PostgreSQL database dump complete
--

