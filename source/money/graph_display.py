# -*- coding: utf-8 -*-
"""
Set of f
unctions used for graphic and culculations
"""
import math, json
import pandas as pd
from datetime import date
import plotly.offline as opy
import plotly.graph_objs as go
import logging
from functools import reduce
from django.utils.translation import ugettext as _
from django.db.models import Sum, F
from django.db import connection
from money.models import Expense, Revenue, RevenuePayment, ExpensePayment, Loan


LOG = logging.getLogger(__name__)
# LOG_DEBUG = logging.getLogger("mydebug")


def format2html(df, displayindex=False):
    """
    function used for display only (cfr view.treso_prev)
    """
    import numpy
    import locale
    locale.setlocale(locale.LC_ALL, "en_GB.UTF-8")
    df = df.round()
    # Add thousand separator
    # https://stackoverflow.com/questions/24922609/formatting-thousand-separator-for-integers-in-a-pandas-dataframe
    # num_format = lambda x: '{:,}'.format(x)
    num_format = lambda x: locale.format('%d', x, 1)

    def build_formatters(df, fmt):
        return {column: fmt
            for (column, dtype) in df.dtypes.iteritems()
                # if column in ("exp", "rev", "solde")
                if dtype in [numpy.dtype('float64')]}
    formatters = build_formatters(df, num_format)
    # Convert to HTML display
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_html.html
    dfhtml = df.to_html(index=displayindex, formatters=formatters,
                        classes=["table", "table-bordered", "table-striped", "table-hover", "table-global"])
    return dfhtml


def build_year(rev, exp, solde, y_minmax, title):
    """
    Build year graphic
    """
    if "Tréso" in title:
        x_title = ["Entrée", "Sortie", "Solde"]
    else:
        x_title = ["Produit", "Charge", "Solde"]

    y = [rev, exp, solde]
    er = go.Bar(x=x_title,
                y=y,
                text=y, textposition='auto',
                marker=dict(color=["#21bf6b", "#f46036", "#4834d4"])
                )
    data = [er]
    layout = go.Layout(
        title=title,
        yaxis=dict(range=y_minmax),
        paper_bgcolor="#FBFBFB")
    figure = go.Figure(data=data, layout=layout)
    div = opy.plot(figure, auto_open=False, output_type="div")
    return div


def build_month(x, ye, yr, solde, soldecum, y_minmax, title):
    """
    Build graphic with plotly based on data collected
    Never use of pay on a period! Non Sense! Kept if needed
    """
    if "Tréso" in title:
        exp_title = "Entrée"
        rev_title = "Sortie"
    else:
        exp_title = "Charge"
        rev_title = "Produit"

    exp = go.Bar(
        x=x, y=ye, marker={"color": "#f46036"}, name=exp_title
    )
    rev = go.Bar(
        x=x, y=yr, marker={"color": "#21bf6b"}, name=rev_title
    )

    # if "Tréso" in title:
    #     solde = go.Scatter(
    #         x=x,
    #         y=solde,
    #         marker={"color": "#17A2B8", "symbol": 104, "size": 10},
    #         mode="lines",
    #         name="Solde",
    #     )
    #     soldecum = go.Scatter(
    #         x=x,
    #         y=soldecum,
    #         marker={"color": "#0000ff", "symbol": 104, "size": 10},
    #         mode="lines",
    #         name="Solde cumulé",
    #     )
    #     data = [exp, rev, solde, soldecum]
    # else:
    data = [exp, rev]

    layout = go.Layout(
        title=title,
        xaxis=dict(showgrid=True, title_text='Mois', nticks=12),
        yaxis=dict(showgrid=True, title_text='Montant (€)', range=y_minmax),
        paper_bgcolor="#FBFBFB",
    )
    figure = go.Figure(data=data, layout=layout)
    div = opy.plot(figure, auto_open=False, output_type="div")
    return div


def build_month_solde(x, solde, soldecum, y_minmax, title):
    """
    Build graphic with plotly based on data collected
    Never use of pay on a period! Non Sense! Kept if needed
    """
    solde = go.Bar(
        x=x, y=solde, marker={"color": "#0000ff"}, name="Solde"
    )

    soldecum = go.Scatter(
        x=x,
        y=soldecum,
        marker={"color": "#0000ff", "symbol": 104, "size": 10},
        mode="lines",
        name="Solde cumulé",
    )
 
    data = [solde, soldecum]

    layout = go.Layout(
        title=title,
        xaxis=dict(showgrid=True, title_text='Mois', nticks=12),
        yaxis=dict(showgrid=True, title_text='Montant (€)', range=y_minmax),
        paper_bgcolor="#FBFBFB",
    )
    figure = go.Figure(data=data, layout=layout)
    div = opy.plot(figure, auto_open=False, output_type="div")
    return div


def build_treso_select(df, year):
    """
    Build treso select or list graphic
    Param1: pandas dataframe
    Param2: year as integer
    """
    if year == 0:
        title_graph = "Trésorerie, année prévisionelle"
    else:
        title_graph = "Trésorerie, année {0}".format(year)

    x = df["month"]
    solde = df["solde"]
    exp = df["exp"]
    rev = df["rev"]


    soldego = go.Scatter(
        x=x,
        y=solde,
        marker={"color": "#17A2B8", "symbol": 104, "size": 10},
        mode="lines",
        name="Solde",
    )
    expgo = go.Bar(
        x=x, y=exp, marker={"color": "#f46036"}, name="Sortie"
    )
    revgo = go.Bar(
        x=x, y=rev, marker={"color": "#21bf6b"}, name="Entrée"
    )

    data = [expgo, revgo, soldego]
    layout = go.Layout(
        title=title_graph,
        paper_bgcolor="#F2F2F2",
        # plot_bgcolor="#333",
        xaxis=dict(showgrid=True, title_text='Mois', nticks=12),
        yaxis=dict(showgrid=True, title_text='Montant (€)')
        )

    fig = go.Figure(data=data, layout=layout)
    div = opy.plot(fig, auto_open=False, output_type="div")
    return div


def build_treso(df_obs, df_prev, year, col):
    """
    Build treso select or list graphic
    Displays 2 years for 2 variables: solde & soldecum
    Param1: pandas dataframe
    Param2: pandas dataframe
    """
    import locale
    locale.setlocale(locale.LC_ALL, "en_GB.UTF-8")

    coltitle = col.capitalize()
    if col == "exp":
        coltitle = "Sortie"
    if col == "rev":
        coltitle = "Entrée"

    colcum = "{0}cum".format(col)
    cum12_prev = df_prev.loc[11, colcum]
    cum12_obs = df_obs.loc[11, colcum]
    cum12_comp = float(cum12_prev) - cum12_obs
    cum12_prev_txt = locale.format('%d', cum12_prev, 1)
    cum12_obs_txt = locale.format('%d', cum12_obs, 1)
    cum12_comp_txt = locale.format('%d', cum12_comp, 1)

    cum12_txt = "Prévision - Année {0} => {1} - {2} = {3}".format(year, cum12_prev_txt, cum12_obs_txt, cum12_comp_txt)

    title_graph = "<b>{0} trésorerie, année prévisionnelle vs année {1}</b><br>{2}".format(coltitle, year, cum12_txt)

    colcum = "{0}cum".format(col)

    obs = go.Bar(
        x=df_obs["month"],
        y=df_obs[col],
        marker={"color": "#0000ff"},
        name="{0} {1}".format(coltitle, year)
    )
    prev = go.Bar(
        x=df_obs["month"],
        y=df_prev[col],
        marker={"color": "#17A2B8"},
        name="{0} prévision".format(coltitle)
    )
    obscum = go.Scatter(
        x=df_obs["month"],
        y=df_obs[colcum],
        marker={"color": "#0000ff", "symbol": 104, "size": 10},
        mode="lines",
        name="{0} cumulé {1}".format(coltitle, year),
    )
    prevcum = go.Scatter(
        x=df_obs["month"],
        y=df_prev[colcum],
        marker={"color": "#17A2B8", "symbol": 104, "size": 10},
        mode="lines",
        name="{0} cumulé prévision".format(coltitle),
    )
    data = [obs, obscum, prev, prevcum]
    layout = go.Layout(
        title=title_graph,
        # font=dict(family='Courier New, monospace', size=18, color='#7f7f7f'),
        # paper_bgcolor="#F2F2F2",
        # plot_bgcolor="#333",
        xaxis=dict(showgrid=True, title_text='Mois', nticks=12),
        yaxis=dict(showgrid=True, title_text='Montant (€)'),
        annotations=[
            go.layout.Annotation(
                x=12,
                y=cum12_obs,
                xref="x",
                yref="y",
                text=cum12_obs_txt,
                showarrow=True,
                arrowhead=7,
                ax=-20,
                ay=-40
            ),
            go.layout.Annotation(
                x=12,
                y=cum12_prev,
                xref="x",
                yref="y",
                text=cum12_prev_txt,
                showarrow=True,
                arrowhead=7,
                ax=-20,
                ay=-40
            )
        ]
        )

    fig = go.Figure(data=data, layout=layout)
    div = opy.plot(fig, auto_open=False, output_type="div")
    return div


# DATA DISPLAY
def build_group_graph(x, y, text, ymax, barcolor, title):
    """
    data_dict struct: {'x': ['a', 'b', 'c'],
                       'y' : [15.6, 11.25, 1.78]}
    Build graphic with plotly based on data collected
    """
    barplot = go.Bar(
        x=y,
        y=x,
        orientation='h',
        marker={"color": barcolor},
        text=text,
        textposition='auto'
    )

    data = [barplot]
    layout = go.Layout(
        title=title,
        xaxis=dict(range=[0, ymax]),
        plot_bgcolor="#E5ECF6",
        # paper_bgcolor="#FBFBFB",
    )
    figure = go.Figure(data=data, layout=layout)
    figure.update_xaxes(nticks=10)
    div = opy.plot(figure, auto_open=False, output_type="div")
    return div
