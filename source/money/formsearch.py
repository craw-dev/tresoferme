# encoding:utf-8
import json
from datetime import date
# from datetime import date
from django import forms
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _
from .models import (Atelier, ExpCat, ExpCatGrp,
                     RevCat, RevCatGrp, Supplier, Client,
                     Revenue, Expense, Product, Loan)
from accounts.models import GeoZip
from .utils_tools import get_year_list


def ddl_loan_inst(request):
    inst = Loan.objects.all().order_by("bankrefdesc")
    if "loantype" in request.GET:
        val = request.GET.get("loantype")
        inst = Loan.objects.filter(loantype=val).order_by("bankrefdesc")
    return inst


def ddl_revcat_inst(request):
    inst = RevCat.objects.exclude(id="EMP_CAI")
    group = request.GET.get("revcatgrp", None)
    origin = request.GET.get("origin", None)

    if group:
        inst = inst.filter(group=group)
        if origin == 'BDC':
            inst = inst.filter(id__in=("FRM_BDC", "FRM_DEV"))
        if origin == 'REVENUE':
            inst = inst.exclude(id__in=("FRM_BDC", "FRM_DEV"))

    return inst


def ddl_expcat_inst(request):
    """
    Used in form.SupplierForm and formsearch.SupplierSearch
    No more in Expense because the choice of a supplier provide a list of categories
    If expcat[] => it comes from a MultiSelect field
    """
    inst = ExpCat.objects.all()
    catlist = request.GET.getlist("expcat[]", None)
    if catlist:
        inst = inst.filter(id__in=catlist)
    else:
        grp = request.GET.get("expcatgrp", "NADA")
        if grp != "NADA":
            inst = inst.filter(group=grp)

    return inst


class ClientSearch(forms.Form):
    PAY_LIST = [("NADA", "--------"),
                ("Paid", "Payé"),
                ("ToPay", "À recevoir")]
    txtsearch = forms.CharField(label=_("Rechercher"), max_length=20,
                                required=False,
                                widget=forms.TextInput(attrs={
                                    "class": "form-control",
                                    "placeholder": _("Rechercher")})
                                )
    # zip = forms.ModelChoiceField(
    #     queryset=GeoZip.objects.all(),
    #     required=False,
    #     label=_("Code postal"),
    #     widget=forms.Select(attrs={"class": "form-control"}),
    # )
    payflag = forms.ChoiceField(required=False,
                    choices=PAY_LIST,
                    widget=forms.Select(attrs={'class': "form-control"}))
    payduedate = forms.DateField(widget=forms.HiddenInput())
    exportxls = forms.CharField(required=True, widget=forms.HiddenInput())

    def __init__(self, request, id_list):
        super().__init__()
        self.fields["txtsearch"].initial = request.GET.get('txtsearch')
        # self.fields["zip"].initial = request.GET.get('zip')
        self.fields["payflag"].initial = request.GET.get('payflag')
        self.fields["payduedate"].initial = date.today().strftime('%Y-%m-%d')
        self.fields["exportxls"].initial = request.GET.get('exportxls')


class SupplierSearch(forms.Form):
    PAY_LIST = [("NADA", "--------"),
                ("Paid", "Payé"),
                ("ToPay", "À payer")]
    txtsearch = forms.CharField(label=_("Rechercher"), max_length=20,
                                required=False,
                                widget=forms.TextInput(attrs={
                                    "class": "form-control",
                                    "placeholder": _("Rechercher")})
                                )
    expcatgrp = forms.ModelChoiceField(
        queryset=ExpCatGrp.objects.order_by("name"),
        required=False, label=_("Groupe"),
        widget=forms.Select(attrs={"class": "form-control"}),
    )
    expcat = forms.ModelChoiceField(
        queryset=None,
        required=False, label=_("Catégorie"),
        widget=forms.Select(attrs={"class": "form-control"}),
    )
    payflag = forms.ChoiceField(required=False,
                    choices=PAY_LIST,
                    widget=forms.Select(attrs={'class': "form-control"}))
    payduedate = forms.DateField(widget=forms.HiddenInput())
    exportxls = forms.CharField(required=True, widget=forms.HiddenInput())

    def __init__(self, request, id_list=None):
        super().__init__()
        self.fields["expcat"].queryset = ddl_expcat_inst(request)
        self.fields["txtsearch"].initial = request.GET.get('txtsearch')
        self.fields["expcat"].initial = request.GET.get('expcat')
        self.fields["expcatgrp"].initial = request.GET.get('expcatgrp')
        self.fields["payflag"].initial = request.GET.get('payflag')
        self.fields["payduedate"].initial = date.today().strftime('%Y-%m-%d')
        self.fields["exportxls"].initial = "NADA"


class RevCatSearch(forms.Form):
    txtsearch = forms.CharField(label=_("Rechercher"), max_length=20,
                                required=False,
                                widget=forms.TextInput(attrs={
                                    "class": "form-control",
                                    "placeholder": _("Rechercher")})
                                )
    revcatgrp = forms.ModelChoiceField(
        queryset=RevCatGrp.objects.all().order_by("name"),
        required=False, label=_("Groupe"),
        widget=forms.Select(attrs={"class": "form-control"}),
    )
    revcat = forms.ModelChoiceField(
        queryset=None,
        required=False, label=_("Catégorie"),
        widget=forms.Select(attrs={"class": "form-control"}),
    )
    exportxls = forms.CharField(required=True, widget=forms.HiddenInput())

    def __init__(self, request):
        super().__init__()
        # !!!queryset MUST come BEFORE initial otherwise we lose the selected value!!!
        self.fields["revcat"].queryset = ddl_revcat_inst(request)

        self.fields["txtsearch"].initial = request.GET.get('txtsearch')
        self.fields["revcat"].initial = request.GET.get('revcat')
        self.fields["revcatgrp"].initial = request.GET.get('revcatgrp')
        self.fields["exportxls"].initial = "NADA"


class ExpenseSearch(forms.Form):
    PAY_LIST = [("NADA", "--------"),
                ("Paid", "Payé"),
                ("ToPay", "À payer")]
    # LIST_YEAR = [
    #     (2018, '2018'),
    #     (2019, '2019'),
    #     (2020, '2020'),
    # ]
    # year = forms.ChoiceField(choices=LIST_YEAR, widget=forms.Select(attrs={'class': "form-control"}))
    expnr = forms.CharField(
                           label=_("Rechercher"),
                           max_length=10,
                           required=False,
                           widget=forms.TextInput(attrs={"class": "form-control"})
                           )
    year = forms.ModelChoiceField(queryset=None, label=_("Année"),
                                  required=False,
                                  widget=forms.Select(attrs={'class': "form-control"}))
    expcatgrp = forms.ModelChoiceField(queryset=None)
    expcat = forms.ModelChoiceField(queryset=None)
    supplier = forms.ModelChoiceField(queryset=None)
    atelier = forms.ModelChoiceField(queryset=None)
    payflag = forms.ChoiceField(required=False,
                                choices=PAY_LIST,
                                widget=forms.Select(attrs={'class': "form-control"}))
    exportxls = forms.CharField(required=True, widget=forms.HiddenInput())

    def __init__(self, request, id_list):
        super().__init__()
        self.fields['atelier'] = forms.ModelChoiceField(
            queryset=Atelier.objects.filter(
                expense__in=id_list).distinct().order_by("atelier"),
            required=False,
            label="Atelier",
            widget=forms.Select(attrs={"class": "form-control"})
            )
        self.fields['supplier'] = forms.ModelChoiceField(
            queryset=Supplier.objects.filter(expense__in=id_list).distinct(),
            required=False,
            label="Fournisseur",
            widget=forms.Select(attrs={"class": "form-control"})
            )
        self.fields['expcatgrp'] = forms.ModelChoiceField(
            queryset=ExpCatGrp.objects.filter(
                expcat__expense__in=id_list).distinct().order_by("name"),
            required=False,
            label="Groupe",
            widget=forms.Select(attrs={"class": "form-control"})
            )

        expcat_qs = ExpCat.objects.filter(expense__in=id_list).distinct().order_by("name")
        val = request.GET.get("expcatgrp", "")
        if val != "":
            expcat_qs = expcat_qs.filter(group=val)
        # print("expcat_qs: {0}".format(expcat_qs))

        self.fields['expcat'] = forms.ModelChoiceField(
            queryset=expcat_qs,
            required=False,
            label="Catégorie",
            widget=forms.Select(attrs={"class": "form-control"})
            )
        self.fields["expnr"].initial = request.GET.get("expnr")
        self.fields["atelier"].initial = request.GET.get("atelier")
        self.fields["supplier"].initial = request.GET.get("supplier")
        self.fields["expcat"].initial = request.GET.get("expcat")
        self.fields["expcatgrp"].initial = request.GET.get("expcatgrp")
        self.fields["payflag"].initial = request.GET.get("payflag")
        self.fields["exportxls"].initial = "NADA"

        userpk = request.session["myuserpk"]
        usermodel = get_user_model()
        myuser = get_object_or_404(usermodel, pk=userpk)

        # self.fields["year"].choices = get_year_list(request, "EXP")
        self.fields["year"].queryset = Expense.objects.for_user(myuser).values_list('inputdate__year', flat=True).distinct().order_by('inputdate__year')
        if request.GET.get("year") is None:
            year = ""
        else:
            year = request.GET.get("year")
        self.fields["year"].initial = year


class RevenueSearch(forms.Form):
    PAY_LIST = [("NADA", "--------"),
                ("Paid", "Payé"),
                ("ToPay", "À recevoir")]
    MINIMI_LIST = [("NADA", "Aide minimi"),
                ("OUI", "Oui"),
                ("NON", "Non")]
    INVOICE_SEND_LIST = [("NADA", "Envoyé(e)"),
                ("OUI", "Oui"),
                ("NON", "Non")]                
    invnr = forms.CharField(
                           label=_("Rechercher"),
                           max_length=20,
                           required=False,
                           widget=forms.TextInput(attrs={"class": "form-control"})
                           )
    year = forms.ModelChoiceField(queryset=None, label=_("Année"),
                                  required=False,
                                  widget=forms.Select(attrs={'class': "form-control"}))
    revcatgrp = forms.ModelChoiceField(queryset=None)
    revcat = forms.ModelChoiceField(queryset=None)
    client = forms.ModelChoiceField(queryset=None)
    atelier = forms.ModelChoiceField(queryset=None)
    payflag = forms.ChoiceField(required=False,
                        choices=PAY_LIST,
                        widget=forms.Select(attrs={'class': "form-control"}))
    minimi = forms.ChoiceField(required=False,
                        choices=MINIMI_LIST,
                        widget=forms.Select(attrs={'class': "form-control"}))
    invoice_send = forms.ChoiceField(required=False,
                        choices=INVOICE_SEND_LIST,
                        widget=forms.Select(attrs={'class': "form-control"}))
    exportxls = forms.CharField(required=True, widget=forms.HiddenInput())

    def __init__(self, request, id_list):
        super().__init__()
        self.fields['atelier'] = forms.ModelChoiceField(
            queryset=Atelier.objects.filter(
                revenue__in=id_list).distinct().order_by("atelier"),
            required=False,
            label="Atelier",
            widget=forms.Select(attrs={"class": "form-control"})
            )
        self.fields['client'] = forms.ModelChoiceField(
            queryset=Client.objects.filter(revenue__in=id_list).distinct(),
            required=False,
            label="Client",
            widget=forms.Select(attrs={"class": "form-control"})
            )
        self.fields['revcatgrp'] = forms.ModelChoiceField(
            queryset=RevCatGrp.objects.filter(
                revenue__in=id_list).distinct().order_by("name"),
            required=False,
            label="Groupe",
            widget=forms.Select(attrs={"class": "form-control"})
            )

        revcat_qs = RevCat.objects.filter(revenue__in=id_list).distinct().order_by("name")
        val = request.GET.get("revcatgrp", "")
        if val != "":
            revcat_qs = revcat_qs.filter(group=val)

        self.fields['revcat'] = forms.ModelChoiceField(
            queryset=revcat_qs,
            required=False,
            label="Catégorie",
            widget=forms.Select(attrs={"class": "form-control"})
            )
        self.fields["invnr"].initial = request.GET.get("invnr")
        self.fields["atelier"].initial = request.GET.get("atelier")
        self.fields["client"].initial = request.GET.get("client")
        self.fields["revcat"].initial = request.GET.get("revcat")
        self.fields["revcatgrp"].initial = request.GET.get("revcatgrp")
        self.fields["payflag"].initial = request.GET.get("payflag")
        self.fields["minimi"].initial = request.GET.get("minimi")
        self.fields["invoice_send"].initial = request.GET.get("invoice_send")
        self.fields["exportxls"].initial = "NADA"

        userpk = request.session["myuserpk"]
        usermodel = get_user_model()
        myuser = get_object_or_404(usermodel, pk=userpk)

        # self.fields["year"].choices = get_year_list(request, "REV")
        self.fields["year"].queryset = Revenue.objects.for_user(myuser).values_list('inputdate__year', flat=True).distinct().order_by('inputdate__year')
        # Get year passed as GET argument
        if request.GET.get("year") is None:
            year = ""
        else:
            year = request.GET.get("year")
        self.fields["year"].initial = year


def ddl_product_inst(request):
    inst = Product.objects.for_user(request.user).all().order_by("name")
    val = request.GET.get("product", "")
    if val != "":
        inst = inst.filter(id=val)
    return inst


class ProductSearch(forms.Form):
    atelier = forms.ModelChoiceField(
        queryset=None, required=False, label=_("Atelier"),
        widget=forms.Select(attrs={"class": "form-control"})
    )
    txtsearch = forms.CharField(label=_("Rechercher"), max_length=20,
                                required=False,
                                widget=forms.TextInput(attrs={
                                    "class": "form-control",
                                    "placeholder": _("Rechercher")})
                                )    
    exportxls = forms.CharField(required=True, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        request = kwargs.pop('request', None)
        super().__init__()
        if "myuserpk" in request.session:
            userpk = request.session["myuserpk"]
            usermodel = get_user_model()
            myuser = get_object_or_404(usermodel, pk=userpk)
        else:
            myuser = request.user
        self.fields["atelier"].queryset = Atelier.objects.for_user(myuser).all().order_by("atelier")
        self.fields["atelier"].initial = request.GET.get("atelier")
        self.fields["exportxls"].initial = "NADA"


class GraphParamForm(forms.Form):
    LIST_YEAR = [
        ('2018', '2018'),
        ('2019', '2019'),
        ('2020', '2020'),
    ]
    year1 = forms.ChoiceField(choices=LIST_YEAR, widget=forms.Select(attrs={'class': "form-control"}))
    year2 = forms.ChoiceField(choices=LIST_YEAR, widget=forms.Select(attrs={'class': "form-control"}))

    def __init__(self, request):
        super().__init__()
        # collect init years
        if request.GET.get("year1") is None:
            year1 = int(request.session["year1"])
        else:
            year1 = int(request.GET.get("year1"))

        if request.GET.get("year2") is None:
            year2 = int(request.session["year2"])
        else:
            year2 = int(request.GET.get("year2"))

        ddlb_list = get_year_list(request, "BOTH")
        self.fields["year1"].initial = year1
        self.fields["year1"].choices = ddlb_list
        self.fields["year2"].initial = year2
        self.fields["year2"].choices = ddlb_list


class GraphExpCatGrp(forms.ModelForm):

    class Meta:
        model = ExpCatGrp
        fields = ["name"]
        widgets = {"name": forms.Select(attrs={
                                "class": "form-control",
                                "required": False
            })}

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.fields["name"].queryset = ExpCatGrp.objects.all()


# class GraphGroupSelect(forms.Form):
#     group = forms.ModelChoiceField(queryset=None,
#                                    required=True,
#                                    widget=forms.Select(attrs={'class': "form-control"}))

#     def __init__(self, *args, **kwargs):
#         user = kwargs.pop('user', None)
#         super().__init__()
#         if args[0].get("group") is not None:
#             group = args[0].get("group")

#         self.fields["group"].queryset = Expense.objects.for_user(user).values_list('expcatgrp', flat=True).distinct().order_by('expcatgrp')
        # self.fields["year1"].initial = year1
