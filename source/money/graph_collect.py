# -*- coding: utf-8 -*-
"""
Set of f
unctions used for graphic and culculations
"""
import math, json, logging
from datetime import date
from functools import reduce
import pandas as pd
from django.db import connection
from money.models import Expense, Revenue, Loan
from . import graph_display
from config.settings import DATABASES

LOG = logging.getLogger(__name__)
LOG_DEBUG = logging.getLogger("mydebug")


class CollectRenta(object):
    """
    Data collection for rentabilité
    """
    dfe = pd.DataFrame
    dfr = pd.DataFrame
    col_def = {
        "Expense": ("atelier__atelier__id", "atelier__atelier__name",
                    "expcat__group__id", "expcat__group__name", "expcat__name",
                    "expcat__categclass__id", "expcat__categclass__name", "supplier__id", "supplier__company"),
        "Revenue": ("atelier__atelier__id", "atelier__atelier__name", "revcat__group__id", "revcatgrp__name", "revcat__name", "client__company"),
    }

    def __init__(self, user, year):
        self.user = user
        self.year = year

    def get_loan(self):
        """
        Loans refund DataFrame
        Gets all appropriate debit year in "tableau d'amortissement"
        returns a pandas DataFrame
        """
        loan_list = []
        df_loan = pd.DataFrame()

        loanqs = Loan.objects.for_user(self.user).filter(closedate=None).exclude(loantype__id="EMP_CAI")
        if loanqs:
            if 'sqlite3' in DATABASES['default']['ENGINE']:
                xdate_yyyymm = "strftime('%%Y-%%m', debitdate)"
            else:
                xdate_yyyymm = "to_char(debitdate, 'YYYY-MM')"

            for loan in loanqs:
                rfd = loan.loanrefund_set.filter(debitdate__year=self.year).order_by("debitdate").extra(
                        select={"xdate": xdate_yyyymm}
                    )                  
                    
                if rfd:
                    rfd_select = rfd.values("xdate", "expense__atelier__atelier__id",
                                            "expense__atelier__atelier__name",
                                            "expense__expcat__group__id", "expense__expcat__group__name",
                                            "expense__expcat__name",
                                            "expense__expcat__categclass__id", "expense__expcat__categclass__name",
                                            "expense__supplier__id", "expense__supplier__company", "interest")
                    df = pd.DataFrame(list(rfd_select))
                    df = df.set_index(['xdate'])
                    loan_list.append(df)
            if loan_list:
                # Concat list into dataframe
                df_loan = pd.concat(loan_list)
                df_loan.columns = self.col_def["Expense"] + ("amount",)
                df_loan["amountvat"] = df_loan["amount"]
                df_loan["expcat__name"] = "Intérêts emprunts"

        return df_loan

    def get_investment(self):
        """
        Queryset INV in Expense
        returns a pandas DataFrame
        """
        invest_list = []
        df_invest = pd.DataFrame
        # investqs = Invest.objects.for_user(self.user).all()
        investqs = Expense.objects.for_user(self.user).filter(expcat__group__id="INV").all()
        if investqs:
            for inv in investqs:
                amort_period = inv.amortyear*12
                amort_val = inv.amountvat/(amort_period)
                rng = pd.date_range(start=inv.inputdate, periods=amort_period, freq="M")
                df = pd.DataFrame(index=rng)
                df.reset_index(inplace=True)
                df["amount"] = amort_val
                df["amountvat"] = amort_val
                df["atelier__atelier__id"] = inv.atelier.atelier.id
                df["atelier__atelier__name"] = inv.atelier.atelier.name
                df["expcat__group__id"] = inv.expcat.group.id
                df["expcat__group__name"] = inv.expcat.group.name
                df["expcat__name"] = inv.expcat.name
                df["expcat__categclass__id"] = inv.expcat.categclass.id
                df["expcat__categclass__name"] = inv.expcat.categclass.name
                df["supplier__id"] = inv.supplier.id
                df["supplier__company"] = inv.supplier.company
                df['xdate'] = df['index'].dt.strftime('%Y-%m')
                df = df.set_index(['xdate']).drop('index', 1)
                invest_list.append(df)
            df_invest = pd.concat(invest_list, sort=True)

        return df_invest

    def build_template(self, modeltxt="NADA"):
        """
        Build template over 1 year
        If no modeltxt, template has only xdate
        Else => xdate + all concerned columns of Expense or Revenue
        """
        begin_date = date(self.year, 1, 1)
        rng = pd.date_range(start=begin_date, freq="M", periods=12)
        if modeltxt != "NADA":
            col_base = ("xdate", "amount", "amountvat")
            col_select = col_base + self.col_def[modeltxt]
            df = pd.DataFrame(index=rng, columns=col_select)
        else:
            df = pd.DataFrame(index=rng)

        df.reset_index(inplace=True)
        df['xdate'] = df['index'].dt.strftime('%Y-%m')
        df = df.set_index(['xdate']).drop('index', 1)
        return df

    def get_data(self, modeltxt):
        """
        Rentabilité: exclude EMP from Revenue, exclude INV from Expense
        """
        df = pd.DataFrame
        df_invest = pd.DataFrame
        df_loan = pd.DataFrame
        model = eval(modeltxt)
        qs = model.objects.for_user(self.user).filter(inputdate__year=self.year)

        if 'sqlite3' in DATABASES['default']['ENGINE']:
            xdate_yyyymm = "strftime('%%Y-%%m', inputdate)"
        else:
            xdate_yyyymm = "to_char(inputdate, 'YYYY-MM')"

        qs = qs.order_by("inputdate").extra(
            select={"xdate": xdate_yyyymm}
        )
        if modeltxt == "Revenue":
            # qs = qs.exclude(revcat="FRM_BDC").exclude(revcatgrp="EMP")
            qs = qs.exclude(revcat__renta_exclude=True)
        if modeltxt == "Expense":
            qs = qs.exclude(expcat__group__in=("INV", "EMP", "CRE")).exclude(expcat__renta_exclude=True)
            df_invest = self.get_investment()
            df_loan = self.get_loan()

        col_base = ("xdate", "amount", "amountvat")
        col_select = col_base + self.col_def[modeltxt]
        qs_select = qs.values(*col_select)
        df = pd.DataFrame(list(qs_select))
        # if no data in df, build template with rev or exp columns
        if df.empty:
            df = self.build_template(modeltxt)
        else:
            df = df.set_index(['xdate'])
        if not df_invest.empty:
            df = pd.concat([df, df_invest], sort=True)
        if not df_loan.empty:
            df = pd.concat([df, df_loan], sort=True)

        df.reset_index(inplace=True)
        df.amount = df.amount.astype(float)
        df.amountvat = df.amountvat.astype(float)
        # filter on year since INV and LOAN have been added before!
        df = df.loc[df["xdate"].str[0:4] == str(self.year)]
        df = df.dropna()
        return df


def build_template(year, columns="NADA"):
    """
    Build template over 1 year
    used also in compute_month
    """
    begin_date = date(year, 1, 1)
    rng = pd.date_range(start=begin_date, freq="M", periods=12)
    df = pd.DataFrame(index=rng)

    if columns != "NADA":
        df = pd.DataFrame(index=rng, columns=columns)
    else:
        df = pd.DataFrame(index=rng)

    df.reset_index(inplace=True)
    df['xdate'] = df['index'].dt.strftime('%Y-%m')
    df = df.set_index(['xdate']).drop('index', 1)
    return df


class CollectTreso(object):
    """
    Tresorerie class
    yearobs: observed year (current year)
    """
    def __init__(self, user, year):
        self.user = user
        self.year = year

    def get_data(self, model, suffixvat="amountvat", suffix="amount"):
        """
        2. Get data in appropriate model
        """
        if model.__name__ == "Expense":
            group_column = "expcat__group__id"
        if model.__name__ == "Revenue":
            group_column = "revcat__group__id"

        df = pd.DataFrame()
        qs = model.objects.for_user(self.user).filter(inputdate__year=self.year)
        if qs:
            # Init xdate_yyyymm
            if 'sqlite3' in DATABASES['default']['ENGINE']:
                xdate_yyyymm = "strftime('%%Y-%%m', inputdate)"
            else:
                xdate_yyyymm = "to_char(inputdate, 'YYYY-MM')"

            if model.__name__ == "Revenue":
                qs = qs.exclude(revcat__id="FRM_BDC")

            qs = qs.order_by("inputdate").extra(
                select={"xdate": xdate_yyyymm, suffixvat: "amountvat", suffix: "amount"}
            )
            qs = qs.values("xdate", group_column, suffixvat, suffix)
            df = pd.DataFrame(list(qs))
            df[[suffixvat, suffix]] = df[[suffixvat, suffix]].apply(pd.to_numeric)
            df = df.set_index(['xdate'])
        else:
            # Create a template with default columns
            df = build_template(self.year, ["xdate", group_column, suffixvat, suffix])
        return df

    def collect_data(self):
        """
        1. collect
        """
        df = pd.DataFrame
        dft = build_template(self.year)
        dfe = self.get_data(Expense, "exp")
        dfe = dfe.groupby(["xdate"]).sum()
        dfr = self.get_data(Revenue, "rev")
        dfr = dfr.groupby(["xdate"]).sum()
        df_list = [dft, dfe, dfr]

        df = reduce(lambda left, right: pd.merge(
                        left, right, left_index=True, right_index=True, how="left"
                    ),
                    df_list,
        ).fillna(0)

        df["solde"] = df["rev"] - df["exp"]
        df.reset_index(inplace=True)
        df["year"] = df["xdate"].str[0:4]
        df["month"] = df["xdate"].str[5:7]
        df[["year", "month", "exp", "rev", "solde"]] = df[["year", "month", "exp", "rev", "solde"]].apply(pd.to_numeric)
        df["soldecum"] = df["solde"].cumsum(axis=0)
        df["revcum"] = df["rev"].cumsum(axis=0)
        df["expcum"] = df["exp"].cumsum(axis=0)

        return df


# DATA COLLECT
def get_product(user, year):
    """
    Specific query to get products
    """
    if 'sqlite3' in DATABASES['default']['ENGINE']:
        xdate_yyyymm = "strftime('%%Y-%%m', inputdate)"
    else:
        xdate_yyyymm = "to_char(inputdate, 'YYYY-MM')"
    df = pd.DataFrame(columns=["xdate", "client_id", "client", "product", "atelier_id", "atelier", "amount", "amountvat"])
    cursor = connection.cursor()
    cursor.execute('''WITH rev AS (
            select r.id, revcat_id, al.id as atelier_id, al.name as atelier, client_id, inputdate, amount, amountvat
            from money_revenue r
            INNER JOIN money_revcat cat ON (r.revcat_id = cat.id)
            LEFT OUTER JOIN farm_atelier a ON (r.atelier_id = a.id)
            LEFT OUTER JOIN farm_atelierlist al ON (al.id = a.atelier_id)
            where r.profile_id = {0} and date_part('year', r.inputdate) = {1}
            and cat.renta_exclude = False
        )
        SELECT {2}, c.id as client_id,
            c.company as client, t.name as product,
            --CASE WHEN p.atelier_id is NULL THEN rev.atelier ELSE al.name END,
            COALESCE(NULLIF(rev.atelier_id, al.id), rev.atelier_id),
            COALESCE(NULLIF(rev.atelier, al.name), rev.atelier),
            COALESCE(NULLIF(p.amounttot, 0), rev.amount),
            COALESCE(NULLIF(p.amounttotvat, 0), rev.amountvat)
        FROM rev INNER JOIN money_client c ON (rev.client_id = c.id)
        LEFT OUTER JOIN money_revenueproduct p ON (rev.id = p.revenue_id)
        LEFT OUTER JOIN farm_atelier a ON (a.id = p.atelier_id)
        LEFT OUTER JOIN farm_atelierlist al ON (a.atelier_id = al.id)
        LEFT OUTER JOIN money_product t ON (p.product_id = t.id)'''.format(user.id, year, xdate_yyyymm)
    )
    rows = cursor.fetchall()
    if rows:
        df = pd.DataFrame(rows)
        df.columns = ("xdate", "client_id", "client", "product", "atelier_id", "atelier", "amount", "amountvat")
        df["year"] = df["xdate"].str[0:4]
        df["month"] = df["xdate"].str[5:7]
        df[["year", "month", "client_id"]] = df[["year", "month", "client_id"]].apply(pd.to_numeric)
        df.amount = df.amount.astype(float)
        df.amountvat = df.amountvat.astype(float)
    return df

######################################
# Single function for data formating
######################################
def df_aggregate(df, group_id, group):
    """
    aggregate df
    Rename x as 'Autres' when y < 3%
    key & x must be of same datatype than data collected!
    otherwise error when df_merge!
    => when group_id = ('expcat__group__id', 'revcat__group__id', 'atelier__atelier__id') key is string
    """
    if df.empty:
        # if group_id == group:
        #     dfg = pd.DataFrame(columns=["x", "y", "yvat"])
        #     dfg.loc[len(dfg)] = ["NA", 0, 0]
        # else:
        dfg = pd.DataFrame(columns=["key", "x", "y", "yvat"])
        if group_id in ('expcat__group__id', 'revcat__group__id', 'atelier__atelier__id', 'atelier_id', 'expcat__categclass__id'):
            dfg.loc[len(dfg)] = ["NA", "NA", 0, 0]
        else:
            dfg.loc[len(dfg)] = [0, "NA", 0, 0]
        dfg.y = dfg.y.astype(float)
        dfg.yvat = dfg.yvat.astype(float)
    else:
        # if group_id == group:
        #     dfg = df.groupby([group]).sum().reset_index()
        #     dfg = dfg[[group, "amount", "amountvat"]]
        #     dfg.columns = ("x", "y", "yvat") # Rename these columns
        #     group_by = ["x"]
        # else:
        dfg = df.groupby([group_id, group]).sum().reset_index()
        dfg = dfg[[group_id, group, "amount", "amountvat"]]
        dfg.columns = ("key", "x", "y", "yvat") # Rename these columns
        group_by = ["key", "x"]

        # Si fournisseur, on crée un fournisseur abstrait (Autre)
        # qui contient tous les montants inférieur à 3%
        if group == "supplier__company":
            # Compute 3% amount ceil
            total = dfg["y"].sum()
            totalxpct = total * 3 / 100
            dfgmax = dfg[dfg["y"] > totalxpct]
            dfgmin = dfg[dfg["y"] < totalxpct]
            # Rename key & x with new label
            lenidx = len(dfgmin.index)
            key = ['AAA'] * lenidx
            x = ['Autres'] * lenidx

            if group_id == group:
                dfgmin = dfgmin.drop(["x"], 1)
                dfgmin["x"] = x
                group_by = ["x"]
            else:
                dfgmin = dfgmin.drop(["key", "x"], 1)
                dfgmin[group_id] = key
                dfgmin[group] = x
                group_by = ["key", "x"]
            # Concat Min & Max
            dfg = pd.concat([dfgmax, dfgmin], sort=True)

        # Avoid several lines on display for Autres
        dfg = dfg.groupby(group_by).sum().sort_values("y").reset_index()

    return dfg


def df_merge(df1, df2, suffix1, suffix2):
    """
    outer join of 2 dataframe with a1 & a2 suffixes
    return a dataframe
    """
    if "key" in df1:
        df = pd.merge(df1, df2, on=['key', 'x'], how='outer', suffixes=(suffix1, suffix2))
    else:
        df = pd.merge(df1, df2, on=['x'], how='outer', suffixes=(suffix1, suffix2))
    return df.fillna(0)


def format_dict(mydict, col):
    """
    add text for label display in plotly
    ymin used for month-year graph
    """
    nrlist = []
    ymax = 0
    ymin = 0
    if mydict[col]:
        for i in mydict[col]:
            if math.isnan(i):
                i = 0
            ir = round(i)
            nr = "{:,}".format(ir) # thousand sep=comma
            nr = nr.replace(",", " ") # replace comma by blank space
            nrlist.append(nr)
        ymax = max(mydict[col])
        ymin = min(mydict[col])
    # add text key to dict
    txt = '{0}_txt'.format(col)
    mydict[txt] = nrlist
    return mydict, ymax, ymin


def df_group2dict(df):
    """Convert dataframe to dict and compute ymax of the 2 df"""
    # Arrondi au segement (10 exp n where n compris entre 1 et infini) supérieur selon le nombre
    # si 88 => 100 si 1800 => 2000, si 25900 => 26000
    def roundup(x):
        if x == 0 or math.isnan(x):
            return 0
        length = math.ceil(math.log10(x));
        segment = math.pow(10, length-1);
        return int(math.ceil(x / segment)) * segment

    bd = df.to_dict('list')
    bd, ymax1, ymin1 = format_dict(bd, 'y_a1')
    bd, ymax2, ymin2 = format_dict(bd, 'y_a2')
    ymax = max(ymax1, ymax2)
    ymin = min(ymin1, ymin2)
    ymax = roundup(ymax)
    return bd, ymax, ymin


def create_group_context(df1, df2, year1, year2,
    group_id, group, barcolor,
    title_group, title_RD, class_color,
    categ=None, title_categ=None):
    """
    Create a context to be displayed in money.view.GraphIndex()
    graph1 & 2: plotly div objects
    list1 & 2: dictionary for ddlb
    subgraph1_data: dictionary for plotlyjs (used to answer dynamic change on ddlb)
    """
    # Init vars
    ctx = {}
    ctx['list1'] = 0
    ctx['subtitle1'] = 0
    ctx['subgraph1_data'] = 0
    ctx['list2'] = 0
    ctx['subtitle2'] = 0
    ctx['subgraph2_data'] = 0

    ctx['ymax_categ'] = 0
    ctx['barcolor'] = barcolor
    ctx['class_color'] = class_color

    ctx['title_group'] = "{} par {}".format(title_RD, title_group)
    ctx['title_categ'] = "{} par {}".format(title_RD, title_categ)
    # Passed over in order to display msg for Product
    if categ == "product":
        ctx['categ_msg'] = "Attention, ce graphique ne reprend que les produits que vous avez détaillés dans les factures de ventes!"
    df1g = df_aggregate(df1, group_id, group)
    # LOG.debug("df1g")
    # LOG.debug(df1g)
    df2g = df_aggregate(df2, group_id, group)
    # LOG.debug("df2g")
    # LOG.debug(df2g)
    df = df_merge(df1g, df2g, '_a1', '_a2')
    # LOG.debug("!!!merge done!!!")
    # remove "NA" lines
    df = df[df.x != "NA"]
    data_dict, ymax, ymin = df_group2dict(df)
    # HBP intégrer min & max ici!!!
    ctx['graph1'] = graph_display.build_group_graph(data_dict['x'],
        data_dict['y_a1'], data_dict['y_a1_txt'], ymax, barcolor, "{} par {}, année {}".format(title_RD, title_group, year1))
    ctx['graph2'] = graph_display.build_group_graph(data_dict['x'],
        data_dict['y_a2'], data_dict['y_a2_txt'], ymax, barcolor, "{} par {}, année {}".format(title_RD, title_group, year2))

    if categ is not None:
        df1c = df_aggregate(df1, group_id, categ)
        df2c = df_aggregate(df2, group_id, categ)
        df = df_merge(df1c, df2c, '_a1', '_a2')
        dc, ymaxc, yminc = df_group2dict(df)
        ctx["ymax_categ"] = ymaxc
        dc1 = {'key': dc['key'], 'x': dc['y_a1'], 'y': dc['x'], 'text': dc['y_a1_txt']}
        dc2 = {'key': dc['key'], 'x': dc['y_a2'], 'y': dc['x'], 'text': dc['y_a2_txt']}
        ctx['subgraph1_data'] = json.dumps(dc1)
        ctx['subgraph2_data'] = json.dumps(dc2)
        # Check if data in categ
        if df1[categ].any():
            ctx['list1'] = dict(zip(df1[group_id], df1[group]))
            ctx['list1'][0] = '-----------'
            ctx['subtitle1'] = "{} par {}, année {}".format(title_RD, title_categ, year1)
        if df2[categ].any():
            ctx['list2'] = dict(zip(df2[group_id], df2[group]))
            ctx['list2'][0] = '-----------'
            ctx['subtitle2'] = "{} par {}, année {}".format(title_RD, title_categ, year2)
    return ctx


def compute_year(exp, rev):
    """Compute Sum Expense and Revenue"""
    exp = sum(exp)
    rev = sum(rev)
    solde = rev - exp
    return exp, rev, solde


def compute_solde(df, y_exp, y_rev):
    """Used for monthly (year) data"""
    df["solde"] = df[y_rev] - df[y_exp]
    # df["solde"] = df["yvat_r"] - df["yvat_e"] # y_rev=yvat_r and y_exp=yvat_e
    df["soldecum"] = df["solde"].cumsum(axis=0)
    df.reset_index(inplace=True) # xdate became x after rename
    df["year"] = df["x"].str[0:4]
    df["month"] = df["x"].str[5:7]
    df = df.set_index(['x'])
    df[["year", "month", "solde", "soldecum"]] = df[["year", "month", "solde", "soldecum"]].apply(pd.to_numeric)
    return df


def compute_month(dfe, dfr, year):
    """
    Create a context to be displayed in money.view.GraphIndex()
    return df aggregated and merged
    """
    # Create global_group key (df_aggregate) column for table display
    if dfe.empty:
        dfeg = pd.DataFrame(columns=["key", "x", "y", "yvat"])
    else:
        dfe["global_group"] = dfe["expcat__group__id"]
        # ~ => not # https://stackoverflow.com/questions/43256402/pandas-analogue-of-sqls-not-in-operator
        dfe.loc[~dfe["global_group"].isin(['INV', 'EMP']), 'global_group'] = 'EXP'
        dfeg = df_aggregate(dfe, "global_group", "xdate")

    if dfr.empty:
        dfrg = pd.DataFrame(columns=["key", "x", "y", "yvat"])
    else:
        dfr["global_group"] = dfr["revcat__group__id"]
        # https://stackoverflow.com/questions/17071871/how-to-select-rows-from-a-dataframe-based-on-column-values
        # All revcat_group different from EMP and CRE become REV
        dfr.loc[~dfr["global_group"].isin(['EMP', 'CRE']), "global_group"] = 'REV'
        # Replace EMP by REVEMP in order to differentiate from EMP (annuité in Expense!)
        dfr.loc[dfr["global_group"] == 'EMP', "global_group"] = 'REVEMP'
        dfr.loc[dfr["global_group"] == 'CRE', "global_group"] = 'REVCRE'
        dfrg = df_aggregate(dfr, "global_group", "xdate")

    # Merge expense with revenue with outer join
    dfs = pd.merge(dfeg, dfrg, on=['key', 'x'], how='outer', suffixes=('_e', '_r'))

    # build template dft
    dft = build_template(year)
    dft.index.names = ['x']

    # Merge dfs with template 12 month (dft) as first table
    df = pd.merge(dft, dfs, on=['x'], how='left', suffixes=('_t', '_s')).fillna(0)

    return df


def compute_month_minmax(dfs1, dfs2, what="EXPREV"):
    """
    Compute min and max
    Must be done after compute_solde
    Compute is done either on Expense and Revnue model
    or on solde and soldecum vars
    """
    # Arrondi au segement (10 exp n where n compris entre 1 et infini) supérieur selon le nombre
    # si 88 => 100 si 1800 => 2000, si 25900 => 26000
    def roundup(x):
        x = abs(x) # valeur absolue de x
        if x == 0 or math.isnan(x):
            return 0
        length = math.ceil(math.log10(x));
        segment = math.pow(10, length-1);
        return int(math.ceil(x / segment)) * segment

    ymin1 = dfs1[['y_e', 'y_r']].min()
    ymax1 = dfs1[['y_e', 'y_r']].max()
    ymin2 = dfs2[['y_e', 'y_r']].min()
    ymax2 = dfs2[['y_e', 'y_r']].max()

    if what == "SOLDE":
        ymin1 = dfs1[['solde', 'soldecum']].min()
        ymax1 = dfs1[['solde', 'soldecum']].max()
        ymin2 = dfs2[['solde', 'soldecum']].min()
        ymax2 = dfs2[['solde', 'soldecum']].max()

    ymin1 = ymin1.min()
    ymax1 = ymax1.max()
    ymin2 = ymin2.min()
    ymax2 = ymax2.max()

    ymin = min(ymin1, ymin2)
    ymax = max(ymax1, ymax2)

    ymax = roundup(ymax)
    # si ymin = 10720 roundup => 20000 (bcp trop)
    # à tester! sinon on garde le code ci-dessous
    # yminok = roundup(ymin)
    # if ymin < 0:
    #     yminok = -yminok
    yminok = ymin

    y_minmax = [yminok, ymax]

    return y_minmax


def compute_year_minmax(exp1, rev1, solde1, exp2, rev2, solde2):
    """Compute min and max"""
    # Arrondi au segement (10 exp n where n compris entre 1 et infini) supérieur selon le nombre
    # si 88 => 100 si 1800 => 2000, si 25900 => 26000
    def roundup(x):
        x = abs(x) # valeur absolue de x
        if x == 0 or math.isnan(x):
            return 0
        length = math.ceil(math.log10(x));
        segment = math.pow(10, length-1);
        return int(math.ceil(x / segment)) * segment

    ymin = min(exp1, rev1, solde1, exp2, rev2, solde2)
    ymax = max(exp1, rev1, solde1, exp2, rev2, solde2)
    ymax = roundup(ymax)
    yminok = roundup(ymin)
    if ymin < 0:
        yminok = -yminok
    y_minmax = [yminok, ymax]

    return y_minmax


def create_table(df, nbr, y_exp, y_rev):
    """
    Used for table display in global graphic
    returns context with 4 list vars
    """
    tbl = {}
    df["month"] = df["x"].str[5:7]
    df["y"] = df[y_exp] + df[y_rev]
    df = df[['month', 'key', 'y']]
    dfp = df.pivot_table(index='key', values=['y'], columns=['month']).fillna(0)
    di = dfp.to_dict('index')
    # Loop must be done because some key-indexes like EMP or INV are not always present!
    for idx in dfp.index:
        mylist = list(di[idx].values())
        mykey = "{}{}".format(idx, nbr)
        tbl[mykey] = [round(x) for x in mylist]
    return tbl


def create_month_context(dfe1, dfr1, year1,
                         dfe2, dfr2, year2,
                         y_exp, y_rev,
                         title):
    """
    Create a context to be displayed in money.view.GraphIndex()
    return graph_month & graph_year
    y_exp: variable containing the column y_e or yvat_e (for expense)
    y_rev: variable containing the column y_r or yvat_r (for revenue)
    """
    def format_number(n):
        if math.isnan(n):
            n = 0
        else:
            n = int(round(n, 0))
            # 2020/01/13 rm Thousand separator for display reason (django 3.1)
            # n = "{:,}".format(n)
            # n = n.replace(",", " ")
        return n

    ctx = {}
    tbl1 = {}
    tbl2 = {}
    y_minmax = []
    # Load table1 & table2
    dfm1 = compute_month(dfe1, dfr1, year1)
    dfm2 = compute_month(dfe2, dfr2, year2)
    tbl1 = create_table(dfm1, "1", y_exp, y_rev)
    tbl2 = create_table(dfm2, "2", y_exp, y_rev)
    ctx.update(tbl1)
    ctx.update(tbl2)
    # ctx["table1"] = graph_display.format2html(dft1.y, True)
    # Affichage null => dict used in create_table

    # !!!Compute solde after removing key (global_group)!!!
    dfs1 = dfm1.groupby(['x']).sum().reset_index()
    dfs1 = compute_solde(dfs1, y_exp, y_rev)
    dfs2 = dfm2.groupby(['x']).sum().reset_index()
    dfs2 = compute_solde(dfs2, y_exp, y_rev)

    y_minmax_exprev = compute_month_minmax(dfs1, dfs2, "EXPREV")
    ctx["graph_month1"] = graph_display.build_month(
        dfs1['month'], dfs1[y_exp], dfs1[y_rev],
        dfs1['solde'], dfs1['soldecum'],
        y_minmax_exprev, "{0} {1}/mois".format(title, year1)
        )
    ctx["graph_month2"] = graph_display.build_month(
        dfs2['month'], dfs2[y_exp], dfs2[y_rev],
        dfs2['solde'], dfs2['soldecum'],
        y_minmax_exprev, "{0} {1}/mois".format(title, year2)
        )

    y_minmax_solde = compute_month_minmax(dfs1, dfs2, "SOLDE")
    ctx["graph_month1_solde"] = graph_display.build_month_solde(
        dfs1['month'],
        dfs1['solde'], dfs1['soldecum'],
        y_minmax_solde, "{0} {1}/mois".format(title, year1)
        )
    ctx["graph_month2_solde"] = graph_display.build_month_solde(
        dfs2['month'],
        dfs2['solde'], dfs2['soldecum'],
        y_minmax_solde, "{0} {1}/mois".format(title, year2)
        )

    exp1, rev1, solde1 = compute_year(dfs1[y_exp], dfs1[y_rev])
    exp2, rev2, solde2 = compute_year(dfs2[y_exp], dfs2[y_rev])
    y_minmax_year = compute_year_minmax(exp1, rev1, solde1, exp2, rev2, solde2)

    exp1 = format_number(exp1)
    rev1 = format_number(rev1)
    solde1 = format_number(solde1)
    exp2 = format_number(exp2)
    rev2 = format_number(rev2)
    solde2 = format_number(solde2)

    ctx["graph_year1"] = graph_display.build_year(rev1, exp1, solde1, y_minmax_year, "{0} {1}".format(title, year1))
    ctx["graph_year2"] = graph_display.build_year(rev2, exp2, solde2, y_minmax_year, "{0} {1}".format(title, year2))

    return ctx
