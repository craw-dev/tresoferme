var TresoPrevForm = {

  onReady : function () {
    $(".rev-change").change(this.computeSolde);
    $(".exp-change").change(this.computeSolde);
  },

  computeSolde : function() {
    let objname = $(this)[0].name;
    let pfx = objname.split('-')[1];
    // var pfx = $("#formset").index() - 2;
  	let exp = parseInt($("#id_form-" + pfx + "-exp").val());
    let rev = parseFloat($("#id_form-" + pfx + "-rev").val());
    let solde = rev - exp;
    let solde = (Math.round(solde*1000000000))/1000000000;
    $("#id_form-" + pfx + "-solde").val(solde);
  },
}

$(document).ready(function () {
	TresoPrevForm.onReady();
});
