var PaymentForm = {
    onReady : function () {
      $(".delete-row").click(this.deleteROW);
      $('[data-toggle="tooltip"]').tooltip();
    },

    deleteROW : function () {
        var pk = $(this)[0].id
        $.ajax({
            url : "/ajax/expensepayment/del",
            type : "POST",
            data : {
                'ip_id' : pk,
            },
            success : function (data) {
                console.log(data);
                location.reload();
            },
            error : function (xhr) {
                console.log(xhr.status + ": " + xhr.responseText);
                location.reload();
            }
        });
    },

}

  $(document).ready(function () {
  	PaymentForm.onReady();
  });
