var RevenueProductForm = {

  onReady : function () {
    $(".atelier-change").change(this.refreshProduct);
    $(".product-change").change(this.setPrice_and_VAT);
    $(".compute-vat").change(this.computeAmountVAT);
    $(".compute-vat").change(this.computeTotal);
    $(".quantity-change").change(this.computeAmountVAT);
    $(".quantity-change").change(this.computeTotal);
    $(".delete-row").click(this.deleteROW);
  },

  computeAmountVAT : function() {
    var objname = $(this)[0].name;
    var pfx = objname.split('-')[1];
    // var pfx = $("#formset").index() - 2;
  	var vat = parseInt($("#id_form-" + pfx + "-vat").val());
    var amount = parseFloat($("#id_form-" + pfx + "-amount").val());

  	if (vat==100) {
      alert("Veuillez dès lors également encoder le prix TVAC");
      $("#id_form-" + pfx + "-amountvat").prop('readonly', false);
      $("#id_form-" + pfx + "-amountvat").focus();
  		return;
  	}
  	if(amount==0) {
  		var amountvat = 0;
  	}
  	else {
      // var amountvat = amount/(100-vat)*100;
      var amountvat = amount + (vat/100 * amount);
      amountvat = (Math.round(amountvat*1000000000))/1000000000;
      $("#id_form-" + pfx + "-amountvat").val(amountvat);
  	}
  },

  computeTotal : function() {
    var objname = $(this)[0].name;
    var pfx = objname.split('-')[1];
    var tot = 0;
    var qt = parseFloat($("#id_form-" + pfx + "-quantity").val());
    var amountvat = parseFloat($("#id_form-" + pfx + "-amountvat").val());
    var amount = parseFloat($("#id_form-" + pfx + "-amount").val());
    if (!isNaN(qt)) {
      if (!isNaN(amountvat)) {
        var totvat = amountvat * qt;
        totvat = (Math.round(totvat*1000000000))/1000000000;
        $("#id_form-" + pfx + "-amounttotvat").val(totvat);
      }
      if (!isNaN(amount)) {
        var tot = amount * qt;
        tot = (Math.round(tot*1000000000))/1000000000
        $("#id_form-" + pfx + "-amounttot").val(tot);
      }
    }
  },

  computeAmountTOT : function() {
    var objname = $(this)[0].name;
    var pfx = objname.split('-')[1];
    var tot = 0;
    var qt = parseFloat($("#id_form-" + pfx + "-quantity").val());
    var amount = parseFloat($("#id_form-" + pfx + "-amount").val());
    if (!isNaN(amount) && !isNaN(qt)) {
      var tot = amount * qt;
    }
  	$("#id_form-" + pfx + "-amounttot").val(tot);
  },

  deleteROW : function () {
      var pk = $(this)[0].id;  
      console.log(pk);
      $.ajax({
          url : "/ajax/revenueproduct/del",
          type : "POST",
          data : {
              'ip_id' : pk,
          },
          success : function (json) {
              console.log(json);             
              location.reload();
          },
          error : function (xhr, errmsg, err) {
              console.log(xhr.status + ": " + xhr.responseText); // provide a
              // bit more info about the error to the console
              // location.reload(); does not work with FireFox which keeps the cache!
              // HBP SOLVED after adding "<form autocomplete="off" in the form!!!
              // location.href = detail_url;
              location.reload();
          }
      });
  },

  refreshProduct : function() {
    var objname = $(this)[0].name;
    console.log(objname);
    var pfx = objname.split('-')[1];
    // set back amount & vat values to default
    $("#id_form-" + pfx + "-amount").val();
    $("#id_form-" + pfx + "-vat").val(6);
    $("#id_form-" + pfx + "-amountvat").val();

    var atelier = $("#id_form-" + pfx + "-atelier").val();
    var product = "#id_form-" + pfx + "-product";
    $.ajax({
      url: '/ajax/ddl_product/',
      data: {
        'atelier': atelier
      },
      success: function (data) {
        $(product).html(data);
        // If only 2 options => select the last one
        var nbr = $(product + " option").length;
        if(nbr===2) {
          var lastVal = $(product + " option:last").val();
          $(product).val(lastVal);
          RevenueProductForm.setPrice_and_VAT(pfx);
        }
      }
    });
    $(product).focus();
  },

  setPrice_and_VAT : function(pfx) {
    if (typeof(pfx) === 'string') {
      var objid = "id_form-" + pfx + "-product";
    } else {
      var objid = $(this)[0].id;
      var pfx = objid.split('-')[1];
    } 
    // var objname = objid.split('-')[2];
    // if (objname != "product") {
    //   var objid = "id_form-" + pfx + "-product";
    // }
    var selOption = "#" + objid + " option:selected";
    // console.log(selOption);
    var sellprice = parseFloat($(selOption).attr('sellprice').replace(',', '.'));
    // console.log(sellprice);
    var vat = parseFloat($(selOption).attr('vat'));
    var sellpricevat = parseFloat($(selOption).attr('sellpricevat').replace(',', '.'));
    var atelier_id = $(selOption).attr('atelier_id');
    var unit = $(selOption).attr('unit');

    $("#id_form-" + pfx + "-atelier").val(atelier_id);
    $("#id_form-" + pfx + "-vat").val(vat);
		$("#id_form-" + pfx + "-amount").val(sellprice);
    $("#id_form-" + pfx + "-amountvat").val(sellpricevat);
    $("#id_form-" + pfx + "-unit").val(unit);
    // Focus on quantity at the end
    $("#id_form-" + pfx + "-quantity").focus();
  },

}

$(document).ready(function () {
	RevenueProductForm.onReady();
});
