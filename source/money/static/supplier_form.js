$(document).ready(function () {
    var categ = $("#id_expcat").val();
    $('#id_expcatgrp').on('change', function() {
        var group = $('#id_expcatgrp').val();
        if (group=="") {
            group = "NADA";
        }
        $.ajax({
            url: '/ajax/ddl_expcat/',
            data: {
                'expcatgrp': group
            },
            success: function (data) {
                $('#id_expcat').html(data);
                if (categ != "") {
                    $("#id_expcat").val(categ);
                }
            }
        });
    });

});