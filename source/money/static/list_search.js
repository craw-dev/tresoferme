var ListHTML = {
    onReady : function () {
      $("#id_idsearch").change(this.submitForm);
      $("#id_year").change(this.submitForm);
      $("#id_zip").change(this.submitForm);
      $("#id_atelier").change(this.submitForm);
      $("#id_supplier").change(this.submitForm);
      $("#id_client").change(this.submitForm);
      $("#id_expcatgrp").change(this.submitForm);
  		$("#id_expcat").change(this.submitForm);
      $("#id_revcatgrp").change(this.submitForm);
  		$("#id_revcat").change(this.submitForm);
  		$("#id_txtsearch").change(this.submitForm);
      $("#id_product").change(this.submitForm);
      $("#id_payflag").change(this.submitForm);
      $("#id_minimi").change(this.submitForm);
      $("#id_invoice_send").change(this.submitForm);

      $('#id_exportbtn').on('click', function() {
        $("#id_exportxls").val("XLSGO");
        $("#list_search").submit();
        $("#id_exportxls").val("NADA");
      });
    },


	submitForm : function() {
		$("#list_search").submit();
  },

}

$(document).ready(function () {
	ListHTML.onReady();
});
