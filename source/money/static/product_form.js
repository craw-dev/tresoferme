var ProductForm = {

    onReady : function () {
  		$("#id_sellpricevat").change(this.computeAmountNOVAT);
		$("#id_vat").on('change', this.computeAmountNOVAT);
		$("#id_sellprice").on('change', this.computeAmountVAT);
    },

    computeAmountNOVAT : function() {
		var valvat = parseFloat($("#id_sellpricevat").val());
		var vat = parseInt($("#id_vat").val());
		var valNOvat = parseFloat($("#id_sellprice").val());
		if(isNaN(valvat)) {valvat = 0};
		if(isNaN(valNOvat)) {valNOvat = 0};
		// Si Autre tva, ne rien faire!
    	if (vat==100) {
    		return;
    	}
			if (vat==0) {
				$("#id_sellprice").val(valvat);
				return;
			}
			valNOvat = valvat/(100+vat) * 100;
			// valNOvat = valvat/100*(100-vat);
			valNOvat = (Math.round(valNOvat*1000000000))/1000000000;
    		$("#id_sellprice").val(valNOvat);
		},

    computeAmountVAT : function() {
    	var valvat = parseFloat($("#id_sellpricevat").val());
			var vat = parseInt($("#id_vat").val());
			var valNOvat = parseFloat($("#id_sellprice").val());
			if(isNaN(valNOvat)) {valNOvat = 0};
			if(isNaN(valvat)) {valvat = 0};
			// Si Autre tva, ne rien faire!
    	if (vat==100) {
    		return;
			}
    	if (vat==0) {
				$("#id_sellpricevat").val(valNOvat);
				return;
			}
			console.log('ici');
			// valvat = valNOvat/(100-vat)*100;
			// TVAC = HTVA + (TVA/100 * HTVA)
			valvat = valNOvat + (vat/100 * valNOvat);
			valvat = (Math.round(valvat*1000000000))/1000000000;
    		$("#id_sellpricevat").val(valvat);
		},
}

$(document).ready(function () {
	ProductForm.onReady();
});
