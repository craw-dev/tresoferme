$(document).ready(function () {
	call_func();
    $('#id_loantype').on('change', function() {
		call_func();
	});
});

function call_func(){
	let loantype = $('#id_loantype').val();
	if (loantype == "CRE_CAI") {
		CRE_CAI();
	}
	if (loantype == "CRE_STR") {
		CRE_STR();
	}
}

function CRE_CAI() {
	$("#id_loanrefund").hide();
	$("label[for=id_loanrefund]").hide();
	$("#id_closedate").hide();
	$("label[for=id_closedate]").hide();
	$(".input-group-addon").hide();

	$('.form-text.text-muted').show();
	$("#id_interestrate").show();
	$("label[for=id_interestrate]").show();

	$("#id_interestrate").prop('required',true);
	$("label[for=id_interestrate]").addClass('requiredfield');
}

function CRE_STR() {
	$("#id_loanrefund").show();
	$("label[for=id_loanrefund]").show();
	$("#id_closedate").show();
	$("label[for=id_closedate]").show();
	$(".input-group-addon").show();

	$(".form-text.text-muted").hide();
	$("#id_interestrate").hide();
	$("label[for=id_interestrate]").hide();

	$("#id_interestrate").prop('required',false);
	$("label[for=id_interestrate]").removeClass('requiredfield');
}
