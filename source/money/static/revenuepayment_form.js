var RevenuePaymentForm = {
    onReady : function () {
      $(".delete-row").click(this.deleteROW);
    },

    deleteROW : function () {
        var ip_id = $(this)[0].id;
        $.ajax({
            url : "/ajax/revenuepayment/del",
            type : "POST",
            data : {
                'ip_id' : ip_id,
            },
            success : function (data) {
                console.log(data);
                location.reload();
                // location.href = "/" + data.root_url + "/revenue/" + data.pk;
            },
            error : function (xhr) {
                console.log(xhr.status + ": " + xhr.responseText);
                location.reload();
            }
        });
    },

}

  $(document).ready(function () {
  	RevenuePaymentForm.onReady();
  });
