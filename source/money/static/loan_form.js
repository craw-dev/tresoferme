var Loan = {

  onReady : function () {
		var url = window.location.pathname;
		if(url.includes('upd')) {
			this.hideFieldsCAI();
		}

		$("#id_loantype").change(this.hideFieldsCAI);

		$("#id_signupdate").on("dp.change", function(event) {
			$("#id_startdate").data("DateTimePicker").date(event.date);
		});

	},

	hideFieldsCAI : function() {
		// hide fields if "Crédit de caisse" HBP no more used here
		var fieldlist = ["startdate","loanyears","yearfreq"];
		var loantype = $("#id_loantype").val();

		if (loantype==="EMP_CAI") {
			// Set default values for loanyears and yearfreq
			$("#id_loanyears").val(100); // durée illimitée de 100 ans
			$("#id_yearfreq").val(0); // Pas d'application				
			$("div.input-group-addon.input-group-append").hide();
			fieldlist.forEach(function (myfield, i) {
				$("#id_"+myfield).hide();
				$("label[for=id_"+myfield+"]").hide();
			});
		} else {
			$("div.input-group-addon.input-group-append").show();
			fieldlist.forEach(function (myfield, i) {
				$("#id_"+myfield).show();
				$("label[for=id_"+myfield+"]").show();
			});
		}
    },

}

$(document).ready(function () {
	Loan.onReady();
});
