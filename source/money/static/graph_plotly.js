function filterDataSet(dataSet, key){
    dataSet = dataSet.key.map(function(key, index) { //convert dataset into a filterable form
        var dico = {
            'key': key,
            'y': dataSet.x[index],
            'x': dataSet.y[index],
            'text': dataSet.text[index],
            'orientation': 'h'
            // cfr https://code.tutsplus.com/tutorials/create-interactive-charts-using-plotlyjs-bar-charts--cms-29208
            // 'type': 'bar',
            // 'marker': {"color": "#f46036"}
        }
        return dico; 
    });

    var filteredData = dataSet.filter(function (item){ //filter dataset
        if (key==0){
            return item.key;
        } else {
            return item.key==key;
        }
    });

    var graphData = {'key':[], 'x':[], 'y':[], 'orientation': 'h',
                     'type': 'bar', 'text': [], textposition: 'auto',
                     'marker': {"color": barcolor}};
    for(var i = 0; i<filteredData.length; i++){ //populate empty dataset
        item = filteredData[i]
        graphData.key.push(item.key);
        graphData.y.push(item.x);
        graphData.x.push(item.y);
        graphData.text.push(item.text);
    }
    return graphData;
}

// Fil in ddlb based on list key, value
function getOptions(list){
    let option = '';
    for (let key in list){
        option += '<option value="'+ key + '">' + list[key] + '</option>';
    }
    return option;
}

function displayGraph(subgraph, subgraph_data, ddlb, layout){
    let mydata = filterDataSet(subgraph_data, ddlb.val());
    let yvalues = mydata.y;
    let ymax = Math.max.apply(null, yvalues);
    // https://stackoverflow.com/questions/14879691/get-number-of-digits-with-javascript
    let length = Math.log(ymax) * Math.LOG10E + 1 | 0;
    // segment = 10 exposant length
    let segment = Math.pow(10, length-1);
    ymax = parseInt(Math.ceil(ymax / segment)) * segment;
    // layout.xaxis = {range: [0, ymax]};
    layout.xaxis = {domain: [0.4, 1], nticks:10};
    Plotly.newPlot(subgraph, [mydata], layout); 
}
