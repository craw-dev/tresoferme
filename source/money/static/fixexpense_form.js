var FixExpense = {
	// Needed to capture the current value when update
	group: '',
	category: '',
	supplier: '',

    onReady : function () {
    	// when upd field2show must be shown + cat & supplier list must be adapted
		var url= window.location.pathname;
		var validated = false;
			
    	if(url.includes('upd')) {
			// var validated = $("#id_validated").val();
    		FixExpense.category = $("#id_expcat").val();
			FixExpense.supplier = $("#id_supplier").val();
		}

		this.refreshCat();
		this.refreshSupplier();

		// do not change dropdown when validated
		// if (validated==false) {
		// Refresh dropdown list
		$("#id_expcatgrp").change(this.refreshCat);
		$("#id_expcatgrp").change(this.refreshSupplier);
		$("#id_supplier").change(this.getGroup_Cat);
		$("#id_expcat").change(this.refreshSupplier);
		$("#id_amountvat").change(this.computeAmountNOVAT);
		$("#id_vat").on('change', this.computeAmountNOVAT);
		$("#id_amount").change(this.computeAmountVAT);
		// }
    },

	computeAmountNOVAT : function() {
		var valvat = parseFloat($("#id_amountvat").val());
		var vat = parseInt($("#id_vat").val());
		var valNOvat = parseFloat($("#id_amount").val());
		if(isNaN(valvat)) {valvat = 0};
		if(isNaN(valNOvat)) {valNOvat = 0};
		// Si Autre tva, ne rien faire!
		if (vat==100) {
			return;
		}
		if (vat==0) {
			$("#id_amount").val(valvat);
			return;
		}
		valNOvat = valvat/(100+vat) * 100;
		valNOvat = (Math.round(valNOvat*1000000000))/1000000000;
		$("#id_amount").val(valNOvat);
	},
		
    computeAmountVAT : function() {
    	var valvat = parseFloat($("#id_amountvat").val());
			var vat = parseInt($("#id_vat").val());
			var valNOvat = parseFloat($("#id_amount").val());
			if(isNaN(valNOvat)) {valNOvat = 0};
			if(isNaN(valvat)) {valvat = 0};
			// Si Autre tva, ne rien faire!
    	if (vat==100) {
    		return;
			}
    	if (vat==0) {
			$("#id_amountvat").val(valNOvat);
			return;
		}	
		var valvat = valNOvat + (vat/100 * valNOvat);
		valvat = (Math.round(valvat*1000000000))/1000000000;
		$("#id_amountvat").val(valvat);
	},

    // Ajax call to refresh Expense Category (ExpCat)
    refreshCat : function() {
			// Hide cat when Emprunt CatGrp
			FixExpense.group = $('#id_expcatgrp').val();
  		$.ajax({
  			url: '/ajax/ddl_expcat/',
  			data: {
  				'expcatgrp': FixExpense.group
  			},
  			success: function (data) {
  				$("#id_expcat").html(data);
  				$("#id_expcat").val(FixExpense.category);
					// $("#id_expcatgrp").val(FixExpense.group);
  				// If only 2 options => select the last one
  				var nbr = $("#id_expcat option").length;
  				if(nbr===2) {
  					var lastVal = $('#id_expcat option:last').val();
  					$('#id_expcat').val(lastVal);
					}
  			}
  		});
  	},

  	// Ajax call to refresh Supplier
    refreshSupplier : function() {
			var grp = $('#id_expcatgrp').val();
    	var categ = $('#id_expcat').val();
  		$.ajax({
  			url: '/ajax/ddl_supplier/',
  			data: {
  				'groupe': grp,
  				'categorie': categ
  			},
  			success: function (data) {
  				$("#id_supplier").html(data);
  				$("#id_supplier").val(FixExpense.supplier);
					// If only 2 options => select the last one
  				var nbr = $("#id_supplier option").length;
  				if(nbr===2) {
  					var lastVal = $('#id_supplier option:last').val();
  					$('#id_supplier').val(lastVal);
  				}
  			}
  		});
		},

		// Ajax call to refresh Expense Category (ExpCat)
    getGroup_Cat : function() {
			// Hide cat when Emprunt CatGrp
			var supplier = $('#id_supplier').val();
  		$.ajax({
  			url: '/ajax/ddl_expgroupcat_expcat/',
  			data: {
  				'supplier': supplier
  			},
  			success: function (data) {
					$("#id_expcatgrp").val(data.group);
					FixExpense.category = data.categ;
  				FixExpense.refreshCat();
  			}
  		});
  	},		

}

$(document).ready(function () {
	FixExpense.onReady();
});
