var MoneyForm = {
	// Needed to capture the current value when update
	category: '',
	catgrp: '',
	origin: '',

    onReady : function () {
		MoneyForm.hidePayFlag();
		MoneyForm.hideLoan();
		
		var url = window.location.pathname;
		if(url.includes('upd')) {
			MoneyForm.category = $("#id_revcat").val();
			MoneyForm.catgrp = $("#id_revcatgrp").val();
			this.hide4Credit();
		}
		
		if (url.includes('revenue')) {
			$("#id_revcatgrp").val("FRM");
			MoneyForm.origin = "REVENUE";
			// Vente directe (FRM_VTE) By default 
			if(!url.includes('upd')) {
				MoneyForm.category = "FRM_VTE";
			}
		}

		if(url.includes('bdc')) {
			$("#id_revcatgrp").val("FRM");
			MoneyForm.origin = "BDC";
			// FRM_BDC or FRM_DEV
			MoneyForm.category = $("#id_revcat").val();
		}
		
    	this.refreshCat();

    	$("#id_revcatgrp").change(this.refreshCat);
		$("#id_revcatgrp").change(this.hideLoan);
		$("#id_revcat").change(this.hideLoan);
		$("#id_revcat").change(this.hideVATfields);
		$("#id_revcat").change(this.refreshLoan);
		$("#id_revcat").change(this.hidePayFlag);
		$("#id_amountvat").change(this.computeAmountNOVAT);
		$("#id_vat").on('change', this.computeAmountNOVAT);
		$("#id_amount").change(this.computeAmountVAT);
		$("#id_vatvalue").change(this.changeNOVAT);

		// https://github.com/monim67/django-bootstrap-datepicker-plus/issues/29
		$("#id_inputdate").on("dp.change", function() {
			// Possible de fixer l'autre date immédiatement mais passage par fixDueDate qui vérifie DueDateList
		// $("#id_payduedate").data("DateTimePicker").date(event.date);
			MoneyForm.fixDueDate();
		});

		$("#id_payduedate").on("dp.change", function() {
			$("#id_duedatelist").val("USR");
		});

		$("#id_duedatelist").change(this.fixDueDate);

		Date.prototype.endMonth = function() {
		var month = this.getMonth(),
			year = this.getFullYear();
		var em = new Date(year, month + 1, 0);
				return em;
		};

		Date.prototype.toInputFormat = function() {
		var yyyy = this.getFullYear().toString();
		var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
		var dd  = this.getDate().toString();
		return (dd[1]?dd:"0"+dd[0]) + "/" + (mm[1]?mm:"0"+mm[0]) + "/" + yyyy; // padding
	    };
    },

	fixDueDate : function() {
		var myval = $("#id_duedatelist").val();
		var daysadd = $('option:selected', $('#id_duedatelist')).attr('daysadd');
		var days = parseInt(daysadd, 10);

		var d = $("#id_inputdate").val().slice(0, 10).split('/');
		var ddstr = d[1] +'/'+ d[0] +'/'+ d[2]; // mm/dd/yyyy
		var dd = new Date(ddstr);

		if (!isNaN(dd.getTime())) {
			dd.setDate(dd.getDate() + days);
			// 30 jours fin de mois
			if (myval === '30DM') {
				dd = dd.endMonth();
			}
			$("#id_payduedate").val(dd.toInputFormat());
		} else {
			alert("Invalid Date");
		}
	},

	hidePayFlag : function() {
		categ = $('#id_revcat').val();
		if (categ == "FRM_FAC" || categ == "FRM_BDC" || categ == "FRM_DEV") {
			$("#id_payflag").hide();
			$("label[for=id_payflag]").hide();
			$("#id_invoice_send").show();
			$("label[for=id_invoice_send]").show();			
		} else {
			$("#id_payflag").show();
			$("label[for=id_payflag]").show();
			$("#id_invoice_send").hide();
			$("label[for=id_invoice_send]").hide();				
		}
	},

	hideLoan : function() {
		grp = $('#id_revcatgrp').val();
		categ = $('#id_revcat').val();
		categ1 = "VIDE";
		if (categ != null) {
			categ1 = categ.split("_")[0]
		}
		if (grp == "EMP" || categ1 == "EMP" || grp == "CRE" || categ1 == "CRE") {
			$("#id_loan").show();
			$("label[for=id_loan]").show();
			$('.form-text.text-muted').show();
		} else {
			$("#id_loan").hide();
			$("label[for=id_loan]").hide();
			$('.form-text.text-muted').hide();
		}
		if (grp == "PRI" || categ1 == "PRI") {
			$("#id_aide_minimi").show();
			$("label[for=id_aide_minimi]").show();
		} else {
			$("#id_aide_minimi").hide();
			$("label[for=id_aide_minimi]").hide();
		}		
    },

	// https://stackoverflow.com/questions/10179815/get-loop-counter-index-using-for-of-syntax-in-javascript
	hideVATfields : function() {
		// hide VAT fields in case of BDC or Devis
		
		id = $('#id_revcat').val();
		// var id = $('option:selected', $('#id_revcat').attr('code'));
		let fieldlist = ["amountvat", "vat", "vatvalue", "amount", "payflag"];
		if (id == "FRM_BDC" || id == "FRM_DEV") {
			$('#id_amount').val(0);
			$('#id_amountvat').val(0);
			fieldlist.forEach(function (myfield, i) {
				$("#id_"+myfield).hide();
				$("label[for=id_"+myfield+"]").hide();
			});
		} else {
			fieldlist.forEach(function (myfield, i) {
				$("#id_"+myfield).show();
				$("label[for=id_"+myfield+"]").show();
			});
		}
    },

	changeNOVAT : function() {
		var valvat = parseFloat($("#id_amountvat").val());
		var valvatcomp = parseInt($("#id_vatvalue").val());
		var valNOvat = 0;
		valNOvat = valvat - valvatcomp;
		$("#id_amount").val(valNOvat);
		// Set vat to 100 (Autre)
		$("#id_vat").val(100);
    },

    computeAmountNOVAT : function() {
    	var valvat = parseFloat($("#id_amountvat").val());
		var vat = parseInt($("#id_vat").val());
		var valvatcomp = 0;
		var valNOvat = parseFloat($("#id_amount").val());
		if(isNaN(valvat)) {valvat = 0};
		if(isNaN(valNOvat)) {valNOvat = 0};
		// Si Autre tva, ne rien faire!
    	if (vat==100) {
    		return;
    	}
			if (vat==0) {
				$("#id_amount").val(valvat);
				$("#id_vatvalue").val(0);
				return;
			}	
			// valNOvat = valvat/100*(100-vat);
			valNOvat = valvat/(100+vat) * 100;
			valvatcomp = valvat - valNOvat;
			valNOvat = (Math.round(valNOvat*1000000000))/1000000000;
			valvatcomp = (Math.round(valvatcomp*1000000000))/1000000000;
		$("#id_amount").val(valNOvat);
		$("#id_vatvalue").val(valvatcomp);
		},
		
    computeAmountVAT : function() {
    	var valvat = parseFloat($("#id_amountvat").val());
		var vat = parseInt($("#id_vat").val());
		var valNOvat = parseFloat($("#id_amount").val());
		if(isNaN(valNOvat)) {valNOvat = 0};
		if(isNaN(valvat)) {valvat = 0};
		// Si Autre tva, ne rien faire!
    	if (vat==100) {
    		return;
			}
    	if (vat==0) {
				$("#id_amountvat").val(valNOvat);
				$("#id_vatvalue").val(0);
				return;
			}			
			// valvat = valNOvat/(100-vat)*100;
			valvat = valNOvat + (vat/100 * valNOvat);
			valvatcomp = valvat - valNOvat;
			valvat = (Math.round(valvat*1000000000))/1000000000;
			valvatcomp = (Math.round(valvatcomp*1000000000))/1000000000;
		$("#id_amountvat").val(valvat);
		$("#id_vatvalue").val(valvatcomp);
		},		

    // Ajax call to refresh Category (revCat) when grp change
    refreshCat : function() {
		var grp = $('#id_revcatgrp').val();
  		$.ajax({
  			url: '/ajax/ddl_revcat/',
  			data: {
				'origin': MoneyForm.origin,
  				'revcatgrp': grp
  			},
  			success: function (data) {
  				$("#id_revcat").html(data);
				$("#id_revcat").val(MoneyForm.category);
				if (MoneyForm.origin=='BDC') {
					MoneyForm.hideVATfields();
				} else {
					var nbr = $("#id_revcat option").length;
					if(nbr===2) {
						var firstVal = $('#id_revcat option:first').val();
						$('#id_revcat').val(firstVal);
					}
				}
  			}
  		});
	},

	// Ajax call to refresh Loan
	refreshLoan : function() {
		var categ = $('#id_revcat').val();
		var categ1 = categ.split("_")[0]
		if (categ1 === "EMP") {
			$('#id_revcatgrp').val("EMP");
			$.ajax({
				url: '/ajax/ddl_loan/',
				data: {
					'loantype': categ
				},
				success: function (data) {
					$("#id_loan").html(data);
					$("#id_loan").val(MoneyForm.loan);
					// If only 2 options => select the last one
					var nbr = $("#id_loan option").length;
					if(nbr===2) {
						var lastVal = $('#id_loan option:last').val();
						$('#id_loan').val(lastVal);
					}
				}
			});
		}
	},

	hide4Credit : function() {
		let classif = $('option:selected', $('#id_client')).attr('classification');
		let fieldlist = ["vat", "vatvalue", "amount", "payflag", "loan"];
		if (classif=="BQE") {
			fieldlist.forEach(function (myfield) {
				$("#id_"+myfield).hide();
				$("label[for=id_"+myfield+"]").hide();
			});
			$('.form-text.text-muted').hide();
		}
    },


}

$(document).ready(function () {
	MoneyForm.onReady();
});

