var FixRevenue = {
	// Needed to capture the current value when update
	group: '',
	category: '',

  onReady : function () {
    	// when cat list must be adapted
    	var url= window.location.pathname;

    	if(url.includes('add') && !$('#id_revcatgrp').val()) {
				$('#id_revcatgrp').val('COM');
			}

  		FixRevenue.category = $("#id_revcat").val();

  		this.refreshCat();

    	// Refresh dropdown list
			$("#id_revcatgrp").change(this.refreshCat);
			$("#id_amountvat").change(this.computeAmountNOVAT);
			$("#id_vat").on('change', this.computeAmountNOVAT);
			$("#id_amount").change(this.computeAmountVAT);
		},

	computeAmountNOVAT : function() {
		var valvat = parseFloat($("#id_amountvat").val());
		var vat = parseInt($("#id_vat").val());
		var valNOvat = parseFloat($("#id_amount").val());
		if(isNaN(valvat)) {valvat = 0};
		if(isNaN(valNOvat)) {valNOvat = 0};
		// Si Autre tva, ne rien faire!
		if (vat==100) {
			return;
		}
		if (vat==0) {
			$("#id_amount").val(valvat);
			return;
		}	
		valNOvat = valvat/(100+vat) * 100;
		valNOvat = (Math.round(valNOvat*1000000000))/1000000000;
		$("#id_amount").val(valNOvat);
		},
		
	computeAmountVAT : function() {
		var valvat = parseFloat($("#id_amountvat").val());
		var vat = parseInt($("#id_vat").val());
		var valNOvat = parseFloat($("#id_amount").val());
		if(isNaN(valNOvat)) {valNOvat = 0};
		if(isNaN(valvat)) {valvat = 0};
		// Si Autre tva, ne rien faire!
		if (vat==100) {
			return;
			}
		if (vat==0) {
			$("#id_amountvat").val(valNOvat);
			return;
		}			
		var valvat = valNOvat + (vat/100 * valNOvat);
		valvat = (Math.round(valvat*1000000000))/1000000000;
		$("#id_amountvat").val(valvat);
		},

	// Ajax call to refresh revense Category (revCat)
	refreshCat : function() {
  		FixRevenue.group = $('#id_revcatgrp').val();
		$.ajax({
			url: '/ajax/ddl_revcat/',
			data: {
				'revcatgrp': FixRevenue.group
			},
			success: function (data) {
				$("#id_revcat").html(data);
				$("#id_revcat").val(FixRevenue.category);
				$("#id_revcatgrp").val(FixRevenue.group);
				// If only 2 options => select the last one
				var nbr = $("#id_revcat option").length;
				if(nbr===2) {
					var lastVal = $('#id_revcat option:last').val();
					// console.log(lastVal);
					$('#id_revcat').val(lastVal);
					// FixRevenue.showFields();
					$("#id_revcat").focus();
				}
			}
		});

	},

}

$(document).ready(function () {
	FixRevenue.onReady();
});
