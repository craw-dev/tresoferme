var MoneyForm = {
	// Needed to capture the current value when update
	group: '',
	category: '',
	supplier: '',

    onReady : function () {
		MoneyForm.category = $("#id_expcat").val();
		MoneyForm.supplier = $("#id_supplier").val();
		MoneyForm.loan = $("#id_loan").val();

		var url = window.location.pathname;
		if(url.includes('upd')) {
			this.hide4Credit();
		}
		// Refresh dropdown list
		if(url.includes('invest')) {
			MoneyForm.group = 'INV';
			this.refreshSupplier();
			// this.hide4Invest();
		}

		MoneyForm.refreshCat();

		$("#id_supplier").change(this.get_Cat);
  		$("#id_amountvat").change(this.computeAmountNOVAT);
		$("#id_vat").on('change', this.computeAmountNOVAT);
		$("#id_amount").change(this.computeAmountVAT);
		$("#id_vatvalue").change(this.changeNOVAT);

		$("#id_inputdate").on("dp.change", function() {
			MoneyForm.fixDueDate();
		});
		$("#id_payduedate").on("dp.change", function() {
			$("#id_duedatelist").val("USR");
		});
		$("#id_duedatelist").change(this.fixDueDate);

		Date.prototype.endMonth = function() {
			let month = this.getMonth(), year = this.getFullYear();
			let em = new Date(year, month + 1, 0);
			return em;
		};

		Date.prototype.toInputFormat = function() {
			let yyyy = this.getFullYear().toString();
			let mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
			let dd  = this.getDate().toString();
			return (dd[1]?dd:"0"+dd[0]) + "/" + (mm[1]?mm:"0"+mm[0]) + "/" + yyyy; // padding
	    };
	},

	fixDueDate : function() {
		var myval = $("#id_duedatelist").val();
		var daysadd = $('option:selected', $('#id_duedatelist')).attr('daysadd');
		var days = parseInt(daysadd, 10);

		var d = $("#id_inputdate").val().slice(0, 10).split('/');
		var ddstr = d[1] +'/'+ d[0] +'/'+ d[2]; // mm/dd/yyyy
		console.log(ddstr);
		var dd = new Date(ddstr);

		if (!isNaN(dd.getTime())) {
			dd.setDate(dd.getDate() + days);
			// 30 jours fin de mois
			if (myval === '30DM') {
				dd = dd.endMonth();
			}
			$("#id_payduedate").val(dd.toInputFormat());
		} else {
			alert("Invalid Date");
		}
	},

	changeNOVAT : function() {
		var valvat = parseFloat($("#id_amountvat").val());
		var valvatcomp = parseFloat($("#id_vatvalue").val());
		var valNOvat = 0;
		valNOvat = valvat - valvatcomp;
		$("#id_amount").val(valNOvat);
		// Set vat to 100 (Autre)
		$("#id_vat").val(100);
    },

	computeAmountNOVAT : function() {
		var valvat = parseFloat($("#id_amountvat").val());
		var vat = parseInt($("#id_vat").val());
		var valvatcomp = 0;
		var valNOvat = parseFloat($("#id_amount").val());
		if(isNaN(valvat)) {valvat = 0};
		if(isNaN(valNOvat)) {valNOvat = 0};
		// Si Autre tva, ne rien faire!
		if (vat==100) {
			return;
		}
			if (vat==0) {
				$("#id_amount").val(valvat);
				$("#id_vatvalue").val(0);
				return;
			}
			valNOvat = valvat/(100+vat) * 100;
			// valNOvat = valvat/100*(100-vat);
			valvatcomp = valvat - valNOvat;
			valNOvat = (Math.round(valNOvat*1000000000))/1000000000;
			valvatcomp = (Math.round(valvatcomp*1000000000))/1000000000;
		$("#id_amount").val(valNOvat);
		$("#id_vatvalue").val(valvatcomp);
		},

    computeAmountVAT : function() {
    	var valvat = parseFloat($("#id_amountvat").val());
		var vat = parseInt($("#id_vat").val());
		var valNOvat = parseFloat($("#id_amount").val());
		if(isNaN(valNOvat)) {valNOvat = 0};
		if(isNaN(valvat)) {valvat = 0};
			// Si Autre tva, caclculer vatvalue
    	if (vat==100) {
			vatvalue = valvat - valNOvat;
			vatvalue = (Math.round(vatvalue*1000000000))/1000000000;
			$("#id_vatvalue").val(vatvalue);
    		return;
			}
    	if (vat==0) {
				$("#id_amountvat").val(valNOvat);
				$("#id_vatvalue").val(0);
				return;
			}
		// valvat = valNOvat/(100-vat)*100;
		valvat = valNOvat + (vat/100 * valNOvat);		
		valvatcomp = valvat - valNOvat;
		valvat = (Math.round(valvat*1000000000))/1000000000;
		valvatcomp = (Math.round(valvatcomp*1000000000))/1000000000;
		$("#id_amountvat").val(valvat);
		$("#id_vatvalue").val(valvatcomp);
	},

	// Ajax call to refresh Expense Category (ExpCat)
	// must be called only if a supplier is selected!
    get_Cat : function() {
		// Hide cat when Emprunt CatGrp
		var supplier = parseInt($('#id_supplier').val());
		console.log(supplier);
		// must be called only if a supplier is selected!
		if (supplier!=undefined) {
			$.ajax({
				url: '/ajax/ddl_get_expcat/',
				data: {
					'supplier': supplier
				},
				success: function (data) {
					MoneyForm.category = data.categ;
					// if (data.group[0] == 'CRE'){
					// 	MoneyForm.refreshCredit();
					// }
					// if (data.group[0] == 'EMP'){
					// 	MoneyForm.refreshLoan();
					// }
					MoneyForm.refreshCat();
				}
			});
		}
  	},

	// Specific fields: hide & set value to NULL
	// Si appel ajax, la suite des événements se fait toujours avant même si placé après
	// => A ne pas faire
    // hideFields : function() {
	// 	// Ajax call to get fields to hide
	// 	$.ajax({
	// 	url: '/ajax/get_item2hide/',
	// 	data: {
	// 		'whichmodel': 'ExpCat'
	// 	},
	// 	success: function (data) {
	// 			let myList = data.myList
	// 			console.log('on cache');
	// 			myList.split(/\s*,\s*/).forEach(function(myitem) {
	// 				let myfield = 'id_'+myitem;
	// 				$("#"+myfield).val(0);
	// 				$("#"+myfield).hide();
	// 				$("label[for="+myfield+"]").hide();
	// 			});
  	// 		}
  	// 	});
	// },
	
	hideFields2 : function() {
		let fieldlist = ["amortyear", "quantity", "unit", "loan"];
		fieldlist.forEach(function (myfield) {
			$("#id_"+myfield).hide();
			$("label[for=id_"+myfield+"]").hide();
		});
	},

    // Show specific fields if needed
    showFields : function() {
		MoneyForm.hideFields2();
		if ($('#id_expcat').val() != "") {
			let listFields = $('option:selected', $('#id_expcat')).attr('field2show');
			if (typeof listFields != "undefined" && listFields != 'None') {
				listFields.split(/\s*,\s*/).forEach(function(myitem) {
					let myfield = 'id_'+myitem;
					$("#"+myfield).show();
					$("label[for="+myfield+"]").show();
				});
			}
		}
    },

    // Ajax call to refresh Expense Category (ExpCat)
    refreshCat : function() {
  		$.ajax({
  			url: '/ajax/ddl_expcat/',
  			data: {
				'expcatgrp': MoneyForm.group,
				'expcat[]': MoneyForm.category
  			},
  			success: function (data) {
  				$("#id_expcat").html(data);
				if (MoneyForm.category != "") {
					$("#id_expcat").val(MoneyForm.category);
					$("#id_expcat option[value='"+MoneyForm.category+"']").prop('selected', true);
				} else {
					// If only 2 options => select the last one
					var nbr = $("#id_expcat option").length;
					if(nbr===2) {
						var lastVal = $('#id_expcat option:last').val();
						$('#id_expcat').val(lastVal);
						}
				}
				MoneyForm.showFields();
  			}
  		});
	  },
	  
	// Hide specific field for invest management (EMP, CRE or TRE)
	// hide4Invest : function() {
	// 	let fieldlist = ["vat", "vatvalue", "amount", "payflag", "duedatelist", "payduedate"];
	// 	$('#id_payflag').prop('checked', true);
	// 	$("div.input-group-addon.input-group-append").hide();
	// 	$("div.form-check").hide();		
	// 	fieldlist.forEach(function (myfield) {
	// 		$("#id_"+myfield).hide();
	// 		$("label[for=id_"+myfield+"]").hide();
	// 	});
	// },

  	// Ajax call to refresh Supplier only for INV
    refreshSupplier : function() {
  		$.ajax({
  			url: '/ajax/ddl_supplier/',
  			data: {
  				'group': 'INV',
  			},
  			success: function (data) {
				// console.log(data);
  				$("#id_supplier").html(data);
  				$("#id_supplier").val(MoneyForm.supplier);
				// If only 2 options => select the last one
				var nbr = $("#id_supplier option").length;
  				if(nbr===2) {
  					var lastVal = $('#id_supplier option:last').val();
					$('#id_supplier').val(lastVal);
					MoneyForm.get_Cat();
				}
  			}
  		});
	},

	hide4Credit : function() {
		let categ = MoneyForm.category;
		let fieldlist = ["vat", "vatvalue", "amount", "payflag", "loan"];
		if (categ == "CRE_STR" || categ == "CRE_CAI" || categ == "EMP_CAP" || categ == "EMP_PAY") {
			fieldlist.forEach(function (myfield) {
				$("#id_"+myfield).hide();
				$("label[for=id_"+myfield+"]").hide();
			});
			$('.form-text.text-muted').hide();
			// $('#id_payflag').prop('checked', true);
		}
	},

}

$(document).ready(function () {
	MoneyForm.onReady();
});
