# encoding:utf-8
import re
import logging
from datetime import date
from collections import OrderedDict
from dateutil.relativedelta import relativedelta

from django.db.models import Sum
from django import forms
from django.utils.translation import ugettext as _
from .widgets import DataAttributesSelect, MyDatePickerInput
from farm.models import Atelier, AtelierList
from .models import (
    DueDateList,
    ExpCatGrp,
    ExpCat,
    RevCat,
    Expense,
    Supplier,
    Client,
    Product,
    Revenue,
    RevenueProduct,
    RevenuePayment,
    ExpensePayment,
    RevCatGrp,
    FixRevenue,
    FixExpense,
    Loan,
    LoanRefund,
    MonthlyPrevision,
    Credit,
    CreditRefund
)
from accounts.models import GeoZip

LOG = logging.getLogger(__name__)
LOG_DEBUG = logging.getLogger("mydebug")


class CreditRefundForm(forms.ModelForm):
    solde = 0

    class Meta:
        model = CreditRefund
        fields = ['withdrawdate', 'withdrawamount', 'refunddate']
        widgets = {
            'withdrawdate': MyDatePickerInput().start_of('mydate'),
            'refunddate': MyDatePickerInput().end_of('mydate'),
        }

    def __init__(self, *args, **kwargs):
        self.operation = kwargs.pop("operation", None)
        self.credit = kwargs.pop("credit", None)
        super().__init__(*args, **kwargs)
        creditrfd = self.credit.creditrefund_set.filter(refunddate=None)
        if self.credit.loantype.id == "CRE_STR":
            self.fields["refunddate"].widget = forms.HiddenInput()
        if self.operation == "UPDATE":
            myself = kwargs["instance"]
            self.fields["withdrawdate"].widget.attrs["readonly"] = True
            self.fields["withdrawamount"].widget.attrs["readonly"] = True
            creditrfd = creditrfd.exclude(id=myself.id)
        else:
            self.fields["withdrawdate"].initial = date.today()
        totwithdraw = creditrfd.aggregate(amount=Sum("withdrawamount"))["amount"]
        if totwithdraw is None:
            totwithdraw = 0
        self.solde = self.credit.loanamount - totwithdraw
        self.fields["withdrawamount"].initial = self.solde

    def clean_withdrawdate(self):
        withdrawdate = self.cleaned_data.get("withdrawdate")
        if withdrawdate < self.credit.startdate:
            # self.add_error("interestrate",
            #         _('La date de prélèvement doit être supérieure à la date de souscription du crédit.'))
            raise forms.ValidationError(_('La date de prélèvement doit être supérieure à la date de souscription du crédit.'))
        return withdrawdate

    def clean_withdrawamount(self):
        withdrawamount = self.cleaned_data.get("withdrawamount")
        if withdrawamount > self.credit.loanamount:
            raise forms.ValidationError(_('Le montant prélevé doit être inférieur ou égal au montant total souscrit pour votre crédit.'))
        if withdrawamount > self.solde:
            raise forms.ValidationError(_('Le montant prélevé doit être inférieur ou égal à {}.'.format(self.solde)))
        return withdrawamount


class LoanRefundForm(forms.ModelForm):

    class Meta:
        model = LoanRefund
        fields = ['debitflag']


class LoanForm(forms.ModelForm):

    class Meta:
        model = Loan
        fields = ['refshort', 'loantype',
                  'bankrefid', 'bankrefdesc',
                  'loanamount',
                  'startdate', 'loanyears', 'yearfreq',
                  'interestrate']
        widgets = {
            'startdate': MyDatePickerInput(),
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("connected_user", None)
        self.operation = kwargs.pop("operation", None)
        super().__init__(*args, **kwargs)
        self.fields["loantype"].queryset = ExpCat.objects.filter(group="EMP").order_by("name")

    def clean_refshort(self):
        refshort = self.cleaned_data.get("refshort")
        try:
            if self.operation == "CREATE":
                Loan.objects.get(refshort=refshort, profile=self.user)
                raise forms.ValidationError(_('Cette étiquette existe déjà.'))
        except Loan.DoesNotExist:
            pass
        return refshort


class CreditForm(forms.ModelForm):

    class Meta:
        model = Credit
        fields = ['refshort', 'loantype',
                  'bankrefid', 'bankrefdesc',
                  'loanamount', 'loanrefund',
                  'startdate', 'closedate', 'interestrate']
        widgets = {
            'startdate': MyDatePickerInput().start_of('mydate'),
            'closedate': MyDatePickerInput().end_of('mydate'),
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("connected_user", None)
        self.operation = kwargs.pop("operation", None)
        super().__init__(*args, **kwargs)
        self.fields["loantype"].queryset = ExpCat.objects.filter(group="CRE").order_by("name")
        self.fields["loanrefund"].widget.attrs = {"placeholder": _("Communiqué par la banque selon votre plan de prélèvement")}
        # ajout attribut field2show aux options de loantype
        data = dict(ExpCat.objects.values_list("id", "field2show"))
        data[""] = "------------"  # empty option
        self.fields["loantype"].widget = DataAttributesSelect(
            choices=self.fields["loantype"].choices, data=data, newattr="field2show"
        )

    def clean_refshort(self):
        refshort = self.cleaned_data.get("refshort")
        try:
            if self.operation == "CREATE":
                Credit.objects.get(refshort=refshort, profile=self.user)
                raise forms.ValidationError(_('Cette étiquette existe déjà.'))
        except Credit.DoesNotExist:
            pass
        return refshort

    def clean_loanamount(self):
        loanamount = self.cleaned_data.get("loanamount")
        if loanamount > 100000:
            raise forms.ValidationError(_('Vous ne pouvez pas solliciter plus de 100.000€.'))
        return loanamount

    def clean_startdate(self):
        startdate = self.cleaned_data.get("startdate")
        if startdate is None:
            raise forms.ValidationError(_("Vous devez compléter une date de souscription."))
        return startdate

    def clean_interestrate(self):
        interestrate = self.cleaned_data.get("interestrate")
        if interestrate is None:
            raise forms.ValidationError(_("Vous devez compléter le taux d'intérêt."))
        return interestrate

    def clean(self):
        dataclean = super().clean()
        loantype = dataclean.get("loantype")
        if dataclean.get("interestrate") is not None:
            interestrate = int(dataclean.get("interestrate"))
        startdate = dataclean.get("startdate")
        if loantype.id == 'CRE_STR':
            # if interestrate > 2:
            #     self.add_error("interestrate",
            #         _("Pour un straight loan, le taux d'intérêt doit être inférieur à 3%."))
            loanrefund = dataclean.get("loanrefund")
            loanamount = dataclean.get("loanamount")
            if loanrefund is None:
                self.add_error("loanrefund", _('Montant à compléter et suppérieur au montant sollicité.'))
            elif loanrefund <= loanamount:
                self.add_error("loanrefund",
                    _("Le montant à rembourser doit être suppérieur au montant sollicité."))
            closedate = dataclean.get("closedate")
            diffdate = closedate - startdate
            if diffdate.days > 365:
                msg = _("Pour un straight loan, la durée ne peut excéder 1 an.")
                self.add_error("closedate", msg)
                raise forms.ValidationError(msg)

        if loantype.id == 'CRE_CAI' and interestrate < 7:
            self.add_error("interestrate",
                _("Pour un crédit de caisse, le taux d'intérêt doit être suppérieur à 7%."))


class ExpenseForm(forms.ModelForm):
    """
    Expense form
    """
    # expcatgrp = forms.ModelChoiceField(queryset=ExpCatGrp.objects.all(),
    #                 label="Groupe",
    #                 required=False,
    #                 widget=forms.SelectMultiple(attrs={'size':'4', 'class': "form-control"})
    #                 )
    
    class Meta:
        model = Expense
        fields = [
            "expnr", "inputdate", "duedatelist", "payduedate",
            "refdesc", "supplier",
            "expcat", "atelier",
            # "quantity", "unit", HBP Not used for the moment
            "amortyear",
            "amountvat", "vat", "vatvalue", "amount",
            "payflag"
        ]
        widgets = {
            "expnr": forms.TextInput(attrs={"placeholder": "Numéro de facture, Nr unique dans l'année, etc."}),
            "vatvalue": forms.TextInput(attrs={"placeholder":
                                        "Optionnel, à compléter pour les TVA multiples ou autres que 6, 12 et 21%."}),
            "inputdate": MyDatePickerInput().start_of('mydate'),
            "payduedate": MyDatePickerInput().end_of('mydate'),
        }

    def __init__(self, *args, **kwargs):
        # get param before init
        self.user = kwargs.pop("connected_user", None)
        self.operation = kwargs.pop("operation", None)
        super(ExpenseForm, self).__init__(*args, **kwargs)
        self.fields["atelier"].empty_label = None
        self.fields["atelier"].queryset = Atelier.objects.for_user(self.user).order_by("atelier")
        supplierqs = Supplier.objects.for_user(self.user)
        self.fields["supplier"].queryset = supplierqs
        if self.operation == "CREATE":
            self.fields["expcat"].queryset = ExpCat.objects.exclude(group__id__in=("CRE", "EMP"))
            self.fields["supplier"].queryset = supplierqs.exclude(expcat__group__id__in=("CRE", "EMP"))
        if self.operation == "UPDATE":
            if self.fields["amortyear"].initial:
                supplier_qs = Supplier.objects.for_user(self.user).all().filter(expcat__group__id='INV')
                self.fields["supplier"].queryset = supplier_qs
        
        try:
            gen = AtelierList.objects.get(id="GEN")
            inst = Atelier.objects.get(atelier=gen, profile=self.user)
            self.fields["atelier"].initial = inst.id
        except Atelier.DoesNotExist:
            LOG.warning("Atelier général absent pour user {0}. \
                Vérifier dans accounts.views.ProfileUpdateView".format(self.user))

        # ajout attribut field2show aux options de expcat
        data = dict(ExpCat.objects.values_list("id", "field2show"))
        data[""] = "------------"  # empty option
        self.fields["expcat"].widget = DataAttributesSelect(
            choices=self.fields["expcat"].choices, data=data, newattr="field2show"
        )
        # ajout attribut daysadd aux options de duedatelist
        data = dict(DueDateList.objects.values_list("id", "daysadd"))
        data[""] = "------------"  # empty option
        self.fields["duedatelist"].widget = DataAttributesSelect(
            choices=self.fields["duedatelist"].choices, data=data, newattr="daysadd"
        )

    def clean_expnr(self):
        expnr = self.cleaned_data.get("expnr")
        try:
            if self.operation == "CREATE":
                Expense.objects.get(expnr=expnr, profile=self.user)
                raise forms.ValidationError(_('Cet identifiant unique existe déjà.'))
        except Expense.DoesNotExist:
            pass
        return expnr

    def clean(self):
        dataclean = super().clean()
        amortyear = dataclean.get("amortyear")
        expcat = dataclean.get("expcat")
        if expcat.group.id == "INV" and amortyear is None:
            errmsg = _(
                "La durée d'amortissement est obligatoire pour un investissement."
            )
            self.add_error("amortyear", errmsg)

class SupplierForm(forms.ModelForm):

    zipsearch = forms.CharField(label=_("Localité recherche"), max_length=10,
                        required=False,
                        help_text=_("Encodez les 3 premiers chiffres/caractères du code postal ou de la localité"),
                        widget=forms.TextInput(attrs={
                            "class": "form-control",
                            "placeholder": _("Recherche code postal ou localité"),
                            "onkeyup": 'fillin_ddl_zip()'
                            })
                        )

    expcatgrp = forms.ModelChoiceField(
        label="Groupe",
        queryset=ExpCatGrp.objects.all(),
        required=False,
        # empty_label=None,
        widget=forms.Select(attrs={'class': "form-control"})
        )

    class Meta:
        model = Supplier
        fields = [
            "company", "contact_name", "email", "vatnr",
            "expcat",
            "mobile", "phone", "zip", "street", "streetnr", "mailboxnr",
            "remarks",
        ]
        widgets = {
            "remarks": forms.Textarea(attrs={"rows": 4})
            }

    def __init__(self, *args, **kwargs):
        
        self.user = kwargs.pop("connected_user", None)
        self.operation = kwargs.pop("operation", None)
        super(SupplierForm, self).__init__(*args, **kwargs)
        # Set zip qs
        self.fields['zip'].queryset = GeoZip.objects.none()
        if 'zipsearch' in self.data:
            try:
                zipsearch = self.data.get('zipsearch')
                if zipsearch.isnumeric():
                    qs = GeoZip.objects.filter(postal_code__istartswith=zipsearch)
                else:
                    qs = GeoZip.objects.filter(place_name__istartswith=zipsearch)
                self.fields['zip'].queryset = qs
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            if self.instance.zip:
                qs = GeoZip.objects.filter(pk=self.instance.zip.id)
                self.fields['zip'].queryset = qs
        # reorder the fields
        new_order = [
            ('company', self.fields['company']),
            ('contact_name', self.fields['contact_name']),
            ('email', self.fields['email']),
            ('vatnr', self.fields['vatnr']),
            ('expcatgrp', self.fields['expcatgrp']),
            ('expcat', self.fields['expcat']),
            ('mobile', self.fields['mobile']),
            ('phone', self.fields['phone']),                  
            ('zipsearch', self.fields['zipsearch']),
        ]
        new_order.extend(list(self.fields.items())[6:])
        self.fields = OrderedDict(new_order)
        # self.fields['expcat'].queryset = ExpCat.objects.all()
        self.fields['expcat'].widget.attrs = {'size':'8'}
        expcatname = self.initial.get('expcat', None)
        if expcatname is not None:
            expcat = ExpCat.objects.get(name=expcatname[0])
            self.fields['expcatgrp'].initial = expcat.group

    # def clean_vatnr(self):
    #     val = self.cleaned_data["vatnr"]
    #     re_pattern = re.compile(r"^BE0[0-9]{9,10}$")
    #     if val is None:
    #         return None
    #     if re_pattern.match(val) and len(val) > 0:
    #         return val
    #     else:
    #         raise forms.ValidationError(
    #             _("Respecter le format: BE0 suivi de 9 ou 10 chiffres."))

    def clean_company(self):
        company = self.cleaned_data.get("company")
        try:
            if self.operation == "CREATE":
                Supplier.objects.get(company=company, profile=self.user)
                raise forms.ValidationError(_('Ce fournisseur existe déjà.'))
        except Supplier.DoesNotExist:
            pass
        return company


class ClientForm(forms.ModelForm):

    zipsearch = forms.CharField(label=_("Localité recherche"), max_length=10,
                        required=False,
                        help_text=_("Encodez les 3 premiers chiffres/caractères du code postal ou de la localité"),
                        widget=forms.TextInput(attrs={
                            "class": "form-control",
                            "placeholder": _("Recherche code postal ou localité"),
                            "onkeyup": 'fillin_ddl_zip()'
                            })
                        )

    class Meta:
        model = Client
        fields = [
            "company", "contact_name", "email", "clienttype",
            "vatnr",
            "mobile", "phone", "zip", "street", "streetnr", "mailboxnr",
            "remarks",
        ]
        widgets = {
            "vatnr": forms.TextInput(attrs={"placeholder": "ex: BE01234567890"}),
            "remarks": forms.Textarea(attrs={"rows": 4}),
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("connected_user", None)
        self.operation = kwargs.pop("operation", None)
        super(ClientForm, self).__init__(*args, **kwargs)
        # Set zip qs
        self.fields['zip'].queryset = GeoZip.objects.none()
        if 'zipsearch' in self.data:
            try:
                zipsearch = self.data.get('zipsearch')
                if zipsearch.isnumeric():
                    qs = GeoZip.objects.filter(postal_code__istartswith=zipsearch)
                else:
                    qs = GeoZip.objects.filter(place_name__istartswith=zipsearch)
                self.fields['zip'].queryset = qs
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            if self.instance.zip:
                qs = GeoZip.objects.filter(pk=self.instance.zip.id)
                self.fields['zip'].queryset = qs
        # reorder the fields
        last_fields = list(self.fields.items())[5:-1]
        first_fields = list(self.fields.items())[:7]
        zipsearch = [('zipsearch', self.fields['zipsearch'])]
        new_order = first_fields + zipsearch + last_fields
        self.fields = OrderedDict(new_order)              

    # def clean_vatnr(self):
    #     val = self.cleaned_data["vatnr"]
    #     re_pattern = re.compile(r"^BE0[0-9]{9,10}$")
    #     if val is None:
    #         return None
    #     if re_pattern.match(val) and len(val) > 0:
    #         return val
    #     else:
    #         raise forms.ValidationError(
    #             _("Respecter le format: BE0 suivi de 9 ou 10 chiffres."))

    def clean_company(self):
        company = self.cleaned_data.get("company")
        try:
            if self.operation == "CREATE":
                Client.objects.get(company=company, profile=self.user)
                raise forms.ValidationError(_('Ce client existe déjà.'))
        except Client.DoesNotExist:
            pass
        return company


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = [
            "name", "atelier", "processed", "buysell",
            "unit", "sellpricevat", "vat", "sellprice",
        ]

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("connected_user", None)
        self.operation = kwargs.pop("operation", None)
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields["vat"].initial = 6
        self.fields["unit"].initial = "kg"
        self.fields["atelier"].queryset = (
            Atelier.objects.for_user(self.user).exclude(atelier_id="GEN").order_by("atelier")
        )
        self.fields["atelier"].empty_label = None
        # self.fields["sellprice"].widget.attrs["disabled"] = True

    def clean_name(self):
        name = self.cleaned_data.get("name")
        try:
            if self.operation == "CREATE":
                Product.objects.get(name=name, profile=self.user)
                raise forms.ValidationError(_('Ce produit existe déjà.'))
        except Product.DoesNotExist:
            pass
        return name

    def clean(self):
        dataclean = super(ProductForm, self).clean()
        sellpricevat = dataclean.get("sellpricevat")
        buyprice = dataclean.get("buyprice")
        unit = dataclean.get("unit")
        if (sellpricevat is not None) or (buyprice is not None):
            if unit is None:
                errmsg = _(
                    "Veuillez préciser l'unité pour le prix. \
                    ex: 8.87€/kg; 0.99€/litre."
                )
                self.add_error("unit", errmsg)


class FixExpenseForm(forms.ModelForm):

    class Meta:
        model = FixExpense
        fields = [
            "refshort", "refdesc",
            "supplier", "expcat",
            "atelier",
            "begindate", "enddate",
            "amountvat", "vat", "amount",
            "yearfreq", "validated",
        ]
        widgets = {
            "begindate": MyDatePickerInput().start_of('mydate'),
            "enddate": MyDatePickerInput().end_of('mydate'),
        }

    def __init__(self, *args, **kwargs):
        # get param before init
        self.user = kwargs.pop("connected_user", None)
        self.operation = kwargs.pop("operation", None)
        self.modeltxt = kwargs.pop("modeltxt_kwarg", None)
        self.model = FixExpense
        super(FixExpenseForm, self).__init__(*args, **kwargs)

        self.fields["validated"].widget = forms.HiddenInput()
        self.fields["atelier"].queryset = Atelier.objects.for_user(self.user).order_by(
            "atelier"
        )
        try:
            self.fields["supplier"].queryset = Supplier.objects.for_user(self.user)
        except KeyError:
            self.fields["client"].queryset = Client.objects.for_user(self.user)
            diverscli = Client.objects.for_user(self.user).get(classification='DIV')
            self.fields["client"].initial = diverscli

        try:
            self.fields["expcatgrp"].queryset = ExpCatGrp.objects.exclude(id="EMP").all()
        except KeyError:
            self.fields["revcatgrp"].queryset = RevCatGrp.objects.exclude(id="EMP").all()

        # In case it is validated, only enddate can be updated!
        if self.instance.validated is True:
            for field_name in self.fields:
                if field_name != "enddate":
                    # self.fields[field_name].widget.attrs["disabled"] = "disabled"
                    self.fields[field_name].widget = forms.HiddenInput()

        # initial values when CREATE
        if self.operation == "CREATE":
            self.fields["vat"].initial = 0
            try:
                gen = AtelierList.objects.get(id="GEN")
                inst = Atelier.objects.get(atelier=gen, profile=self.user)
                self.fields["atelier"].initial = inst.id
            except Atelier.DoesNotExist:
                LOG.warning("Atelier général absent pour user {0}. \
                    Vérifier dans accounts.views.ProfileUpdateView".format(self.user))
        # if self.operation == "UPDATE":
        #     # Set all fields not required!
        #     for visible in self.visible_fields():
        #         visible.field.required = False
        #     self.fields["enddate"].required = True


    def clean_refshort(self):
        refshort = self.cleaned_data.get("refshort")
        try:
            if self.operation == "CREATE":
                self.model.objects.get(refshort=refshort, profile=self.user)
                raise forms.ValidationError(_('Cette étiquette existe déjà.'))
        except self.model.DoesNotExist:
            pass
        return refshort

    def clean(self):
        if self.operation == "CREATE":
            dataclean = super(FixExpenseForm, self).clean()
            begindate = dataclean.get("begindate")
            enddate = dataclean.get("enddate")
            # refshort = dataclean.get("refshort")
            # Change info done via model
            # if self.has_changed():
            #     for field_name in self.changed_data:
            #         if field_name == 'enddate':
            #             enddate_old = self.instance.enddate
            #             if enddate > enddate_old:
            #                 print('insert')
            #             elif enddate < enddate_old:
            #                 print('delete')

            diffdate = enddate - begindate
            diffyear = (diffdate.days) / 365
            if diffyear > 30:
                errmsg = _(
                    "L'écart entre la date début et la date fin doit être inférieur à 30 ans"
                )
                self.add_error("enddate", errmsg)

            maxdate = date.today() + relativedelta(years=35)
            mindate = date.today() + relativedelta(years=-20)
            if begindate < mindate:
                errmsg = _("La date début doit être supérieure à {0:%d/%m/%Y}").format(
                    mindate
                )
                self.add_error("begindate", errmsg)
            if enddate > maxdate:
                errmsg = _("La date fin doit être inférieure à {0:%d/%m/%Y}").format(
                    maxdate
                )
                self.add_error("enddate", errmsg)
            if begindate > enddate:
                errmsg = _("La date début doit être inférieure à la date fin.")
                self.add_error("begindate", errmsg)


class FixRevenueForm(FixExpenseForm):
    class Meta:
        model = FixRevenue
        fields = [
            "refshort", "refdesc",
            "client", "revcatgrp", "revcat",
            "atelier",
            "begindate", "enddate",
            "amountvat", "vat", "amount",
            "yearfreq", "validated",
        ]
        widgets = {
            "begindate": MyDatePickerInput().start_of('mydate'),
            "enddate": MyDatePickerInput().end_of('mydate'),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = FixRevenue


class RevenueForm(forms.ModelForm):
    """
    dropdown display is done vie revenue_form.js because interactive with user!
    """
    user = None

    class Meta:
        model = Revenue
        fields = [
            "invnr", "inputdate", "duedatelist", "payduedate",
            "refdesc", "client",
            "revcatgrp", "revcat", "aide_minimi", "loan", "atelier",
            "amountvat", "vat", "vatvalue", "amount",
            "payflag", "invoice_send"
        ]
        widgets = {
            "inputdate": MyDatePickerInput().start_of('mydate'),
            "payduedate": MyDatePickerInput().end_of('mydate'),
            "vatvalue": forms.TextInput(attrs={"placeholder":
                                        "Optionnel, à compléter pour les TVA multiples ou autres que 6, 12 et 21%."}),
        }     

    def __init__(self, *args, **kwargs):
        # get param before init
        self.user = kwargs.pop("connected_user", None)
        self.operation = kwargs.pop("operation", None)
        self.origin = kwargs.pop("origin", None)
        
        super(RevenueForm, self).__init__(*args, **kwargs)
        self.fields["atelier"].queryset = Atelier.objects.for_user(self.user).order_by("atelier")
        if self.operation == "CREATE":
            self.fields["client"].queryset = Client.objects.for_user(self.user).exclude(classification="BQE")
            if self.origin == "BDCDEVIS":
                self.fields["revcatgrp"].empty_label = None
                self.fields["revcatgrp"].queryset = RevCatGrp.objects.filter(id="FRM")
                self.fields["revcat"].queryset = RevCat.objects.filter(id__in=("FRM_BDC", "FRM_DEV"))
            else:
                self.fields["revcatgrp"].queryset = RevCatGrp.objects.exclude(id__in=("CRE", "EMP"))
                self.fields["revcat"].queryset = RevCat.objects.exclude(group__id__in=("CRE", "EMP"))
        else:
            self.fields["client"].queryset = Client.objects.for_user(self.user)

        # Get General item for current user
        try:
            gen = AtelierList.objects.get(id="GEN")
            inst = Atelier.objects.for_user(self.user).get(atelier=gen, profile=self.user)
            self.fields["atelier"].initial = inst.id
        except Atelier.DoesNotExist:
            LOG.warning("Atelier général absent pour user {0}. \
                Vérifier dans accounts.views.ProfileUpdateView".format(self.user))
        # ajout attribut daysadd aux options de duedatelist
        data = dict(DueDateList.objects.values_list("id", "daysadd"))
        data[""] = "------------"  # empty option
        self.fields["duedatelist"].widget = DataAttributesSelect(
            choices=self.fields["duedatelist"].choices, data=data, newattr="daysadd"
        )
        # ajout attribut classification aux options de client
        data = dict(Client.objects.values_list("id", "classification"))
        data[""] = "------------"  # empty option
        self.fields["client"].widget = DataAttributesSelect(
            choices=self.fields["client"].choices, data=data, newattr="classification"
        )        

    def clean_invnr(self):
        invnr = self.cleaned_data.get("invnr")
        try:
            if self.operation == "CREATE":
                Revenue.objects.get(invnr=invnr, profile=self.user)
                raise forms.ValidationError(_('Cet identifiant unique existe déjà.'))
        except Revenue.DoesNotExist:
            pass
        return invnr


ProductFormSet = forms.modelformset_factory(
    RevenueProduct,
    fields=(
        "id", "atelier", "product", "lot", "dlc", "quantity", "unit",
        "amount", "vat", "amountvat", "amounttotvat",
    ),
    extra=5,
    widgets={
        "atelier": forms.Select(attrs={"class": "form-control atelier-change"}),
        "product": forms.Select(attrs={"class": "form-control product-change"}),
        "lot": forms.TextInput(attrs={"class": "form-control"}),
        "dlc": MyDatePickerInput(attrs={"class": "form-control"}),
        # product-change also setup in widgets.py
        "vat": forms.Select(attrs={"class": "form-control compute-vat"}),
        "unit": forms.Select(attrs={"class": "form-control"}),
        "amount": forms.NumberInput(
            attrs={
                "class": "form-control compute-vat",
                "placeholder": "€ Unitaire HTVA",
            }
        ),
        "quantity": forms.NumberInput(
            attrs={"class": "form-control quantity-change", "placeholder": "Qt/Nbre"}
        ),
        "amountvat": forms.NumberInput(
            attrs={
                "readonly": True,
                "class": "form-control",
                "placeholder": "€ Unitaire TVAC",
            }
        ),
        "amounttotvat": forms.NumberInput(
            attrs={
                "readonly": True,
                "class": "form-control",
                "placeholder": "€ Total TVAC",
            }
        ),
    },
)


class PaymentForm(forms.Form):
    amount = forms.DecimalField(
        max_digits=10,
        decimal_places=2,
        widget=forms.NumberInput(
            attrs={
                "class": "form-control",
                "placeholder": "Montant (€)",
                "required": False,
            }
        ),
    )
    paydate = forms.DateField(widget=MyDatePickerInput())

    def __init__(self, *args, **kwargs):
        self.solde = kwargs.pop("solde", None)
        self.instdate = kwargs.pop("instdate", None)

        super(PaymentForm, self).__init__(*args, **kwargs)
        self.fields["amount"].initial = self.solde
        self.fields["paydate"].initial = date.today()
        for visible in self.visible_fields():
            visible.field.required = False

    # def clean_amount(self):
    #     # solde existe slt si passé via POST!
    #     amount = self.cleaned_data['amount']
    #     if amount is None:
    #         raise forms.ValidationError(_('Veuillez introduire un montant.'))
    #     if amount > self.solde:
    #         raise forms.ValidationError(_('Payement suppérieur au solde à payer.'))
    #     else:
    #         return amount

    def clean_paydate(self):
        paydate = self.cleaned_data["paydate"]
        if paydate is None:
            raise forms.ValidationError(_("Veuillez introduire une date de payement."))
        else:
            if paydate > date.today():
                raise forms.ValidationError(
                    _("Le payement ne peux pas avoir lieu dans le futur.")
                )
            elif self.instdate > paydate:
                raise forms.ValidationError(
                    _("Le payement doit avoir lieu après la date de facturation.")
                )
            else:
                return paydate


# Display only!!!
# Adding record is done thougth separate form in order to manage checks after clean_data
PaymentRevenueFormSet = forms.modelformset_factory(
    RevenuePayment,
    fields=("id", "amount", "paydate"),
    extra=0,
    widgets={
        "paydate": MyDatePickerInput(attrs={"readonly": True}),
        "amount": forms.NumberInput(
            attrs={
                "class": "form-control",
                "placeholder": "Montant (€)",
                "readonly": True,
            }
        ),
    },
)

PaymentExpenseFormSet = forms.modelformset_factory(
    ExpensePayment,
    fields=("id", "amount", "paydate"),
    extra=0,
    widgets={
        "paydate": MyDatePickerInput(attrs={"readonly": True}),
        "amount": forms.NumberInput(
            attrs={
                "class": "form-control",
                "placeholder": "Montant (€)",
                "readonly": True,
            }
        ),
    },
)


MonthlyPrevisionFormSet = forms.modelformset_factory(
    MonthlyPrevision,
    fields=(
        "month", "rev", "exp", "solde"
    ),
    extra=0,
    widgets={
        "month": forms.NumberInput(
            attrs={"readonly": True, "class": "form-control"}
        ),
        "rev": forms.NumberInput(
            attrs={"class": "form-control rev-change"}
        ),
        "exp": forms.NumberInput(
            attrs={"class": "form-control exp-change"}
        ),
        "solde": forms.NumberInput(
            attrs={"readonly": True, "class": "form-control"}
        ),
    },
)
