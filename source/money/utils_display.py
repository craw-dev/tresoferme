# -*- coding: utf-8 -*-
"""
Set of functions used for data display
"""
from datetime import date
import logging
import pandas as pd
from django.db.models import F, Sum, QuerySet
from money.models import Expense, Revenue, RevenuePayment, ExpensePayment, Loan, Credit, CreditRefund

LOG = logging.getLogger(__name__)
# LOG_DEBUG = logging.getLogger("mydebug")


def get_soldepay_tot(user):
    """Can be called without instanciation"""
    rev = (RevenuePayment.objects.for_user(user).filter(paydate__lt=date.today())
           .aggregate(amount=Sum("amount"))["amount"])
    exp = (ExpensePayment.objects.for_user(user).filter(paydate__lt=date.today())
           .aggregate(amount=Sum("amount"))["amount"])
    if rev is None:
        rev = 0
    if exp is None:
        exp = 0
    solde = rev - exp
    return round(solde, 0)


def get_upaid_invest(user):
    """Investissements impayés"""
    solde = (
        Expense.objects.for_user(user)
        .filter(payflag=False, expcat__group="INV", payduedate__lt=date.today())
        .aggregate(solde=Sum(F("amountvat") - F("payamount")))["solde"]
    )
    if solde is None:
        solde = 0
    return round(-solde, 0)


def get_unpaid_loan(user):
    """Emprunt échu non payés sans les crédits de caisse"""
    solde = (
        Expense.objects.for_user(user)
        .filter(payflag=False, expcat__group="EMP", payduedate__lt=date.today())
        .exclude(expcat="EMP_CAI")
        .aggregate(solde=Sum(F("amountvat") - F("payamount")))["solde"]
    )
    if solde is None:
        solde = 0
    return round(-solde, 0)


def get_unpaid_creditcaisse(user):
    """Crédits de caisse non rembourssés"""
    calctot = 0
    credit = (
        Credit.objects.for_user(user)
        .filter(loantype__id="CRE_CAI")
    )
    for cai in credit:
        calc = cai.compute_interest()
        calctot += calc
    return -calctot


def get_unpaid_supplier(user):
    """Fournisseurs non payés à la date d'échéance"""
    solde = (
        Expense.objects.for_user(user)
        .filter(payflag=False, payduedate__lt=date.today())
        .exclude(supplier__isnull=True)
        .aggregate(solde=Sum(F("amountvat") - F("payamount")))["solde"]
    )
    if solde is None:
        solde = 0
    return round(-solde, 0)


def get_unpaid_client(user):
    """Client n'ayant pas encore payé à la date d'échéance"""
    solde = (
        Revenue.objects.for_user(user)
        .exclude(client__isnull=True)
        .exclude(revcat__id="FRM_BDC")
        .filter(payflag=False, payduedate__lt=date.today())
        .aggregate(solde=Sum(F("amountvat") - F("payamount")))["solde"]
    )
    if solde is None:
        solde = 0
    return round(solde, 0)


class LoanDisplay(object):
    """
    Loan display used in loan_list.html & money_index.html
    """
    loanqs = QuerySet

    def __init__(self, user):
        self.loanqs = Loan.objects.for_user(user).filter(closedate=None)

    def sum_loan_amount(self):
        """Sum loanamount pour Emprunt non clôturé (aucun sens, fait paniquer pour rien)"""
        val = (self.loanqs.aggregate(amt=Sum("loanamount"))["amt"])
        if val is None:
            val = 0
        return round(val, 0)

    def compute_loanrfd(self):
        """Sum payment loanrefund to be done (debitflag=False)"""
        ctn = 0
        df = pd.DataFrame()
        for loan in self.loanqs.exclude(loantype__id="EMP_CAI"):
            ctn += 1
            pp = {"loan": loan}           
            paid = (
                loan.loanrefund_set.filter(debitflag=True)
                .aggregate(payment=Sum("payment"),
                           capital=Sum("capital"),
                           interest=Sum("interest"))
                )
            unpaid = (
                loan.loanrefund_set.filter(debitflag=False)
                .aggregate(payment2=Sum("payment"),
                           capital2=Sum("capital"),
                           interest2=Sum("interest"))
                )
            # Ajout paid dict à loan dict
            pp.update(paid)
            pp.update(unpaid)
            dfp = pd.DataFrame(pp, index=[ctn])
            df = pd.concat([df, dfp], sort=True)
        if df.empty:
            df = pd.DataFrame(columns=("index", "loan", "payment", "capital", "interest",
                                                    "payment2", "capital2", "interest2"))
        return(df)

    def sum_all_df(self, df):
        """build context dict to collect all computed fields"""
        mydict = {"payment": 0, "payment2": 0, "capital": 0, "capital2": 0, "interest": 0, "interest2": 0}
        # Obligé pour garder capital, payment, etc comme clé
        dfsum = df.sum().reset_index().set_index("index")
        # On ne prend que les indices 0 et 1 et pas Name ou dbtype propre à Pandas
        for i in dfsum.itertuples():
            mydict[i[0]] = i[1]
        mydict["totpay"] = mydict["payment"] + mydict["payment2"]
        mydict["totcap"] = mydict["capital"] + mydict["capital2"]
        mydict["totint"] = mydict["interest"] + mydict["interest2"]
        # Aussi possible de faire T= transpose et to_dict pour avoir dico mais bcp de chipo dans template
        # d3 = dfsum.T.to_dict('list')

        return mydict

    def get_context(self):
        """build context dict to collect all computed fields"""
        ctx = {}
        ctx["loanamount_tot"] = self.sum_loan_amount()
        df = self.compute_loanrfd()
        ctx["df"] = df
        ctx["dfsum"] = self.sum_all_df(df)
        # ctx["loan_soldepay"] = ctx["dfsum"]["totpay"] - ctx["loanamount_tot"]
        return ctx
