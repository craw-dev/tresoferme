# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def load_from_sql():
    import os
    from config.settings import BASE_DIR
    DATA_DIR = 'info/data_insert/'
    sql_statements = ""
    sql_statements += open(os.path.join(BASE_DIR, DATA_DIR, 'info_faq.sql'), 'r').read()
    return sql_statements


class Migration(migrations.Migration):

    dependencies = [
        ('info', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(load_from_sql()),
    ]
