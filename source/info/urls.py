# encoding:utf-8
'''
Created on 12/01/2020
@author: p.houben
'''
from django.urls import path
from info.views import InfoTemplate, LexTemplate, GroupCategList, InfoFaqList

urlpatterns = [
    path('', InfoTemplate.as_view(template_name="index_info.html"), name='money_index_info'),

    path('lexique/credit', LexTemplate.as_view(include_page="lex_credit.html", title="Type de crédit"), name='lex_credit'),
    path('lexique/emprunt', LexTemplate.as_view(include_page="lex_emprunt.html", title="Type d'emprunt"), name='lex_emprunt'),
    path('lexique/rentabilite', LexTemplate.as_view(include_page="lex_renta.html", title="Rentabilité"), name='lex_renta'),
    path('lexique/tresorerie', LexTemplate.as_view(include_page="lex_treso.html", title="Trésorerie"), name='lex_treso'),
    path('lexique/amortissement', LexTemplate.as_view(include_page="lex_amort.html", title="Amortissement"), name='lex_amort'),

    path('lexique/categ_recette', GroupCategList.as_view(model="RevCat"), name='revcat_list'),
    path('lexique/categ_depense', GroupCategList.as_view(model="ExpCat"), name='expcat_list'),

    path('tuto/compte', InfoTemplate.as_view(template_name="tuto_accounts.html"), name='tuto_accounts'),
    path('tuto/reference', InfoTemplate.as_view(template_name="tuto_reference.html"), name='tuto_reference'),
    path('tuto/gest_courante', InfoTemplate.as_view(template_name="tuto_gestcourante.html"), name='tuto_gestcourante'),
    path('tuto/gest_annexe', InfoTemplate.as_view(template_name="tuto_gestannexe.html"), name='tuto_gestannexe'),
    path('tuto/analyse', InfoTemplate.as_view(template_name="tuto_analyse.html"), name='tuto_analyse'),
    
    path('faq', InfoFaqList.as_view(), name='faq_index'),
    path('version', InfoTemplate.as_view(template_name="version_index.html"), name='version_index'),
]
