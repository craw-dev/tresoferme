--
-- Data for Name: info_faq; Type: TABLE DATA; Schema: public; Owner: tresoadmin
--

INSERT INTO info_faq (id, faqnr, question, answer) VALUES (3, 2, 'Puis-je utiliser Tresoferme pour une partie de mon exploitation uniquement ?', '<p>Oui. Tr&eacute;soferme est un outil de suivi de gestion simplifi&eacute; qui vise &agrave; fournir une information de base pour la gestion de l&#39;exploitation. Bien entendu, l&#39;information produite ne sera que partielle. Par exemple vous pourriez introduire toutes les entr&eacute;es et sorties relatives &agrave; un atelier, ou un canal de commercialisation. Dans ce cas, vous pourriez suivre l&#39;&eacute;volution des co&ucirc;ts et recettes directes li&eacute;es &agrave; cet atelier ou &agrave; ce canal de vente.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (4, 3, 'Est-il possible d’ajouter des ateliers et des sous-catégories ?', '<p>Les diff&eacute;rentes listes propos&eacute;es se veulent les plus exhaustives possible mais nul n&#39;est &agrave; l&#39;abri d&#39;un oubli essentiel. N&#39;h&eacute;sitez donc pas &agrave; nous <a href="mailto:tresoferme@cra.wallonie.be" target="_top">contacter</a> si vous constatez certains manquements.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (12, 11, 'Puis-je calculer mon prix de revient avec Tresoferme?', '<p>Tr&eacute;soferme n&#39;est pas un logicile de comptabilit&eacute; analytique qui permet de fournir ce type d&#39;information de fa&ccedil;on pr&eacute;cise, par produit ou par atelier de production. En revanche, il permet selon le param&eacute;trage d&#39;attribuer les co&ucirc;ts et les recettes directement li&eacute;s &agrave; un produit ou un atelier. Comme il n&#39;est pas possible de r&eacute;partir les charges fixes sur diff&eacute;rents atelier ou production, l&#39;analyse du co&ucirc;t de revient dans Tr&eacute;soferme est incompl&egrave;te mais reste pertiente en termes de gestion de l&#39;&eacute;volution des marges brutes par atelier et par production.</p>

<p>Par ailleurs, vous pouvez enteprendre des analyses plus pouss&eacute;es de votre c&ocirc;t&eacute; &agrave; partir des exports csv de vos entr&eacute;es et sorties.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (7, 6, 'Est-il possible de dupliquer une facture ?', '<p>Oui, rendez-vous sur la facture que vous souhaitez dupliquer, &eacute;ditez l&agrave; puis cliquez sur le bouton dupliquer.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (5, 4, 'Puis-je inscrire des activités telles que l''accueil à la ferme ou ferme pédagogique ?', '<p>Oui, l&#39;atelier &quot;Accueil &agrave; la ferme&quot; est pr&eacute;vu &agrave; cet effet.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (10, 9, 'Est-il possible d''établir ma déclaration TVA avec Tresoferme ?', '<p>Un export des diff&eacute;rentes charges et produits depuis Tr&eacute;soferme peut constituer une bonne base &agrave; l&#39;&eacute;tablissement de la d&eacute;claration TVA. Il vous fournira les montant HTVA, la TVA et le montant TTC. Toutefois sur base de cet export, il reste de nombreuses manipulations &agrave; effectuer pour pouvoir obtenir les diff&eacute;rents montants n&eacute;cessaires &agrave; la d&eacute;claration de la TVA. On peut citer par exemple le classement des &eacute;l&eacute;ments selon les diff&eacute;rents taux, la gestion de note de cr&eacute;dit ou encore les achats intracommunautaires.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (8, 7, 'Est-ce possible d''associer plusieurs catégories à un fournisseur ?', '<p>Oui. Pour ce faire, lors du param&eacute;trage du fournisseur, vous devez s&eacute;lectionner plusieurs cat&eacute;gories en maintenant la touche CTRL enfonc&eacute;e tout en s&eacute;lectionnant les cat&eacute;gories d&eacute;sir&eacute;es. Pour une information compl&egrave;te, vous pouvez vous r&eacute;f&eacute;rez au tutoriel sur l&#39;encodage des fournisseurs.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (11, 10, 'Puis-je fournir l''export csv de Tresoferme à mon comptable ?', '<p>Tout d&eacute;pend de votre comptable et du syst&egrave;me qu&#39;il utilise. Pour des comptabilit&eacute;s simples en personne physique, le fichier CSV permet au comptable de r&eacute;duire sa charge de travail et de v&eacute;rifier plus facilement les documents comptables physiques. Toutefois, certains comptables pr&eacute;f&eacute;rerons utiliser leur propre syst&egrave;me d&#39;information et n&#39;auront pas d&#39;utilit&eacute; de l&#39;export.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (9, 8, 'A quelle fréquence  est-il conseillé d''encoder mes factures dans Tresoferme ?', '<p>Sur base de l&#39;exp&eacute;rience des b&eacute;n&eacute;ficiaires en phase test mais aussi d&#39;autres logiciels de gestion, l&#39;id&eacute;al est de se construire une routine hebdomadaire. Lorsque la gestion est &eacute;rig&eacute;e en priorit&eacute; au travers d&#39;une pratique ritualis&eacute;e, r&eacute;guli&egrave;re et structur&eacute;e, on constate une efficience beaucoup plus importante, une moindre pertes de documents et une moins grande r&eacute;ticense &agrave; &quot;s&#39;y mettre&quot;. Moins d&#39;accumulation et plus d&#39;efficacit&eacute; permettent de garder davantage de motivation. De cette fa&ccedil;on, on peut par exemple encoder ses entr&eacute;es et sorties toutes les semaines et faire un rapprochement des paiements via extrait bancaire tous les mois. A cette occasion, on peut &eacute;galement regarder l&#39;&eacute;volution de ses ventes par exemple si l&#39;exploitation est partiellement ou totalement en vente directe.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (6, 5, 'Puis-je utiliser Tresoferme pour deux finalités différentes: circuit court vs circuit long ?', '<p>Tout &agrave; fait. Tr&eacute;soferme vous permet de classer vos diff&eacute;rentes ventes selon les clients et les canaux de commercialisation. G&eacute;n&eacute;ralement, les op&eacute;rations de commercialisation circuit long s&rsquo;encodent via la cr&eacute;ation de clients et ensuite de factures. Pour ce qui est de la vente directe, type caisse ou march&eacute;, on cr&eacute;e un client factice correspondant au canal de vente (&quot;vente comptoir&quot;, &quot;vente march&eacute;&quot;, &quot;vente groupement d&rsquo;achat&quot;, etc&hellip;). En s&eacute;lectionnant ce client lors de l&rsquo;enregistrement de la vente, vous pourrez ainsi obtenir une analyse de vos ventes pour chacun de vos canaux.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (2, 1, 'Mes données sont-elles sécurisées et seront-elles exploitées ?', '<p>Nous sommes particuli&egrave;rement soucieux de votre <strong>vie priv&eacute;e</strong> et de la <strong>s&eacute;curisation de vos donn&eacute;es</strong>. Ces aspects sont repris en d&eacute;tails dans l&rsquo;article &laquo;&nbsp;Traitement des donn&eacute;es&nbsp;&raquo; de nos <a href="/static/tresoferme_condition_utilisation.pdf" target="_blank">Conditions d&rsquo;utilisation</a> que nous vous invitons &agrave; lire attentivement. Nous reprenons les points principaux ci-dessous.</p>

<p><u><span style="color:black"><strong>Votre vie priv&eacute;e et l&rsquo;acc&egrave;s &agrave; vos donn&eacute;es</strong></span></u></p>

<ul>
	<li><span style="color:black">Nous respectons la vie priv&eacute;e de tous les utilisateurs et nous nous conformons strictement aux lois en vigueur sur la protection de la vie priv&eacute;e et des libert&eacute;s individuelles, notamment le <strong>R&egrave;glement g&eacute;n&eacute;ral sur la protection des donn&eacute;es</strong> (RGPD).&nbsp; </span></li>
	<li><span style="color:black">Nous collectons uniquement les donn&eacute;es n&eacute;cessaires pour vous offrir des fonctionnalit&eacute;s performantes. Nous <strong>ne vendons aucune</strong> de vos donn&eacute;es. </span></li>
	<li><span style="color:black">Vos donn&eacute;es sont uniquement accessibles aux employ&eacute;s du CRA-W qui (i) ont besoin de conna&icirc;tre ces informations pour faire fonctionner les services du site et (ii) et qui se sont engag&eacute;s &agrave; maintenir la plus grande <strong>confidentialit&eacute; </strong>&agrave; l&rsquo;occasion de leur prestation.</span></li>
	<li><span style="color:black">Vous pouvez &eacute;galement rendre vos donn&eacute;es accessibles &agrave; un &laquo;&nbsp;<strong>gestionnaire&nbsp;</strong>&raquo;. A ce stade, le seul gestionnaire repris dans Tresoferme est le CRA-W. Dans le futur, des accompagnateurs d&rsquo;autres structures pourront avoir un compte &laquo; conseiller &raquo; dans le cadre d&rsquo;un service d&rsquo;accompagnement individuel par exemple. A partir de votre compte, vous pourrez alors s&eacute;lectionner votre gestionnaire et d&eacute;terminer le degr&eacute;s d&rsquo;acc&egrave;s &agrave; vos donn&eacute;es.&nbsp; </span></li>
	<li><span style="color:black">Toutes vos donn&eacute;es collect&eacute;es dans le cadre de l&rsquo;utilisation de Tresoferme restent <strong>votre propri&eacute;t&eacute;</strong> et vous en conservez la <strong>maitrise exclusive</strong>. Conform&eacute;ment au RGPD, vous disposez des droits d&rsquo;interrogation, d&rsquo;acc&egrave;s, de modification, d&rsquo;opposition, de rectification et de portabilit&eacute; de vos donn&eacute;es.</span></li>
</ul>

<p><u><span style="color:black"><strong>Les donn&eacute;es collect&eacute;es</strong></span></u></p>

<ul>
	<li><span style="color:black">Nous n&rsquo;utilisons pas de cookies. Les seules donn&eacute;es collect&eacute;es automatiquement sont votre adresse IP et vos informations de connexion (type de navigateur utilis&eacute;, date et heure de chaque demande).&nbsp; Nous collectons ces donn&eacute;es pour analyser le trafic sur Tresoferme, diagnostiquer et r&eacute;parer des probl&egrave;mes de r&eacute;seau et aider &agrave; pr&eacute;venir la fraude et les abus. </span></li>
	<li><span style="color:black">Les autres donn&eacute;es sont encod&eacute;es par vos soins dans le cadre des fonctionnalit&eacute;s de Tresoferme. </span></li>
	<li><span style="color:black">Votre adresse &eacute;lectronique et votre num&eacute;ro de t&eacute;l&eacute;phone ne seront jamais vendus ou fournis &agrave; un tiers &agrave; des fins marketing. </span></li>
</ul>

<p><u><span style="color:black"><strong>Exploitations des donn&eacute;es anonymis&eacute;es et agr&eacute;g&eacute;es </strong></span></u></p>

<ul>
	<li><span style="color:black">Lors de la cr&eacute;ation vote compte, vous &ecirc;tes libre de donner votre consentement pour que vos donn&eacute;es puissent &ecirc;tre utilis&eacute;es de<strong> mani&egrave;re anonyme</strong> et a<strong>gr&eacute;g&eacute;e</strong> dans le cadre d&rsquo;&eacute;tudes statistiques. </span></li>
	<li><span style="color:black">Dans tous les cas <strong>aucun lien ne peut &ecirc;tre &eacute;tabli avec votre identit&eacute; </strong>et <strong>aucune information individuelle ne sera publi&eacute;e</strong></span></li>
	<li><span style="color:black">L&rsquo;acc&egrave;s &agrave; vos donn&eacute;es anonymis&eacute;es et agr&eacute;g&eacute;es nous permettra de conduire:</span></li>
</ul>

<ol style="margin-left:40px">
	<li><span style="color:black">Des &eacute;tudes statistiques de <strong>l&rsquo;utilisation</strong> de Tresoferme pour optimiser le site et les services&nbsp;;</span></li>
	<li><span style="color:black">Des &eacute;tudes statistiques pour des <strong>recommandations</strong> visant &agrave; soutenir et faire progresser le secteur agricole&nbsp;;</span></li>
	<li><span style="color:black">Des &eacute;ventuelles &eacute;tudes <strong>scientifique</strong>s. Dans ce cas, les objectifs et la dur&eacute;e de l&rsquo;&eacute;tude devront &ecirc;tre explicitement mentionn&eacute;s &agrave; tous les utilisateurs ayant consenti &agrave; l&rsquo;acc&egrave;s de leurs donn&eacute;es anonymis&eacute;es et agr&eacute;g&eacute;es. </span></li>
</ol>

<ul>
	<li><span style="color:black">Vous pouvez &agrave; tout moment retirer votre consentement via votre compte.</span></li>
</ul>

<p><u><span style="color:black"><strong>S&eacute;curisation des donn&eacute;es</strong></span></u></p>

<ul>
	<li><span style="color:black">Aucune transmission de donn&eacute;es sur Internet ne peut &ecirc;tre garantie &agrave; 100%. Toutefois, nous mettons en place toutes les mesures raisonnables pour prot&eacute;ger vos donn&eacute;es&nbsp;:</span></li>
</ul>

<ol style="margin-left:40px">
	<li><span style="color:black">Vos informations sont prot&eacute;g&eacute;es par un <strong>mot de passe</strong>. Nous utilisons un <strong>cryptage</strong> conforme aux normes de l&#39;industrie pour prot&eacute;ger votre mot de passe. Nous vous conseillons de choisir un mot de passe fort et s&ucirc;r et de vous d&eacute;connectez de votre compte &agrave; la fin de chaque utilisation. </span></li>
	<li><span style="color:black">Vos donn&eacute;es &agrave; caract&egrave;re personnel sont syst&eacute;matiquement <strong>crypt&eacute;es</strong> jusque dans la base de donn&eacute;es. Ce cryptage assure qu&rsquo;aucun lien ne peut &ecirc;tre &eacute;tabli avec votre identit&eacute;.</span></li>
	<li><span style="color:black">L&rsquo;ensembles de donn&eacute;es sont <strong>stock&eacute;es sur les serveurs internes</strong> au CRA-W qui r&eacute;pondent &agrave; des normes strictes en mati&egrave;re de s&eacute;curit&eacute; et font l&rsquo;objet de sauvegardes r&eacute;currentes. </span></li>
</ol>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (14, 13, 'Comment référencer la vente aux particuliers (marchés, ventes compoir, GASAP etc.) dans la partie "Client" ?', '<p>Le <strong>client </strong>est la personne ou l&rsquo;entreprise avec lequel se conclut une vente. Il peut s&rsquo;agir d&rsquo;une <strong>entreprise </strong>ou de <strong>particuliers</strong>.</p>

<p>Lorsqu&rsquo;il s&rsquo;agit d&rsquo;une <strong>entreprise</strong>, on proc&egrave;de &agrave; la r&eacute;daction d&rsquo;une <strong>facture </strong>en bonne et due forme.</p>

<p>S&rsquo;il s&rsquo;agit de <strong>particuliers </strong>et que la vente se fait lors d&rsquo;un <strong>march&eacute; </strong>ou au comptoir, on cr&eacute;era un compte client avec une d&eacute;nomination s&rsquo;y r&eacute;f&eacute;rent, par exemple un client &laquo;&nbsp;vente comptoir&nbsp;&raquo; ou &laquo;&nbsp;vente march&eacute;&nbsp;&raquo;, ou &laquo;&nbsp;vente groupement d&rsquo;achat X&nbsp;&raquo;. Dans ce cas, l&rsquo;exploitation doit &ecirc;tre capable de fournir le d&eacute;tail des ventes mais des montants journaliers, hebdomadaires ou mensuels, selon les besoins d&rsquo;analyse, peuvent &ecirc;tre introduit dans Tr&eacute;soferme.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (15, 14, 'Puis-je gérer mes bons de commande avec Tresoferme?', '<p>Le <strong>bon de commande</strong> est un document qui pr&eacute;c&egrave;de la facture et qui permet de soumettre une offre pour validation aupr&egrave;s d&rsquo;un client. Dans le cadre de Tr&eacute;soferme, la fonctionnalit&eacute; permet d&rsquo;&eacute;diter ce bon de commande pour envoi. Si ce dernier est accept&eacute;, il peut alors &ecirc;tre converti en facture pour envoi et suivi du paiement.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (16, 15, 'Puis-je analyser la rentabilité d''une activité/d''un atelier avec tresoferme?', '<p>Tr&eacute;soferme propose une analyse simplifi&eacute;e et donc partielle de la rentabilit&eacute; (notamment les effets de stock ne sont pas pris en compte). Il permet d&rsquo;avoir une vue claire sur les produits li&eacute;s &agrave; une activit&eacute;/atelier. Il permet &eacute;galement de suivre les charges qui peuvent totalement et directement &ecirc;tre attribu&eacute;es cet atelier. Pour avoir une information de rentabilit&eacute; compl&egrave;te il faut encore r&eacute;partir les frais fixes qui impactent plusieurs ateliers et prendre en compte les variations de stock. Il s&rsquo;agit donc d&rsquo;une premi&egrave;re information sur la rentabilit&eacute; qui s&rsquo;apparente davantage &agrave; la notion de marge brute.</p>');
INSERT INTO info_faq (id, faqnr, question, answer) VALUES (13, 12, 'Comment numéroter les encodages?  Un indentifiant unique est-il créé automatiquement à chaque nouvel encodage?', '<p>Tr&eacute;soferme ne fournit pas un identifiant automatique et unique lors d&rsquo;un encodage. Il appartient &agrave; l&rsquo;utilisateur de s&rsquo;assurer que la num&eacute;rotation reprise correspond &agrave; la num&eacute;rotation r&eacute;elle des pi&egrave;ces comptables, par exemple la r&eacute;f&eacute;rence du facturier.</p>

<p>Il vous est conseill&eacute; de convenir de r&eacute;f&eacute;rences diff&eacute;rentes selon le type d&rsquo;entr&eacute;e afin de pouvoir les classer et les retrouver facilement. Par exemple F2020001 pour la premi&egrave;re facture de l&rsquo;ann&eacute;e avec VMAG2020001 pour la premi&egrave;re vente du magasin.</p>

<p>Cette r&eacute;f&eacute;rence est modifiable mais tentez d&rsquo;adopter une logique et ensuite de vous y tenir, cela vous &eacute;vitera fortement les erreurs d&rsquo;encodage et vous procura une plus grande facilit&eacute; d&rsquo;analyse.</p>');


--
-- Name: info_faq_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tresoadmin
--

-- SELECT pg_catalog.setval('info_faq_id_seq', 16, true);


--
-- PostgreSQL database dump complete
--

