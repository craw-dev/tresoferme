# encoding:utf-8
'''
Created on 12/01/2020
@author: p.houben
'''
from braces.views import LoginRequiredMixin

from django.views.generic import TemplateView, ListView
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render

from money.utils_tools import SaveAsXLS
from money.models import RevCat, ExpCat
from info.models import Faq
from money.formsearch import SupplierSearch, RevCatSearch


class InfoTemplate(TemplateView):

    def __init__(self, template_name):
        super().__init__()
        self.template_name = template_name


class LexTemplate(TemplateView):
    template_name = "base_lexique.html"
    include_page = None
    title = None

    def __init__(self, include_page, title="No title defined in url"):
        super().__init__()
        self.include_page = include_page
        self.title = title


class InfoFaqList(ListView):
    model = Faq


class GroupCategList(LoginRequiredMixin, ListView):
    form = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.modeltxt = self.model
        self.template_name = "lex_{}.html".format(self.modeltxt.lower())
        self.model = eval(self.modeltxt)
        if self.modeltxt == 'ExpCat':
            self.form_name = SupplierSearch
        if self.modeltxt == 'RevCat':
            self.form_name = RevCatSearch
        self.paginate_by = 100

    def build_where(self, param):
        where = Q(pk__gt=0)
        if param.get("expcatgrp"):
            where &= Q(group=param.get("expcatgrp", ""))

        if param.get("expcat"):
            where &= Q(id=param.get("expcat", ""))

        if param.get("revcatgrp"):
            where &= Q(group=param.get("revcatgrp", ""))

        if param.get("revcat"):
            where &= Q(id=param.get("revcat", ""))

        if param.get("txtsearch"):
            search_list = param.get("txtsearch", "").split()

            for search_item in search_list:
                where &= (
                    Q(group__name__icontains=search_item) |
                    Q(name__icontains=search_item) |
                    Q(comptaplan__name__icontains=search_item) |
                    Q(comptaagri__name__icontains=search_item) |
                    Q(rentatxt__icontains=search_item) |
                    Q(tresotxt__icontains=search_item)                    
                    )
        return where

    def get_queryset(self):
        qs = self.model.objects.select_related().all()
        self.form = self.form_name(request=self.request)
        return qs.filter(self.build_where(param=self.request.GET))

    def get(self, request, *args, **kwargs):
        # self.list_path = "{0}_list".format(self.modeltxt.lower())
        # self.title = self.model._meta.verbose_name
        xlsparam = self.request.GET.get("exportxls")
        if xlsparam == 'XLSGO':
            xls = SaveAsXLS(self.get_queryset())
            msg_back = xls.get_model_and_columns()
            if msg_back != "OK":
                return render(request, "500.html", {"error_msg": msg_back})
            else:
                workbook = xls.create_workbook()
                sheet_added, msg_back = xls.add_sheet(self.modeltxt)
                if sheet_added:
                    filename = "{}.xls".format(self.modeltxt)
                    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                    response['Content-Disposition'] = 'attachment; filename={0}'.format(filename)
                    workbook.save(response)
                    return response
                else:
                    return render(request, "500.html", {"error_msg": msg_back})
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["list_path"] = "{0}_list".format(self.modeltxt.lower())       
        context["title"] = self.model._meta.verbose_name
        context["create_path"] = None
        return context
