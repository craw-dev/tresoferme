# encoding:utf-8
from django.contrib import admin
from django.utils.translation import ugettext as _
from django.db import models
from django.forms import TextInput, Textarea

from .models import Faq


class FaqAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':'120'})},
        # models.TextField: {'widget': Textarea(attrs={'rows':4, 'cols':40})},
    }
    list_display = ('question', 'faqnr',)
    list_filter = ('faqnr',)
    search_fields = ('question', 'answer', )

    class Media:
        extend = True
        js = ('ckeditor.js',)


admin.site.register(Faq, FaqAdmin)
