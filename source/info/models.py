# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext as _
from ckeditor.fields import RichTextField

class Faq(models.Model):
    """
    FAQ list for info TresoFerme
    """
    faqnr = models.PositiveSmallIntegerField(verbose_name=_("Nr FAQ"))
    question = models.CharField(max_length=500, verbose_name=_("Question"))
    answer = RichTextField(verbose_name=_("Réponse"), blank=True, null=True)

    class Meta:
        verbose_name = _("FAQ")
        ordering = ['faqnr']

    def __str__(self):
        return "faq_{}".format(self.faqnr)
