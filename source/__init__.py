__author__ = 'Patrick HOUBEN'
__email__ = 'p.houben@cra.wallonie.be'
__copyright__ = 'Copyright 2018-2020, Patrick HOUBEN'
__license__ = 'MIT'
__date__ = '2021-01-13'
__version__ = '1.6.0'
__status__ = 'Stable'

# Version synonym
VERSION = __version__


