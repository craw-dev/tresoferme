#!/bin/bash
<<COMMENT1
----------------------------------------------------------------
Shell script to be stored in source django folder
Script steps
1. drop db
2. create db
3. remove all migration files
4. manage.py makemigration
5. manage.py migrate
6. copy initial_data.sql into migration folder
7. manage.py migrate
----------------------------------------------------------------
Author: Patrick Houben
Date: 17/10/2018
----------------------------------------------------------------
COMMENT1
db="tresoferme"
declare -a arr=("accounts" "farm" "info" "money")
echo $db

# https://stackoverflow.com/questions/37072245/check-return-status-of-psql-command-in-unix-shell-scripting
# postgres
# psql -U postgres -h localhost -c "drop database "$db
# psql -U postgres -h localhost -c "create database "$db" --owner=tresoadmin"

# adapt accounts/models.py file with appropriate user_model
echo "adapt accounts/models.py file with appropriate user_model"
sed -i 's/ForeignKey(User/ForeignKey(Profile/g' accounts/models.py

# drop database tresoferme.sqlite3
if [ -f tresoferme.sqlite3 ]
then
echo "drop database tresoferme.sqlite3"
rm tresoferme.sqlite3
fi

# remove old migrations files
for i in "${arr[@]}"
do
	echo $i " remove migrations"
	rm -r $i/migrations/*.py
	touch $i/migrations/__init__.py
done

# makemigration
for i in "${arr[@]}"
do
	echo $i " makemigrations"
	pipenv run python manage.py makemigrations $i
done

pipenv run python manage.py migrate

for i in "${arr[@]}"
do
	echo $i " move sql files"
	cp $i/data_insert/0002_initial_data.py $i/migrations/0002_initial_data.py
done

pipenv run python manage.py migrate
