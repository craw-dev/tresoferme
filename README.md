# TresoFerme - by [CRA-W](https://www.cra.wallonie.be/en)

TresoFerme, a simplified farm cash flow management tool, to help producers reappropriate their economic indicators and better manage their farms.


## Prerequisite
[Git](https://gitforwindows.org/) - 
[Python](https://www.python.org/downloads/) -
[Pipenv](https://pipenv.pypa.io/en/latest/install/)

## Installation

#### 1.  Clone repo and configure virtualenv

```bash
$ git clone git@gitrural.cra.wallonie.be:craw-app/tresoferme.git
$ cd tresoferme
$ pipenv install
$ pipenv shell
$ cd source # DO NOT FORGET!!!
```

#### 2. Check django install
```bash
(tresoferme) $ python -m django --version
# This should return django version number, 3 or upper, like 3.0.7
```


####  3. Check everything has been correctly installed
```bash
(tresoferme) $ python manage.py check
```
If you get the following error:
```bash
File "<install-dir>.local/share/virtualenvs/tresoferme-DMPJAftp/lib/python3.6/site-packages/easy_pdf/rendering.py", line 12, in <module>
    from django.utils.six import BytesIO
ModuleNotFoundError: No module named 'django.utils.six'
```
Error raised since [django3 features](https://github.com/nigma/django-easy-pdf/pull/73/files) is not yet integrated in latest version

Edit the file and change **Line 12**

```python
from django.utils.six import BytesIO
```

to

```python
from six import BytesIO
```

#### 4. run the shell source.accounts4sqlite.sh
```bash
(tresoferme) $ sh accounts4sqlite.sh
```
This shell is just changing profile foreign key from User to Profile in "source.accounts.models.py" file

#### 5. Launch localhost server
```bash
(tresoferme) $ python manage.py runserver
```

Load the site at http://127.0.0.1:8000


#### 6. Connect using the following credentials:
usernames: admin, manager, demo, eleveur, maraicher

password: mybestpasswd

1. admin: allows to access django-admin and change key parameters in the database ie: django_site, money_unitlist, accounts_supportstructure, etc.

2. manager: allows you to manage you users: eleveur & maraicher. Go to top right menu and select 'Mes utilisateurs'

3. demo: simple test user

4. eleveur: stockbreeding example

5. maraicher: market gardening example


## Mailing from TresoFerme
If you want to use mailing from localhost (@SignIn for instance), you have to adapt EMAIL_HOST_USER and EMAIL_HOST_PASSWORD in settings_localhost.json file

```json
"EMAIL_HOST_USER": "hello@google.be",
"EMAIL_HOST_PASSWORD": "mybestpasswd",
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Acknowledgments
This project is heavily inspired by [wsvincent-djangoX](https://github.com/wsvincent/djangox).

## License
[MIT](https://choosealicense.com/licenses/mit/)
